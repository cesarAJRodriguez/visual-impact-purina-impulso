package com.visual.purina_impulso.base;


public abstract class BaseInteractor {

    public interface ErrorCallback {
        void onError(Exception exception);
    }

    public interface AuthErrorCallback {
        void onAuthenticationError(Exception exception);
    }
}
