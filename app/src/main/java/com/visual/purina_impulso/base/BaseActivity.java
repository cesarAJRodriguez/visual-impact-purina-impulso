package com.visual.purina_impulso.base;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.visual.purina_impulso.R;
import com.visual.purina_impulso.util.DialogUtil;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.visual.purina_impulso.util.DialogUtil.setupDialog;

//BaseActivity el cual todas los activity van extender BaseActivity

public abstract class BaseActivity extends AppCompatActivity implements BaseView {

    //layout base en todas las activity
    @BindView(R.id.root_view) View mRootView;
    @Nullable
    @BindView(R.id.toolbar) Toolbar mToolbar;
    //toobar base en todas las activity

    private BaseFragment mBaseFragment;
    private ProgressDialog mProgressDialog;
    private boolean mLoading;

    private  Dialog dialog;


    //metodo en el cual va ir la vista de la activity
    protected abstract int getResLayout();
    //metodo en el cual se va a declara la accion que la activity va realizar cuando comienze
    protected abstract void setupView(Bundle savedInstanceState);

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getResLayout());
        //ButterKnife es un libreria alternativa y simplificada en vez de
        // ejem:
        //   Nativo : LinearLayout linearTest = (LinearLayout) findViewById(R.id.ll_test)
        //   ButterKnife :  @BindView(R.id.ll_test) LinearLayout linearTest;
        ButterKnife.bind(this);
        if (mToolbar != null) {
            setSupportActionBar(mToolbar);
        }


        setupView(savedInstanceState);
    }

    protected void setupToolbar(boolean homeEnabled) {
        // metodo a heredar para llamar a los toolbar
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setDisplayHomeAsUpEnabled(homeEnabled);
            actionBar.setDisplayShowHomeEnabled(homeEnabled);
            actionBar.setHomeAsUpIndicator(R.drawable.ic_back_white_24dp);
        }
    }

    protected void setToolbatTitle(String title) {
        ActionBar actionBar = getSupportActionBar();

        if (actionBar != null && title != null) {
            actionBar.setDisplayShowTitleEnabled(true);
            actionBar.setTitle(title);
        }
    }

    @Override
    public void startLoading() {
        //metodo que se va heredar para mostrar un loading
        dialog = setupDialog(this, R.layout.dialog_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        dialog.setCancelable(false);
        dialog.show();


    }

    @Override
    public void startLoadingText(String message) {
        //metodo que se va heredar para mostrar un loading con texto
        dialog = setupDialog(this, R.layout.dialog_loading);
        TextView messageLoading = (TextView) dialog.findViewById(R.id.message_loading);
        messageLoading.setVisibility(View.VISIBLE);
        messageLoading.setText(message);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        dialog.setCancelable(false);
        dialog.show();
    }



    @Override
    public void stopLoading() {
        //metodo el cual termina el loading
        if (dialog != null) {
            dialog.dismiss();
        }
    }


    //metodo el cual muestra un mensaje de error
    @Override
    public void showErrorMessage(Exception exception) {
        DialogUtil.showDialogMessage(this,exception.getMessage());

    }

    //metodo el cual muestra un mensaje de cuando es correcto
    @Override
    public void showSuccessMessage(String message) {
        DialogUtil.showDialogMessage(this,message);
    }



    //metodo el cual permite dar back a un activity
    @Override
    public void onBackPressed() {
        //si se comenta esta linea se va bloquear el back
        super.onBackPressed();
    }
}