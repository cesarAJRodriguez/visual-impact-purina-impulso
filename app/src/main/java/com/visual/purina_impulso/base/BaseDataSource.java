package com.visual.purina_impulso.base;


public interface BaseDataSource {

    interface ErrorCallback {

        void onError(Exception exception);
    }
}