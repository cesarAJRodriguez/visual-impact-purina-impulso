package com.visual.purina_impulso.base;


public interface BaseView {
    void startLoading();
    void startLoadingText(String message);
    void stopLoading();
    void showErrorMessage(Exception exception);
    void showSuccessMessage(String message);
}
