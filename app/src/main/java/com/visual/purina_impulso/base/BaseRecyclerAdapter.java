package com.visual.purina_impulso.base;

import android.content.Context;
import android.support.v7.widget.RecyclerView;


public abstract class BaseRecyclerAdapter<T extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<T> {

    protected Context CONTEXT;
    protected OnItemClickListener mListener;

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    public BaseRecyclerAdapter(Context context, OnItemClickListener listener) {
        super();
        CONTEXT = context;
        mListener = listener;
    }

}
