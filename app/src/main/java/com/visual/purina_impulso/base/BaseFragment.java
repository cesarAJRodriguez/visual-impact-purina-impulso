package com.visual.purina_impulso.base;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;

//BaseFragment el cual todas los fragment van extender BaseFragment
public abstract class BaseFragment extends Fragment implements BaseView {

    protected Context mContext;

    protected abstract int getResLayout();
    protected abstract void setupView(Bundle savedInstanceState);
    protected abstract void onRestoreView(Bundle savedInstanceState);


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(getResLayout(), container, false);
        if (view != null) {
            return view;
        }
        setHasOptionsMenu(true);
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        if (savedInstanceState != null) {
            onRestoreView(savedInstanceState);
        } else {
            setupView(getArguments());
        }
    }

    @Override
    public void startLoading() {
        ((BaseActivity) mContext).startLoading();
    }

    @Override
    public void startLoadingText(String message) {
        ((BaseActivity) mContext).startLoadingText(message);
    }

    @Override
    public void stopLoading() {
        ((BaseActivity) mContext).stopLoading();
    }

    protected void customizeToolbar (boolean homeEnabled) {
        ((BaseActivity) mContext).setupToolbar(homeEnabled);
    }

    protected void customizeToolbarTitle(String title) {
        ((BaseActivity) mContext).setToolbatTitle(title);
    }

    @Override
    public void showErrorMessage(Exception e) {
        ((BaseActivity) mContext).showErrorMessage(e);
    }

    @Override
    public void showSuccessMessage(String message) {
        ((BaseActivity) mContext).showSuccessMessage(message);
    }


}