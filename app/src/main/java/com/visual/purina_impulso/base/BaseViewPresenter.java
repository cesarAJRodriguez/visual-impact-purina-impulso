package com.visual.purina_impulso.base;

import android.support.annotation.NonNull;


public interface BaseViewPresenter<T extends BasePresenter> extends BaseView {

    void setPresenter(@NonNull T presenter);
}
