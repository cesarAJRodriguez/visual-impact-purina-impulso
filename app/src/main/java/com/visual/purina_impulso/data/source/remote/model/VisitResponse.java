package com.visual.purina_impulso.data.source.remote.model;

import com.google.gson.annotations.SerializedName;


public class VisitResponse {

    @SerializedName("bdLocalEliminarVisita")
    String visitLocalDelete;

    @SerializedName("bdLocalEliminarEncuesta")
    String quizLocalDelete;

    @SerializedName("bdLocalEliminarEncuestaDetalle")
    String quizDetailLocalDelete;

    @SerializedName("bdLocalEliminarFotos")
    String photoLocalDelete;

    @SerializedName("bdLocalEliminarAccionCompetencia")
    String actionLocalDelete;

    @SerializedName("bdLocalEliminarCShooper")
    String shooperLocalDelete;

    @SerializedName("bdLocalEliminarPrecioCompetencia")
    String PriceCompetitionLocalDelete;

    @SerializedName("bdLocalEliminarPrecioCompetenciaDetalle")
    String DetailPriceCompetitionLocalDelete;

    @SerializedName("bdLocalEliminarPrecioQuiebre")
    String PriceBreakLocalDelete;

    @SerializedName("bdLocalEliminarPrecioQuiebreDetalle")
    String DetailPriceBreakLocalDelete;

    @SerializedName("bdLocalEliminarEM")
    String MerchandisingLocalDelete;

    @SerializedName("bdLocalEliminarEMD")
    String DetailMerchandisingLocalDelete;

    @SerializedName("bdLocalEliminarEMP")
    String MerchandisingPersonalLocalDelete;

    @SerializedName("bdLocalEliminarEMF")
    String MerchandisingFinalLocalDelete;

    @SerializedName("bdLocalEliminarEMFD")
    String MerchandisingFinalDetailLocalDelete;




    @SerializedName("estadoEnvio")
    String sendStatus;

    public String getVisitLocalDelete() {
        return visitLocalDelete;
    }

    public String getQuizLocalDelete() {
        return quizLocalDelete;
    }

    public String getQuizDetailLocalDelete() {
        return quizDetailLocalDelete;
    }

    public String getPhotoLocalDelete() {
        return photoLocalDelete;
    }

    public String getActionLocalDelete() {
        return actionLocalDelete;
    }

    public String getShooperLocalDelete() {
        return shooperLocalDelete;
    }

    public String getSendStatus() {
        return sendStatus;
    }

    public String getPriceCompetitionLocalDelete() {
        return PriceCompetitionLocalDelete;
    }

    public String getDetailPriceCompetitionLocalDelete() {
        return DetailPriceCompetitionLocalDelete;
    }

    public String getMerchandisingLocalDelete() {
        return MerchandisingLocalDelete;
    }

    public String getDetailMerchandisingLocalDelete() {
        return DetailMerchandisingLocalDelete;
    }

    public String getMerchandisingPersonalLocalDelete() {
        return MerchandisingPersonalLocalDelete;
    }

    public String getPriceBreakLocalDelete() {
        return PriceBreakLocalDelete;
    }

    public String getDetailPriceBreakLocalDelete() {
        return DetailPriceBreakLocalDelete;
    }

    public String getMerchandisingFinalLocalDelete() {
        return MerchandisingFinalLocalDelete;
    }

    public String getMerchandisingFinalDetailLocalDelete() {
        return MerchandisingFinalDetailLocalDelete;
    }
}
