package com.visual.purina_impulso.data.source.local;

import com.visual.purina_impulso.Constants;
import com.visual.purina_impulso.data.source.ClientDataSource;
import com.visual.purina_impulso.data.source.local.realm.RealmService;
import com.visual.purina_impulso.domain.Client;

import java.util.ArrayList;

import io.realm.Realm;


public class LocalClientDataSource implements ClientDataSource {

    private static LocalClientDataSource INSTANCE = null;
    private final RealmService mRealService;

    private LocalClientDataSource() {
        mRealService = new RealmService(Realm.getDefaultInstance());
    }

    public synchronized static LocalClientDataSource getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new LocalClientDataSource();
        }
        return INSTANCE;
    }

    //obtener el listado de clientes de la bd local (los asignados y para la busqueda))
    @Override
    public void getClients(String employeeId, String type,String search, getClientsCallback callback, ErrorCallback errorCallback) {
        ArrayList<String> keys = new ArrayList<>();
        keys.add("employeeId");
        keys.add("type");
        ArrayList<String> values = new ArrayList<>();
        values.add(employeeId);
        values.add(type);

        if (search == null){
            mRealService.onReadObjects(Client.class,keys, values, new RealmService.DataBaseCallback<Client>() {
                @Override
                public void onTransactionSuccess(ArrayList<Client> object) {
                    callback.onSuccessGetClients(object);
                }

                @Override
                public void onTransactionError(Exception error) {
                    errorCallback.onError(error);
                }
            });
        }else{

            if (search.equals(Constants.FLAG_PENDING_CLIENTS_VISITS)){
                keys.add("pending");
                values.add(Constants.FLAG_DATA_PENDING_YES);
                mRealService.onReadObjects(Client.class,keys, values, new RealmService.DataBaseCallback<Client>() {
                    @Override
                    public void onTransactionSuccess(ArrayList<Client> object) {
                        callback.onSuccessGetClients(object);
                    }

                    @Override
                    public void onTransactionError(Exception error) {
                        errorCallback.onError(error);
                    }
                });
            }else{
                ArrayList<String> keysLike = new ArrayList<>();
                keysLike.add("code");
                ArrayList<String> valuesLike = new ArrayList<>();
                valuesLike.add(search);
                mRealService.onReadObjectsWhereLike(Client.class,keys, values,keysLike,valuesLike, new RealmService.DataBaseCallback<Client>() {
                    @Override
                    public void onTransactionSuccess(ArrayList<Client> object) {
                        callback.onSuccessGetClients(object);
                    }

                    @Override
                    public void onTransactionError(Exception error) {
                        errorCallback.onError(error);
                    }
                });
            }
        }


    }

    // guarda/actualiza los datos del cliente de la bd local
    @Override
    public void registerClient(Client client, registerClientCallback callback, ErrorCallback errorCallback) {
        ArrayList<Client> clients = new ArrayList<>();
        clients.add(client);

        mRealService.onWriteObjects(clients, new RealmService.DataBaseCallback<Client>() {
            @Override
            public void onTransactionSuccess(ArrayList<Client> object) {
                callback.onSuccessRegisterClient();
            }
            @Override
            public void onTransactionError(Exception error) {
                errorCallback.onError(error);
            }
        });
    }

    // obtenie los datos del cliente de la bd local
    @Override
    public void getClient(Client client, getClientCallback callback, ErrorCallback errorCallback) {

        ArrayList<String> keysString = new ArrayList<>();
        keysString.add("employeeId");
        ArrayList<String> valuesString = new ArrayList<>();
        valuesString.add(client.getEmployeeId());

        ArrayList<String> keysInteger = new ArrayList<>();
        keysInteger.add("clientId");
        ArrayList<Integer> valuesInteger = new ArrayList<>();
        valuesInteger.add(client.getClientId());



        mRealService.onReadValuesIntegerStringObjects(Client.class,keysInteger,valuesInteger,keysString, valuesString, new RealmService.DataBaseCallback<Client>() {
            @Override
            public void onTransactionSuccess(ArrayList<Client> object) {
                callback.onSuccessGetClient(object);
            }
            @Override
            public void onTransactionError(Exception error) {
                errorCallback.onError(error);
            }

        });
    }
}
