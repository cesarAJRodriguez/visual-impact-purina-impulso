package com.visual.purina_impulso.data.source.local.realm;

import android.support.annotation.NonNull;

import com.visual.purina_impulso.Constants;
import com.visual.purina_impulso.domain.Alternative;
import com.visual.purina_impulso.domain.BaseProduct;
import com.visual.purina_impulso.domain.BrandProduct;
import com.visual.purina_impulso.domain.Client;
import com.visual.purina_impulso.domain.Incidence;
import com.visual.purina_impulso.domain.ListAdvanceDay;
import com.visual.purina_impulso.domain.ListBusinessDay;
import com.visual.purina_impulso.domain.ListFee;
import com.visual.purina_impulso.domain.ListFeeImpulso;
import com.visual.purina_impulso.domain.ListSale;
import com.visual.purina_impulso.domain.ListVisit;
import com.visual.purina_impulso.domain.Merchandising;
import com.visual.purina_impulso.domain.Module;
import com.visual.purina_impulso.domain.Photo;
import com.visual.purina_impulso.domain.Price;
import com.visual.purina_impulso.domain.Product;
import com.visual.purina_impulso.domain.ProductCategory;
import com.visual.purina_impulso.domain.Question;
import com.visual.purina_impulso.domain.Quiz;
import com.visual.purina_impulso.domain.SubCategoryProduct;
import com.visual.purina_impulso.util.ThreadUtil;

import java.util.ArrayList;

import io.realm.Case;
import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmResults;

public class RealmService {

    private Realm mRealm;
    private RealmAsyncThread mRealmAsyncThread;

    public RealmService(Realm realm){
        mRealm = realm;
        mRealmAsyncThread = new RealmAsyncThread(this.getClass().getName());
    }

    public static RealmService getInstance(Realm realm) {
        return new RealmService(realm);
    }

    public Realm getRealm(){
        return (mRealm == null || mRealm.isClosed())?Realm.getDefaultInstance():mRealm;
    }

    public void closeRealm(){ mRealm.close();}



    public <T extends RealmObject> void onWriteObjects(   @NonNull final ArrayList<T> objects,
                                                          @NonNull final DataBaseCallback<T> callback){


        if(ThreadUtil.isMainThread()){
            final Realm  realm = getRealm();
            realm.executeTransactionAsync(new Realm.Transaction() {
                @Override
                public void execute(Realm bgRealm) {
                    for (T element : objects) {
                        bgRealm.copyToRealmOrUpdate(element);
                    }
                }
            }, new Realm.Transaction.OnSuccess() {
                @Override
                public void onSuccess() {
                    callback.onTransactionSuccess(null);
                    realm.close();
                }
            }, new Realm.Transaction.OnError() {
                @Override
                public void onError(Throwable error) {
                    error.printStackTrace();
                    callback.onTransactionError(new Exception(Constants.MESSAGE_GENERIC_ERROR));
                    realm.close();
                }
            });
        }else{
            final Lock lock = new Lock();
            mRealmAsyncThread.getHandler().post(new Runnable() {
                @Override
                public void run() {
                    final Realm realm = Realm.getDefaultInstance();
                    realm.executeTransactionAsync(new Realm.Transaction() {
                        @Override
                        public void execute(Realm bgRealm) {
                            for (T element : objects) {
                                bgRealm.copyToRealmOrUpdate(element);
                            }
                            lock.unlock();
                        }
                    }, new Realm.Transaction.OnSuccess() {
                        @Override
                        public void onSuccess() {
                            callback.onTransactionSuccess(null);
                            realm.close();
                        }
                    }, new Realm.Transaction.OnError() {
                        @Override
                        public void onError(Throwable error) {
                            error.printStackTrace();
                            callback.onTransactionError(new Exception(Constants.MESSAGE_GENERIC_ERROR));
                            realm.close();
                        }
                    });
                }
            });
            try {
                lock.lock();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }


    public <T extends RealmObject> void onWriteSyncObjects(   @NonNull final ArrayList<T> objects,
                                                          @NonNull final DataBaseCallback<T> callback){


        if(ThreadUtil.isMainThread()){
            final Realm  realm = getRealm();
            realm.executeTransactionAsync(new Realm.Transaction() {
                @Override
                public void execute(Realm bgRealm) {
                    bgRealm.delete(Client.class);
                    bgRealm.delete(Module.class);
                    bgRealm.delete(Photo.class);
                    bgRealm.delete(Incidence.class);
                    bgRealm.delete(Quiz.class);
                    bgRealm.delete(Question.class);
                    bgRealm.delete(Alternative.class);
                    bgRealm.delete(Price.class);
                    bgRealm.delete(ProductCategory.class);
                    bgRealm.delete(Merchandising.class);
                    bgRealm.delete(SubCategoryProduct.class);
                    bgRealm.delete(BrandProduct.class);
                    bgRealm.delete(BaseProduct.class);
                    bgRealm.delete(Product.class);
                    bgRealm.delete(ListAdvanceDay.class);
                    bgRealm.delete(ListBusinessDay.class);
                    bgRealm.delete(ListFee.class);
                    bgRealm.delete(ListSale.class);
                    bgRealm.delete(ListVisit.class);
                    bgRealm.delete(ListFeeImpulso.class);

                    for (T element : objects) {
                        bgRealm.copyToRealmOrUpdate(element);
                    }
                }
            }, new Realm.Transaction.OnSuccess() {
                @Override
                public void onSuccess() {
                    callback.onTransactionSuccess(null);
                    realm.close();
                }
            }, new Realm.Transaction.OnError() {
                @Override
                public void onError(Throwable error) {
                    error.printStackTrace();
                    callback.onTransactionError(new Exception(Constants.MESSAGE_GENERIC_ERROR));
                    realm.close();
                }
            });
        }else{
            final Lock lock = new Lock();
            mRealmAsyncThread.getHandler().post(new Runnable() {
                @Override
                public void run() {
                    final Realm realm = Realm.getDefaultInstance();
                    realm.executeTransactionAsync(new Realm.Transaction() {
                        @Override
                        public void execute(Realm bgRealm) {
                            bgRealm.delete(Client.class);
                            bgRealm.delete(Module.class);
                            bgRealm.delete(Photo.class);
                            bgRealm.delete(Incidence.class);
                            bgRealm.delete(Quiz.class);
                            bgRealm.delete(Question.class);
                            bgRealm.delete(Alternative.class);
                            bgRealm.delete(Price.class);
                            bgRealm.delete(ProductCategory.class);
                            bgRealm.delete(Merchandising.class);
                            bgRealm.delete(SubCategoryProduct.class);
                            bgRealm.delete(BrandProduct.class);
                            bgRealm.delete(BaseProduct.class);
                            bgRealm.delete(Product.class);
                            bgRealm.delete(ListAdvanceDay.class);
                            bgRealm.delete(ListBusinessDay.class);
                            bgRealm.delete(ListFee.class);
                            bgRealm.delete(ListSale.class);
                            bgRealm.delete(ListVisit.class);
                            bgRealm.delete(ListFeeImpulso.class);
                            for (T element : objects) {
                                bgRealm.copyToRealmOrUpdate(element);
                            }
                            lock.unlock();
                        }
                    }, new Realm.Transaction.OnSuccess() {
                        @Override
                        public void onSuccess() {
                            callback.onTransactionSuccess(null);
                            realm.close();
                        }
                    }, new Realm.Transaction.OnError() {
                        @Override
                        public void onError(Throwable error) {
                            error.printStackTrace();
                            callback.onTransactionError(new Exception(Constants.MESSAGE_GENERIC_ERROR));
                            realm.close();
                        }
                    });
                }
            });
            try {
                lock.lock();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public <T extends RealmObject> void onWriteSyncObjects(   @NonNull final boolean syncTotal,
                                                            @NonNull final ArrayList<T> objects,
                                                              @NonNull final DataBaseCallback<T> callback){


        if(ThreadUtil.isMainThread()){
            final Realm  realm = getRealm();
            realm.executeTransactionAsync(new Realm.Transaction() {
                @Override
                public void execute(Realm bgRealm) {
                    if (syncTotal){
                        bgRealm.deleteAll();
                    }else{
                        bgRealm.delete(Client.class);
                        bgRealm.delete(Module.class);
                        bgRealm.delete(Photo.class);
                        bgRealm.delete(Incidence.class);
                        bgRealm.delete(Quiz.class);
                        bgRealm.delete(Question.class);
                        bgRealm.delete(Alternative.class);
                        bgRealm.delete(Price.class);
                        bgRealm.delete(ProductCategory.class);
                        bgRealm.delete(Merchandising.class);
                        bgRealm.delete(SubCategoryProduct.class);
                        bgRealm.delete(BrandProduct.class);
                        bgRealm.delete(BaseProduct.class);
                        bgRealm.delete(Product.class);
                        bgRealm.delete(ListAdvanceDay.class);
                        bgRealm.delete(ListBusinessDay.class);
                        bgRealm.delete(ListFee.class);
                        bgRealm.delete(ListSale.class);
                        bgRealm.delete(ListVisit.class);
                        bgRealm.delete(ListFeeImpulso.class);
                    }


                    for (T element : objects) {
                        bgRealm.copyToRealmOrUpdate(element);
                    }
                }
            }, new Realm.Transaction.OnSuccess() {
                @Override
                public void onSuccess() {
                    callback.onTransactionSuccess(null);
                    realm.close();
                }
            }, new Realm.Transaction.OnError() {
                @Override
                public void onError(Throwable error) {
                    error.printStackTrace();
                    callback.onTransactionError(new Exception(Constants.MESSAGE_GENERIC_ERROR));
                    realm.close();
                }
            });
        }else{
            final Lock lock = new Lock();
            mRealmAsyncThread.getHandler().post(new Runnable() {
                @Override
                public void run() {
                    final Realm realm = Realm.getDefaultInstance();
                    realm.executeTransactionAsync(new Realm.Transaction() {
                        @Override
                        public void execute(Realm bgRealm) {
                            if (syncTotal){
                                bgRealm.deleteAll();
                            }else{
                                bgRealm.delete(Client.class);
                                bgRealm.delete(Module.class);
                                bgRealm.delete(Photo.class);
                                bgRealm.delete(Incidence.class);
                                bgRealm.delete(Quiz.class);
                                bgRealm.delete(Question.class);
                                bgRealm.delete(Alternative.class);
                                bgRealm.delete(Price.class);
                                bgRealm.delete(ProductCategory.class);
                                bgRealm.delete(Merchandising.class);
                                bgRealm.delete(SubCategoryProduct.class);
                                bgRealm.delete(BrandProduct.class);
                                bgRealm.delete(BaseProduct.class);
                                bgRealm.delete(Product.class);
                                bgRealm.delete(ListAdvanceDay.class);
                                bgRealm.delete(ListBusinessDay.class);
                                bgRealm.delete(ListFee.class);
                                bgRealm.delete(ListSale.class);
                                bgRealm.delete(ListVisit.class);
                                bgRealm.delete(ListFeeImpulso.class);
                            }
                            for (T element : objects) {
                                bgRealm.copyToRealmOrUpdate(element);
                            }
                            lock.unlock();
                        }
                    }, new Realm.Transaction.OnSuccess() {
                        @Override
                        public void onSuccess() {
                            callback.onTransactionSuccess(null);
                            realm.close();
                        }
                    }, new Realm.Transaction.OnError() {
                        @Override
                        public void onError(Throwable error) {
                            error.printStackTrace();
                            callback.onTransactionError(new Exception(Constants.MESSAGE_GENERIC_ERROR));
                            realm.close();
                        }
                    });
                }
            });
            try {
                lock.lock();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public <T extends RealmObject> void onReadObjects(  @NonNull final Class<T> tClass,
                                                        @NonNull final ArrayList<String> keys,
                                                        @NonNull final ArrayList<String> values,
                                                        @NonNull final DataBaseCallback<T> callback) {
        final ArrayList<T> objects = new ArrayList<>();
        if(ThreadUtil.isMainThread()){
            getRealm().executeTransactionAsync(new Realm.Transaction() {
                @Override
                public void execute(Realm bgRealm) {
                    RealmResults<T> realmObjects = bgRealm.where(tClass).findAll();


                    if (keys.size() == 1){
                        realmObjects = bgRealm.where(tClass).equalTo(keys.get(0),values.get(0)).findAll();
                    }

                    if (keys.size() == 2){
                        realmObjects = bgRealm.where(tClass).equalTo(keys.get(0),values.get(0)).equalTo(keys.get(1),values.get(1)).findAll();
                    }

                    if (keys.size() == 3){
                        realmObjects = bgRealm.where(tClass).equalTo(keys.get(0),values.get(0)).equalTo(keys.get(1),values.get(1)).equalTo(keys.get(2),values.get(2)).findAll();

                    }

                    /// RealmResults<T> realmObjects = bgRealm.where(tClass).findAll();0
                    for (T element : realmObjects) {
                        objects.add(bgRealm.copyFromRealm(element));
                    }
                }
            }, new Realm.Transaction.OnSuccess() {
                @Override
                public void onSuccess() {
                    callback.onTransactionSuccess(objects);
                    closeRealm();
                }
            }, new Realm.Transaction.OnError() {
                @Override
                public void onError(Throwable error) {
                    error.printStackTrace();
                    callback.onTransactionError(new Exception(Constants.MESSAGE_GENERIC_ERROR));
                    closeRealm();
                }
            });
        }else{
            final Lock lock = new Lock();
            mRealmAsyncThread.getHandler().post(new Runnable() {
                @Override
                public void run() {
                    final Realm realm =  Realm.getDefaultInstance();
                    realm.executeTransactionAsync(new Realm.Transaction() {
                        @Override
                        public void execute(Realm bgRealm) {
                            RealmResults<T> realmObjects = bgRealm.where(tClass).findAll();
                            if (keys.size() == 1){
                                realmObjects = bgRealm.where(tClass).equalTo(keys.get(0),values.get(0)).findAll();
                            }

                            if (keys.size() == 2){
                                realmObjects = bgRealm.where(tClass).equalTo(keys.get(0),values.get(0)).equalTo(keys.get(1),values.get(1)).findAll();
                            }

                            if (keys.size() == 3){
                                realmObjects = bgRealm.where(tClass).equalTo(keys.get(0),values.get(0)).equalTo(keys.get(1),values.get(1)).equalTo(keys.get(2),values.get(2)).findAll();

                            }

                            for (T element : realmObjects) {
                                objects.add(bgRealm.copyFromRealm(element));
                            }
                            lock.unlock();
                        }
                    }, new Realm.Transaction.OnSuccess() {
                        @Override
                        public void onSuccess() {
                            callback.onTransactionSuccess(objects);
                            realm.close();
                        }
                    }, new Realm.Transaction.OnError() {
                        @Override
                        public void onError(Throwable error) {
                            error.printStackTrace();
                            callback.onTransactionError(new Exception(Constants.MESSAGE_GENERIC_ERROR));
                            realm.close();
                        }
                    });
                }
            });
            try {
                lock.lock();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public <T extends RealmObject> void onReadValuesIntegerObjects(  @NonNull final Class<T> tClass,
                                                        @NonNull final ArrayList<String> keys,
                                                        @NonNull final ArrayList<Integer> values,
                                                        @NonNull final DataBaseCallback<T> callback) {
        final ArrayList<T> objects = new ArrayList<>();
        if(ThreadUtil.isMainThread()){
            getRealm().executeTransactionAsync(new Realm.Transaction() {
                @Override
                public void execute(Realm bgRealm) {
                    RealmResults<T> realmObjects = bgRealm.where(tClass).findAll();
                    if (keys.size() == 2){
                       realmObjects = bgRealm.where(tClass).equalTo(keys.get(0),values.get(0)).equalTo(keys.get(1),values.get(1)).findAll();
                    }

                    if (keys.size() == 1){
                       realmObjects = bgRealm.where(tClass).equalTo(keys.get(0),values.get(0)).findAll();
                    }

                    for (T element : realmObjects) {
                        objects.add(bgRealm.copyFromRealm(element));
                    }
                }
            }, new Realm.Transaction.OnSuccess() {
                @Override
                public void onSuccess() {
                    callback.onTransactionSuccess(objects);
                    closeRealm();
                }
            }, new Realm.Transaction.OnError() {
                @Override
                public void onError(Throwable error) {
                    error.printStackTrace();
                    callback.onTransactionError(new Exception(Constants.MESSAGE_GENERIC_ERROR));
                    closeRealm();
                }
            });
        }else{
            final Lock lock = new Lock();
            mRealmAsyncThread.getHandler().post(new Runnable() {
                @Override
                public void run() {
                    final Realm realm =  Realm.getDefaultInstance();
                    realm.executeTransactionAsync(new Realm.Transaction() {
                        @Override
                        public void execute(Realm bgRealm) {
                            RealmResults<T> realmObjects = bgRealm.where(tClass).findAll();
                            if (keys.size() == 2){
                                realmObjects = bgRealm.where(tClass).equalTo(keys.get(0),values.get(0)).equalTo(keys.get(1),values.get(1)).findAll();
                            }

                            if (keys.size() == 1){
                                realmObjects = bgRealm.where(tClass).equalTo(keys.get(0),values.get(0)).findAll();
                            }

                            for (T element : realmObjects) {
                                objects.add(bgRealm.copyFromRealm(element));
                            }
                            lock.unlock();
                        }
                    }, new Realm.Transaction.OnSuccess() {
                        @Override
                        public void onSuccess() {
                            callback.onTransactionSuccess(objects);
                            realm.close();
                        }
                    }, new Realm.Transaction.OnError() {
                        @Override
                        public void onError(Throwable error) {
                            error.printStackTrace();
                            callback.onTransactionError(new Exception(Constants.MESSAGE_GENERIC_ERROR));
                            realm.close();
                        }
                    });
                }
            });
            try {
                lock.lock();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public <T extends RealmObject> void onReadValuesIntegerStringObjects(  @NonNull final Class<T> tClass,
                                                                     @NonNull final ArrayList<String> keysInteger,
                                                                     @NonNull final ArrayList<Integer> valuesInteger,
                                                                   @NonNull final ArrayList<String> keysString,
                                                                   @NonNull final ArrayList<String> valuesString,
                                                                     @NonNull final DataBaseCallback<T> callback) {
        final ArrayList<T> objects = new ArrayList<>();
        if(ThreadUtil.isMainThread()){
            getRealm().executeTransactionAsync(new Realm.Transaction() {
                @Override
                public void execute(Realm bgRealm) {
                    RealmResults<T> realmObjects = bgRealm.where(tClass).findAll();

                    realmObjects = bgRealm.where(tClass).equalTo(keysInteger.get(0),valuesInteger.get(0)).equalTo(keysString.get(0),valuesString.get(0)).findAll();


                    for (T element : realmObjects) {
                        objects.add(bgRealm.copyFromRealm(element));
                    }
                }
            }, new Realm.Transaction.OnSuccess() {
                @Override
                public void onSuccess() {
                    callback.onTransactionSuccess(objects);
                    closeRealm();
                }
            }, new Realm.Transaction.OnError() {
                @Override
                public void onError(Throwable error) {
                    error.printStackTrace();
                    callback.onTransactionError(new Exception(Constants.MESSAGE_GENERIC_ERROR));
                    closeRealm();
                }
            });
        }else{
            final Lock lock = new Lock();
            mRealmAsyncThread.getHandler().post(new Runnable() {
                @Override
                public void run() {
                    final Realm realm =  Realm.getDefaultInstance();
                    realm.executeTransactionAsync(new Realm.Transaction() {
                        @Override
                        public void execute(Realm bgRealm) {
                            RealmResults<T> realmObjects = bgRealm.where(tClass).findAll();
                            realmObjects = bgRealm.where(tClass).equalTo(keysInteger.get(0),valuesInteger.get(0)).equalTo(keysString.get(0),valuesString.get(0)).findAll();

                            for (T element : realmObjects) {
                                objects.add(bgRealm.copyFromRealm(element));
                            }
                            lock.unlock();
                        }
                    }, new Realm.Transaction.OnSuccess() {
                        @Override
                        public void onSuccess() {
                            callback.onTransactionSuccess(objects);
                            realm.close();
                        }
                    }, new Realm.Transaction.OnError() {
                        @Override
                        public void onError(Throwable error) {
                            error.printStackTrace();
                            callback.onTransactionError(new Exception(Constants.MESSAGE_GENERIC_ERROR));
                            realm.close();
                        }
                    });
                }
            });
            try {
                lock.lock();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public <T extends RealmObject> void onReadValuesIntegerStringLikeObjects(  @NonNull final Class<T> tClass,
                                                                           @NonNull final ArrayList<String> keysInteger,
                                                                           @NonNull final ArrayList<Integer> valuesInteger,
                                                                           @NonNull final ArrayList<String> keysString,
                                                                           @NonNull final ArrayList<String> valuesString,
                                                                           @NonNull final ArrayList<String> keysLike,
                                                                           @NonNull final ArrayList<String> valuesLike,
                                                                           @NonNull final DataBaseCallback<T> callback) {
        final ArrayList<T> objects = new ArrayList<>();
        if(ThreadUtil.isMainThread()){
            getRealm().executeTransactionAsync(new Realm.Transaction() {
                @Override
                public void execute(Realm bgRealm) {
                    RealmResults<T> realmObjects = bgRealm.where(tClass).findAll();

                    realmObjects = bgRealm.where(tClass).equalTo(keysInteger.get(0),valuesInteger.get(0)).equalTo(keysString.get(0),valuesString.get(0)).contains(keysLike.get(0), valuesLike.get(0), Case.INSENSITIVE).findAll();


                    for (T element : realmObjects) {
                        objects.add(bgRealm.copyFromRealm(element));
                    }
                }
            }, new Realm.Transaction.OnSuccess() {
                @Override
                public void onSuccess() {
                    callback.onTransactionSuccess(objects);
                    closeRealm();
                }
            }, new Realm.Transaction.OnError() {
                @Override
                public void onError(Throwable error) {
                    error.printStackTrace();
                    callback.onTransactionError(new Exception(Constants.MESSAGE_GENERIC_ERROR));
                    closeRealm();
                }
            });
        }else{
            final Lock lock = new Lock();
            mRealmAsyncThread.getHandler().post(new Runnable() {
                @Override
                public void run() {
                    final Realm realm =  Realm.getDefaultInstance();
                    realm.executeTransactionAsync(new Realm.Transaction() {
                        @Override
                        public void execute(Realm bgRealm) {
                            RealmResults<T> realmObjects = bgRealm.where(tClass).findAll();
                            realmObjects = bgRealm.where(tClass).equalTo(keysInteger.get(0),valuesInteger.get(0)).equalTo(keysString.get(0),valuesString.get(0)).contains(keysLike.get(0), valuesLike.get(0), Case.INSENSITIVE).findAll();

                            for (T element : realmObjects) {
                                objects.add(bgRealm.copyFromRealm(element));
                            }
                            lock.unlock();
                        }
                    }, new Realm.Transaction.OnSuccess() {
                        @Override
                        public void onSuccess() {
                            callback.onTransactionSuccess(objects);
                            realm.close();
                        }
                    }, new Realm.Transaction.OnError() {
                        @Override
                        public void onError(Throwable error) {
                            error.printStackTrace();
                            callback.onTransactionError(new Exception(Constants.MESSAGE_GENERIC_ERROR));
                            realm.close();
                        }
                    });
                }
            });
            try {
                lock.lock();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public <T extends RealmObject> void onReadObjectsWhereLike(  @NonNull final Class<T> tClass,
                                                        @NonNull final ArrayList<String> keys,
                                                        @NonNull final ArrayList<String> values,
                                                        @NonNull final ArrayList<String> keysLike,
                                                        @NonNull final ArrayList<String> valuesLike,
                                                        @NonNull final DataBaseCallback<T> callback) {
        final ArrayList<T> objects = new ArrayList<>();
        if(ThreadUtil.isMainThread()){
            getRealm().executeTransactionAsync(new Realm.Transaction() {
                @Override
                public void execute(Realm bgRealm) {
                    RealmResults<T> realmObjects = bgRealm.where(tClass).findAll();
                    if (keys.size() == 2){
                    realmObjects = bgRealm.where(tClass).equalTo(keys.get(0),values.get(0)).equalTo(keys.get(1),values.get(1)).contains(keysLike.get(0), valuesLike.get(0), Case.INSENSITIVE).findAll();

                    }

                    if (keys.size() == 1){
                       realmObjects = bgRealm.where(tClass).equalTo(keys.get(0),values.get(0)).contains(keysLike.get(0), valuesLike.get(0), Case.INSENSITIVE).findAll();

                    }
                    /// RealmResults<T> realmObjects = bgRealm.where(tClass).findAll();0
                    for (T element : realmObjects) {
                        objects.add(bgRealm.copyFromRealm(element));
                    }
                }
            }, new Realm.Transaction.OnSuccess() {
                @Override
                public void onSuccess() {
                    callback.onTransactionSuccess(objects);
                    closeRealm();
                }
            }, new Realm.Transaction.OnError() {
                @Override
                public void onError(Throwable error) {
                    error.printStackTrace();
                    callback.onTransactionError(new Exception(Constants.MESSAGE_GENERIC_ERROR));
                    closeRealm();
                }
            });
        }else{
            final Lock lock = new Lock();
            mRealmAsyncThread.getHandler().post(new Runnable() {
                @Override
                public void run() {
                    final Realm realm =  Realm.getDefaultInstance();
                    realm.executeTransactionAsync(new Realm.Transaction() {
                        @Override
                        public void execute(Realm bgRealm) {
                            RealmResults<T> realmObjects = bgRealm.where(tClass).findAll();
                            if (keys.size() == 2){
                                realmObjects = bgRealm.where(tClass).equalTo(keys.get(0),values.get(0)).equalTo(keys.get(1),values.get(1)).contains(keysLike.get(0), valuesLike.get(0), Case.INSENSITIVE).findAll();

                            }

                            if (keys.size() == 1){
                                realmObjects = bgRealm.where(tClass).equalTo(keys.get(0),values.get(0)).contains(keysLike.get(0), valuesLike.get(0), Case.INSENSITIVE).findAll();

                            }
                            for (T element : realmObjects) {
                                objects.add(bgRealm.copyFromRealm(element));
                            }
                            lock.unlock();
                        }
                    }, new Realm.Transaction.OnSuccess() {
                        @Override
                        public void onSuccess() {
                            callback.onTransactionSuccess(objects);
                            realm.close();
                        }
                    }, new Realm.Transaction.OnError() {
                        @Override
                        public void onError(Throwable error) {
                            error.printStackTrace();
                            callback.onTransactionError(new Exception(Constants.MESSAGE_GENERIC_ERROR));
                            realm.close();
                        }
                    });
                }
            });
            try {
                lock.lock();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }



    public <T extends RealmObject> void onDeleteObjects(@NonNull final Class tClass,
                                                        @NonNull final String key,
                                                        @NonNull final String value,
                                                        @NonNull final DataBaseCallback<T> callback) {
        final ArrayList<T> objects = new ArrayList<>();
        if(ThreadUtil.isMainThread()){
            getRealm().executeTransactionAsync(new Realm.Transaction() {
                @Override
                public void execute(Realm bgRealm) {
                    RealmResults realmObjects = bgRealm.where(tClass).equalTo(key,value).findAll();
                    /// RealmResults<T> realmObjects = bgRealm.where(tClass).findAll();0
                  /*  for (T element : realmObjects) {
                        objects.removeAll(bgRealm.copyFromRealm(element));
                    }*/
                  realmObjects.deleteAllFromRealm();
                }
            }, new Realm.Transaction.OnSuccess() {
                @Override
                public void onSuccess() {
                    callback.onTransactionSuccess(objects);
                    closeRealm();
                }
            }, new Realm.Transaction.OnError() {
                @Override
                public void onError(Throwable error) {
                    error.printStackTrace();
                    callback.onTransactionError(new Exception(Constants.MESSAGE_GENERIC_ERROR));
                    closeRealm();
                }
            });
        }else{
            final Lock lock = new Lock();
            mRealmAsyncThread.getHandler().post(new Runnable() {
                @Override
                public void run() {
                    final Realm realm =  Realm.getDefaultInstance();
                    realm.executeTransactionAsync(new Realm.Transaction() {
                        @Override
                        public void execute(Realm bgRealm) {
                           /* final RealmResults<T> realmObjects = bgRealm.where(tClass).equalTo(keys.get(0),values.get(0)).equalTo(keys.get(1),values.get(1)).findAll();
                            for (T element : realmObjects) {
                                objects.add(bgRealm.copyFromRealm(element));
                            }*/
                            RealmResults<T> realmObjects = bgRealm.where(tClass).equalTo(key,value).findAll();
                            /// RealmResults<T> realmObjects = bgRealm.where(tClass).findAll();0
                  /*  for (T element : realmObjects) {
                        objects.removeAll(bgRealm.copyFromRealm(element));
                    }*/
                            realmObjects.deleteAllFromRealm();
                            lock.unlock();
                        }
                    }, new Realm.Transaction.OnSuccess() {
                        @Override
                        public void onSuccess() {
                            callback.onTransactionSuccess(objects);
                            realm.close();
                        }
                    }, new Realm.Transaction.OnError() {
                        @Override
                        public void onError(Throwable error) {
                            error.printStackTrace();
                            callback.onTransactionError(new Exception(Constants.MESSAGE_GENERIC_ERROR));
                            realm.close();
                        }
                    });
                }
            });
            try {
                lock.lock();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public <T extends RealmObject> void onReadAllObjects(   @NonNull final Class<T> tClass,
                                                            @NonNull final DataBaseCallback<T> callback) {
        final ArrayList<T> objects = new ArrayList<>();
        if(ThreadUtil.isMainThread()){
            getRealm().executeTransactionAsync(new Realm.Transaction() {
                @Override
                public void execute(Realm bgRealm) {
                    RealmResults<T> realmObjects = bgRealm.where(tClass).findAll();
                    for (T element : realmObjects) {
                        objects.add(bgRealm.copyFromRealm(element));
                    }
                }
            }, new Realm.Transaction.OnSuccess() {
                @Override
                public void onSuccess() {
                    callback.onTransactionSuccess(objects);
                    closeRealm();
                }
            }, new Realm.Transaction.OnError() {
                @Override
                public void onError(Throwable error) {
                    error.printStackTrace();
                    callback.onTransactionError(new Exception(Constants.MESSAGE_GENERIC_ERROR));
                    closeRealm();
                }
            });
        }else{
            final Lock lock = new Lock();
            mRealmAsyncThread.getHandler().post(new Runnable() {
                @Override
                public void run() {
                    final Realm realm =  Realm.getDefaultInstance();
                    realm.executeTransactionAsync(new Realm.Transaction() {
                        @Override
                        public void execute(Realm bgRealm) {
                            final RealmResults<T> realmObjects = bgRealm.where(tClass).findAll();
                            for (T element : realmObjects) {
                                objects.add(bgRealm.copyFromRealm(element));
                            }
                            lock.unlock();
                        }
                    }, new Realm.Transaction.OnSuccess() {
                        @Override
                        public void onSuccess() {
                            callback.onTransactionSuccess(objects);
                            realm.close();
                        }
                    }, new Realm.Transaction.OnError() {
                        @Override
                        public void onError(Throwable error) {
                            error.printStackTrace();
                            callback.onTransactionError(new Exception(Constants.MESSAGE_GENERIC_ERROR));
                            realm.close();
                        }
                    });
                }
            });
            try {
                lock.lock();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public interface DataBaseCallback<T extends RealmObject>{
        void onTransactionSuccess(ArrayList<T> object);
        void onTransactionError(Exception error);
    }

    public interface DataBaseCallbackDelete{
        void onTransactionDeleteSuccess();
        void onTransactionDeleteError(Exception error);
    }

    public void onClearDB(@NonNull final DataBaseCallbackDelete callback) {

        if(ThreadUtil.isMainThread()){
            getRealm().executeTransactionAsync(new Realm.Transaction() {
                @Override
                public void execute(Realm bgRealm) {
                    bgRealm.deleteAll();

                }
            }, new Realm.Transaction.OnSuccess() {
                @Override
                public void onSuccess() {
                    callback.onTransactionDeleteSuccess();
                    closeRealm();
                }
            }, new Realm.Transaction.OnError() {
                @Override
                public void onError(Throwable error) {
                    error.printStackTrace();
                    callback.onTransactionDeleteError(new Exception(Constants.MESSAGE_GENERIC_ERROR));
                    closeRealm();
                }
            });
        }else{
            final Lock lock = new Lock();
            mRealmAsyncThread.getHandler().post(new Runnable() {
                @Override
                public void run() {
                    final Realm realm =  Realm.getDefaultInstance();
                    realm.executeTransactionAsync(new Realm.Transaction() {
                        @Override
                        public void execute(Realm bgRealm) {
                            bgRealm.deleteAll();
                            lock.unlock();
                        }
                    }, new Realm.Transaction.OnSuccess() {
                        @Override
                        public void onSuccess() {
                            callback.onTransactionDeleteSuccess();
                            realm.close();
                        }
                    }, new Realm.Transaction.OnError() {
                        @Override
                        public void onError(Throwable error) {
                            error.printStackTrace();
                            callback.onTransactionDeleteError(new Exception(Constants.MESSAGE_GENERIC_ERROR));
                            realm.close();
                        }
                    });
                }
            });
            try {
                lock.lock();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }

    public void onClearPartialDB(@NonNull final DataBaseCallbackDelete callback) {

        if(ThreadUtil.isMainThread()){
            getRealm().executeTransactionAsync(new Realm.Transaction() {
                @Override
                public void execute(Realm bgRealm) {
                    bgRealm.delete(Client.class);
                    bgRealm.delete(Module.class);
                    bgRealm.delete(Photo.class);
                    bgRealm.delete(Incidence.class);
                    bgRealm.delete(Quiz.class);
                    bgRealm.delete(Question.class);
                    bgRealm.delete(Alternative.class);
                    bgRealm.delete(Price.class);
                    bgRealm.delete(ProductCategory.class);
                    bgRealm.delete(Merchandising.class);
                    bgRealm.delete(SubCategoryProduct.class);
                    bgRealm.delete(BrandProduct.class);
                    bgRealm.delete(BaseProduct.class);
                    bgRealm.delete(Product.class);
                    bgRealm.delete(ListAdvanceDay.class);
                    bgRealm.delete(ListBusinessDay.class);
                    bgRealm.delete(ListFee.class);
                    bgRealm.delete(ListSale.class);
                    bgRealm.delete(ListVisit.class);
                    bgRealm.delete(ListFeeImpulso.class);

                }
            }, new Realm.Transaction.OnSuccess() {
                @Override
                public void onSuccess() {
                    callback.onTransactionDeleteSuccess();
                    closeRealm();
                }
            }, new Realm.Transaction.OnError() {
                @Override
                public void onError(Throwable error) {
                    error.printStackTrace();
                    callback.onTransactionDeleteError(new Exception(Constants.MESSAGE_GENERIC_ERROR));
                    closeRealm();
                }
            });
        }else{
            final Lock lock = new Lock();
            mRealmAsyncThread.getHandler().post(new Runnable() {
                @Override
                public void run() {
                    final Realm realm =  Realm.getDefaultInstance();
                    realm.executeTransactionAsync(new Realm.Transaction() {
                        @Override
                        public void execute(Realm bgRealm) {
                            bgRealm.delete(Client.class);
                            bgRealm.delete(Module.class);
                            bgRealm.delete(Photo.class);
                            bgRealm.delete(Incidence.class);
                            bgRealm.delete(Quiz.class);
                            bgRealm.delete(Question.class);
                            bgRealm.delete(Alternative.class);
                            bgRealm.delete(Price.class);
                            bgRealm.delete(ProductCategory.class);
                            bgRealm.delete(Merchandising.class);
                            bgRealm.delete(SubCategoryProduct.class);
                            bgRealm.delete(BrandProduct.class);
                            bgRealm.delete(BaseProduct.class);
                            bgRealm.delete(Product.class);
                            bgRealm.delete(ListAdvanceDay.class);
                            bgRealm.delete(ListBusinessDay.class);
                            bgRealm.delete(ListFee.class);
                            bgRealm.delete(ListSale.class);
                            bgRealm.delete(ListVisit.class);
                            bgRealm.delete(ListFeeImpulso.class);
                            lock.unlock();
                        }
                    }, new Realm.Transaction.OnSuccess() {
                        @Override
                        public void onSuccess() {
                            callback.onTransactionDeleteSuccess();
                            realm.close();
                        }
                    }, new Realm.Transaction.OnError() {
                        @Override
                        public void onError(Throwable error) {
                            error.printStackTrace();
                            callback.onTransactionDeleteError(new Exception(Constants.MESSAGE_GENERIC_ERROR));
                            realm.close();
                        }
                    });
                }
            });
            try {
                lock.lock();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }




}
