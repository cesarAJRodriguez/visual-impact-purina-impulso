package com.visual.purina_impulso.data.source.remote.api;

import com.visual.purina_impulso.data.source.remote.model.SyncResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;


public class SyncService {
    public interface Api{

        @FormUrlEncoded
        @POST("sincronizar")
        Call<SyncResponse> sync(
                @Field("sincronizar_tradicional") String syncTraditional,
                @Field("sincronizar_moderno") String syncModern,
                @Field("sincronizar_especializado") String syncSpecialized

        );

    }
}
