package com.visual.purina_impulso.data.source.local;

import com.visual.purina_impulso.data.source.UserDataSource;
import com.visual.purina_impulso.domain.User;


public class  LocalUserDataSource implements UserDataSource {

    private static LocalUserDataSource INSTANCE = null;

    private LocalUserDataSource() {}

    public synchronized static LocalUserDataSource getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new LocalUserDataSource();
        }
        return INSTANCE;
    }


    // login de prueba local
    @Override
    public void login(String dni, String password, LoginCallback callback, ErrorCallback errorCallback) {

        User user = new User("FALSE",8164,4,1, "GUSTAVO ALBERTO PALOMINO NUÑEZ","TODOS","DESARROLLADOR");
        callback.onSuccess(user);
    }

    @Override
    public void registerTokenPush(String userId, String tokenPush, RegisterTokenPushCallback callback, ErrorCallback errorCallback) {

    }

    @Override
    public void checkVersion(String version, CheckVersionCallback callback, ErrorCallback errorCallback) {

    }
}
