package com.visual.purina_impulso.data.source;

import com.visual.purina_impulso.base.BaseDataSource;
import com.visual.purina_impulso.domain.User;


public interface UserDataSource extends BaseDataSource {

    interface LoginCallback {
        void onSuccess(User user);
    }

    interface RegisterTokenPushCallback {
        void onSuccessRegisterTokenPush();
    }

    interface CheckVersionCallback {
        void onSuccessCheckVersion();
    }

    void login(String dni, String password, final LoginCallback callback, final ErrorCallback errorCallback);

    void registerTokenPush(String userId, String tokenPush, final RegisterTokenPushCallback callback, final ErrorCallback errorCallback);

    void checkVersion(String version,final CheckVersionCallback callback, final ErrorCallback errorCallback);

}
