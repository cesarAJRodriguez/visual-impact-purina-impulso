package com.visual.purina_impulso.data.source;

import com.visual.purina_impulso.base.BaseDataSource;
import com.visual.purina_impulso.domain.Alternative;
import com.visual.purina_impulso.domain.BaseProduct;
import com.visual.purina_impulso.domain.BrandProduct;
import com.visual.purina_impulso.domain.Client;
import com.visual.purina_impulso.domain.Incidence;
import com.visual.purina_impulso.domain.IncidenceVisit;
import com.visual.purina_impulso.domain.ListAdvanceDay;
import com.visual.purina_impulso.domain.ListBusinessDay;
import com.visual.purina_impulso.domain.ListFee;
import com.visual.purina_impulso.domain.ListFeeImpulso;
import com.visual.purina_impulso.domain.ListSale;
import com.visual.purina_impulso.domain.ListVisit;
import com.visual.purina_impulso.domain.Merchandising;
import com.visual.purina_impulso.domain.Photo;
import com.visual.purina_impulso.domain.Price;
import com.visual.purina_impulso.domain.Product;
import com.visual.purina_impulso.domain.ProductCategory;
import com.visual.purina_impulso.domain.Question;
import com.visual.purina_impulso.domain.Quiz;
import com.visual.purina_impulso.domain.StartVisit;
import com.visual.purina_impulso.domain.SubCategoryProduct;
import com.visual.purina_impulso.domain.Visit;

import java.util.ArrayList;


public interface VisitDataSource extends BaseDataSource {

    interface assignVisitCallback {
        void onSuccessAssignVisit();
    }

    interface getQuizzesVisitCallback {
        void onSuccessGetQuizzesVisit(ArrayList<Quiz> quizzes);
    }

    interface getQuizQuestionsCallback {
        void onSuccessGetQuizQuestions(ArrayList<Question> questions);
    }

    interface getQuizQuestionAlternativesCallback {
        void onSuccessGetQuizQuestionAlternatives(ArrayList<Alternative> alternatives);
    }

    interface getPricesCallback {
        void onSuccessGetPrices(ArrayList<Price> pricesBreak);
    }

    interface getTypePhotosCallback {
        void onSuccessGetTypePhotos(ArrayList<Photo> photos);
    }

    interface getCategoriesProductCallback {
        void onSuccessGetCategoriesProduct(ArrayList<ProductCategory> productCategories);
    }

    interface getTypeIncidencesCallback {
        void onSuccessGetTypeIncidences(ArrayList<Incidence> incidences);
    }


    interface registerStartVisitCallback {
        void onSuccessRegisterStartVisit(Visit visit);
    }

    interface registerIncidenceVisitCallback {
        void onSuccessRegisterIncidenceVisit();
    }

    interface registerVisitCallback {
        void onSuccessRegisterVisit();
    }

    interface checkVisitsPendingCallback {
        void onSuccessCheckVisitsPending();
    }

    interface getVisitClientCallback {
        void onSuccessGetVisitClient(ArrayList<Visit> visit);
    }

    interface getMerchandisingCallback {
        void onSuccessGetMerchandising(ArrayList<Merchandising> merchandisings);
    }

    interface getSubCategoryProductCallback {
        void onSuccessGetSubCategoryProduct(ArrayList<SubCategoryProduct> subCategoryProducts);
    }

    interface getBrandProductCallback {
        void onSuccessGetBrandProduct(ArrayList<BrandProduct> brandProducts);
    }

    interface getBaseProductCallback {
        void onSuccessGetBaseProduct(ArrayList<BaseProduct> baseProducts);
    }

    interface getProductCallback {
        void onSuccessGetProduct(ArrayList<Product> products);
    }


    interface getListAdvanceDayCallback {
        void onSuccessGetListAdvanceDay(ArrayList<ListAdvanceDay> listAdvanceDays);
    }

    interface getListBusinessDayCallback {
        void onSuccessGetListBusinessDay(ArrayList<ListBusinessDay> listBusinessDays);
    }

    interface getListFeeCallback {
        void onSuccessGetListFeeDay(ArrayList<ListFee> listFees);
    }

    interface getListSaleCallback {
        void onSuccessGetListSaleDay(ArrayList<ListSale> listSales);
    }

    interface getListVisitCallback {
        void onSuccessGetListVisit(ArrayList<ListVisit> listVisits);
    }

    interface getListFeeImpulsoCallback {
        void onSuccessGetListFeeImpulso(ArrayList<ListFeeImpulso> listFeeImpulsos);
    }


    void assignVisit(String currentDate,String[] clients, final assignVisitCallback callback, final ErrorCallback errorCallback);

    void getQuizzesVisit(int channelId, final getQuizzesVisitCallback callback, final ErrorCallback errorCallback);

    void getQuizQuestions(int channelId,int quizId, final getQuizQuestionsCallback callback, final ErrorCallback errorCallback);

    void getQuizQuestionAlternatives(int channelId,int quizId, final getQuizQuestionAlternativesCallback callback, final ErrorCallback errorCallback);

    void getPrices(int clientId,String typePrice,String search, final getPricesCallback callback, final ErrorCallback errorCallback);

    void getTypePhotos(int channelId, final getTypePhotosCallback callback, final ErrorCallback errorCallback);

    void getCategoriesProduct(int channelId, final getCategoriesProductCallback callback, final ErrorCallback errorCallback);

    void getTypeIncidences(int channelId, final getTypeIncidencesCallback callback, final ErrorCallback errorCallback);

    void registerStartVisit(StartVisit startVisit, Visit visit, final registerStartVisitCallback callback, final ErrorCallback errorCallback);

    void registerIncidenceVisit(StartVisit startVisit, IncidenceVisit incidenceVisit, final registerIncidenceVisitCallback callback, final ErrorCallback errorCallback);

    void registerVisit(Visit visit,String moduleVisit,final registerVisitCallback callback, final ErrorCallback errorCallback);

    void checkVisitsPending(Client client, final checkVisitsPendingCallback callback, final ErrorCallback errorCallback);

    void getVisitClient(String visitId, final getVisitClientCallback callback, final ErrorCallback errorCallback);

    void getMerchandising( final getMerchandisingCallback callback, final ErrorCallback errorCallback);

    void getSubCategoryProduct( final getSubCategoryProductCallback callback, final ErrorCallback errorCallback);

    void getBrandProduct(int SubCategoryProduct, final getBrandProductCallback callback, final ErrorCallback errorCallback);

    void getBaseProduct(int SubCategoryProduct,int BrandProduct, final getBaseProductCallback callback, final ErrorCallback errorCallback);

    void getProducts(String baseProduct, final getProductCallback callback, final ErrorCallback errorCallback);

    void getListAdvanceDay( final getListAdvanceDayCallback callback, final ErrorCallback errorCallback);

    void getListBusinessDay( final getListBusinessDayCallback callback, final ErrorCallback errorCallback);

    void getListFee(Client client, final getListFeeCallback callback, final ErrorCallback errorCallback);

    void getListSale(Client client, final getListSaleCallback callback, final ErrorCallback errorCallback);

    void getListVisit(Client client, final getListVisitCallback callback, final ErrorCallback errorCallback);

    void getListFeeImpulso(Client client, final getListFeeImpulsoCallback callback, final ErrorCallback errorCallback);
}
