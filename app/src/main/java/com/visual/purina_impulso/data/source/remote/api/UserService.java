package com.visual.purina_impulso.data.source.remote.api;

import com.visual.purina_impulso.data.source.remote.model.UserResponse;
import com.visual.purina_impulso.data.source.remote.model.VersionResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public class UserService {
    public interface Api{

        @FormUrlEncoded
        @POST("login")
        Call<UserResponse> login(
                @Field("usuario") String dni,
                @Field("contrasenia") String password

        );

        @FormUrlEncoded
        @POST("validar_version")
        Call<VersionResponse> checkVersion(
                @Field("versionApp") String version
        );



    }
}
