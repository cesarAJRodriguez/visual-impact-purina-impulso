package com.visual.purina_impulso.data.source.remote.model;

import com.google.gson.annotations.SerializedName;


public class AssistanceResponse {

    @SerializedName("estadoEnvio")
    protected String status;

    public String getStatus() {
        return status;
    }

}
