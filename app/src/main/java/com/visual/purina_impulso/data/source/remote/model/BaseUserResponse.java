package com.visual.purina_impulso.data.source.remote.model;

import com.google.gson.annotations.SerializedName;



public class BaseUserResponse {

    @SerializedName("logged_in")
    protected String login;

    @SerializedName("msj")
    protected String message;

    public String getLogin() {
        return login;
    }

    public String getMessage() {
        return message;
    }

}
