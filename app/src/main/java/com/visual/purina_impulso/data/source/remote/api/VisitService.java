package com.visual.purina_impulso.data.source.remote.api;

import com.visual.purina_impulso.data.source.remote.model.AssignVisitResponse;
import com.visual.purina_impulso.data.source.remote.model.IncidenceVisitResponse;
import com.visual.purina_impulso.data.source.remote.model.StartVisitResponse;
import com.visual.purina_impulso.data.source.remote.model.VisitResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;


public class VisitService {
    public interface Api{
        @FormUrlEncoded
        @POST("registrar_visita")
        Call<AssignVisitResponse> assignVisit(
                @Field("datosVisita") String clients
        );

        @FormUrlEncoded
        @POST("visita")
        Call<VisitResponse> registerVisit(
                @Field("datosVisita") String visit
        );

        @FormUrlEncoded
        @POST("visita_ingreso")
        Call<StartVisitResponse> startVisit(
                @Field("datosVisita") String visit
        );

        @FormUrlEncoded
        @POST("visita_ingreso_incidencia")
        Call<IncidenceVisitResponse> incidenceVisit(
                @Field("datosVisita") String incidenceVisit
        );
    }
}
