package com.visual.purina_impulso.data.source.remote.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;


public class VisitRequest {

    @SerializedName("sistema")
    protected String systemId;

    @SerializedName("visita")
    protected ArrayList<Visit> visit;

    @SerializedName("encuesta")
    protected ArrayList<Quiz> quiz;

    @SerializedName("detalleEncuesta")
    protected ArrayList<QuizDetail> quizDetail;

    @SerializedName("fotos")
    protected ArrayList<Photos> photos;

    @SerializedName("accion")
    protected ArrayList<Action> actions;

    @SerializedName("precioCompetencia")
    protected ArrayList<CompetitionPrice> competitionPrices;

    @SerializedName("detallePrecioCompetencia")
    protected ArrayList<DetailCompetitionPrice> detailCompetitionPrices;

    @SerializedName("precioQuiebre")
    protected ArrayList<BreakPrice> breakPrices;

    @SerializedName("detallePrecioQuiebre")
    protected ArrayList<DetailBreakPrice> detailBreakPrices;

    @SerializedName("promociones")
    protected ArrayList<Photos> y;

    @SerializedName("detallePromociones")
    protected ArrayList<Photos> p;

    @SerializedName("cshooper")
    protected ArrayList<Shooper> shoopers;

    @SerializedName("emerchandising")
    protected ArrayList<Merchandising> merchandising;

    @SerializedName("emerchandisingDetalle")
    protected ArrayList<DetailMerchandising> detailMerchandising;

    @SerializedName("emerchandisingPersonal")
    protected ArrayList<MerchandisingPersonal> merchandisingPersonal;

    @SerializedName("emerchandisingFinal")
    protected ArrayList<MerchandisingFinal> merchandisingFinal;

    @SerializedName("emerchandisingFinalDetalle")
    protected ArrayList<FinalDetailMerchandising> finalDetailMerchandising;



    public static class Visit{
        @SerializedName("idEmpleado")
        int employeeId;

        @SerializedName("fecha")
        String date;

        @SerializedName("idCliente")
        int clientId;

        @SerializedName("horaInicio")
        String startHour;

        @SerializedName("horaFin")
        String exitHour;

        @SerializedName("longInicio")
        String startLongitude;

        @SerializedName("latiInicio")
        String startLatitude;

        @SerializedName("longFin")
        String exitLongitude;

        @SerializedName("latiFin")
        String exitLatitude;

        @SerializedName("flagEstado")
        String statusFlag;

        public Visit(int employeeId, String date, int clientId, String startHour, String exitHour, String startLongitude, String startLatitude, String exitLongitude, String exitLatitude, String statusFlag) {
            this.employeeId = employeeId;
            this.date = date;
            this.clientId = clientId;
            this.startHour = startHour;
            this.exitHour = exitHour;
            this.startLongitude = startLongitude;
            this.startLatitude = startLatitude;
            this.exitLongitude = exitLongitude;
            this.exitLatitude = exitLatitude;
            this.statusFlag = statusFlag;
        }
    }

    public static class Quiz{
        @SerializedName("idEmpleado")
        int employeeId;

        @SerializedName("fecha")
        String date;

        @SerializedName("idCliente")
        int clientId;

        @SerializedName("idEncuesta")
        int quizId;

        @SerializedName("foto")
        String photo;

        @SerializedName("hora")
        String hour;

        public Quiz(int employeeId, String date, int clientId, int quizId, String photo, String hour) {
            this.employeeId = employeeId;
            this.date = date;
            this.clientId = clientId;
            this.quizId = quizId;
            this.photo = photo;
            this.hour = hour;
        }
    }

    public static class QuizDetail{

        @SerializedName("idEmpleado")
        int employeeId;

        @SerializedName("fecha")
        String date;

        @SerializedName("idCliente")
        int clientId;

        @SerializedName("idEncuesta")
        int quizId;

        @SerializedName("idPregunta")
        int questionId;

        @SerializedName("idAlternativa")
        String alternativeId;

        @SerializedName("respuesta")
        String answer;

        public QuizDetail(int employeeId, String date, int clientId, int quizId, int questionId, String alternativeId, String answer) {
            this.employeeId = employeeId;
            this.date = date;
            this.clientId = clientId;
            this.quizId = quizId;
            this.questionId = questionId;
            this.alternativeId = alternativeId;
            this.answer = answer;
        }
    }

    public static class Photos{

        @SerializedName("idEmpleado")
        int employeeId;

        @SerializedName("fecha")
        String date;

        @SerializedName("idCliente")
        int clientId;

        @SerializedName("idTipoFoto")
        int typePhotoId;

        @SerializedName("foto")
        String photo;

        @SerializedName("comentario")
        String comment;

        @SerializedName("hora")
        String hour;

        public Photos(int employeeId, String date, int clientId, int typePhotoId, String photo, String comment, String hour) {
            this.employeeId = employeeId;
            this.date = date;
            this.clientId = clientId;
            this.typePhotoId = typePhotoId;
            this.photo = photo;
            this.comment = comment;
            this.hour = hour;
        }
    }

    public static class Action{

        @SerializedName("idEmpleado")
        int employeeId;

        @SerializedName("fecha")
        String date;

        @SerializedName("idCliente")
        int clientId;

        @SerializedName("idCategoriaProducto")
        int productCategoryId;

        @SerializedName("producto")
        String product;

        @SerializedName("mecanica")
        String mechanics;

        @SerializedName("precio")
        int price;

        @SerializedName("fecVencimiento")
        String expirationDate;

        @SerializedName("foto")
        String photo;

        @SerializedName("hora")
        String hour;

        public Action(int employeeId, String date, int clientId, int productCategoryId, String product, String mechanics, int price, String expirationDate, String photo, String hour) {
            this.employeeId = employeeId;
            this.date = date;
            this.clientId = clientId;
            this.productCategoryId = productCategoryId;
            this.product = product;
            this.mechanics = mechanics;
            this.price = price;
            this.expirationDate = expirationDate;
            this.photo = photo;
            this.hour = hour;
        }
    }

    public static class Shooper{

        @SerializedName("idEmpleado")
        int employeeId;

        @SerializedName("fecha")
        String date;

        @SerializedName("idCliente")
        int clientId;

        @SerializedName("tipoShooper")
        String typeShooper;

        @SerializedName("foto")
        String photo;

        @SerializedName("comentario")
        String comment;

        @SerializedName("oportunidades")
        String opportunities;

        @SerializedName("hora")
        String hour;

        public Shooper(int employeeId, String date, int clientId, String typeShooper, String photo, String comment, String opportunities, String hour) {
            this.employeeId = employeeId;
            this.date = date;
            this.clientId = clientId;
            this.typeShooper = typeShooper;
            this.photo = photo;
            this.comment = comment;
            this.opportunities = opportunities;
            this.hour = hour;
        }
    }

    public static class Merchandising{

        @SerializedName("idEmpleado")
        int employeeId;

        @SerializedName("fecha")
        String date;

        @SerializedName("idCliente")
        int clientId;

        @SerializedName("foto")
        String photo;

        @SerializedName("hora")
        String hour;

        public Merchandising(int employeeId, String date, int clientId, String photo, String hour) {
            this.employeeId = employeeId;
            this.date = date;
            this.clientId = clientId;
            this.photo = photo;
            this.hour = hour;
        }
    }

    public static class DetailMerchandising{

        @SerializedName("idEmpleado")
        int employeeId;

        @SerializedName("fecha")
        String date;

        @SerializedName("idCliente")
        int clientId;

        @SerializedName("idMerchandising")
        int merchandisingId;

        @SerializedName("cantidad")
        int stock;


        public DetailMerchandising(int employeeId, String date, int clientId, int merchandisingId, int stock) {
            this.employeeId = employeeId;
            this.date = date;
            this.clientId = clientId;
            this.merchandisingId = merchandisingId;
            this.stock = stock;
        }
    }

    public static class CompetitionPrice{

        @SerializedName("idEmpleado")
        int employeeId;

        @SerializedName("fecha")
        String date;

        @SerializedName("idCliente")
        int clientId;

        @SerializedName("hora")
        String hour;

        public CompetitionPrice(int employeeId, String date, int clientId, String hour) {
            this.employeeId = employeeId;
            this.date = date;
            this.clientId = clientId;
            this.hour = hour;
        }
    }

    public static class DetailCompetitionPrice{

        @SerializedName("idEmpleado")
        int employeeId;

        @SerializedName("fecha")
        String date;

        @SerializedName("idCliente")
        int clientId;

        @SerializedName("idProducto")
        int productId;

        @SerializedName("distribucion")
        int distribution;

        @SerializedName("precio")
        String price;

        @SerializedName("precioPromo")
        String pricePromotion;

        public DetailCompetitionPrice(int employeeId, String date, int clientId, int productId, int distribution, String price, String pricePromotion) {
            this.employeeId = employeeId;
            this.date = date;
            this.clientId = clientId;
            this.productId = productId;
            this.distribution = distribution;
            this.price = price;
            this.pricePromotion = pricePromotion;
        }
    }

    public static class MerchandisingPersonal{

        @SerializedName("idEmpleado")
        int employeeId;

        @SerializedName("fecha")
        String date;

        @SerializedName("idCliente")
        int clientId;

        @SerializedName("idMerchandising")
        int merchandidingId;

        @SerializedName("cantidadMerchandising")
        int amountMerchandisng;

        @SerializedName("idSubcategoria")
        int subCategoryId;

        @SerializedName("idCategoriaProducto")
        int categoryProductId;

        @SerializedName("idBaseProducto")
        int baseProductId;

        @SerializedName("idProducto")
        int productId;

        @SerializedName("cantidadProducto")
        int amountProduct;

        @SerializedName("hora")
        String hour;

        @SerializedName("foto")
        String photo;

        public MerchandisingPersonal(int employeeId, String date, int clientId, int merchandidingId, int amountMerchandisng, int subCategoryId, int categoryProductId, int baseProductId, int productId, int amountProduct, String hour, String photo) {
            this.employeeId = employeeId;
            this.date = date;
            this.clientId = clientId;
            this.merchandidingId = merchandidingId;
            this.amountMerchandisng = amountMerchandisng;
            this.subCategoryId = subCategoryId;
            this.categoryProductId = categoryProductId;
            this.baseProductId = baseProductId;
            this.productId = productId;
            this.amountProduct = amountProduct;
            this.hour = hour;
            this.photo = photo;
        }
    }

    public static class BreakPrice{

        @SerializedName("idEmpleado")
        int employeeId;

        @SerializedName("fecha")
        String date;

        @SerializedName("idCliente")
        int clientId;

        @SerializedName("hora")
        String hour;

        public BreakPrice(int employeeId, String date, int clientId, String hour) {
            this.employeeId = employeeId;
            this.date = date;
            this.clientId = clientId;
            this.hour = hour;
        }
    }

    public static class DetailBreakPrice{

        @SerializedName("idEmpleado")
        int employeeId;

        @SerializedName("fecha")
        String date;

        @SerializedName("idCliente")
        int clientId;

        @SerializedName("idProducto")
        int productId;

        @SerializedName("precio")
        String price;

        @SerializedName("precioPromo")
        String pricePromotion;

        @SerializedName("quiebre")
        String priceBreak;

        @SerializedName("sugerido")
        String priceSuggested;

        @SerializedName("foto")
        String photo;

        public DetailBreakPrice(int employeeId, String date, int clientId, int productId, String price, String pricePromotion, String priceBreak, String priceSuggested, String photo) {
            this.employeeId = employeeId;
            this.date = date;
            this.clientId = clientId;
            this.productId = productId;
            this.price = price;
            this.pricePromotion = pricePromotion;
            this.priceBreak = priceBreak;
            this.priceSuggested = priceSuggested;
            this.photo = photo;
        }
    }

    public static class MerchandisingFinal{

        @SerializedName("idEmpleado")
        int employeeId;

        @SerializedName("fecha")
        String date;

        @SerializedName("idCliente")
        int clientId;

        @SerializedName("foto")
        String photo;

        @SerializedName("hora")
        String hour;

        @SerializedName("comentario")
        String comment;

        public MerchandisingFinal(int employeeId, String date, int clientId, String photo, String hour, String comment) {
            this.employeeId = employeeId;
            this.date = date;
            this.clientId = clientId;
            this.photo = photo;
            this.hour = hour;
            this.comment = comment;
        }
    }

    public static class FinalDetailMerchandising{

        @SerializedName("idEmpleado")
        int employeeId;

        @SerializedName("fecha")
        String date;

        @SerializedName("idCliente")
        int clientId;

        @SerializedName("idMerchandising")
        int merchandisingId;

        @SerializedName("cantidad")
        int stock;

        public FinalDetailMerchandising(int employeeId, String date, int clientId, int merchandisingId, int stock) {
            this.employeeId = employeeId;
            this.date = date;
            this.clientId = clientId;
            this.merchandisingId = merchandisingId;
            this.stock = stock;
        }
    }

    public VisitRequest(String systemId, ArrayList<Visit> visit, ArrayList<Quiz> quiz, ArrayList<QuizDetail> quizDetail, ArrayList<Photos> photos, ArrayList<Action> actions, ArrayList<CompetitionPrice> competitionPrices,
                        ArrayList<DetailCompetitionPrice> detailCompetitionPrices, ArrayList< BreakPrice> breakPrices, ArrayList<DetailBreakPrice> detailBreakPrices, ArrayList<Photos> y, ArrayList<Photos> p, ArrayList<Shooper> shoopers, ArrayList<Merchandising> merchandising,
                        ArrayList<DetailMerchandising> detailMerchandising , ArrayList<MerchandisingPersonal> merchandisingPersonal,
                        ArrayList<MerchandisingFinal> merchandisingFinal, ArrayList<FinalDetailMerchandising> finalDetailMerchandising) {
        this.systemId = systemId;
        this.visit = visit;
        this.quiz = quiz;
        this.quizDetail = quizDetail;
        this.photos = photos;
        this.actions = actions;
        this.competitionPrices = competitionPrices;
        this.detailCompetitionPrices = detailCompetitionPrices;
        this.breakPrices = breakPrices;
        this.detailBreakPrices = detailBreakPrices;
        this.y = y;
        this.p = p;
        this.shoopers = shoopers;
        this.merchandising = merchandising;
        this.detailMerchandising = detailMerchandising;
        this.merchandisingPersonal = merchandisingPersonal;
        this.merchandisingFinal = merchandisingFinal;
        this.finalDetailMerchandising = finalDetailMerchandising;
    }
}
