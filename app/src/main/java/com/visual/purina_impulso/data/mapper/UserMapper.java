package com.visual.purina_impulso.data.mapper;

import com.visual.purina_impulso.data.source.remote.model.UserResponse;
import com.visual.purina_impulso.domain.User;


public class UserMapper {
    // mapper de login
    public static User createUser(UserResponse response) {

        return new User(response.getAssistance(),Integer.parseInt(response.getEmployeeId()) ,
                Integer.parseInt(response.getSystemId()),Integer.parseInt(response.getTypeId()),response.getNames(),
                    response.getUserChannel(),response.getUserType());

        }

    }


