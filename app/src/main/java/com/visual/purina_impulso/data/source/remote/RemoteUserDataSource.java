package com.visual.purina_impulso.data.source.remote;

import com.visual.purina_impulso.App;
import com.visual.purina_impulso.Constants;
import com.visual.purina_impulso.R;
import com.visual.purina_impulso.data.mapper.UserMapper;
import com.visual.purina_impulso.data.source.UserDataSource;
import com.visual.purina_impulso.data.source.remote.api.FactoryService;
import com.visual.purina_impulso.data.source.remote.api.UserService;
import com.visual.purina_impulso.data.source.remote.model.UserResponse;
import com.visual.purina_impulso.data.source.remote.model.VersionResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class RemoteUserDataSource implements UserDataSource {

    private static RemoteUserDataSource INSTANCE = null;

    public synchronized static RemoteUserDataSource getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new RemoteUserDataSource();
        }
        return INSTANCE;
    }



    @Override
    public void login(String dni, String password, LoginCallback callback, ErrorCallback errorCallback) {
        FactoryService.retrofitServicePost(UserService.Api.class)
                .login(dni,password)
                .enqueue(new Callback<UserResponse>() {
                    @Override
                    public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                        if (response.body().getLogin().equals("TRUE")){
                            UserResponse userResponse = response.body();
                            if (response.body().getSystemId().equals(Constants.FLAG_CHANNEL_MODERN) || response.body().getSystemId().equals(Constants.FLAG_CHANNEL_GENERAL) ){
                                callback.onSuccess(UserMapper.createUser(userResponse));
                            }else{
                                errorCallback.onError(new Exception(App.context.getString(R.string.message_login_only_channel_modern)));
                            }

                        }else{
                            errorCallback.onError(new Exception(response.body().getMessage()));
                        }
                    }

                    @Override
                    public void onFailure(Call<UserResponse> call, Throwable t) {
                        errorCallback.onError(new Exception(App.context.getString(R.string.services_error_general)));
                    }
                });

    }

    @Override
    public void registerTokenPush(String userId, String tokenPush, RegisterTokenPushCallback callback, ErrorCallback errorCallback) {

    }

    @Override
    public void checkVersion(String version, CheckVersionCallback callback, ErrorCallback errorCallback) {
       FactoryService.retrofitServicePost(UserService.Api.class)
               .checkVersion(version)
               .enqueue(new Callback<VersionResponse>() {
                   @Override
                   public void onResponse(Call<VersionResponse> call, Response<VersionResponse> response) {
                       callback.onSuccessCheckVersion();
                   }

                   @Override
                   public void onFailure(Call<VersionResponse> call, Throwable t) {
                       errorCallback.onError(new Exception(App.context.getString(R.string.services_error_general)));
                   }
               });
    }
}
