package com.visual.purina_impulso.data.source.local.realm;

public class Lock {

    private boolean isLocked;
    public synchronized void lock() throws InterruptedException {
        isLocked = true;
        while (isLocked) {
            wait();
        }
    }
    public synchronized void unlock() {
        isLocked = false;
        notify();
    }
}
