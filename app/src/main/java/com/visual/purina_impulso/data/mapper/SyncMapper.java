package com.visual.purina_impulso.data.mapper;

import com.visual.purina_impulso.Constants;
import com.visual.purina_impulso.data.source.remote.model.SyncResponse;
import com.visual.purina_impulso.domain.Alternative;
import com.visual.purina_impulso.domain.BaseProduct;
import com.visual.purina_impulso.domain.BrandProduct;
import com.visual.purina_impulso.domain.Client;
import com.visual.purina_impulso.domain.Incidence;
import com.visual.purina_impulso.domain.ListAdvanceDay;
import com.visual.purina_impulso.domain.ListBusinessDay;
import com.visual.purina_impulso.domain.ListFee;
import com.visual.purina_impulso.domain.ListFeeImpulso;
import com.visual.purina_impulso.domain.ListSale;
import com.visual.purina_impulso.domain.ListVisit;
import com.visual.purina_impulso.domain.Merchandising;
import com.visual.purina_impulso.domain.Module;
import com.visual.purina_impulso.domain.Photo;
import com.visual.purina_impulso.domain.Price;
import com.visual.purina_impulso.domain.Product;
import com.visual.purina_impulso.domain.ProductCategory;
import com.visual.purina_impulso.domain.Question;
import com.visual.purina_impulso.domain.Quiz;
import com.visual.purina_impulso.domain.SubCategoryProduct;
import com.visual.purina_impulso.domain.Sync;

import java.util.UUID;

import io.realm.RealmList;

// En general la carpeta mapper va servir para poder mapiar todas objetos que trae el servicio , de los cuales no todos se utilizar


public class SyncMapper {
    // mapper de sincronizar
    public static Sync createSync(SyncResponse response) {

        // se mapea todos los objetos que trae el servicio de sincronizar
        RealmList<Client> clients = new RealmList<>();
        RealmList<Module> modules = new RealmList<>();
        RealmList<Photo> photos = new RealmList<>();
        RealmList<Incidence> incidences = new RealmList<>();
        RealmList<Quiz> quizzes = new RealmList<>();
        RealmList<Question> questions = new RealmList<>();
        RealmList<Alternative> alternatives = new RealmList<>();
        RealmList<Price> prices = new RealmList<>();
        RealmList<ProductCategory> productCategories = new RealmList<>();
        RealmList<Merchandising> merchandisings = new RealmList<>();

        RealmList<SubCategoryProduct> subCategoryProducts = new RealmList<>();
        RealmList<BrandProduct> brandProducts = new RealmList<>();
        RealmList<BaseProduct> baseProducts = new RealmList<>();
        RealmList<Product> products = new RealmList<>();

        RealmList<ListVisit> listVisits = new RealmList<>();
        RealmList<ListSale> listSales = new RealmList<>();
        RealmList<ListFee> listFees = new RealmList<>();
        RealmList<ListAdvanceDay> listAdvanceDays = new RealmList<>();
        RealmList<ListBusinessDay> listBusinessDays = new RealmList<>();
        RealmList<ListFeeImpulso> listFeeImpulsos = new RealmList<>();


        if (response!= null){
            if (response.getAssignedClients() != null && response.getAssignedClients().size()>0){
                for (int i =0 ; i< response.getAssignedClients().size();i++){
                    Client client = new Client(response.getAssignedClients().get(i).getClientId(),
                            response.getAssignedClients().get(i).getEmployeeId(),
                            response.getAssignedClients().get(i).getDni(),
                            response.getAssignedClients().get(i).getRuc(),
                            response.getAssignedClients().get(i).getCode(),
                            response.getAssignedClients().get(i).getAddress(),
                            response.getAssignedClients().get(i).getBannerId(),
                            response.getAssignedClients().get(i).getBanner(),
                            response.getAssignedClients().get(i).getClusterId(),
                            response.getAssignedClients().get(i).getCluster(),
                            Constants.FLAG_CLIENT_ASSIGNED
                    );
                    clients.add(client);

                }
            }

            if (response.getClients() != null && response.getClients().size()>0){
                for (int i =0 ; i< response.getClients().size();i++){
                    String visitId = UUID.randomUUID().toString();
                    Client client = new Client(response.getClients().get(i).getClientId(),
                            response.getClients().get(i).getEmployeeId(),
                            response.getClients().get(i).getDni(),
                            response.getClients().get(i).getRuc(),
                            response.getClients().get(i).getCode(),
                            response.getClients().get(i).getAddress(),
                            response.getClients().get(i).getBannerId(),
                            response.getClients().get(i).getBanner(),
                            response.getClients().get(i).getClusterId(),
                            response.getClients().get(i).getCluster(),
                            Constants.FLAG_CLIENT,
                            visitId,
                            Constants.FLAG_CLIENT_VISIT_NO_START,
                            "",
                            "",
                            Constants.FLAG_CLIENT_VISIT_STATUS_START,
                            Constants.FLAG_DATA_PENDING_NO
                    );
                    clients.add(client);
                }
            }

            if (response.getModules() != null && response.getModules().size()>0){
                for (int i =0 ; i< response.getModules().size();i++){
                    Module module = new Module(response.getModules().get(i).getModuleId(),
                            response.getModules().get(i).getModuleName()
                    );
                    modules.add(module);
                }
            }

            if (response.getTypePhotos() != null && response.getTypePhotos().size()>0){
                for (int i =0 ; i< response.getTypePhotos().size();i++){
                    Photo photo = new Photo(response.getTypePhotos().get(i).getTypePhotoId(),
                            response.getTypePhotos().get(i).getChannelId(),
                            response.getTypePhotos().get(i).getTypePhotoName()
                    );
                    photos.add(photo);
                }
            }

            if (response.getTypeIncidences() != null && response.getTypeIncidences().size()>0){
                for (int i =0 ; i< response.getTypeIncidences().size();i++){
                    Incidence incidence = new Incidence(response.getTypeIncidences().get(i).getTypeIncidenceId(),
                            response.getTypeIncidences().get(i).getChannelId(),
                            response.getTypeIncidences().get(i).getTypeIncidenceName()
                    );
                    incidences.add(incidence);
                }
            }

            if (response.getQuizzes() != null && response.getQuizzes().size()>0){
                for (int i =0 ; i< response.getQuizzes().size();i++){
                    Quiz quiz = new Quiz(response.getQuizzes().get(i).getQuizId(),
                            response.getQuizzes().get(i).getChannelId(),
                            response.getQuizzes().get(i).getQuizName()
                    );
                    quizzes.add(quiz);
                }
            }

            if (response.getQuestions() != null && response.getQuestions().size()>0){
                for (int i =0 ; i< response.getQuestions().size();i++){
                    Question question = new Question(response.getQuestions().get(i).getQuestionId(),

                            response.getQuestions().get(i).getChannelId(),
                            response.getQuestions().get(i).getQuizId(),
                            response.getQuestions().get(i).getTypeQuestion(),
                            response.getQuestions().get(i).getQuestionName()
                    );
                    questions.add(question);
                }
            }

            if (response.getAlternatives() != null && response.getAlternatives().size()>0){
                for (int i =0 ; i< response.getAlternatives().size();i++){
                    Alternative alternative = new Alternative(response.getAlternatives().get(i).getAlternativeId(),
                            response.getAlternatives().get(i).getChannelId(),
                            response.getAlternatives().get(i).getQuizId(),
                            response.getAlternatives().get(i).getQuestionId(),
                            response.getAlternatives().get(i).getAlternativeName()
                    );
                    alternatives.add(alternative);
                }
            }

            if (response.getBreakPrices() != null && response.getBreakPrices().size()>0){
                for (int i =0 ; i< response.getBreakPrices().size();i++){
                    Price price = new Price(response.getBreakPrices().get(i).getProductId(),
                            response.getBreakPrices().get(i).getProductoName(),
                            response.getBreakPrices().get(i).getType(),
                            response.getBreakPrices().get(i).getClientId(),
                            response.getBreakPrices().get(i).getBannerId(),
                            Constants.FLAG_PRICE_TYPE_BREAK
                    );
                    prices.add(price);
                }
            }
            if (response.getCompetitionPrices() != null && response.getCompetitionPrices().size()>0){

                for (int i =0 ; i< response.getCompetitionPrices().size();i++){
                    Price price = new Price(response.getCompetitionPrices().get(i).getProductId(),
                            response.getCompetitionPrices().get(i).getProductoName(),
                            Constants.FLAG_PRICE_TYPE_COMPETITION
                    );
                    prices.add(price);
                }
            }

            if (response.getProductCategories() != null && response.getProductCategories().size()>0){

                for (int i =0 ; i< response.getProductCategories().size();i++){
                    ProductCategory productCategory = new ProductCategory(response.getProductCategories().get(i).getProductCategoryId(),
                            response.getProductCategories().get(i).getChannelId(),
                            response.getProductCategories().get(i).getProductCategory()
                    );
                    productCategories.add(productCategory);
                }
            }

            if (response.getMerchandisingList() != null && response.getMerchandisingList().size()>0){

                for (int i =0 ; i< response.getMerchandisingList().size();i++){
                    Merchandising merchandising = new Merchandising(response.getMerchandisingList().get(i).getMerchandisingId(),
                            response.getMerchandisingList().get(i).getName(),
                            response.getMerchandisingList().get(i).getStatus()
                    );
                    merchandisings.add(merchandising);
                }
            }

            if (response.getProducts() != null && response.getProducts().size()>0){

                for (int i =0 ; i< response.getProducts().size();i++){

                    String subCategoryProductId = UUID.randomUUID().toString();
                    SubCategoryProduct subCategoryProduct  = new SubCategoryProduct(subCategoryProductId,
                            response.getProducts().get(i).getSubCategoryId(),
                            response.getProducts().get(i).getSubCategory()
                            );


                    String brandProductId = UUID.randomUUID().toString();
                    BrandProduct brandProduct  = new BrandProduct(brandProductId,
                            response.getProducts().get(i).getCategoryProductId(),
                            response.getProducts().get(i).getCategoryProduct(),
                            response.getProducts().get(i).getSubCategoryId()
                    );

                    String BaseProductId = UUID.randomUUID().toString();
                    BaseProduct baseProduct  = new BaseProduct(BaseProductId,
                            response.getProducts().get(i).getBaseProductId(),
                            response.getProducts().get(i).getBaseProduct(),
                            response.getProducts().get(i).getSubCategoryId(),
                            response.getProducts().get(i).getCategoryProductId()
                    );


                    String ProductId = UUID.randomUUID().toString();
                    Product product  = new Product(ProductId,
                            response.getProducts().get(i).getProductId(),
                            response.getProducts().get(i).getProduct(),
                            response.getProducts().get(i).getBaseProductId()
                    );

                    subCategoryProducts.add(subCategoryProduct);
                    brandProducts.add(brandProduct);
                    baseProducts.add(baseProduct);
                    products.add(product);

                }
            }

            if (response.getListVisit() != null && response.getListVisit().size()>0){
                for (int i =0 ; i< response.getListVisit().size();i++){
                    String listVisitId = UUID.randomUUID().toString();

                    ListVisit listVisit = new  ListVisit(
                            listVisitId,
                            response.getListVisit().get(i).getClientId(),
                            response.getListVisit().get(i).getViProg(),
                            response.getListVisit().get(i).getViExc(),
                            response.getListVisit().get(i).getViHab(),
                            response.getListVisit().get(i).getViEfec(),
                            response.getListVisit().get(i).getViIn()
                    );

                    listVisits.add(listVisit);
                }
            }

            if (response.getListSale() != null && response.getListSale().size()>0){
                for (int i =0 ; i< response.getListSale().size();i++){
                    String listSaleId = UUID.randomUUID().toString();

                    ListSale listSale = new  ListSale(
                            listSaleId,
                            response.getListSale().get(i).getClientId(),
                            response.getListSale().get(i).getSale()
                    );
                    listSales.add(listSale);
                }
            }

            if (response.getListFee() != null && response.getListFee().size()>0){
                for (int i =0 ; i< response.getListFee().size();i++){
                    String listFeeId = UUID.randomUUID().toString();

                    ListFee listFee = new  ListFee(
                            listFeeId,
                            response.getListFee().get(i).getClientId(),
                            response.getListFee().get(i).getFee()
                    );
                    listFees.add(listFee);
                }
            }

            if (response.getListDA() != null && response.getListDA().size()>0){
                for (int i =0 ; i< response.getListDA().size();i++){
                    String listAdvanceId = UUID.randomUUID().toString();

                    ListAdvanceDay listAdvanceDay = new  ListAdvanceDay(
                            listAdvanceId,
                            response.getListDA().get(i).getTimeId()
                    );
                    listAdvanceDays.add(listAdvanceDay);
                }
            }

            if (response.getListDH() != null && response.getListDH().size()>0){
                for (int i =0 ; i< response.getListDH().size();i++){
                    String listBusinessId = UUID.randomUUID().toString();

                    ListBusinessDay listBusinessDay = new  ListBusinessDay(
                            listBusinessId,
                            response.getListDH().get(i).getTimeId()
                    );
                    listBusinessDays.add(listBusinessDay);
                }
            }

            if (response.getListFeeImpulso() != null && response.getListFeeImpulso().size()>0){
                for (int i =0 ; i< response.getListFeeImpulso().size();i++){
                    String listFeeImpulsoId = UUID.randomUUID().toString();

                    ListFeeImpulso listFeeImpulso = new  ListFeeImpulso(
                            listFeeImpulsoId,
                            response.getListFeeImpulso().get(i).getClientId(),
                            response.getListFeeImpulso().get(i).getFee(),
                            response.getListFeeImpulso().get(i).getRange(),
                            response.getListFeeImpulso().get(i).getSaleDay()
                    );
                    listFeeImpulsos.add(listFeeImpulso);
                }
            }

        }


        Sync sync = new Sync(clients,modules,photos,incidences,quizzes,questions,
                alternatives,prices,productCategories,
                merchandisings,subCategoryProducts,brandProducts,baseProducts,products,
                listAdvanceDays,listBusinessDays,listFees,listSales,listVisits,listFeeImpulsos);


        return sync;

    }

}
