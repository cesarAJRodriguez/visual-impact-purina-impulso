package com.visual.purina_impulso.data.source.local.realm;

import android.os.Handler;
import android.os.HandlerThread;

public class RealmAsyncThread extends HandlerThread{

    private Handler mHandler;

    public RealmAsyncThread(String name) {
        super(name);
        start();
        mHandler = new Handler(getLooper());
    }
    public Handler getHandler() {
        return mHandler;
    }
}
