package com.visual.purina_impulso.data.source.remote;

import com.google.gson.Gson;
import com.visual.purina_impulso.App;
import com.visual.purina_impulso.R;
import com.visual.purina_impulso.data.source.AssistanceDataSource;
import com.visual.purina_impulso.data.source.remote.api.AssistanceService;
import com.visual.purina_impulso.data.source.remote.api.FactoryService;
import com.visual.purina_impulso.data.source.remote.model.AssistanceRequest;
import com.visual.purina_impulso.data.source.remote.model.AssistanceResponse;
import com.visual.purina_impulso.domain.Assistance;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class RemoteAssistanceDataSource implements AssistanceDataSource {

    private static RemoteAssistanceDataSource INSTANCE = null;

    public synchronized static RemoteAssistanceDataSource getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new RemoteAssistanceDataSource();
        }
        return INSTANCE;
    }


    @Override
    public void assistance(Assistance assistance,ArrayList<Assistance> assistancesPending,String pending, AssistanceCallback callback, ErrorCallback errorCallback) {
        AssistanceRequest.AssistanceDetail assistanceDetail = new AssistanceRequest.AssistanceDetail(   assistance.getDate(),
                                                                                                        assistance.getStartPhoto(),
                                                                                                        assistance.getExitPhoto(),
                                                                                                        assistance.getEmployeeId(),
                                                                                                        assistance.getStartHour(),
                                                                                                        assistance.getExitHour(),
                                                                                                        assistance.getStartLatitude(),
                                                                                                        assistance.getStartLongitude(),
                                                                                                        assistance.getExitLatitude(),
                                                                                                        assistance.getExitLatitude(),
                                                                                                        assistance.getStartComment(),
                                                                                                        assistance.getExitComment()
                                                                                                        );
        ArrayList<AssistanceRequest.AssistanceDetail> assistanceDetails = new ArrayList<>();
        assistanceDetails.add(assistanceDetail);
        AssistanceRequest  assistanceRequest = new AssistanceRequest(assistanceDetails);
        Gson gson = new Gson();
        String assistanceString = gson.toJson(assistanceRequest);

        FactoryService.retrofitServicePost(AssistanceService.Api.class)
                .assistance(assistanceString)
                .enqueue(new Callback<AssistanceResponse>() {
                    @Override
                    public void onResponse(Call<AssistanceResponse> call, Response<AssistanceResponse> response) {
                        if (response.body().getStatus().equals("TRUE")){
                            callback.onSuccessAssistence();
                        }else{
                            errorCallback.onError(new Exception(App.context.getString(R.string.message_assistance_register_error)));
                        }
                    }

                    @Override
                    public void onFailure(Call<AssistanceResponse> call, Throwable t) {
                        errorCallback.onError(new Exception(App.context.getString(R.string.services_error_general)));
                    }
                });
    }

    @Override
    public void checkAssistance(Assistance assistance, checkAssistanceCallback callback, ErrorCallback errorCallback) {

    }
}
