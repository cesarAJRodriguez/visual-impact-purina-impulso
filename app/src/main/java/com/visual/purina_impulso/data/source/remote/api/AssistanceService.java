package com.visual.purina_impulso.data.source.remote.api;

import com.visual.purina_impulso.data.source.remote.model.AssistanceResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public class AssistanceService {
    //@POST()   ---> nombre del metodo del servicio
    //Call<>   ---> nombre de la clase el cual se mapea los valores que trae el servicio
    //@FIELD()    ---> nombre del valor de post , ppueden ver varias
    public interface Api{
        @FormUrlEncoded
        @POST("asistencia")
        Call<AssistanceResponse> assistance(
                @Field("datosAsistencia") String assistance
        );
    }
}
