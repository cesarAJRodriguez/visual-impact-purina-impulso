package com.visual.purina_impulso.data.source.remote;

import com.visual.purina_impulso.App;
import com.visual.purina_impulso.R;
import com.visual.purina_impulso.data.mapper.SyncMapper;
import com.visual.purina_impulso.data.source.SyncDataSource;
import com.visual.purina_impulso.data.source.remote.api.FactoryService;
import com.visual.purina_impulso.data.source.remote.api.SyncService;
import com.visual.purina_impulso.data.source.remote.model.SyncResponse;
import com.visual.purina_impulso.domain.Sync;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;



public class RemoteSyncDataSource implements SyncDataSource {

    private static RemoteSyncDataSource INSTANCE = null;

    public synchronized static RemoteSyncDataSource getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new RemoteSyncDataSource();
        }
        return INSTANCE;
    }


    @Override
    public void sync(String syncTraditional, String syncModern, String syncSpecialized, SyncCallback callback, ErrorCallback errorCallback) {
        FactoryService.retrofitServicePost(SyncService.Api.class)
                .sync(syncTraditional,syncModern,syncSpecialized)
                .enqueue(new Callback<SyncResponse>() {
                    @Override
                    public void onResponse(Call<SyncResponse> call, Response<SyncResponse> response) {
                        SyncResponse syncResponse = response.body();
                        callback.onSuccess(SyncMapper.createSync(syncResponse));
                    }

                    @Override
                    public void onFailure(Call<SyncResponse> call, Throwable t) {
                        errorCallback.onError(new Exception(App.context.getString(R.string.services_error_general)));
                    }
                });
    }

    @Override
    public void saveSync(Sync sync,boolean typeSync, saveSyncCallback callback, ErrorCallback errorCallback) {

    }

    @Override
    public void clearSync(clearSyncCallback callback, ErrorCallback errorCallback) {

    }

    @Override
    public void clearPartialSync(clearPartialSyncCallback callback, ErrorCallback errorCallback) {

    }


}
