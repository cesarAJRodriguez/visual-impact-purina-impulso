package com.visual.purina_impulso.data.source.remote;

import com.visual.purina_impulso.data.source.ClientDataSource;
import com.visual.purina_impulso.domain.Client;


public class RemoteClientDataSource implements ClientDataSource {

    private static RemoteClientDataSource INSTANCE = null;

    public synchronized static RemoteClientDataSource getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new RemoteClientDataSource();
        }
        return INSTANCE;
    }


    @Override
    public void getClients(String employeeId, String type,String search, getClientsCallback callback, ErrorCallback errorCallback) {

    }

    @Override
    public void registerClient(Client client, registerClientCallback callback, ErrorCallback errorCallback) {

    }

    @Override
    public void getClient(Client client, getClientCallback callback, ErrorCallback errorCallback) {

    }
}
