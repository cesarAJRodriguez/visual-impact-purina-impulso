package com.visual.purina_impulso.data.source.remote.model;

import com.google.gson.annotations.SerializedName;


public class VersionResponse {

    @SerializedName("version_app")
    String version;

    public String getVersion() {
        return version;
    }
}
