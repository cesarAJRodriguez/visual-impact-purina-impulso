package com.visual.purina_impulso.data.source.local;

import com.visual.purina_impulso.Constants;
import com.visual.purina_impulso.data.source.AssistanceDataSource;
import com.visual.purina_impulso.data.source.local.realm.RealmService;
import com.visual.purina_impulso.domain.Assistance;

import java.util.ArrayList;

import io.realm.Realm;

public class LocalAssistanceDataSource implements AssistanceDataSource {

    private static LocalAssistanceDataSource INSTANCE = null;
    private final RealmService mRealService;

    private LocalAssistanceDataSource() {
        mRealService = new RealmService(Realm.getDefaultInstance());
    }

    public synchronized static LocalAssistanceDataSource getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new LocalAssistanceDataSource();
        }
        return INSTANCE;
    }

    //Para guardar la asistencia en la bd local
    @Override
    public void assistance(Assistance assistance,ArrayList<Assistance> assistancesPending,String pending, AssistanceCallback callback, ErrorCallback errorCallback) {

        ArrayList<Assistance> assistances = new ArrayList<>();
        if (pending.equals(Constants.FLAG_PENDING_ASSISTANCE_YES)){
            assistances = assistancesPending ;
        }else{
            assistances.add(assistance);
        }

        mRealService.onWriteObjects(assistances, new RealmService.DataBaseCallback<Assistance>() {
            @Override
            public void onTransactionSuccess(ArrayList<Assistance> object) {
                callback.onSuccessAssistence();
            }
            @Override
            public void onTransactionError(Exception error) {
                errorCallback.onError(error);
            }
        });
    }

    //obtenemos las asistencias de la bd local
    @Override
    public void checkAssistance(Assistance assistance, checkAssistanceCallback callback, ErrorCallback errorCallback) {

        // key  :  campos para  el where
        // values  :  valores para  el where

        ArrayList<String> keys = new ArrayList<>();
        keys.add("date");
        keys.add("employeeId");

        ArrayList<String> values = new ArrayList<>();
        values.add(assistance.getDate());
        values.add(assistance.getEmployeeId());

        mRealService.onReadObjects(Assistance.class,keys, values, new RealmService.DataBaseCallback<Assistance>() {
            @Override
            public void onTransactionSuccess(ArrayList<Assistance> object) {
                callback.onSuccessCheckAssistence(object);
            }

            @Override
            public void onTransactionError(Exception error) {
                errorCallback.onError(error);
            }
        });
    }
}
