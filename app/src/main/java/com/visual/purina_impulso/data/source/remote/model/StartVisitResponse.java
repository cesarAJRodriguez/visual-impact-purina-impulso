package com.visual.purina_impulso.data.source.remote.model;

import com.google.gson.annotations.SerializedName;



public class StartVisitResponse {

    @SerializedName("estadoEnvio")
    protected String sendStatus;

    public String getSendStatus() {
        return sendStatus;
    }
}
