package com.visual.purina_impulso.data.source;

import com.visual.purina_impulso.base.BaseDataSource;
import com.visual.purina_impulso.domain.Assistance;

import java.util.ArrayList;


public interface AssistanceDataSource extends BaseDataSource {

    interface AssistanceCallback {
        void onSuccessAssistence();
    }

    interface checkAssistanceCallback {
        void onSuccessCheckAssistence(ArrayList<Assistance> assistances);
    }


    void assistance(Assistance assistance,ArrayList<Assistance> assistances,String pending, final AssistanceCallback callback, final ErrorCallback errorCallback);
    void checkAssistance(Assistance assistance , final checkAssistanceCallback callback, final ErrorCallback errorCallback);
}
