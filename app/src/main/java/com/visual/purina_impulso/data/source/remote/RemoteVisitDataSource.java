package com.visual.purina_impulso.data.source.remote;

import com.google.gson.Gson;
import com.visual.purina_impulso.App;
import com.visual.purina_impulso.Constants;
import com.visual.purina_impulso.R;
import com.visual.purina_impulso.data.source.VisitDataSource;
import com.visual.purina_impulso.data.source.helper.PreferencesHelper;
import com.visual.purina_impulso.data.source.remote.api.FactoryService;
import com.visual.purina_impulso.data.source.remote.api.VisitService;
import com.visual.purina_impulso.data.source.remote.model.AssignVisitRequest;
import com.visual.purina_impulso.data.source.remote.model.AssignVisitResponse;
import com.visual.purina_impulso.data.source.remote.model.IncidenceVisitRequest;
import com.visual.purina_impulso.data.source.remote.model.IncidenceVisitResponse;
import com.visual.purina_impulso.data.source.remote.model.StartVisitRequest;
import com.visual.purina_impulso.data.source.remote.model.StartVisitResponse;
import com.visual.purina_impulso.data.source.remote.model.VisitRequest;
import com.visual.purina_impulso.data.source.remote.model.VisitResponse;
import com.visual.purina_impulso.domain.Client;
import com.visual.purina_impulso.domain.IncidenceVisit;
import com.visual.purina_impulso.domain.StartVisit;
import com.visual.purina_impulso.domain.Visit;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class RemoteVisitDataSource implements VisitDataSource {

    private static RemoteVisitDataSource INSTANCE = null;

    public synchronized static RemoteVisitDataSource getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new RemoteVisitDataSource();
        }
        return INSTANCE;
    }

    @Override
    public void assignVisit(String currentDate, String[] clients, assignVisitCallback callback, ErrorCallback errorCallback) {

        String employeeId = String.valueOf(PreferencesHelper.getUser().getEmployeeId());
        String systemId = "2";

        AssignVisitRequest assignVisitRequest = new AssignVisitRequest(employeeId,systemId,currentDate,clients);
        Gson gson = new Gson();
        String assignVisitString = gson.toJson(assignVisitRequest);

        FactoryService.retrofitServicePost(VisitService.Api.class)
                .assignVisit(assignVisitString)
                .enqueue(new Callback<AssignVisitResponse>() {
                    @Override
                    public void onResponse(Call<AssignVisitResponse> call, Response<AssignVisitResponse> response) {
                        if (response.body().getStatus().equals("TRUE")){
                            callback.onSuccessAssignVisit();
                        }else{
                            errorCallback.onError(new Exception(App.context.getString(R.string.message_visit_assign_error)));
                        }
                    }

                    @Override
                    public void onFailure(Call<AssignVisitResponse> call, Throwable t) {
                        errorCallback.onError(new Exception(App.context.getString(R.string.services_error_general)));
                    }
                });
    }

    @Override
    public void getQuizzesVisit(int channelId, getQuizzesVisitCallback callback, ErrorCallback errorCallback) {

    }

    @Override
    public void getQuizQuestions(int channelId, int quizId, getQuizQuestionsCallback callback, ErrorCallback errorCallback) {

    }

    @Override
    public void getQuizQuestionAlternatives(int channelId, int quizId, getQuizQuestionAlternativesCallback callback, ErrorCallback errorCallback) {

    }

    @Override
    public void getPrices(int clientId,String typePrice,String search, getPricesCallback callback, ErrorCallback errorCallback) {

    }

    @Override
    public void getTypePhotos(int channelId, getTypePhotosCallback callback, ErrorCallback errorCallback) {

    }

    @Override
    public void getCategoriesProduct(int channelId, getCategoriesProductCallback callback, ErrorCallback errorCallback) {

    }

    @Override
    public void getTypeIncidences(int channelId, getTypeIncidencesCallback callback, ErrorCallback errorCallback) {

    }

    @Override
    public void registerStartVisit(StartVisit startVisit, Visit visit ,registerStartVisitCallback callback, ErrorCallback errorCallback) {

        StartVisitRequest.Visit visitRequest = new StartVisitRequest.Visit(startVisit.getEmployeeId(),
                startVisit.getDate(),
                startVisit.getClientId(),
                startVisit.getStartHour(),
                startVisit.getExitHour(),
                startVisit.getStartLatitude(),
                startVisit.getStartLongitude(),
                startVisit.getExitLongitude(),
                startVisit.getExitLatitude(),
                startVisit.getStatusFlag()
        );
        ArrayList<StartVisitRequest.Visit> startVisitArray = new ArrayList<>();
        startVisitArray.add(visitRequest);
        StartVisitRequest  startVisitRequest = new StartVisitRequest("2",startVisitArray);
        Gson gson = new Gson();
        String visitString = gson.toJson(startVisitRequest);

        FactoryService.retrofitServicePost(VisitService.Api.class)
                .startVisit(visitString)
                .enqueue(new Callback<StartVisitResponse>() {
                    @Override
                    public void onResponse(Call<StartVisitResponse> call, Response<StartVisitResponse> response) {
                        if (response.body().getSendStatus().equals("TRUE")){
                            callback.onSuccessRegisterStartVisit(visit);
                        }else{
                            errorCallback.onError(new Exception(App.context.getString(R.string.message_visit_assign_error)));
                        }
                    }

                    @Override
                    public void onFailure(Call<StartVisitResponse> call, Throwable t) {
                        errorCallback.onError(new Exception(App.context.getString(R.string.services_error_general)));
                    }
                });
    }

    @Override
    public void registerIncidenceVisit(StartVisit startVisit, IncidenceVisit incidenceVisit, registerIncidenceVisitCallback callback, ErrorCallback errorCallback) {

        IncidenceVisitRequest.Visit visit = new IncidenceVisitRequest.Visit(
                startVisit.getEmployeeId(),
                startVisit.getDate(),
                startVisit.getClientId(),
                startVisit.getStartHour(),
                startVisit.getExitHour(),
                startVisit.getStartLatitude(),
                startVisit.getStartLongitude(),
                startVisit.getExitLongitude(),
                startVisit.getExitLatitude(),
                startVisit.getStatusFlag()
        );
        ArrayList<IncidenceVisitRequest.Visit> visitIn = new ArrayList<>();
        visitIn.add(visit);

        IncidenceVisitRequest.Incidence incidence = new IncidenceVisitRequest.Incidence(
                incidenceVisit.getEmployeeId(),
                incidenceVisit.getDate(),
                incidenceVisit.getClientId(),
                incidenceVisit.getTypeIncidenceId(),
                incidenceVisit.getPhoto(),
                incidenceVisit.getComment(),
                incidenceVisit.getHour()
        );

        ArrayList<IncidenceVisitRequest.Incidence> incidences = new ArrayList<>();
        incidences.add(incidence);

        IncidenceVisitRequest  incidenceVisitRequest = new IncidenceVisitRequest("2",visitIn,incidences);
        Gson gson = new Gson();

        String visitString = gson.toJson(incidenceVisitRequest);

        FactoryService.retrofitServicePost(VisitService.Api.class)
                .incidenceVisit(visitString)
                .enqueue(new Callback<IncidenceVisitResponse>() {
                    @Override
                    public void onResponse(Call<IncidenceVisitResponse> call, Response<IncidenceVisitResponse> response) {
                        if (response.body().getSendStatus().equals("TRUE")){
                            callback.onSuccessRegisterIncidenceVisit();
                        }else{
                            errorCallback.onError(new Exception(App.context.getString(R.string.message_visit_assign_error)));
                        }
                    }

                    @Override
                    public void onFailure(Call<IncidenceVisitResponse> call, Throwable t) {
                        errorCallback.onError(new Exception(App.context.getString(R.string.services_error_general)));
                    }

                });
    }

    @Override
    public void registerVisit(Visit visit,String moduleVisit, registerVisitCallback callback, ErrorCallback errorCallback) {

        VisitRequest.Visit visitRequestVisit = new VisitRequest.Visit(visit.getStartVisit().get(0).getEmployeeId(),
                visit.getStartVisit().get(0).getDate(),
                visit.getStartVisit().get(0).getClientId(),
                visit.getStartVisit().get(0).getStartHour(),
                visit.getStartVisit().get(0).getExitHour(),
                visit.getStartVisit().get(0).getStartLongitude(),
                visit.getStartVisit().get(0).getStartLatitude(),
                visit.getStartVisit().get(0).getExitLongitude(),
                visit.getStartVisit().get(0).getExitLatitude(),
                visit.getStartVisit().get(0).getStatusFlag()
        );

        ArrayList<VisitRequest.Visit> visitArrayList = new ArrayList<>();
        visitArrayList.add(visitRequestVisit);
        ArrayList<VisitRequest.Photos> photosArrayList = new ArrayList<>();
        ArrayList<VisitRequest.Action> actionArrayList = new ArrayList<>();

        if (moduleVisit.equals(Constants.FLAG_VISIT_PHOTO)){
            int indexLast = visit.getPhotoVisit().size()-1;
            VisitRequest.Photos visitPhotoVisit = new VisitRequest.Photos(visit.getPhotoVisit().get(indexLast).getEmployeeId(),
                    visit.getPhotoVisit().get(indexLast).getDate(),
                    visit.getPhotoVisit().get(indexLast).getClientId(),
                    visit.getPhotoVisit().get(indexLast).getTypePhotoId(),
                    visit.getPhotoVisit().get(indexLast).getPhoto(),
                    visit.getPhotoVisit().get(indexLast).getComment(),
                    visit.getPhotoVisit().get(indexLast).getHour()
            );

            photosArrayList.add(visitPhotoVisit);
        }

        if (moduleVisit.equals(Constants.FLAG_VISIT_ACTION)){
            int indexLast = visit.getActionVisit().size()-1;
            VisitRequest.Action action = new VisitRequest.Action(visit.getActionVisit().get(indexLast).getEmployeeId(),
                    visit.getActionVisit().get(indexLast).getDate(),
                    visit.getActionVisit().get(indexLast).getClientId(),
                    visit.getActionVisit().get(indexLast).getProductCategoryId(),
                    visit.getActionVisit().get(indexLast).getProduct(),
                    visit.getActionVisit().get(indexLast).getMechanics(),
                    visit.getActionVisit().get(indexLast).getPrice(),
                    visit.getActionVisit().get(indexLast).getExpirationDate(),
                    visit.getActionVisit().get(indexLast).getPhoto(),
                    visit.getActionVisit().get(indexLast).getHour()
            );
            actionArrayList.add(action);
        }

        ArrayList<VisitRequest.Quiz> quizArrayList = new ArrayList<>();
        ArrayList<VisitRequest.QuizDetail> quizDetailArrayListi = new ArrayList<>();
        ArrayList<VisitRequest.Shooper> shooperArrayList = new ArrayList<>();
        ArrayList<VisitRequest.CompetitionPrice> competitionPrices = new ArrayList<>();
        ArrayList<VisitRequest.DetailCompetitionPrice> detailCompetitionPrices = new ArrayList<>();
        ArrayList<VisitRequest.Merchandising> merchandisings = new ArrayList<>();
        ArrayList<VisitRequest.DetailMerchandising> detailMerchandisings = new ArrayList<>();
        ArrayList<VisitRequest.MerchandisingPersonal> merchandisingPersonals = new ArrayList<>();
        ArrayList<VisitRequest.BreakPrice> breakPrices = new ArrayList<>();
        ArrayList<VisitRequest.DetailBreakPrice> detailBreakPrices = new ArrayList<>();
        ArrayList<VisitRequest.MerchandisingFinal> merchandisingFinals = new ArrayList<>();
        ArrayList<VisitRequest.FinalDetailMerchandising> finalDetailMerchandisings = new ArrayList<>();

        if (moduleVisit.equals(Constants.FLAG_VISIT_QUIZ)){
            int indexLast = visit.getQuizVisit().size()-1;
            String quizVisitId = visit.getQuizVisit().get(indexLast).getId();
            VisitRequest.Quiz quiz = new VisitRequest.Quiz(visit.getQuizVisit().get(indexLast).getEmployeeId(),
                    visit.getQuizVisit().get(indexLast).getDate(),
                    visit.getQuizVisit().get(indexLast).getClientId(),
                    visit.getQuizVisit().get(indexLast).getQuizId(),
                    visit.getQuizVisit().get(indexLast).getPhoto(),
                    visit.getQuizVisit().get(indexLast).getHour()
            );
            quizArrayList.add(quiz);

            if (visit.getDetailQuizVisit()!= null && visit.getDetailQuizVisit().size()>0){
                for (int i=0;i<visit.getDetailQuizVisit().size();i++){
                    if (visit.getDetailQuizVisit().get(i).getQuizVisitId().equals(quizVisitId)){
                        int alter  =   visit.getDetailQuizVisit().get(i).getAlternativeId();
                        String alterId = "";

                        if (alter != -1){
                            alterId = String.valueOf(alter);
                        }
                        VisitRequest.QuizDetail quizDetail = new VisitRequest.QuizDetail(visit.getDetailQuizVisit().get(i).getEmployeeId(),
                                visit.getDetailQuizVisit().get(i).getDate(),
                                visit.getDetailQuizVisit().get(i).getClientId(),
                                visit.getDetailQuizVisit().get(i).getQuizId(),
                                visit.getDetailQuizVisit().get(i).getQuestionId(),
                                alterId,
                                visit.getDetailQuizVisit().get(i).getAnswer()
                        );
                        quizDetailArrayListi.add(quizDetail);
                    }
                }
            }
        }


        if (moduleVisit.equals(Constants.FLAG_VISIT_PRICE_COMPETITION)){
            int indexLast = visit.getPriceCompetitionVisit().size()-1;
            String priceCompetitionVisitId = visit.getPriceCompetitionVisit().get(indexLast).getId();
            VisitRequest.CompetitionPrice competitionPrice = new VisitRequest.CompetitionPrice(
                    visit.getPriceCompetitionVisit().get(indexLast).getEmployeeId(),
                    visit.getPriceCompetitionVisit().get(indexLast).getDate(),
                    visit.getPriceCompetitionVisit().get(indexLast).getClientId(),
                    visit.getPriceCompetitionVisit().get(indexLast).getHour()
            );

            competitionPrices.add(competitionPrice);

            if (visit.getDetailPriceCompetitionVisit()!= null && visit.getDetailPriceCompetitionVisit().size()>0){
                for (int i=0;i<visit.getDetailPriceCompetitionVisit().size();i++){
                    if (visit.getDetailPriceCompetitionVisit().get(i).getPriceCompetitionVisitId().equals(priceCompetitionVisitId)){
                        VisitRequest.DetailCompetitionPrice detailCompetitionPrice = new VisitRequest.DetailCompetitionPrice(
                                visit.getDetailPriceCompetitionVisit().get(i).getEmployeeId(),
                                visit.getDetailPriceCompetitionVisit().get(i).getDate(),
                                visit.getDetailPriceCompetitionVisit().get(i).getClientId(),
                                visit.getDetailPriceCompetitionVisit().get(i).getProductId(),
                                visit.getDetailPriceCompetitionVisit().get(i).getDistribution(),
                                visit.getDetailPriceCompetitionVisit().get(i).getPrice(),
                                visit.getDetailPriceCompetitionVisit().get(i).getPricePromotion()
                        );
                        detailCompetitionPrices.add(detailCompetitionPrice);
                    }
                }
            }
        }

        if (moduleVisit.equals(Constants.FLAG_VISIT_PRICE_BREAK)) {
            int indexLast = visit.getPriceBreakVisit().size() - 1;
            String priceBreakId = visit.getPriceBreakVisit().get(indexLast).getId();
            VisitRequest.BreakPrice breakPrice = new VisitRequest.BreakPrice(
                    visit.getPriceBreakVisit().get(indexLast).getEmployeeId(),
                    visit.getPriceBreakVisit().get(indexLast).getDate(),
                    visit.getPriceBreakVisit().get(indexLast).getClientId(),
                    visit.getPriceBreakVisit().get(indexLast).getHour()
            );

            breakPrices.add(breakPrice);

            if (visit.getDetailPriceBreakVisit()!= null && visit.getDetailPriceBreakVisit().size()>0){
                for (int i=0;i<visit.getDetailPriceBreakVisit().size();i++){
                    if (visit.getDetailPriceBreakVisit().get(i).getPriceBreakId().equals(priceBreakId)){
                        VisitRequest.DetailBreakPrice detailBreakPrice = new VisitRequest.DetailBreakPrice(
                                visit.getDetailPriceBreakVisit().get(i).getEmployeeId(),
                                visit.getDetailPriceBreakVisit().get(i).getDate(),
                                visit.getDetailPriceBreakVisit().get(i).getClientId(),
                                visit.getDetailPriceBreakVisit().get(i).getProductId(),
                                visit.getDetailPriceBreakVisit().get(i).getPrice(),
                                visit.getDetailPriceBreakVisit().get(i).getPricePromotion(),
                                visit.getDetailPriceBreakVisit().get(i).getPriceBreak(),
                                visit.getDetailPriceBreakVisit().get(i).getPriceSuggested(),
                                visit.getDetailPriceBreakVisit().get(i).getPhoto()
                        );
                        detailBreakPrices.add(detailBreakPrice);
                    }

                }
            }

        }

        if (moduleVisit.equals(Constants.FLAG_VISIT_SHOOPER)){
            int index = visit.getCommentShooperVisit().size()-1;
            VisitRequest.Shooper shooper = new VisitRequest.Shooper(visit.getCommentShooperVisit().get(index).getEmployeeId(),
                    visit.getCommentShooperVisit().get(index).getDate(),
                    visit.getCommentShooperVisit().get(index).getClientId(),
                    visit.getCommentShooperVisit().get(index).getTypeShooper(),
                    visit.getCommentShooperVisit().get(index).getPhoto(),
                    visit.getCommentShooperVisit().get(index).getComment(),
                    visit.getCommentShooperVisit().get(index).getOpportunities(),
                    visit.getCommentShooperVisit().get(index).getHour()
            );
            shooperArrayList.add(shooper);
        }

        if (moduleVisit.equals(Constants.FLAG_VISIT_STOCK)){
            int index = visit.getStockSampligVisit().size()-1;
            String stockId = visit.getStockSampligVisit().get(index).getId();
            VisitRequest.Merchandising merchandising = new VisitRequest.Merchandising(visit.getStockSampligVisit().get(index).getEmployeeId(),
                    visit.getStockSampligVisit().get(index).getDate(),
                    visit.getStockSampligVisit().get(index).getClientId(),
                    visit.getStockSampligVisit().get(index).getPhoto(),
                    visit.getStockSampligVisit().get(index).getHour()
            );
            merchandisings.add(merchandising);

            if (visit.getDetailStockSampligVisit()!= null && visit.getDetailStockSampligVisit().size()>0){
                for (int i=0;i<visit.getDetailStockSampligVisit().size();i++){
                    if (visit.getDetailStockSampligVisit().get(i).getStockSampligVisitId().equals(stockId)){
                        VisitRequest.DetailMerchandising detailMerchandising = new VisitRequest.DetailMerchandising(visit.getDetailStockSampligVisit().get(i).getEmployeeId(),
                                visit.getDetailStockSampligVisit().get(i).getDate(),
                                visit.getDetailStockSampligVisit().get(i).getClientId(),
                                visit.getDetailStockSampligVisit().get(i).getMerchandisingId(),
                                visit.getDetailStockSampligVisit().get(i).getStock()
                        );
                        detailMerchandisings.add(detailMerchandising);

                    }

                }
            }

        }

        if (moduleVisit.equals(Constants.FLAG_VISIT_STOCK_FINAL)){
            int index = visit.getStockSamplingFinalVisit().size()-1;
            String stockSamplingFinalId = visit.getStockSamplingFinalVisit().get(index).getId();
            VisitRequest.MerchandisingFinal merchandisingFinal = new VisitRequest.MerchandisingFinal(
                    visit.getStockSamplingFinalVisit().get(index).getEmployeeId(),
                    visit.getStockSamplingFinalVisit().get(index).getDate(),
                    visit.getStockSamplingFinalVisit().get(index).getClientId(),
                    visit.getStockSamplingFinalVisit().get(index).getPhoto(),
                    visit.getStockSamplingFinalVisit().get(index).getHour(),
                    visit.getStockSamplingFinalVisit().get(index).getComment()

            );
            merchandisingFinals.add(merchandisingFinal);

            if (visit.getDetailStockSamplingFinalVisit()!= null && visit.getDetailStockSamplingFinalVisit().size()>0){
                for (int i=0;i<visit.getDetailStockSamplingFinalVisit().size();i++){
                    if (visit.getDetailStockSamplingFinalVisit().get(i).getStockSamplingFinalVisitId().equals(stockSamplingFinalId)){
                        VisitRequest.FinalDetailMerchandising finalDetailMerchandising = new VisitRequest.FinalDetailMerchandising(
                                visit.getDetailStockSamplingFinalVisit().get(i).getEmployeeId(),
                                visit.getDetailStockSamplingFinalVisit().get(i).getDate(),
                                visit.getDetailStockSamplingFinalVisit().get(i).getClientId(),
                                visit.getDetailStockSamplingFinalVisit().get(i).getMerchandisingId(),
                                visit.getDetailStockSamplingFinalVisit().get(i).getStockFinal()
                        );
                        finalDetailMerchandisings.add(finalDetailMerchandising);
                    }
                }
            }

        }


        if (moduleVisit.equals(Constants.FLAG_VISIT_DELIVERY)){
            int index = visit.getDeliveryDayVisit().size()-1;
            VisitRequest.MerchandisingPersonal merchandisingPersonal = new VisitRequest.MerchandisingPersonal(
                    visit.getDeliveryDayVisit().get(index).getEmployeeId(),
                    visit.getDeliveryDayVisit().get(index).getDate(),
                    visit.getDeliveryDayVisit().get(index).getClientId(),
                    visit.getDeliveryDayVisit().get(index).getMerchandisingId(),
                    visit.getDeliveryDayVisit().get(index).getAmountMerchandising(),
                    visit.getDeliveryDayVisit().get(index).getSubCategoryId(),
                    visit.getDeliveryDayVisit().get(index).getCategoryProductId(),
                    visit.getDeliveryDayVisit().get(index).getBaseProductId(),
                    visit.getDeliveryDayVisit().get(index).getProductId(),
                    visit.getDeliveryDayVisit().get(index).getAmountProduct(),
                    visit.getDeliveryDayVisit().get(index).getHour(),
                    visit.getDeliveryDayVisit().get(index).getPhoto()
            );

            merchandisingPersonals.add(merchandisingPersonal);
        }

        ArrayList<VisitRequest.Photos> Listi = new ArrayList<>();

        if (moduleVisit.equals(Constants.FLAG_VISIT_T)){
            for (int i= 0 ; i<visit.getPhotoVisit().size() ; i++){
                if(visit.getPhotoVisit().get(i).getPending().equals(Constants.FLAG_DATA_PENDING_YES)){
                    VisitRequest.Photos visitPhotoVisit = new VisitRequest.Photos(visit.getPhotoVisit().get(i).getEmployeeId(),
                            visit.getPhotoVisit().get(i).getDate(),
                            visit.getPhotoVisit().get(i).getClientId(),
                            visit.getPhotoVisit().get(i).getTypePhotoId(),
                            visit.getPhotoVisit().get(i).getPhoto(),
                            visit.getPhotoVisit().get(i).getComment(),
                            visit.getPhotoVisit().get(i).getHour()
                    );
                    photosArrayList.add(visitPhotoVisit);
                }
            }

            for (int i= 0 ; i<visit.getActionVisit().size() ; i++){
                if(visit.getActionVisit().get(i).getPending().equals(Constants.FLAG_DATA_PENDING_YES)){
                    VisitRequest.Action action = new VisitRequest.Action(visit.getActionVisit().get(i).getEmployeeId(),
                            visit.getActionVisit().get(i).getDate(),
                            visit.getActionVisit().get(i).getClientId(),
                            visit.getActionVisit().get(i).getProductCategoryId(),
                            visit.getActionVisit().get(i).getProduct(),
                            visit.getActionVisit().get(i).getMechanics(),
                            visit.getActionVisit().get(i).getPrice(),
                            visit.getActionVisit().get(i).getExpirationDate(),
                            visit.getActionVisit().get(i).getPhoto(),
                            visit.getActionVisit().get(i).getHour()
                    );
                    actionArrayList.add(action);
                }
            }


            for (int i= 0 ; i<visit.getPriceCompetitionVisit().size() ; i++){
                if(visit.getPriceCompetitionVisit().get(i).getPending().equals(Constants.FLAG_DATA_PENDING_YES)){
                    VisitRequest.CompetitionPrice competitionPrice = new VisitRequest.CompetitionPrice(
                            visit.getPriceCompetitionVisit().get(i).getEmployeeId(),
                            visit.getPriceCompetitionVisit().get(i).getDate(),
                            visit.getPriceCompetitionVisit().get(i).getClientId(),
                            visit.getPriceCompetitionVisit().get(i).getHour()
                    );

                    competitionPrices.add(competitionPrice);
                }
            }


            if (visit.getDetailPriceCompetitionVisit()!= null && visit.getDetailPriceCompetitionVisit().size()>0){
                for (int i=0;i<visit.getDetailPriceCompetitionVisit().size();i++){
                    if(visit.getDetailPriceCompetitionVisit().get(i).getPending().equals(Constants.FLAG_DATA_PENDING_YES)){
                        VisitRequest.DetailCompetitionPrice detailCompetitionPrice = new VisitRequest.DetailCompetitionPrice(
                                visit.getDetailPriceCompetitionVisit().get(i).getEmployeeId(),
                                visit.getDetailPriceCompetitionVisit().get(i).getDate(),
                                visit.getDetailPriceCompetitionVisit().get(i).getClientId(),
                                visit.getDetailPriceCompetitionVisit().get(i).getProductId(),
                                visit.getDetailPriceCompetitionVisit().get(i).getDistribution(),
                                visit.getDetailPriceCompetitionVisit().get(i).getPrice(),
                                visit.getDetailPriceCompetitionVisit().get(i).getPricePromotion()
                        );
                        detailCompetitionPrices.add(detailCompetitionPrice);
                    }
                }
            }


            for (int i= 0 ; i<visit.getPriceBreakVisit().size() ; i++){
                if(visit.getPriceBreakVisit().get(i).getPending().equals(Constants.FLAG_DATA_PENDING_YES)){
                    VisitRequest.BreakPrice breakPrice = new VisitRequest.BreakPrice(
                            visit.getPriceBreakVisit().get(i).getEmployeeId(),
                            visit.getPriceBreakVisit().get(i).getDate(),
                            visit.getPriceBreakVisit().get(i).getClientId(),
                            visit.getPriceBreakVisit().get(i).getHour()
                    );
                    breakPrices.add(breakPrice);
                }
            }


            if (visit.getDetailPriceBreakVisit()!= null && visit.getDetailPriceBreakVisit().size()>0){
                for (int i=0;i<visit.getDetailPriceBreakVisit().size();i++){
                    if(visit.getPriceBreakVisit().get(i).getPending().equals(Constants.FLAG_DATA_PENDING_YES)){
                        VisitRequest.DetailBreakPrice detailBreakPrice = new VisitRequest.DetailBreakPrice(
                                visit.getDetailPriceBreakVisit().get(i).getEmployeeId(),
                                visit.getDetailPriceBreakVisit().get(i).getDate(),
                                visit.getDetailPriceBreakVisit().get(i).getClientId(),
                                visit.getDetailPriceBreakVisit().get(i).getProductId(),
                                visit.getDetailPriceBreakVisit().get(i).getPrice(),
                                visit.getDetailPriceBreakVisit().get(i).getPricePromotion(),
                                visit.getDetailPriceBreakVisit().get(i).getPriceBreak(),
                                visit.getDetailPriceBreakVisit().get(i).getPriceSuggested(),
                                visit.getDetailPriceBreakVisit().get(i).getPhoto()
                        );
                        detailBreakPrices.add(detailBreakPrice);
                    }

                }
            }


            for (int i= 0 ; i<visit.getCommentShooperVisit().size() ; i++){
                if(visit.getCommentShooperVisit().get(i).getPending().equals(Constants.FLAG_DATA_PENDING_YES)){
                    VisitRequest.Shooper shooper = new VisitRequest.Shooper(visit.getCommentShooperVisit().get(i).getEmployeeId(),
                            visit.getCommentShooperVisit().get(i).getDate(),
                            visit.getCommentShooperVisit().get(i).getClientId(),
                            visit.getCommentShooperVisit().get(i).getTypeShooper(),
                            visit.getCommentShooperVisit().get(i).getPhoto(),
                            visit.getCommentShooperVisit().get(i).getComment(),
                            visit.getCommentShooperVisit().get(i).getOpportunities(),
                            visit.getCommentShooperVisit().get(i).getHour()
                    );
                    shooperArrayList.add(shooper);
                }
            }


            for (int i= 0 ; i<visit.getQuizVisit().size() ; i++){
                if(visit.getQuizVisit().get(i).getPending().equals(Constants.FLAG_DATA_PENDING_YES)){
                    VisitRequest.Quiz quiz = new VisitRequest.Quiz(visit.getQuizVisit().get(i).getEmployeeId(),
                            visit.getQuizVisit().get(i).getDate(),
                            visit.getQuizVisit().get(i).getClientId(),
                            visit.getQuizVisit().get(i).getQuizId(),
                            visit.getQuizVisit().get(i).getPhoto(),
                            visit.getQuizVisit().get(i).getHour()
                    );
                    quizArrayList.add(quiz);
                }
            }


            if (visit.getDetailQuizVisit()!= null && visit.getDetailQuizVisit().size()>0){
                for (int i=0;i<visit.getDetailQuizVisit().size();i++){
                    if(visit.getDetailQuizVisit().get(i).getPending().equals(Constants.FLAG_DATA_PENDING_YES)){
                      int alter  =   visit.getDetailQuizVisit().get(i).getAlternativeId();
                      String alterId = "";
                      if (alter != -1){
                          alterId = String.valueOf(alter);
                      }
                    VisitRequest.QuizDetail quizDetail = new VisitRequest.QuizDetail(visit.getDetailQuizVisit().get(i).getEmployeeId(),
                            visit.getDetailQuizVisit().get(i).getDate(),
                            visit.getDetailQuizVisit().get(i).getClientId(),
                            visit.getDetailQuizVisit().get(i).getQuizId(),
                            visit.getDetailQuizVisit().get(i).getQuestionId(),
                            alterId,
                            visit.getDetailQuizVisit().get(i).getAnswer()
                    );
                    quizDetailArrayListi.add(quizDetail);
                    }
                }
            }

            if (visit.getStockSampligVisit()!= null && visit.getStockSampligVisit().size()>0){
                for (int i=0;i<visit.getStockSampligVisit().size();i++){
                    if(visit.getStockSampligVisit().get(i).getPending().equals(Constants.FLAG_DATA_PENDING_YES)){
                        VisitRequest.Merchandising merchandising = new VisitRequest.Merchandising(visit.getStockSampligVisit().get(i).getEmployeeId(),
                                visit.getStockSampligVisit().get(i).getDate(),
                                visit.getStockSampligVisit().get(i).getClientId(),
                                visit.getStockSampligVisit().get(i).getPhoto(),
                                visit.getStockSampligVisit().get(i).getHour()
                        );
                        merchandisings.add(merchandising);
                    }
                }
            }



            if (visit.getDetailStockSampligVisit()!= null && visit.getDetailStockSampligVisit().size()>0){
                for (int i=0;i<visit.getDetailStockSampligVisit().size();i++){
                    if(visit.getDetailStockSampligVisit().get(i).getPending().equals(Constants.FLAG_DATA_PENDING_YES)){
                    VisitRequest.DetailMerchandising detailMerchandising = new VisitRequest.DetailMerchandising(visit.getDetailStockSampligVisit().get(i).getEmployeeId(),
                            visit.getDetailStockSampligVisit().get(i).getDate(),
                            visit.getDetailStockSampligVisit().get(i).getClientId(),
                            visit.getDetailStockSampligVisit().get(i).getMerchandisingId(),
                            visit.getDetailStockSampligVisit().get(i).getStock()
                    );
                    detailMerchandisings.add(detailMerchandising);
                    }
                }
            }


            ////

            if (visit.getStockSamplingFinalVisit()!= null && visit.getStockSamplingFinalVisit().size()>0){
                for (int i=0;i<visit.getStockSamplingFinalVisit().size();i++){
                    if(visit.getStockSamplingFinalVisit().get(i).getPending().equals(Constants.FLAG_DATA_PENDING_YES)){
                        VisitRequest.MerchandisingFinal merchandisingFinal = new VisitRequest.MerchandisingFinal(
                                visit.getStockSamplingFinalVisit().get(i).getEmployeeId(),
                                visit.getStockSamplingFinalVisit().get(i).getDate(),
                                visit.getStockSamplingFinalVisit().get(i).getClientId(),
                                visit.getStockSamplingFinalVisit().get(i).getPhoto(),
                                visit.getStockSamplingFinalVisit().get(i).getHour(),
                                visit.getStockSamplingFinalVisit().get(i).getComment()
                        );
                        merchandisingFinals.add(merchandisingFinal);
                    }
                }
            }



            if (visit.getDetailStockSamplingFinalVisit()!= null && visit.getDetailStockSamplingFinalVisit().size()>0){
                for (int i=0;i<visit.getDetailStockSamplingFinalVisit().size();i++){
                    if(visit.getDetailStockSamplingFinalVisit().get(i).getPending().equals(Constants.FLAG_DATA_PENDING_YES)){
                        VisitRequest.FinalDetailMerchandising finalDetailMerchandising = new VisitRequest.FinalDetailMerchandising(
                                visit.getDetailStockSamplingFinalVisit().get(i).getEmployeeId(),
                                visit.getDetailStockSamplingFinalVisit().get(i).getDate(),
                                visit.getDetailStockSamplingFinalVisit().get(i).getClientId(),
                                visit.getDetailStockSamplingFinalVisit().get(i).getMerchandisingId(),
                                visit.getDetailStockSamplingFinalVisit().get(i).getStockFinal()
                        );
                        finalDetailMerchandisings.add(finalDetailMerchandising);
                    }
                }
            }

            ////


            if (visit.getDeliveryDayVisit()!= null && visit.getDeliveryDayVisit().size()>0){
                for (int i=0;i<visit.getDeliveryDayVisit().size();i++){
                    if(visit.getDeliveryDayVisit().get(i).getPending().equals(Constants.FLAG_DATA_PENDING_YES)){
                VisitRequest.MerchandisingPersonal merchandisingPersonal = new VisitRequest.MerchandisingPersonal(
                        visit.getDeliveryDayVisit().get(i).getEmployeeId(),
                        visit.getDeliveryDayVisit().get(i).getDate(),
                        visit.getDeliveryDayVisit().get(i).getClientId(),
                        visit.getDeliveryDayVisit().get(i).getMerchandisingId(),
                        visit.getDeliveryDayVisit().get(i).getAmountMerchandising(),
                        visit.getDeliveryDayVisit().get(i).getSubCategoryId(),
                        visit.getDeliveryDayVisit().get(i).getCategoryProductId(),
                        visit.getDeliveryDayVisit().get(i).getBaseProductId(),
                        visit.getDeliveryDayVisit().get(i).getProductId(),
                        visit.getDeliveryDayVisit().get(i).getAmountProduct(),
                        visit.getDeliveryDayVisit().get(i).getHour(),
                        visit.getDeliveryDayVisit().get(i).getPhoto()
                );

                merchandisingPersonals.add(merchandisingPersonal);
                 }
                }
            }

        }


        VisitRequest  visitRequest = new VisitRequest("2",visitArrayList,quizArrayList,quizDetailArrayListi,photosArrayList,actionArrayList,competitionPrices,detailCompetitionPrices,breakPrices,detailBreakPrices,Listi,Listi,shooperArrayList,merchandisings,detailMerchandisings,merchandisingPersonals,merchandisingFinals,finalDetailMerchandisings);
        Gson gson = new Gson();
        String visitString = gson.toJson(visitRequest);

        FactoryService.retrofitServicePost(VisitService.Api.class)
                .registerVisit(visitString)
                .enqueue(new Callback<VisitResponse>() {
                    @Override
                    public void onResponse(Call<VisitResponse> call, Response<VisitResponse> response) {
                        if (response.body().getSendStatus().equals("TRUE")){
                            if (moduleVisit.equals(Constants.FLAG_VISIT_PHOTO)){
                                if (response.body().getPhotoLocalDelete().equals("TRUE")){
                                    callback.onSuccessRegisterVisit();
                                }else{
                                    errorCallback.onError(new Exception(App.context.getString(R.string.services_error_general)));
                                }
                           }

                            if (moduleVisit.equals(Constants.FLAG_VISIT_ACTION)){
                                if (response.body().getActionLocalDelete().equals("TRUE")){
                                    callback.onSuccessRegisterVisit();
                                }else{
                                    errorCallback.onError(new Exception(App.context.getString(R.string.services_error_general)));
                                }
                            }

                            if (moduleVisit.equals(Constants.FLAG_VISIT_QUIZ)){
                                if (response.body().getQuizLocalDelete().equals("TRUE") && response.body().getQuizDetailLocalDelete().equals("TRUE")){
                                    callback.onSuccessRegisterVisit();
                                }else{
                                    errorCallback.onError(new Exception(App.context.getString(R.string.services_error_general)));
                                }
                            }

                            if (moduleVisit.equals(Constants.FLAG_VISIT_SHOOPER)){
                                if (response.body().getShooperLocalDelete().equals("TRUE")){
                                    callback.onSuccessRegisterVisit();
                                }else{
                                    errorCallback.onError(new Exception(App.context.getString(R.string.services_error_general)));
                                }
                            }

                            if (moduleVisit.equals(Constants.FLAG_VISIT_T)){
                                if (response.body().getShooperLocalDelete().equals("TRUE") &&
                                        response.body().getShooperLocalDelete().equals("TRUE") &&
                                        response.body().getQuizLocalDelete().equals("TRUE") &&
                                        response.body().getActionLocalDelete().equals("TRUE") &&
                                        response.body().getPhotoLocalDelete().equals("TRUE") &&
                                        response.body().getQuizDetailLocalDelete().equals("TRUE") &&
                                        response.body().getPhotoLocalDelete().equals("TRUE")){
                                    callback.onSuccessRegisterVisit();
                                }else{
                                    errorCallback.onError(new Exception(App.context.getString(R.string.services_error_general)));
                                }
                            }

                            if (moduleVisit.equals(Constants.FLAG_VISIT_PRICE_COMPETITION)){
                                if (response.body().getPriceCompetitionLocalDelete().equals("TRUE") && response.body().getDetailPriceCompetitionLocalDelete().equals("TRUE")){
                                    callback.onSuccessRegisterVisit();
                                }else{
                                    errorCallback.onError(new Exception(App.context.getString(R.string.services_error_general)));
                                }
                            }

                            if (moduleVisit.equals(Constants.FLAG_VISIT_PRICE_BREAK)){
                                if (response.body().getPriceBreakLocalDelete().equals("TRUE") && response.body().getDetailPriceBreakLocalDelete().equals("TRUE")){
                                    callback.onSuccessRegisterVisit();
                                }else{
                                    errorCallback.onError(new Exception(App.context.getString(R.string.services_error_general)));
                                }
                            }

                            if (moduleVisit.equals(Constants.FLAG_VISIT_STOCK)){
                                if (response.body().getMerchandisingLocalDelete().equals("TRUE") && response.body().getDetailMerchandisingLocalDelete().equals("TRUE")){
                                    callback.onSuccessRegisterVisit();
                                }else{
                                    errorCallback.onError(new Exception(App.context.getString(R.string.services_error_general)));
                                }
                            }

                            if (moduleVisit.equals(Constants.FLAG_VISIT_STOCK_FINAL)){
                                if (response.body().getMerchandisingFinalLocalDelete().equals("TRUE") && response.body().getMerchandisingFinalDetailLocalDelete().equals("TRUE")){
                                    callback.onSuccessRegisterVisit();
                                }else{
                                    errorCallback.onError(new Exception(App.context.getString(R.string.services_error_general)));
                                }
                            }

                            if (moduleVisit.equals(Constants.FLAG_VISIT_DELIVERY)){
                                if (response.body().getMerchandisingPersonalLocalDelete().equals("TRUE") ){
                                    callback.onSuccessRegisterVisit();
                                }else{
                                    errorCallback.onError(new Exception(App.context.getString(R.string.services_error_general)));
                                }
                            }

                        }else{
                            errorCallback.onError(new Exception(App.context.getString(R.string.services_error_general)));
                        }
                    }

                    @Override
                    public void onFailure(Call<VisitResponse> call, Throwable t) {
                        errorCallback.onError(new Exception(App.context.getString(R.string.services_error_general)));
                    }
                });
    }

    @Override
    public void checkVisitsPending(Client client, checkVisitsPendingCallback callback, ErrorCallback errorCallback) {

    }

    @Override
    public void getVisitClient(String visitId, getVisitClientCallback callback, ErrorCallback errorCallback) {

    }

    @Override
    public void getMerchandising(getMerchandisingCallback callback, ErrorCallback errorCallback) {

    }

    @Override
    public void getSubCategoryProduct(getSubCategoryProductCallback callback, ErrorCallback errorCallback) {

    }

    @Override
    public void getBrandProduct(int SubCategoryProduct, getBrandProductCallback callback, ErrorCallback errorCallback) {

    }

    @Override
    public void getBaseProduct(int SubCategoryProduct, int BrandProduct, getBaseProductCallback callback, ErrorCallback errorCallback) {

    }

    @Override
    public void getProducts(String baseProduct, getProductCallback callback, ErrorCallback errorCallback) {

    }

    @Override
    public void getListAdvanceDay(getListAdvanceDayCallback callback, ErrorCallback errorCallback) {

    }

    @Override
    public void getListBusinessDay(getListBusinessDayCallback callback, ErrorCallback errorCallback) {

    }

    @Override
    public void getListFee(Client client, getListFeeCallback callback, ErrorCallback errorCallback) {

    }

    @Override
    public void getListSale(Client client, getListSaleCallback callback, ErrorCallback errorCallback) {

    }

    @Override
    public void getListVisit(Client client, getListVisitCallback callback, ErrorCallback errorCallback) {

    }

    @Override
    public void getListFeeImpulso(Client client, getListFeeImpulsoCallback callback, ErrorCallback errorCallback) {

    }


}
