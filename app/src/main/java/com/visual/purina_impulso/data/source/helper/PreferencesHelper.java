package com.visual.purina_impulso.data.source.helper;

import com.google.gson.Gson;
import com.visual.purina_impulso.domain.Assistance;
import com.visual.purina_impulso.domain.User;
import com.visual.purina_impulso.util.PreferenceUtils;

public class PreferencesHelper {

    private static final String USER = "_USER_";
    private static final String USER_SYSTEM_ID = "_USER_SYSTEM_ID_";

    private PreferencesHelper() {}

    // PREFERENCIAS
    // metodo el cual ayuda a guardar en preferencia al usuario logeado
    public static void setUser(User user) {
        Gson gson = new Gson();
        PreferenceUtils.setStringPreference(USER, gson.toJson(user));
    }
    // metodo el cual ayuda a obtener en preferencia al usuario logeado
    public static User getUser() {
        Gson gson = new Gson();
        String json = PreferenceUtils.getStringPreference(USER);
        return gson.fromJson(json, User.class);
    }

    // metodo el cual ayuda a guardar en preferencia al id de sistema (Tradicional,Moderno,Especializado,Todos)
    public static void setSystemId(String value) {
        PreferenceUtils.setStringPreference(USER_SYSTEM_ID,value);
    }

    public static String getSystemId() {
        String value = PreferenceUtils.getStringPreference(USER_SYSTEM_ID);
        return value;
    }
}
