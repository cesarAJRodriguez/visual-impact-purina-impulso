package com.visual.purina_impulso.data.source.remote.model;

import com.google.gson.annotations.SerializedName;


public class UserResponse extends BaseUserResponse {

    @SerializedName("asistencia")
    String assistance;

    @SerializedName("idEmpleado")
    String employeeId;

    @SerializedName("idSistema")
    String systemId;

    @SerializedName("idTipo")
    String typeId;

    @SerializedName("nombresApellidos")
    String names;

    @SerializedName("canalUsuario")
    String userChannel;

    @SerializedName("tipoUsuario")
    String userType;


    public String getAssistance() {
        return assistance;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public String getSystemId() {
        return systemId;
    }

    public String getTypeId() {
        return typeId;
    }

    public String getNames() {
        return names;
    }

    public String getUserChannel() {
        return userChannel;
    }

    public String getUserType() {
        return userType;
    }



}
