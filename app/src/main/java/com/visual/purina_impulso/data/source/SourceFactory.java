package com.visual.purina_impulso.data.source;

import com.visual.purina_impulso.data.source.local.LocalAssistanceDataSource;
import com.visual.purina_impulso.data.source.local.LocalClientDataSource;
import com.visual.purina_impulso.data.source.local.LocalSyncDataSource;
import com.visual.purina_impulso.data.source.local.LocalUserDataSource;
import com.visual.purina_impulso.data.source.local.LocalVisitDataSource;
import com.visual.purina_impulso.data.source.remote.RemoteAssistanceDataSource;
import com.visual.purina_impulso.data.source.remote.RemoteClientDataSource;
import com.visual.purina_impulso.data.source.remote.RemoteSyncDataSource;
import com.visual.purina_impulso.data.source.remote.RemoteUserDataSource;
import com.visual.purina_impulso.data.source.remote.RemoteVisitDataSource;


public class SourceFactory {
    public static UserDataSource createUserDataSource(Source source) {
        switch (source) {
            case REMOTE: return RemoteUserDataSource.getInstance();
            case LOCAL: return LocalUserDataSource.getInstance();
            default: return LocalUserDataSource.getInstance();
        }
    }


    public static SyncDataSource createSyncDataSource(Source source) {
        switch (source) {
            case REMOTE: return RemoteSyncDataSource.getInstance();
            case LOCAL: return LocalSyncDataSource.getInstance();
            default: return LocalSyncDataSource.getInstance();
        }
    }

    public static AssistanceDataSource createAssistanceDataSource(Source source) {
        switch (source) {
            case REMOTE: return RemoteAssistanceDataSource.getInstance();
            case LOCAL: return LocalAssistanceDataSource.getInstance();
            default: return LocalAssistanceDataSource.getInstance();
        }
    }

    public static ClientDataSource createClientDataSource(Source source) {
        switch (source) {
            case REMOTE: return RemoteClientDataSource.getInstance();
            case LOCAL: return LocalClientDataSource.getInstance();
            default: return LocalClientDataSource.getInstance();
        }
    }

    public static VisitDataSource createVisitDataSource(Source source) {
        switch (source) {
            case REMOTE: return RemoteVisitDataSource.getInstance();
            case LOCAL: return LocalVisitDataSource.getInstance();
            default: return LocalVisitDataSource.getInstance();
        }
    }






}