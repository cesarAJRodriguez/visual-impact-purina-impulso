package com.visual.purina_impulso.data.source.remote.model;

import com.google.gson.annotations.SerializedName;

//Response : clase el cual recibe los valores del servicio
// @SerializedName() --> nombre del objeto a recibir

public class AssignVisitResponse {

    @SerializedName("estado")
    protected String status;

    public String getStatus() {
        return status;
    }

}
