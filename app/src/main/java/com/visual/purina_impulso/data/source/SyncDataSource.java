package com.visual.purina_impulso.data.source;

import com.visual.purina_impulso.base.BaseDataSource;
import com.visual.purina_impulso.domain.Sync;


public interface SyncDataSource extends BaseDataSource {

    interface SyncCallback {
        void onSuccess(Sync sync);
    }

    interface saveSyncCallback {
        void onSuccessSaveSync(String message);
    }

    interface clearSyncCallback {
        void onSuccessclearSync();
    }

    interface clearPartialSyncCallback {
        void onSuccessClearPartialSync();
    }



    void sync(String syncTraditional, String syncModern, String syncSpecialized, final SyncCallback callback, final ErrorCallback errorCallback);
    void saveSync(Sync sync,boolean typeSync, final saveSyncCallback callback, final ErrorCallback errorCallback);
    void clearSync(final clearSyncCallback callback, final ErrorCallback errorCallback);
    void clearPartialSync(final clearPartialSyncCallback callback, final ErrorCallback errorCallback);
}
