package com.visual.purina_impulso.data.source.remote.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;


public class StartVisitRequest {

    @SerializedName("sistema")
    protected String systemId;

    @SerializedName("visita")
    protected ArrayList<Visit> visit;

    public static class Visit{

        @SerializedName("idEmpleado")
        int employeeId;

        @SerializedName("fecha")
        String date;

        @SerializedName("idCliente")
        int clientId;

        @SerializedName("horaInicio")
        String startHour;

        @SerializedName("horaFin")
        String exitHour;

        @SerializedName("longInicio")
        String startLongitude;

        @SerializedName("latiInicio")
        String startLatitude;

        @SerializedName("longFin")
        String exitLongitude;

        @SerializedName("latiFin")
        String exitLatitude;

        @SerializedName("flagEstado")
        String statusFlag;

        public Visit(int employeeId, String date, int clientId, String startHour, String exitHour, String startLongitude, String startLatitude, String exitLongitude, String exitLatitude, String statusFlag) {
            this.employeeId = employeeId;
            this.date = date;
            this.clientId = clientId;
            this.startHour = startHour;
            this.exitHour = exitHour;
            this.startLongitude = startLongitude;
            this.startLatitude = startLatitude;
            this.exitLongitude = exitLongitude;
            this.exitLatitude = exitLatitude;
            this.statusFlag = statusFlag;
        }
    }

    public StartVisitRequest(String systemId, ArrayList<Visit> visit) {
        this.systemId = systemId;
        this.visit = visit;
    }
}
