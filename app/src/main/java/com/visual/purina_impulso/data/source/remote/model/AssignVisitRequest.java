package com.visual.purina_impulso.data.source.remote.model;

import com.google.gson.annotations.SerializedName;

//Request : clase el cual envia los datos al servicio
//     @SerializedName() --> nombre del objeto a enviar

public class AssignVisitRequest {

    @SerializedName("idEmpleado")
    protected String employeeId;

    @SerializedName("idSistema")
    protected String systemId;

    @SerializedName("fechaActual")
    protected String currentDate;

    @SerializedName("clientes")
    protected String[] clientsVisits;

    public AssignVisitRequest(String employeeId, String systemId, String currentDate, String[] clientsVisits) {
        this.employeeId = employeeId;
        this.systemId = systemId;
        this.currentDate = currentDate;
        this.clientsVisits = clientsVisits;
    }
}
