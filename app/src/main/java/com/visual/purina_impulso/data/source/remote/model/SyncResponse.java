package com.visual.purina_impulso.data.source.remote.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;



public class SyncResponse extends BaseUserResponse {

    @SerializedName("clientes_asignados")
    List<AssignedClients> assignedClients;

    @SerializedName("clientes")
    List<AssignedClients> clients;

    @SerializedName("tipo_incidencias")
    List<TypeIncidences> typeIncidences;

    @SerializedName("tipo_fotos")
    List<TypePhoto> typePhotos;

    @SerializedName("modulos")
    List<Module> modules;

    @SerializedName("encuestas")
    List<Quiz> quizzes;

    @SerializedName("preguntas")
    List<Question> questions;

    @SerializedName("alternativas")
    List<Alternative> alternatives;

    @SerializedName("lista_precio_quiebre")
    List<Price> breakPrice;

    @SerializedName("lista_precio_competencia")
    List<Price> competitionPrice;

    @SerializedName("accion_competencia")
    List<ProductCategory> productCategories;


    @SerializedName("lista_merchandising_moderno")
    List<Merchandising> merchandisingList;

    @SerializedName("categoriasProductos")
    List<Product> products;

    @SerializedName("lista_visita_moderno")
    List<ListVisit> listVisit;

    @SerializedName("lista_venta_moderno")
    List<ListSale> listSale;

    @SerializedName("lista_cuota_moderno")
    List<ListFee> listFee;

    @SerializedName("lista_dh_moderno")
    List<ListBusinessDay> listDH;

    @SerializedName("lista_da_moderno")
    List<ListAdvanceDay> listDA;


    @SerializedName("lista_cuota")
    List<ListFeeImpulso> listFeeImpulso;




    public List<AssignedClients> getAssignedClients() {
        return assignedClients;
    }

    public List<AssignedClients> getClients() {
        return clients;
    }

    public List<TypeIncidences> getTypeIncidences() {
        return typeIncidences;
    }

    public List<TypePhoto> getTypePhotos() {
        return typePhotos;
    }

    public List<Module> getModules() {
        return modules;
    }

    public List<Quiz> getQuizzes() {
        return quizzes;
    }

    public List<Question> getQuestions() {
        return questions;
    }

    public List<Alternative> getAlternatives() {
        return alternatives;
    }

    public List<Price> getBreakPrices() {
        return breakPrice;
    }

    public List<Price> getCompetitionPrices() {
        return competitionPrice;
    }

    public List<ProductCategory> getProductCategories() {
        return productCategories;
    }

    public List<Merchandising> getMerchandisingList() {
        return merchandisingList;
    }

    public List<Product> getProducts() {
        return products;
    }

    public List<ListVisit> getListVisit() {
        return listVisit;
    }

    public List<ListSale> getListSale() {
        return listSale;
    }

    public List<ListFee> getListFee() {
        return listFee;
    }

    public List<ListBusinessDay> getListDH() {
        return listDH;
    }

    public List<ListAdvanceDay> getListDA() {
        return listDA;
    }

    public List<ListFeeImpulso> getListFeeImpulso() {
        return listFeeImpulso;
    }

    public static class AssignedClients{

        @SerializedName("idEmpleado")
        String employeeId;

        @SerializedName("idCliente")
        int clientId;

        @SerializedName("dni")
        String dni;

        @SerializedName("ruc")
        String ruc;

        @SerializedName("razonSocial")
        String code;

        @SerializedName("direccion")
        String address;

        @SerializedName("idBanner")
        int bannerId;

        @SerializedName("banner")
        String banner;

        @SerializedName("idCluster")
        int clusterId;

        @SerializedName("cluster")
        String cluster;

        public String getEmployeeId() {
            return employeeId;
        }

        public int getClientId() {
            return clientId;
        }

        public String getDni() {
            return dni;
        }

        public String getRuc() {
            return ruc;
        }

        public String getCode() {
            return code;
        }

        public String getAddress() {
            return address;
        }

        public int getBannerId() {
            return bannerId;
        }

        public String getBanner() {
            return banner;
        }

        public int getClusterId() {
            return clusterId;
        }

        public String getCluster() {
            return cluster;
        }
    }

    public static class TypeIncidences{

        @SerializedName("idTipoIncidencia")
        int typeIncidenceId;

        @SerializedName("idCanal")
        int channelId;

        @SerializedName("nombreIncidencia")
        String typeIncidenceName;

        public int getTypeIncidenceId() {
            return typeIncidenceId;
        }

        public void setTypeIncidenceId(int typeIncidenceId) {
            this.typeIncidenceId = typeIncidenceId;
        }

        public int getChannelId() {
            return channelId;
        }

        public void setChannelId(int channelId) {
            this.channelId = channelId;
        }

        public String getTypeIncidenceName() {
            return typeIncidenceName;
        }

        public void setTypeIncidenceName(String typeIncidenceName) {
            this.typeIncidenceName = typeIncidenceName;
        }
    }

    public static class TypePhoto{

        @SerializedName("idTipoFoto")
        int typePhotoId;

        @SerializedName("idCanal")
        int channelId;

        @SerializedName("nombreFoto")
        String typePhotoName;

        public int getTypePhotoId() {
            return typePhotoId;
        }

        public void setTypePhotoId(int typePhotoId) {
            this.typePhotoId = typePhotoId;
        }

        public int getChannelId() {
            return channelId;
        }

        public void setChannelId(int channelId) {
            this.channelId = channelId;
        }

        public String getTypePhotoName() {
            return typePhotoName;
        }

        public void setTypePhotoName(String typePhotoName) {
            this.typePhotoName = typePhotoName;
        }
    }

    public static class Module{

        @SerializedName("idModulo")
        int moduleId;

        @SerializedName("modulo")
        String moduleName;

        public int getModuleId() {
            return moduleId;
        }

        public void setModuleId(int moduleId) {
            this.moduleId = moduleId;
        }

        public String getModuleName() {
            return moduleName;
        }

        public void setModuleName(String moduleName) {
            this.moduleName = moduleName;
        }
    }

    public static class Quiz{

        @SerializedName("idCanal")
        int channelId;

        @SerializedName("idEncuesta")
        int quizId;

        @SerializedName("encuesta")
        String quizName;

        public int getChannelId() {
            return channelId;
        }

        public void setChannelId(int channelId) {
            this.channelId = channelId;
        }

        public int getQuizId() {
            return quizId;
        }

        public void setQuizId(int quizId) {
            this.quizId = quizId;
        }

        public String getQuizName() {
            return quizName;
        }

        public void setQuizName(String quizName) {
            this.quizName = quizName;
        }
    }

    public static class Question{

        @SerializedName("idCanal")
        int channelId;

        @SerializedName("idEncuesta")
        int quizId;

        @SerializedName("idPregunta")
        int questionId;

        @SerializedName("idTipoPregunta")
        int typeQuestion;

        @SerializedName("pregunta")
        String questionName;

        public int getChannelId() {
            return channelId;
        }

        public void setChannelId(int channelId) {
            this.channelId = channelId;
        }

        public int getQuizId() {
            return quizId;
        }

        public void setQuizId(int quizId) {
            this.quizId = quizId;
        }

        public int getQuestionId() {
            return questionId;
        }

        public void setQuestionId(int questionId) {
            this.questionId = questionId;
        }

        public int getTypeQuestion() {
            return typeQuestion;
        }

        public void setTypeQuestion(int typeQuestion) {
            this.typeQuestion = typeQuestion;
        }

        public String getQuestionName() {
            return questionName;
        }

        public void setQuestionName(String questionName) {
            this.questionName = questionName;
        }
    }

    public static class Alternative{

        @SerializedName("idCanal")
        int channelId;

        @SerializedName("idEncuesta")
        int quizId;

        @SerializedName("idPregunta")
        int questionId;

        @SerializedName("idAlternativa")
        int alternativeId;

        @SerializedName("alternativa")
        String alternativeName;

        public int getChannelId() {
            return channelId;
        }

        public void setChannelId(int channelId) {
            this.channelId = channelId;
        }

        public int getQuizId() {
            return quizId;
        }

        public void setQuizId(int quizId) {
            this.quizId = quizId;
        }

        public int getQuestionId() {
            return questionId;
        }

        public void setQuestionId(int questionId) {
            this.questionId = questionId;
        }

        public int getAlternativeId() {
            return alternativeId;
        }

        public void setAlternativeId(int alternativeId) {
            this.alternativeId = alternativeId;
        }

        public String getAlternativeName() {
            return alternativeName;
        }

        public void setAlternativeName(String alternativeName) {
            this.alternativeName = alternativeName;
        }
    }

    public static class Price{

        @SerializedName("idProducto")
        int productId;

        @SerializedName("producto")
        String productoName;

        @SerializedName("tipo")
        String type;

        @SerializedName("idCliente")
        int clientId;

        @SerializedName("idBanner")
        int bannerId;

        public int getProductId() {
            return productId;
        }

        public void setProductId(int productId) {
            this.productId = productId;
        }

        public String getProductoName() {
            return productoName;
        }

        public void setProductoName(String productoName) {
            this.productoName = productoName;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public int getClientId() {
            return clientId;
        }

        public void setClientId(int clientId) {
            this.clientId = clientId;
        }

        public int getBannerId() {
            return bannerId;
        }

        public void setBannerId(int bannerId) {
            this.bannerId = bannerId;
        }
    }

    public static class ProductCategory{

        @SerializedName("idCanal")
        int channelId;

        @SerializedName("idCategoriaProducto")
        int productCategoryId;

        @SerializedName("categoriaProducto")
        String productCategory;

        public int getChannelId() {
            return channelId;
        }

        public void setChannelId(int channelId) {
            this.channelId = channelId;
        }

        public int getProductCategoryId() {
            return productCategoryId;
        }

        public void setProductCategoryId(int productCategoryId) {
            this.productCategoryId = productCategoryId;
        }

        public String getProductCategory() {
            return productCategory;
        }

        public void setProductCategory(String productCategory) {
            this.productCategory = productCategory;
        }
    }

    public static class Merchandising{

        @SerializedName("idMerchandising")
        int merchandisingId;

        @SerializedName("nombre")
        String name;

        @SerializedName("estado")
        int status;

        public int getMerchandisingId() {
            return merchandisingId;
        }

        public void setMerchandisingId(int merchandisingId) {
            this.merchandisingId = merchandisingId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }
    }

    public static class Product{

        @SerializedName("idSubcategoria")
        int subCategoryId;

        @SerializedName("subcategoria")
        String subCategory;

        @SerializedName("idCategoriaProducto")
        int categoryProductId;

        @SerializedName("categoriaproducto")
        String categoryProduct;

        @SerializedName("idBaseProducto")
        String baseProductId;

        @SerializedName("baseproducto")
        String baseProduct;

        @SerializedName("idProducto")
        int productId;

        @SerializedName("produto")
        String product;

        public int getSubCategoryId() {
            return subCategoryId;
        }

        public void setSubCategoryId(int subCategoryId) {
            this.subCategoryId = subCategoryId;
        }

        public String getSubCategory() {
            return subCategory;
        }

        public void setSubCategory(String subCategory) {
            this.subCategory = subCategory;
        }

        public int getCategoryProductId() {
            return categoryProductId;
        }

        public void setCategoryProductId(int categoryProductId) {
            this.categoryProductId = categoryProductId;
        }

        public String getCategoryProduct() {
            return categoryProduct;
        }

        public void setCategoryProduct(String categoryProduct) {
            this.categoryProduct = categoryProduct;
        }

        public String getBaseProductId() {
            return baseProductId;
        }

        public void setBaseProductId(String baseProductId) {
            this.baseProductId = baseProductId;
        }

        public String getBaseProduct() {
            return baseProduct;
        }

        public void setBaseProduct(String baseProduct) {
            this.baseProduct = baseProduct;
        }

        public int getProductId() {
            return productId;
        }

        public void setProductId(int productId) {
            this.productId = productId;
        }

        public String getProduct() {
            return product;
        }

        public void setProduct(String product) {
            this.product = product;
        }
    }

    public static class ListAdvanceDay{

        @SerializedName("idTiempo")
        String timeId;

        public String getTimeId() {
            return timeId;
        }

        public void setTimeId(String timeId) {
            this.timeId = timeId;
        }
    }

    public static class ListBusinessDay{

        @SerializedName("idTiempo")
        String timeId;

        public String getTimeId() {
            return timeId;
        }

        public void setTimeId(String timeId) {
            this.timeId = timeId;
        }
    }

    public static class ListVisit{

        @SerializedName("idCliente")
        int clientId;

        @SerializedName("vi_prog")
        int viProg;

        @SerializedName("vi_exc")
        int viExc;

        @SerializedName("vi_hab")
        int viHab;

        @SerializedName("vi_efec")
        int viEfec;

        @SerializedName("vi_in")
        int viIn;

        public int getClientId() {
            return clientId;
        }

        public void setClientId(int clientId) {
            this.clientId = clientId;
        }

        public int getViProg() {
            return viProg;
        }

        public void setViProg(int viProg) {
            this.viProg = viProg;
        }

        public int getViExc() {
            return viExc;
        }

        public void setViExc(int viExc) {
            this.viExc = viExc;
        }

        public int getViHab() {
            return viHab;
        }

        public void setViHab(int viHab) {
            this.viHab = viHab;
        }

        public int getViEfec() {
            return viEfec;
        }

        public void setViEfec(int viEfec) {
            this.viEfec = viEfec;
        }

        public int getViIn() {
            return viIn;
        }

        public void setViIn(int viIn) {
            this.viIn = viIn;
        }

    }

    public static class ListFee{

        @SerializedName("idCliente")
        int clientId;

        @SerializedName("cuota")
        String fee;

        public int getClientId() {
            return clientId;
        }

        public void setClientId(int clientId) {
            this.clientId = clientId;
        }

        public String getFee() {
            return fee;
        }

        public void setFee(String fee) {
            this.fee = fee;
        }
    }

    public static class ListSale{

        @SerializedName("idCliente")
        int clientId;

        @SerializedName("ventas")
        String sale;

        public int getClientId() {
            return clientId;
        }

        public void setClientId(int clientId) {
            this.clientId = clientId;
        }

        public String getSale() {
            return sale;
        }

        public void setSale(String sale) {
            this.sale = sale;
        }
    }

    public static class ListFeeImpulso{

        @SerializedName("idCliente")
        int clientId;

        @SerializedName("cuota")
        int fee;

        @SerializedName("alcance_merch")
        int range;

        @SerializedName("venta_req_dia")
        int saleDay;

        public int getClientId() {
            return clientId;
        }

        public int getFee() {
            return fee;
        }

        public int getRange() {
            return range;
        }

        public int getSaleDay() {
            return saleDay;
        }
    }


}
