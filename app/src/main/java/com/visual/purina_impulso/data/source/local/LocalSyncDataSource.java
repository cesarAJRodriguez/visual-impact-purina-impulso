package com.visual.purina_impulso.data.source.local;

import com.visual.purina_impulso.App;
import com.visual.purina_impulso.R;
import com.visual.purina_impulso.data.source.SyncDataSource;
import com.visual.purina_impulso.data.source.local.realm.RealmService;
import com.visual.purina_impulso.domain.Sync;

import java.util.ArrayList;

import io.realm.Realm;


public class LocalSyncDataSource implements SyncDataSource {

    private static LocalSyncDataSource INSTANCE = null;
    private final RealmService mRealService;

    private LocalSyncDataSource() {
        mRealService = new RealmService(Realm.getDefaultInstance());
    }

    public synchronized static LocalSyncDataSource getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new LocalSyncDataSource();
        }
        return INSTANCE;
    }

    @Override
    public void sync(String syncTraditional, String syncModern, String syncSpecialized, SyncCallback callback, ErrorCallback errorCallback) {

    }

    // guarda/actualiza los datos de la sincronizacion  en la bd local
    @Override
    public void saveSync(Sync sync,boolean typeSync, saveSyncCallback callback, ErrorCallback errorCallback) {
        ArrayList<Sync> syncs = new ArrayList<>();
        syncs.add(sync);

        mRealService.onWriteSyncObjects(typeSync,syncs, new RealmService.DataBaseCallback<Sync>() {
            @Override
            public void onTransactionSuccess(ArrayList<Sync> object) {
                callback.onSuccessSaveSync((App.context).getResources().getString(R.string.message_save_sync));
            }

            @Override
            public void onTransactionError(Exception error) {
               errorCallback.onError(error);
            }
        });
    }

    // borra toda la bd local
    @Override
    public void clearSync(clearSyncCallback callback, ErrorCallback errorCallback) {
        mRealService.onClearDB(new RealmService.DataBaseCallbackDelete() {
            @Override
            public void onTransactionDeleteSuccess() {
                callback.onSuccessclearSync();
            }

            @Override
            public void onTransactionDeleteError(Exception error) {
                errorCallback.onError(error);
            }
        });
    }

    // borra los campos de la sincronizacion (se utiliza para cuando se agrega una visita)
    @Override
    public void clearPartialSync(clearPartialSyncCallback callback, ErrorCallback errorCallback) {
        mRealService.onClearPartialDB(new RealmService.DataBaseCallbackDelete() {
            @Override
            public void onTransactionDeleteSuccess() {
                callback.onSuccessClearPartialSync();
            }

            @Override
            public void onTransactionDeleteError(Exception error) {
                errorCallback.onError(error);
            }
        });
    }

}
