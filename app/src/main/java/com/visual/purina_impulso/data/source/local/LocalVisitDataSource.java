package com.visual.purina_impulso.data.source.local;

import com.visual.purina_impulso.data.source.VisitDataSource;
import com.visual.purina_impulso.data.source.local.realm.RealmService;
import com.visual.purina_impulso.domain.Alternative;
import com.visual.purina_impulso.domain.BaseProduct;
import com.visual.purina_impulso.domain.BrandProduct;
import com.visual.purina_impulso.domain.Client;
import com.visual.purina_impulso.domain.Incidence;
import com.visual.purina_impulso.domain.IncidenceVisit;
import com.visual.purina_impulso.domain.ListAdvanceDay;
import com.visual.purina_impulso.domain.ListBusinessDay;
import com.visual.purina_impulso.domain.ListFee;
import com.visual.purina_impulso.domain.ListFeeImpulso;
import com.visual.purina_impulso.domain.ListSale;
import com.visual.purina_impulso.domain.ListVisit;
import com.visual.purina_impulso.domain.Merchandising;
import com.visual.purina_impulso.domain.Photo;
import com.visual.purina_impulso.domain.Price;
import com.visual.purina_impulso.domain.Product;
import com.visual.purina_impulso.domain.ProductCategory;
import com.visual.purina_impulso.domain.Question;
import com.visual.purina_impulso.domain.Quiz;
import com.visual.purina_impulso.domain.StartVisit;
import com.visual.purina_impulso.domain.SubCategoryProduct;
import com.visual.purina_impulso.domain.Visit;

import java.util.ArrayList;

import io.realm.Realm;



public class LocalVisitDataSource implements VisitDataSource {

    private static LocalVisitDataSource INSTANCE = null;
    private final RealmService mRealService;

    private LocalVisitDataSource() {
        mRealService = new RealmService(Realm.getDefaultInstance());
    }

    public synchronized static LocalVisitDataSource getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new LocalVisitDataSource();
        }
        return INSTANCE;
    }

    @Override
    public void assignVisit(String currentDate, String[] clients, assignVisitCallback callback, ErrorCallback errorCallback) {

    }

    //obtiene las encuestas de la bd local
    @Override
    public void getQuizzesVisit(int channelId, getQuizzesVisitCallback callback, ErrorCallback errorCallback) {
        ArrayList<String> keys = new ArrayList<>();
        keys.add("channelId");
        ArrayList<Integer> values = new ArrayList<>();
        values.add(channelId);

            mRealService.onReadValuesIntegerObjects(Quiz.class,keys, values, new RealmService.DataBaseCallback<Quiz>() {
                @Override
                public void onTransactionSuccess(ArrayList<Quiz> object) {
                    callback.onSuccessGetQuizzesVisit(object);
                }

                @Override
                public void onTransactionError(Exception error) {
                    errorCallback.onError(error);
                }
            });
    }

    //obtiene las preguntas de la db local, dependiendo del canal y encuesta
    @Override
    public void getQuizQuestions(int channelId, int quizId, getQuizQuestionsCallback callback, ErrorCallback errorCallback) {
        ArrayList<String> keys = new ArrayList<>();
        keys.add("channelId");
        keys.add("quizId");
        ArrayList<Integer> values = new ArrayList<>();
        values.add(channelId);
        values.add(quizId);

        mRealService.onReadValuesIntegerObjects(Question.class,keys, values, new RealmService.DataBaseCallback<Question>() {
            @Override
            public void onTransactionSuccess(ArrayList<Question> object) {
                callback.onSuccessGetQuizQuestions(object);
            }

            @Override
            public void onTransactionError(Exception error) {
                errorCallback.onError(error);
            }
        });
    }

    //obtiene las alternativas de las preguntas de la bd bd local, dependiendo del canal y encuesta
    @Override
    public void getQuizQuestionAlternatives(int channelId, int quizId, getQuizQuestionAlternativesCallback callback, ErrorCallback errorCallback) {
        ArrayList<String> keys = new ArrayList<>();
        keys.add("channelId");
        keys.add("quizId");
        ArrayList<Integer> values = new ArrayList<>();
        values.add(channelId);
        values.add(quizId);

        mRealService.onReadValuesIntegerObjects(Alternative.class,keys, values, new RealmService.DataBaseCallback<Alternative>() {
            @Override
            public void onTransactionSuccess(ArrayList<Alternative> object) {
                callback.onSuccessGetQuizQuestionAlternatives(object);
            }

            @Override
            public void onTransactionError(Exception error) {
                errorCallback.onError(error);
            }
        });
    }

    //obtiene las precios/productos de la bd local, se utliza para el modulo de precios en visita
    @Override
    public void getPrices(int clientId,String typePrice,String search, getPricesCallback callback, ErrorCallback errorCallback) {


        if (clientId != 0){
            ArrayList<String> keysInteger = new ArrayList<>();
            ArrayList<Integer> valuesInteger = new ArrayList<>();

            keysInteger.add("clientId");
            valuesInteger.add(clientId);

            ArrayList<String> keysString = new ArrayList<>();
            ArrayList<String> valuesString = new ArrayList<>();

            keysString.add("typePrice");
            valuesString.add(typePrice);

            if (search == null){
                mRealService.onReadValuesIntegerStringObjects(Price.class,keysInteger, valuesInteger,keysString,valuesString, new RealmService.DataBaseCallback<Price>() {
                    @Override
                    public void onTransactionSuccess(ArrayList<Price> object) {
                        callback.onSuccessGetPrices(object);
                    }

                    @Override
                    public void onTransactionError(Exception error) {
                        errorCallback.onError(error);
                    }
                });
            }else{
                ArrayList<String> keysLike = new ArrayList<>();
                keysLike.add("productName");
                ArrayList<String> valuesLike = new ArrayList<>();
                valuesLike.add(search);
                mRealService.onReadValuesIntegerStringLikeObjects(Price.class,keysInteger, valuesInteger,keysString,valuesString,keysLike,valuesLike, new RealmService.DataBaseCallback<Price>() {
                    @Override
                    public void onTransactionSuccess(ArrayList<Price> object) {
                        callback.onSuccessGetPrices(object);
                    }

                    @Override
                    public void onTransactionError(Exception error) {
                        errorCallback.onError(error);
                    }
                });
            }


        }else{

            ArrayList<String> keys = new ArrayList<>();
            ArrayList<String> values = new ArrayList<>();

            keys.add("typePrice");
            values.add(typePrice);

            if (search == null){
                mRealService.onReadObjects(Price.class,keys, values, new RealmService.DataBaseCallback<Price>() {
                    @Override
                    public void onTransactionSuccess(ArrayList<Price> object) {
                        callback.onSuccessGetPrices(object);
                    }

                    @Override
                    public void onTransactionError(Exception error) {
                        errorCallback.onError(error);
                    }
                });
            }else{
                ArrayList<String> keysLike = new ArrayList<>();
                keysLike.add("productName");
                ArrayList<String> valuesLike = new ArrayList<>();
                valuesLike.add(search);

                mRealService.onReadObjectsWhereLike(Price.class,keys, values,keysLike,valuesLike, new RealmService.DataBaseCallback<Price>() {
                    @Override
                    public void onTransactionSuccess(ArrayList<Price> object) {
                        callback.onSuccessGetPrices(object);
                    }

                    @Override
                    public void onTransactionError(Exception error) {
                        errorCallback.onError(error);
                    }
                });

            }

        }
    }

    //obtiene los tipos de fotos de la bd local, se utliza para el modulo de fotos en visita
    @Override
    public void getTypePhotos(int channelId, getTypePhotosCallback callback, ErrorCallback errorCallback) {
        ArrayList<String> keys = new ArrayList<>();
        ArrayList<Integer> values = new ArrayList<>();

        keys.add("channelId");
        values.add(channelId);

        mRealService.onReadValuesIntegerObjects(Photo.class,keys, values, new RealmService.DataBaseCallback<Photo>() {
            @Override
            public void onTransactionSuccess(ArrayList<Photo> object) {
                callback.onSuccessGetTypePhotos(object);
            }

            @Override
            public void onTransactionError(Exception error) {
                errorCallback.onError(error);
            }
        });
    }

    //obtiene los categori de productos de la bd local, se utliza para el modulo de accion de la competencia
    @Override
    public void getCategoriesProduct(int channelId, getCategoriesProductCallback callback, ErrorCallback errorCallback) {
        ArrayList<String> keys = new ArrayList<>();
        ArrayList<Integer> values = new ArrayList<>();

        keys.add("channelId");
        values.add(channelId);

        mRealService.onReadValuesIntegerObjects(ProductCategory.class,keys, values, new RealmService.DataBaseCallback<ProductCategory>() {
            @Override
            public void onTransactionSuccess(ArrayList<ProductCategory> object) {
                callback.onSuccessGetCategoriesProduct(object);
            }

            @Override
            public void onTransactionError(Exception error) {
                errorCallback.onError(error);
            }
        });
    }

    //obtiene los tipos de incidencia de la bd local, se utliza para el modulo incidencia de la visita
    @Override
    public void getTypeIncidences(int channelId, getTypeIncidencesCallback callback, ErrorCallback errorCallback) {
        ArrayList<String> keys = new ArrayList<>();
        ArrayList<Integer> values = new ArrayList<>();

        keys.add("channelId");
        values.add(channelId);

        mRealService.onReadValuesIntegerObjects(Incidence.class,keys, values, new RealmService.DataBaseCallback<Incidence>() {
            @Override
            public void onTransactionSuccess(ArrayList<Incidence> object) {
                callback.onSuccessGetTypeIncidences(object);
            }

            @Override
            public void onTransactionError(Exception error) {
                errorCallback.onError(error);
            }
        });
    }

    //guarda/actualiza el inicio de la visita en la bd
    @Override
    public void registerStartVisit(StartVisit startVisit,Visit visit, registerStartVisitCallback callback, ErrorCallback errorCallback) {
        ArrayList<Visit> visits = new ArrayList<>();
        visits.add(visit);

        mRealService.onWriteObjects(visits, new RealmService.DataBaseCallback<Visit>() {
            @Override
            public void onTransactionSuccess(ArrayList<Visit> object) {
                callback.onSuccessRegisterStartVisit(visit);
            }

            @Override
            public void onTransactionError(Exception error) {
                errorCallback.onError(error);
            }

        });
    }

    @Override
    public void registerIncidenceVisit(StartVisit startVisit, IncidenceVisit incidenceVisit, registerIncidenceVisitCallback callback, ErrorCallback errorCallback) {

    }


    //guarda/actualiza la visita en la bd
    @Override
    public void registerVisit(Visit visit,String moduleVisit, registerVisitCallback callback, ErrorCallback errorCallback) {
        ArrayList<Visit> visits = new ArrayList<>();
        visits.add(visit);

        mRealService.onWriteObjects(visits, new RealmService.DataBaseCallback<Visit>() {
            @Override
            public void onTransactionSuccess(ArrayList<Visit> object) {
                callback.onSuccessRegisterVisit();
            }

            @Override
            public void onTransactionError(Exception error) {
                errorCallback.onError(error);
            }

        });
    }

    @Override
    public void checkVisitsPending(Client client, checkVisitsPendingCallback callback, ErrorCallback errorCallback) {

    }

    //obtiene la data de visita de un cliente en la bd local
    @Override
    public void getVisitClient(String visitId, getVisitClientCallback callback, ErrorCallback errorCallback) {
        ArrayList<String> keys = new ArrayList<>();
        ArrayList<String> values = new ArrayList<>();

        keys.add("id");
        values.add(visitId);

        mRealService.onReadObjects(Visit.class,keys, values, new RealmService.DataBaseCallback<Visit>() {
            @Override
            public void onTransactionSuccess(ArrayList<Visit> object) {
                callback.onSuccessGetVisitClient(object);
            }

            @Override
            public void onTransactionError(Exception error) {
                errorCallback.onError(error);
            }
        });
    }


    //obtiene los merchandising de la bd local , para el modulo de stock sampling en la visita
    @Override
    public void getMerchandising(getMerchandisingCallback callback, ErrorCallback errorCallback) {


        mRealService.onReadAllObjects(Merchandising.class, new RealmService.DataBaseCallback<Merchandising>() {
            @Override
            public void onTransactionSuccess(ArrayList<Merchandising> object) {
                callback.onSuccessGetMerchandising(object);
            }

            @Override
            public void onTransactionError(Exception error) {
                errorCallback.onError(error);
            }
        });
    }

    //obtiene las subcategorias del producto de la bd local , para el modulo de entrega de producto en la visita
    @Override
    public void getSubCategoryProduct(getSubCategoryProductCallback callback, ErrorCallback errorCallback) {
        mRealService.onReadAllObjects(SubCategoryProduct.class, new RealmService.DataBaseCallback<SubCategoryProduct>() {
            @Override
            public void onTransactionSuccess(ArrayList<SubCategoryProduct> object) {
                callback.onSuccessGetSubCategoryProduct(object);
            }

            @Override
            public void onTransactionError(Exception error) {
                errorCallback.onError(error);
            }
        });
    }

    //obtiene las marcas del producto de la bd local , para el modulo de entrega de producto en la visita
    @Override
    public void getBrandProduct(int SubCategoryProduct, getBrandProductCallback callback, ErrorCallback errorCallback) {
        ArrayList<String> keys = new ArrayList<>();
        ArrayList<Integer> values = new ArrayList<>();

        keys.add("subProductCategoryId");
        values.add(SubCategoryProduct);

        mRealService.onReadValuesIntegerObjects(BrandProduct.class,keys,values, new RealmService.DataBaseCallback<BrandProduct>() {
            @Override
            public void onTransactionSuccess(ArrayList<BrandProduct> object) {
                callback.onSuccessGetBrandProduct(object);
            }

            @Override
            public void onTransactionError(Exception error) {
                errorCallback.onError(error);
            }
        });
    }

    //obtiene las base del producto de la bd local , para el modulo de entrega de producto en la visita
    @Override
    public void getBaseProduct(int SubCategoryProduct, int BrandProduct, getBaseProductCallback callback, ErrorCallback errorCallback) {
        ArrayList<String> keys = new ArrayList<>();
        ArrayList<Integer> values = new ArrayList<>();

        keys.add("subCategoryProductId");
        keys.add("brandProductId");
        values.add(SubCategoryProduct);
        values.add(BrandProduct);

        mRealService.onReadValuesIntegerObjects(BaseProduct.class,keys,values, new RealmService.DataBaseCallback<BaseProduct>() {
            @Override
            public void onTransactionSuccess(ArrayList<BaseProduct> object) {
                callback.onSuccessGetBaseProduct(object);
            }

            @Override
            public void onTransactionError(Exception error) {
                errorCallback.onError(error);
            }
        });
    }

    //obtiene los producto de la bd local , para el modulo de entrega de producto en la visita
    @Override
    public void getProducts(String baseProduct, getProductCallback callback, ErrorCallback errorCallback) {
        ArrayList<String> keys = new ArrayList<>();
        ArrayList<String> values = new ArrayList<>();

        keys.add("baseProductId");
        values.add(baseProduct);

        mRealService.onReadObjects(Product.class,keys,values, new RealmService.DataBaseCallback<Product>() {
            @Override
            public void onTransactionSuccess(ArrayList<Product> object) {
                callback.onSuccessGetProduct(object);
            }

            @Override
            public void onTransactionError(Exception error) {
                errorCallback.onError(error);
            }
        });

    }

    //obtiene la lista de avance de dias de la bd local , para el modulo de venta pro dia en la visita
    @Override
    public void getListAdvanceDay(getListAdvanceDayCallback callback, ErrorCallback errorCallback) {
        mRealService.onReadAllObjects(ListAdvanceDay.class, new RealmService.DataBaseCallback<ListAdvanceDay>() {
            @Override
            public void onTransactionSuccess(ArrayList<ListAdvanceDay> object) {
                callback.onSuccessGetListAdvanceDay(object);
            }

            @Override
            public void onTransactionError(Exception error) {
                errorCallback.onError(error);
            }
        });
    }

    //obtiene la lista de negocio de dias de la bd local , para el modulo de venta pro dia en la visita
    @Override
    public void getListBusinessDay(getListBusinessDayCallback callback, ErrorCallback errorCallback) {
        mRealService.onReadAllObjects(ListBusinessDay.class, new RealmService.DataBaseCallback<ListBusinessDay>() {
            @Override
            public void onTransactionSuccess(ArrayList<ListBusinessDay> object) {
                callback.onSuccessGetListBusinessDay(object);
            }

            @Override
            public void onTransactionError(Exception error) {
                errorCallback.onError(error);
            }
        });
    }

    //obtiene la lista de cuotas  de la bd local , para el modulo de venta pro dia en la visita
    @Override
    public void getListFee(Client client, getListFeeCallback callback, ErrorCallback errorCallback) {
        ArrayList<String> keys = new ArrayList<>();
        ArrayList<Integer> values = new ArrayList<>();

        keys.add("clientId");
        values.add(client.getClientId());

        mRealService.onReadValuesIntegerObjects(ListFee.class,keys,values, new RealmService.DataBaseCallback<ListFee>() {
            @Override
            public void onTransactionSuccess(ArrayList<ListFee> object) {
                callback.onSuccessGetListFeeDay(object);
            }

            @Override
            public void onTransactionError(Exception error) {
                errorCallback.onError(error);
            }
        });
    }

    //obtiene la lista de ventas  de la bd local , para el modulo de venta pro dia en la visita
    @Override
    public void getListSale(Client client, getListSaleCallback callback, ErrorCallback errorCallback) {
        ArrayList<String> keys = new ArrayList<>();
        ArrayList<Integer> values = new ArrayList<>();

        keys.add("clientId");
        values.add(client.getClientId());

        mRealService.onReadValuesIntegerObjects(ListSale.class,keys,values, new RealmService.DataBaseCallback<ListSale>() {
            @Override
            public void onTransactionSuccess(ArrayList<ListSale> object) {
                callback.onSuccessGetListSaleDay(object);
            }

            @Override
            public void onTransactionError(Exception error) {
                errorCallback.onError(error);
            }
        });
    }

    //obtiene la lista de visitas  de la bd local , para el modulo de venta pro dia en la visita
    @Override
    public void getListVisit(Client client, getListVisitCallback callback, ErrorCallback errorCallback) {
        ArrayList<String> keys = new ArrayList<>();
        ArrayList<Integer> values = new ArrayList<>();

        keys.add("clientId");
        values.add(client.getClientId());

        mRealService.onReadValuesIntegerObjects(ListVisit.class,keys,values, new RealmService.DataBaseCallback<ListVisit>() {
            @Override
            public void onTransactionSuccess(ArrayList<ListVisit> object) {
                callback.onSuccessGetListVisit(object);
            }

            @Override
            public void onTransactionError(Exception error) {
                errorCallback.onError(error);
            }
        });
    }

    //obtiene la lista de cuota impulso  de la bd local , para el modulo de venta pro dia en la visita
    @Override
    public void getListFeeImpulso(Client client, getListFeeImpulsoCallback callback, ErrorCallback errorCallback) {
        ArrayList<String> keys = new ArrayList<>();
        ArrayList<Integer> values = new ArrayList<>();

        keys.add("clientId");
        values.add(client.getClientId());

        mRealService.onReadValuesIntegerObjects(ListFeeImpulso.class,keys,values, new RealmService.DataBaseCallback<ListFeeImpulso>() {
            @Override
            public void onTransactionSuccess(ArrayList<ListFeeImpulso> object) {
                callback.onSuccessGetListFeeImpulso(object);
            }

            @Override
            public void onTransactionError(Exception error) {
                errorCallback.onError(error);
            }
        });
    }
}
