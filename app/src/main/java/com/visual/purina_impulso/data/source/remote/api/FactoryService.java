package com.visual.purina_impulso.data.source.remote.api;

import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.franmontiel.persistentcookiejar.ClearableCookieJar;
import com.franmontiel.persistentcookiejar.PersistentCookieJar;
import com.franmontiel.persistentcookiejar.cache.SetCookieCache;
import com.franmontiel.persistentcookiejar.persistence.SharedPrefsCookiePersistor;
import com.visual.purina_impulso.BuildConfig;
import com.visual.purina_impulso.util.StringUtil;

import java.io.IOException;

import okhttp3.FormBody;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.visual.purina_impulso.App.context;


public class FactoryService {

    public static final String SUCCESS_RESPONSE = "1";
    private static final String AUTHORIZATION = "Authorization";

    public static <T> T retrofitService(final Class<T> tClass) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BuildConfig.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(clientCustom())
                .build();
        return retrofit.create(tClass);
    }

    public static <T> T retrofitServiceGet(final Class<T> tClass, String tokenAuth) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BuildConfig.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(clientCustomGet(tokenAuth))
                .build();
        return retrofit.create(tClass);
    }

    public static <T> T retrofitServicePost(final Class<T> tClass) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BuildConfig.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(clientCustomPost())
                .build();
        return retrofit.create(tClass);
    }

    public static <T> T retrofitServiceJsonPost(final Class<T> tClass) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BuildConfig.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(clientCustomJsonPost())
                .build();
        return retrofit.create(tClass);
    }

    public static <T> T retrofitServiceMultipart(final Class<T> tClass, String tokenAuth) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BuildConfig.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(clientCustomMultipart(tokenAuth))
                .build();
        return retrofit.create(tClass);
    }

    private static OkHttpClient clientCustomGet(String tokenAuth) {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new HeaderInterceptorGet(tokenAuth));
        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
            loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            httpClient.addInterceptor(loggingInterceptor);
        }
        return httpClient.build();
    }

    private static OkHttpClient clientCustomPost() {
        ClearableCookieJar cookieJar =
                new PersistentCookieJar(new SetCookieCache(), new SharedPrefsCookiePersistor(context));

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder().cookieJar(cookieJar);
        httpClient.addNetworkInterceptor(new StethoInterceptor());
        httpClient.addInterceptor(new HeaderInterceptorPost());

        httpClient.addInterceptor(chain -> {
            Request request = chain.request();
            Request.Builder requestBuilder = request.newBuilder();
            RequestBody formBody = new FormBody.Builder().build();

            String postBodyString = StringUtil.bodyToString(request.body());
            postBodyString += ((postBodyString.length() > 0) ? "&" : "") + StringUtil.bodyToString(formBody);

            if (postBodyString.length() > 0){
                postBodyString = postBodyString.substring(0,postBodyString.length()-1);
            }

            request = requestBuilder
                    .post(RequestBody.create(MediaType.parse("application/x-www-form-urlencoded;charset=UTF-8"), postBodyString))
                    .build();

            return chain.proceed(request);
        });



        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
            loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            httpClient.addInterceptor(loggingInterceptor);
        }
        return httpClient.build();
    }

    private static OkHttpClient clientCustomJsonPost() {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new HeaderInterceptorPost());
        httpClient.addInterceptor(chain -> {
            Request request = chain.request();
            Request.Builder requestBuilder = request.newBuilder();
            //RequestBody formBody = new FormBody.Builder().build();

            String postBodyString = StringUtil.bodyToString(request.body());
            //postBodyString += ((postBodyString.length() > 0) ? "&" : "") + StringUtil.bodyToString(formBody);

            request = requestBuilder
                    .post(RequestBody.create(MediaType.parse("application/json;charset=UTF-8"), postBodyString))
                    .build();

            return chain.proceed(request);
        });
        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
            loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            httpClient.addInterceptor(loggingInterceptor);
        }
        return httpClient.build();
    }

    private static OkHttpClient clientCustomMultipart(String tokenAuth) {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new HeaderInterceptorMultiPart(tokenAuth));
        httpClient.addInterceptor(chain -> {
            Request request = chain.request();
            return chain.proceed(request);
        });
        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
            loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            httpClient.addInterceptor(loggingInterceptor);
        }
        return httpClient.build();
    }

    private static OkHttpClient clientCustom() {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(chain -> {
            Request request = chain.request();
            Request.Builder requestBuilder = request.newBuilder();
            RequestBody formBody = new FormBody.Builder().build();

            String postBodyString = StringUtil.bodyToString(request.body());
            postBodyString += ((postBodyString.length() > 0) ? "&" : "") + StringUtil.bodyToString(formBody);


            if (postBodyString.length() > 0){
                postBodyString = postBodyString.substring(0,postBodyString.length()-1);
            }

            request = requestBuilder
                    .post(RequestBody.create(MediaType.parse("application/x-www-form-urlencoded;charset=UTF-8"), postBodyString))
                    .build();

            return chain.proceed(request);
        });
        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
            loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            httpClient.addInterceptor(loggingInterceptor);
        }
        return httpClient.build();
    }

    private static class HeaderInterceptor implements Interceptor {
        @Override
        public Response intercept(Chain chain) throws IOException {
            Request original = chain.request();

            Request request = original.newBuilder()
                    .addHeader("Content-Type", "application/x-www-form-urlencoded")
                    .method(original.method(), original.body())
                    .build();
            return chain.proceed(request);
        }
    }

    private static class HeaderInterceptorGet implements Interceptor {

        private String tokenAuth;

        public HeaderInterceptorGet(String token) {
            this.tokenAuth = token;
        }

        @Override
        public Response intercept(Chain chain) throws IOException {
            Request original = chain.request();


            Request request = original.newBuilder()
                    .addHeader("Authorization", "Bearer " + this.tokenAuth)
                    .method(original.method(), original.body())
                    .build();

            Response response = chain.proceed(request);
            String bodyString = new String(response.body().bytes());

            return response.newBuilder()
                    .body(ResponseBody.create(response.body().contentType(), bodyString))
                    .build();
        }
    }

    private static class HeaderInterceptorPost implements Interceptor {

        private String username;
        private String password;

        public HeaderInterceptorPost() {
           // this.username = PreferencesHelper.getUserAutorization().getEmail();
            //this.password = PreferencesHelper.getUserAutorization().getPassword();
        }

        @Override
        public Response intercept(Chain chain) throws IOException {
            Request original = chain.request();

          // String credentials = Credentials.basic(this.username,this.password);
           /* final String basic =
                    "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);*/

            Request request = original.newBuilder()
                    .addHeader("Content-Type","application/x-www-form-urlencoded")
                  //  .addHeader("Authorization",credentials)
                    .method(original.method(), original.body())
                    .build();

            Response response = chain.proceed(request);
            String bodyString = new String(response.body().bytes());

            return response.newBuilder()
                    .body(ResponseBody.create(response.body().contentType(), bodyString))
                    .build();
        }
    }

    private static class HeaderInterceptorMultiPart implements Interceptor {

        private String tokenAuth;

        public HeaderInterceptorMultiPart(String token) {
            this.tokenAuth = token;
        }

        @Override
        public Response intercept(Chain chain) throws IOException {
            Request original = chain.request();


            Request request = original.newBuilder()

                    .addHeader("Authorization", "Bearer " + this.tokenAuth)
                    .method(original.method(), original.body())
                    .build();

            Response response = chain.proceed(request);
            String bodyString = new String(response.body().bytes());

            return response.newBuilder()
                    .body(ResponseBody.create(response.body().contentType(), bodyString))
                    .build();
        }
    }
}

