package com.visual.purina_impulso.data.source;

import com.visual.purina_impulso.base.BaseDataSource;
import com.visual.purina_impulso.domain.Client;

import java.util.ArrayList;


public interface ClientDataSource extends BaseDataSource {

    interface getClientsCallback {
        void onSuccessGetClients(ArrayList<Client> clients);
    }

    interface registerClientCallback {
        void onSuccessRegisterClient();
    }

    interface getClientCallback {
        void onSuccessGetClient(ArrayList<Client> clients);
    }


    void getClients(String employeeId,String type,String search, final getClientsCallback callback, final ErrorCallback errorCallback);

    void registerClient(Client client, final registerClientCallback callback, final ErrorCallback errorCallback);

    void getClient(Client client, final getClientCallback callback, final ErrorCallback errorCallback);

}
