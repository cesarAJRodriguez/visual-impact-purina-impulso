package com.visual.purina_impulso.data.source.remote.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;


public class AssistanceRequest {

    @SerializedName("asistencia")
    protected ArrayList<AssistanceDetail> assistance;

    public static class AssistanceDetail{

        @SerializedName("fecha")
        String date;

        @SerializedName("fotoIngreso")
        String startPhoto;

        @SerializedName("fotoSalida")
        String exitPhoto;

        @SerializedName("idEmpleado")
        String employeeId;

        @SerializedName("horaIngreso")
        String startHour;

        @SerializedName("horaSalida")
        String exitHour;

        @SerializedName("longIngreso")
        String startLongitude;

        @SerializedName("latiIngreso")
        String startLatitude;

        @SerializedName("longSalida")
        String exitLongitude;

        @SerializedName("latiSalida")
        String exitLatitude;

        @SerializedName("comentarioIngreso")
        String startComment;

        @SerializedName("comentarioSalida")
        String exitComment;

        public AssistanceDetail(String date,String startPhoto, String exitPhoto, String employeeId, String startHour, String exitHour, String startLongitude, String startLatitude, String exitLongitude, String exitLatitude, String startComment, String exitComment) {
            this.date = date;
            this.startPhoto = startPhoto;
            this.exitPhoto = exitPhoto;
            this.employeeId = employeeId;
            this.startHour = startHour;
            this.exitHour = exitHour;
            this.startLongitude = startLongitude;
            this.startLatitude = startLatitude;
            this.exitLongitude = exitLongitude;
            this.exitLatitude = exitLatitude;
            this.startComment = startComment;
            this.exitComment = exitComment;
        }
    }

    public AssistanceRequest(ArrayList<AssistanceDetail> assistance) {
        this.assistance = assistance;
    }
}
