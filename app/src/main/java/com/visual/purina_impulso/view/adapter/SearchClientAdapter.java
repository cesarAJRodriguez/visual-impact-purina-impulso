package com.visual.purina_impulso.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.visual.purina_impulso.App;
import com.visual.purina_impulso.Constants;
import com.visual.purina_impulso.R;
import com.visual.purina_impulso.domain.Client;
import com.visual.purina_impulso.util.widget.RecyclerAdapter;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class SearchClientAdapter extends RecyclerAdapter<Client> {

    private Context mContext;


    public SearchClientAdapter(OnItemClickListener listener,
                               Context context,
                               ArrayList<Client> clients) {
        super();
        mItemClickListener = listener;
        mList = clients;
        mContext = context;
    }


    @Override
    public ItemClientViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        View view = LayoutInflater.from(context).inflate(R.layout.item_search_client, parent, false);
        final ItemClientViewHolder holder = new ItemClientViewHolder(view);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mItemClickListener != null) mItemClickListener.onItemClick(mList.get(holder.getAdapterPosition()),holder.getAdapterPosition());
            }
        });
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder != null) {
           int totalItem =  getItemCount();
            Client client = mList.get(position);

            if (client != null) {

                ((ItemClientViewHolder)holder).searchClientsClientTextView.setText(client.getCode());
                ((ItemClientViewHolder)holder).searchClientsAddressTextView.setText(client.getAddress());

                ((ItemClientViewHolder)holder).searchClientsAddCheckBox.setChecked(false);
                if (Constants.SEARCH_CLIENTS_CHECK != null){
                    for (int j = 0; j <  Constants.SEARCH_CLIENTS_CHECK.size(); j++) {
                        App.log("search --> "+ Constants.SEARCH_CLIENTS_CHECK.get(j));
                        if (Constants.SEARCH_CLIENTS_CHECK.get(j).equals(String.valueOf(client.getClientId()))){
                            ((ItemClientViewHolder)holder).searchClientsAddCheckBox.setChecked(true);
                            App.log("check --> "+ Constants.SEARCH_CLIENTS_CHECK.get(j));
                        }else{
                            if(!((ItemClientViewHolder)holder).searchClientsAddCheckBox.isChecked()){
                                ((ItemClientViewHolder)holder).searchClientsAddCheckBox.setChecked(false);
                            }
                        }
                    }
                }

                ((ItemClientViewHolder)holder).searchClientsAddCheckBox.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(((ItemClientViewHolder)holder).searchClientsAddCheckBox.isChecked()){
                            Constants.SEARCH_CLIENTS_CHECK.add(String.valueOf(client.getClientId()));
                            App.log("add --> "+ String.valueOf(client.getClientId()));
                        }else{
                            if (Constants.SEARCH_CLIENTS_CHECK != null){
                                for (int j = 0; j <  Constants.SEARCH_CLIENTS_CHECK.size(); j++) {
                                    App.log("search remove--> "+ Constants.SEARCH_CLIENTS_CHECK.get(j));
                                    if (Constants.SEARCH_CLIENTS_CHECK.get(j).equals(String.valueOf(client.getClientId()))){
                                        App.log("remove --> "+ String.valueOf(client.getClientId()));
                                        Constants.SEARCH_CLIENTS_CHECK.remove(Constants.SEARCH_CLIENTS_CHECK.remove(j));
                                    }
                                }
                            }
                        }
                    }
                });
            }
        }
    }


    public static class ItemClientViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.ll_item_search_client) LinearLayout itemSearchClientLinearLayout;
        @BindView(R.id.txt_search_clients_client) TextView searchClientsClientTextView;
        @BindView(R.id.txt_search_clients_address) TextView searchClientsAddressTextView;
        @BindView(R.id.cb_search_clients_add) CheckBox searchClientsAddCheckBox;

        public ItemClientViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
