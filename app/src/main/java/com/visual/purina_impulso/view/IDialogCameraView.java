package com.visual.purina_impulso.view;

public interface IDialogCameraView {
    void callCamera();
    void callGallery();
    void removeItem(int position);
    void showMessageError();
    void SuccessUpload(String size, String path);
}
