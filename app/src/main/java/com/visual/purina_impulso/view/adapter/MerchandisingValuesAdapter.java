package com.visual.purina_impulso.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.visual.purina_impulso.R;
import com.visual.purina_impulso.domain.DetailStockSampligVisit;
import com.visual.purina_impulso.util.widget.RecyclerAdapter;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class MerchandisingValuesAdapter extends RecyclerAdapter<DetailStockSampligVisit> {

    private Context mContext;

    public MerchandisingValuesAdapter(OnItemClickListener listener,
                                      Context context,
                                      ArrayList<DetailStockSampligVisit> detailStockSampligVisits) {
        super();
        mItemClickListener = listener;
        mList = detailStockSampligVisits;
        mContext = context;
    }


    @Override
    public ItemMerchandidingValuesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        View view = LayoutInflater.from(context).inflate(R.layout.item_merchandising, parent, false);

        final ItemMerchandidingValuesViewHolder holder = new ItemMerchandidingValuesViewHolder(view);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mItemClickListener != null) mItemClickListener.onItemClick(mList.get(holder.getAdapterPosition()),holder.getAdapterPosition());
            }
        });
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder != null) {
           int totalItem =  getItemCount();
            DetailStockSampligVisit merchandising = mList.get(position);

            if (merchandising != null) {
                ((ItemMerchandidingValuesViewHolder)holder).merchandisingTextView.setText(merchandising.getMerchandisingName());
                ((ItemMerchandidingValuesViewHolder)holder).countMerchandisingEditText.setEnabled(false);
                ((ItemMerchandidingValuesViewHolder)holder).countMerchandisingEditText.setText(String.valueOf(merchandising.getStock()));

            }
        }
    }


    public static class ItemMerchandidingValuesViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txt_merchandising) TextView merchandisingTextView;
        @BindView(R.id.et_count_merchandising) EditText countMerchandisingEditText;

        public ItemMerchandidingValuesViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
