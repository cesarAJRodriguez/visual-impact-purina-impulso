package com.visual.purina_impulso.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

import com.visual.purina_impulso.util.ViewUtil;

public class TextOpenSansLight extends TextView {

    public TextOpenSansLight(Context context) {
        super(context);
        setupTypeFace();
    }

    public TextOpenSansLight(Context context, AttributeSet attrs) {
        super(context, attrs);
        setupTypeFace();
    }

    public TextOpenSansLight(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setupTypeFace();
    }

    public void setupTypeFace() {
        setTypeface(ViewUtil.openSansLight());
    }
}
