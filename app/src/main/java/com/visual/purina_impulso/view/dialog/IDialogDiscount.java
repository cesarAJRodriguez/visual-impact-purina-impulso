package com.visual.purina_impulso.view.dialog;


public interface IDialogDiscount {
    void closeRequest();
    void generateRequest();
    void deleteMaterial(int position);
    void onRecordDiscount();
}
