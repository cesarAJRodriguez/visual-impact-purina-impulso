package com.visual.purina_impulso.view.dialog;

public interface IDialogVisit {

    interface StartVisit{
        void onConfirmStartVisit();
    }
    interface Visit{
        void onConfirmAssignVisit();
        void onSyncVisitAccept();
        void onSyncVisitCancel();
    }

    interface VisitMenu{
        void onRegisterVisitSuccess();
    }

    interface VisitFinish{
        void onConfirmFinishVisit();
    }

    interface VisitFinishDialog{
        void onFinishVisitDialog();
    }

    interface VisitPriceDialog{
        void onConfirmPriceVisit();
        void onOpenCameraPriceBreak(int position,int priceId);
    }

    interface VisitStockDialog{
        void onConfirmStockVisit();
    }

    interface VisitDeliveryDialog{
        void onConfirmDeliveryVisit();
    }

    interface VisitSendPendingDialog{
        void onConfirmSendPendingVisit();
    }

    interface VisitSendPendingSuccessDialog{
        void onConfirmSendPendingSuccessVisit();
    }

    interface VisitSendIncidenceSuccessDialog{
        void onConfirmSendIncidenceSuccessVisit();
    }

    interface VisitStockFinalDialog{
        void onConfirmStockFinalVisit();
    }
}
