package com.visual.purina_impulso.view.dialog;

import com.visual.purina_impulso.domain.Quiz;


public interface IDialogQuiz {
    void onQuiz(Quiz quiz);
}
