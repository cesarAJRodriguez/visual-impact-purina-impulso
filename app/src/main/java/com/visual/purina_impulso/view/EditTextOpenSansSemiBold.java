package com.visual.purina_impulso.view;

import android.content.Context;
import android.util.AttributeSet;

import com.visual.purina_impulso.util.ViewUtil;


public class EditTextOpenSansSemiBold extends android.support.design.widget.TextInputEditText {

    public EditTextOpenSansSemiBold(Context context) {
        super(context);
        setupTypeFace();
    }

    public EditTextOpenSansSemiBold(Context context, AttributeSet attrs) {
        super(context, attrs);
        setupTypeFace();
    }

    public EditTextOpenSansSemiBold(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setupTypeFace();
    }

    public void setupTypeFace() {
        setTypeface(ViewUtil.openSansSemiBold());
    }
}
