package com.visual.purina_impulso.view.dialog;

import android.content.Context;

public interface IDialogCameraView {
    void callCamera(Context context);
    void callGallery();
    void removeItem(int position);
    void showMessageError();
    void SuccessUpload(String size, String path);
}
