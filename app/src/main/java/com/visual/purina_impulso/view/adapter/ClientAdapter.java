package com.visual.purina_impulso.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.visual.purina_impulso.Constants;
import com.visual.purina_impulso.R;
import com.visual.purina_impulso.domain.Client;
import com.visual.purina_impulso.util.widget.RecyclerAdapter;
import com.visual.purina_impulso.view.dialog.IDialogClient;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class ClientAdapter extends RecyclerAdapter<Client> {

    private Context mContext;
    private IDialogClient iDialog;


    public ClientAdapter(OnItemClickListener listener,
                         IDialogClient iDialogClient,
                         Context context,
                         ArrayList<Client> clients) {
        super();
        mItemClickListener = listener;
        iDialog = iDialogClient;
        mList = clients;
        mContext = context;
    }


    @Override
    public ItemClientViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        View view = LayoutInflater.from(context).inflate(R.layout.item_client, parent, false);
        final ItemClientViewHolder holder = new ItemClientViewHolder(view);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mItemClickListener != null) mItemClickListener.onItemClick(mList.get(holder.getAdapterPosition()),holder.getAdapterPosition());
            }
        });
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder != null) {
           int totalItem =  getItemCount();
            Client client = mList.get(position);

            if (client != null) {
                ((ItemClientViewHolder)holder).clusterClientTextView.setText(client.getCluster());
                ((ItemClientViewHolder)holder).codeClientTextView.setText(String.valueOf(client.getClientId()));
                ((ItemClientViewHolder)holder).reasonClientTextView.setText(client.getCode());
                ((ItemClientViewHolder)holder).addressClientTextView.setText(client.getAddress());
                ((ItemClientViewHolder)holder).itemClientLinearLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (!client.getVisit().equals(Constants.FLAG_CLIENT_VISIT_EXIT)){
                            iDialog.onVisit(client);
                        }
                    }
                });
                if (client.getVisit().equals(Constants.FLAG_CLIENT_VISIT_NO_START)){
                    ((ItemClientViewHolder)holder).statusVisitLinearLayout.setBackground(mContext.getResources().getDrawable(R.drawable.back_visit_empty));
                    ((ItemClientViewHolder)holder).statusVisitTextView.setText(mContext.getResources().getString(R.string.text_client_not_visit));
                }

                if (client.getVisit().equals(Constants.FLAG_CLIENT_VISIT_START)){
                    ((ItemClientViewHolder)holder).statusVisitLinearLayout.setBackground(mContext.getResources().getDrawable(R.drawable.back_visit_start));
                    ((ItemClientViewHolder)holder).statusVisitTextView.setText(client.getStartHour());
                }

                if (client.getVisit().equals(Constants.FLAG_CLIENT_VISIT_EXIT)){
                    ((ItemClientViewHolder)holder).statusVisitLinearLayout.setBackground(mContext.getResources().getDrawable(R.drawable.back_visit_finish));
                    ((ItemClientViewHolder)holder).statusVisitTextView.setText(client.getExitHour());
                }

            }
        }
    }


    public static class ItemClientViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.ll_item_client) LinearLayout itemClientLinearLayout;
        @BindView(R.id.txt_client_cluster) TextView clusterClientTextView;
        @BindView(R.id.txt_client_code) TextView codeClientTextView;
        @BindView(R.id.txt_client_reason) TextView reasonClientTextView;
        @BindView(R.id.txt_client_address) TextView addressClientTextView;
        @BindView(R.id.ll_status_visit) LinearLayout statusVisitLinearLayout;
        @BindView(R.id.txt_status_visit) TextView statusVisitTextView;

        public ItemClientViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
