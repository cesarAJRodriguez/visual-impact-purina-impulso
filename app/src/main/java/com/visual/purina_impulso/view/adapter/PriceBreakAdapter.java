package com.visual.purina_impulso.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.visual.purina_impulso.Constants;
import com.visual.purina_impulso.R;
import com.visual.purina_impulso.domain.Price;
import com.visual.purina_impulso.util.widget.RecyclerAdapter;
import com.visual.purina_impulso.view.dialog.IDialogVisit;

import java.io.File;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.view.View.GONE;


public class PriceBreakAdapter extends RecyclerAdapter<Price> {

    private Context mContext;
    private  IDialogVisit.VisitPriceDialog iDialog;



    public PriceBreakAdapter(OnItemClickListener listener,
                             Context context,
                             ArrayList<Price> prices,
                             IDialogVisit.VisitPriceDialog visitPriceDialog
                              ) {
        super();
        mItemClickListener = listener;
        mList = prices;
        mContext = context;
        iDialog = visitPriceDialog;

    }


    @Override
    public ItemPriceBreakViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        View view = LayoutInflater.from(context).inflate(R.layout.item_price_break, parent, false);
        final ItemPriceBreakViewHolder holder = new ItemPriceBreakViewHolder(view);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mItemClickListener != null) mItemClickListener.onItemClick(mList.get(holder.getAdapterPosition()),holder.getAdapterPosition());
            }
        });
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder != null) {
           int totalItem =  getItemCount();
            Price price = mList.get(position);

            if (price != null) {

                for (int i = 0; i < Constants.SEARCH_PRICE_BREAK.size(); i++) {
                    if (Constants.SEARCH_PRICE_BREAK.get(i).getProductId() == price.getProductId()) {
                        if(Constants.SEARCH_PRICE_BREAK.get(i).getPhotoPath() != null){
                            if(!Constants.SEARCH_PRICE_BREAK.get(i).getPhotoPath().equals("")){
                                File file = new File(Constants.SEARCH_PRICE_BREAK.get(i).getPhotoPath());
                                Picasso.with(mContext).load(file).placeholder(android.R.drawable.progress_horizontal).into(((ItemPriceBreakViewHolder)holder).photoImageView);
                                ((ItemPriceBreakViewHolder)holder).photoImageView.setVisibility(View.VISIBLE);
                            }else{
                                ((ItemPriceBreakViewHolder)holder).photoImageView.setVisibility(GONE);
                            }
                        }else {
                            ((ItemPriceBreakViewHolder)holder).photoImageView.setVisibility(GONE);
                        }
                    }
                }

                for (int i = 0; i < Constants.SEARCH_PRICE_BREAK.size(); i++) {
                    if (Constants.SEARCH_PRICE_BREAK.get(i).getProductId() == price.getProductId()) {

                        if ( Constants.SEARCH_PRICE_BREAK.get(i).getPrice() != null){
                            if (!Constants.SEARCH_PRICE_BREAK.get(i).getPrice().equals("")){
                                ((ItemPriceBreakViewHolder)holder).breakPriceEditText.setText(Constants.SEARCH_PRICE_BREAK.get(i).getPrice());
                            }else{
                                ((ItemPriceBreakViewHolder)holder).breakPriceEditText.setText("");
                            }
                        }else{
                            ((ItemPriceBreakViewHolder)holder).breakPriceEditText.setText("");
                        }

                        if (Constants.SEARCH_PRICE_BREAK.get(i).getPricePromotion() != null){
                            if (!Constants.SEARCH_PRICE_BREAK.get(i).getPricePromotion().equals("")){
                                ((ItemPriceBreakViewHolder)holder).breakPromotionEditText.setText(Constants.SEARCH_PRICE_BREAK.get(i).getPricePromotion());

                            }else{
                                ((ItemPriceBreakViewHolder)holder).breakPromotionEditText.setText("");
                            }
                        }else{
                            ((ItemPriceBreakViewHolder)holder).breakPromotionEditText.setText("");
                        }

                        if (Constants.SEARCH_PRICE_BREAK.get(i).getPriceSuggested() != null){
                            if (!Constants.SEARCH_PRICE_BREAK.get(i).getPriceSuggested().equals("")){
                                ((ItemPriceBreakViewHolder)holder).breakSuggestedEditText.setText(Constants.SEARCH_PRICE_BREAK.get(i).getPriceSuggested());

                            }else{
                                ((ItemPriceBreakViewHolder)holder).breakSuggestedEditText.setText("");
                            }
                        }else{
                            ((ItemPriceBreakViewHolder)holder).breakSuggestedEditText.setText("");
                        }


                        if ( Constants.SEARCH_PRICE_BREAK.get(i).getPriceBreak()== 1){
                            ((ItemPriceBreakViewHolder)holder).priceBreakCheckBox.setChecked(true);
                        }else{
                            ((ItemPriceBreakViewHolder)holder).priceBreakCheckBox.setChecked(false);
                        }
                    }
                }



                ((ItemPriceBreakViewHolder)holder).priceTextView.setText(price.getProductName());

                ((ItemPriceBreakViewHolder)holder).cameraImageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        iDialog.onOpenCameraPriceBreak(position,price.getProductId());
                    }
                });

                ((ItemPriceBreakViewHolder)holder).priceBreakCheckBox.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if( ((ItemPriceBreakViewHolder)holder).priceBreakCheckBox.isChecked()) {
                            for (int i = 0; i < Constants.SEARCH_PRICE_BREAK.size(); i++) {
                                if (Constants.SEARCH_PRICE_BREAK.get(i).getProductId() == price.getProductId()) {
                                    Constants.SEARCH_PRICE_BREAK.get(i).setPriceBreak(1);
                                }
                            }
                        }else{
                            for (int i = 0; i < Constants.SEARCH_PRICE_BREAK.size(); i++) {
                                if (Constants.SEARCH_PRICE_BREAK.get(i).getProductId() == price.getProductId()) {
                                    Constants.SEARCH_PRICE_BREAK.get(i).setPriceBreak(0);
                                }
                            }

                        }
                    }});

                ((ItemPriceBreakViewHolder)holder).breakPriceEditText.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        for (int i = 0; i < Constants.SEARCH_PRICE_BREAK.size(); i++) {
                            if (Constants.SEARCH_PRICE_BREAK.get(i).getProductId() == price.getProductId()) {
                                Constants.SEARCH_PRICE_BREAK.get(i).setPrice(((ItemPriceBreakViewHolder)holder).breakPriceEditText.getText().toString());
                            }
                        }
                    }
                });


                ((ItemPriceBreakViewHolder)holder).breakPromotionEditText.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        for (int i = 0; i < Constants.SEARCH_PRICE_BREAK.size(); i++) {
                            if (Constants.SEARCH_PRICE_BREAK.get(i).getProductId() == price.getProductId()) {
                                Constants.SEARCH_PRICE_BREAK.get(i).setPricePromotion(((ItemPriceBreakViewHolder)holder).breakPromotionEditText.getText().toString());
                            }
                        }
                    }
                });

                ((ItemPriceBreakViewHolder)holder).breakSuggestedEditText.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        for (int i = 0; i < Constants.SEARCH_PRICE_BREAK.size(); i++) {
                            if (Constants.SEARCH_PRICE_BREAK.get(i).getProductId() == price.getProductId()) {
                                Constants.SEARCH_PRICE_BREAK.get(i).setPriceSuggested(((ItemPriceBreakViewHolder)holder).breakSuggestedEditText.getText().toString());
                            }
                        }
                    }
                });



            }
        }
    }


    public static class ItemPriceBreakViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txt_price_break_sku) TextView priceTextView;
        @BindView(R.id.btn_camera) ImageView cameraImageView;
        @BindView(R.id.cb_price_break_break) CheckBox priceBreakCheckBox;
        @BindView(R.id.et_price_break_price_regular) EditText breakPriceEditText;
        @BindView(R.id.et_price_break_price_promotion) EditText breakPromotionEditText;
        @BindView(R.id.et_price_break_suggested) EditText breakSuggestedEditText;
        @BindView(R.id.iv_photo) ImageView photoImageView;

        public ItemPriceBreakViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }


}
