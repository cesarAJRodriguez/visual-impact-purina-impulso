package com.visual.purina_impulso.view.dialog;

public interface IDialogLogOut {
    void onLogOut();
}
