package com.visual.purina_impulso.view;

import android.content.Context;
import android.util.AttributeSet;

import com.visual.purina_impulso.util.ViewUtil;


public class EditTextOpenSansRegular extends android.support.design.widget.TextInputEditText {

    public EditTextOpenSansRegular(Context context) {
        super(context);
        setupTypeFace();
    }

    public EditTextOpenSansRegular(Context context, AttributeSet attrs) {
        super(context, attrs);
        setupTypeFace();
    }

    public EditTextOpenSansRegular(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setupTypeFace();
    }

    public void setupTypeFace() {
        setTypeface(ViewUtil.openSansRegular());
    }
}
