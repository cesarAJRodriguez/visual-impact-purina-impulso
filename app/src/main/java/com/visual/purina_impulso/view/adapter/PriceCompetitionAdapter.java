package com.visual.purina_impulso.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.visual.purina_impulso.Constants;
import com.visual.purina_impulso.R;
import com.visual.purina_impulso.domain.Price;
import com.visual.purina_impulso.util.widget.RecyclerAdapter;
import com.visual.purina_impulso.view.dialog.IDialogClient;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class PriceCompetitionAdapter extends RecyclerAdapter<Price> {

    private Context mContext;
    private IDialogClient iDialog;



    public PriceCompetitionAdapter(OnItemClickListener listener,
                                   Context context,
                                   ArrayList<Price> prices
                              ) {
        super();
        mItemClickListener = listener;
        mList = prices;
        mContext = context;

    }


    @Override
    public ItemPriceCompetitionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        View view = LayoutInflater.from(context).inflate(R.layout.item_price_competition, parent, false);
        final ItemPriceCompetitionViewHolder holder = new ItemPriceCompetitionViewHolder(view);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mItemClickListener != null) mItemClickListener.onItemClick(mList.get(holder.getAdapterPosition()),holder.getAdapterPosition());
            }
        });
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder != null) {
           int totalItem =  getItemCount();
            Price price = mList.get(position);

            if (price != null) {
                    ((ItemPriceCompetitionViewHolder)holder).precioTextView.setText(price.getProductName());


                ((ItemPriceCompetitionViewHolder)holder).precioCheckBox.setChecked(false);
                if (Constants.SEARCH_PRICE_COMPETITION != null){
                    for (int j = 0; j <  Constants.SEARCH_PRICE_COMPETITION.size(); j++) {
                        if (Constants.SEARCH_PRICE_COMPETITION.get(j).getProductId() == price.getProductId()){
                            ((ItemPriceCompetitionViewHolder)holder).precioCheckBox.setChecked(true);
                            ((ItemPriceCompetitionViewHolder)holder).precioRegularTextView.setText(Constants.SEARCH_PRICE_COMPETITION.get(j).getPrice());
                            ((ItemPriceCompetitionViewHolder)holder).precioPromotionTextView.setText(Constants.SEARCH_PRICE_COMPETITION.get(j).getPricePromotion());
                            ((ItemPriceCompetitionViewHolder)holder).precioRegularTextView.setEnabled(false);
                            ((ItemPriceCompetitionViewHolder)holder).precioPromotionTextView.setEnabled(false);
                        }else{
                            if(!((ItemPriceCompetitionViewHolder)holder).precioCheckBox.isChecked()){
                                ((ItemPriceCompetitionViewHolder)holder).precioCheckBox.setChecked(false);
                                ((ItemPriceCompetitionViewHolder)holder).precioRegularTextView.setText("");
                                ((ItemPriceCompetitionViewHolder)holder).precioPromotionTextView.setText("");
                                ((ItemPriceCompetitionViewHolder)holder).precioRegularTextView.setEnabled(true);
                                ((ItemPriceCompetitionViewHolder)holder).precioPromotionTextView.setEnabled(true);
                            }
                        }
                    }
                }


                    ((ItemPriceCompetitionViewHolder)holder).precioCheckBox.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(((ItemPriceCompetitionViewHolder)holder).precioCheckBox.isChecked()){
                            ((ItemPriceCompetitionViewHolder)holder).precioRegularTextView.setEnabled(false);
                            ((ItemPriceCompetitionViewHolder)holder).precioPromotionTextView.setEnabled(false);

                            Price price1 =  new Price(price.getProductId(),Constants.PRICE_COMPETITION_DISTRIBUTION,((ItemPriceCompetitionViewHolder)holder).precioRegularTextView.getText().toString(),((ItemPriceCompetitionViewHolder)holder).precioPromotionTextView.getText().toString());
                            Constants.SEARCH_PRICE_COMPETITION.add(price1);
                        }else{
                            if (Constants.SEARCH_PRICE_COMPETITION != null){
                                for (int j = 0; j <  Constants.SEARCH_PRICE_COMPETITION.size(); j++) {
                                    if (Constants.SEARCH_PRICE_COMPETITION.get(j).getProductId() == price.getProductId()){
                                        Constants.SEARCH_PRICE_COMPETITION.remove(Constants.SEARCH_PRICE_COMPETITION.remove(j));
                                        ((ItemPriceCompetitionViewHolder)holder).precioRegularTextView.setText("");
                                        ((ItemPriceCompetitionViewHolder)holder).precioPromotionTextView.setText("");
                                        ((ItemPriceCompetitionViewHolder)holder).precioRegularTextView.setEnabled(true);
                                        ((ItemPriceCompetitionViewHolder)holder).precioPromotionTextView.setEnabled(true);

                                    }
                                }
                            }
                        }
                    }
                });


            }
        }
    }


    public static class ItemPriceCompetitionViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txt_price_competition_sku) TextView precioTextView;
        @BindView(R.id.cb_price_competition) CheckBox precioCheckBox;
        @BindView(R.id.et_price_competition_regular) EditText precioRegularTextView;
        @BindView(R.id.et_price_competition_promotion) EditText precioPromotionTextView;

        public ItemPriceCompetitionViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
