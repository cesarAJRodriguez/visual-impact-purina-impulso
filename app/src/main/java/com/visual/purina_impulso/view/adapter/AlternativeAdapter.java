package com.visual.purina_impulso.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.visual.purina_impulso.R;
import com.visual.purina_impulso.domain.Alternative;
import com.visual.purina_impulso.util.widget.RecyclerAdapter;
import com.visual.purina_impulso.view.dialog.IDialogClient;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class AlternativeAdapter extends RecyclerAdapter<Alternative> {

    private Context mContext;
    private IDialogClient iDialog;
    private int lastSelectedPosition = -1;


    public AlternativeAdapter(OnItemClickListener listener,
                              Context context,
                              ArrayList<Alternative> alternatives
                              ) {
        super();
        mItemClickListener = listener;
        mList = alternatives;
        mContext = context;

    }


    @Override
    public ItemQuestionAlternativeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        View view = LayoutInflater.from(context).inflate(R.layout.item_question_alternative, parent, false);
        final ItemQuestionAlternativeViewHolder holder = new ItemQuestionAlternativeViewHolder(view);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mItemClickListener != null) mItemClickListener.onItemClick(mList.get(holder.getAdapterPosition()),holder.getAdapterPosition());
            }
        });
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder != null) {
           int totalItem =  getItemCount();
            Alternative alternative = mList.get(position);

            if (alternative != null) {

                ((ItemQuestionAlternativeViewHolder)holder).alternativeTextView.setText(alternative.getAlternativeName());

                if (alternative.getAlternativeId()== 387 && alternative.getQuestionId() == 200){
                    ((ItemQuestionAlternativeViewHolder)holder).alternativeRadioButton.setChecked(true);
                }

                ((ItemQuestionAlternativeViewHolder)holder).alternativeRadioButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {



                    }
                });
            }
        }
    }


    public static class ItemQuestionAlternativeViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txt_alternative_question) TextView alternativeTextView;
        @BindView(R.id.rb_alternative) RadioButton alternativeRadioButton;
        @BindView(R.id.rg_alternative) RadioGroup alternativeRadioGroup;

        public ItemQuestionAlternativeViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
