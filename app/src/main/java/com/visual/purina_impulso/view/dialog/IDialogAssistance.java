package com.visual.purina_impulso.view.dialog;



public interface IDialogAssistance {
    void onRegisterSuccess();
    void onConfirmSendAssistancePending();
}
