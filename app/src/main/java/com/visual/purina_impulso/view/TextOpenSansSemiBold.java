package com.visual.purina_impulso.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

import com.visual.purina_impulso.util.ViewUtil;


public class TextOpenSansSemiBold extends TextView {

    public TextOpenSansSemiBold(Context context) {
        super(context);
        setupTypeFace();
    }

    public TextOpenSansSemiBold(Context context, AttributeSet attrs) {
        super(context, attrs);
        setupTypeFace();
    }

    public TextOpenSansSemiBold(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setupTypeFace();
    }

    public void setupTypeFace() {
        setTypeface(ViewUtil.openSansSemiBold());
    }
}