package com.visual.purina_impulso.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.visual.purina_impulso.Constants;
import com.visual.purina_impulso.R;
import com.visual.purina_impulso.domain.Merchandising;
import com.visual.purina_impulso.util.widget.RecyclerAdapter;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;



public class MerchandisingAdapter extends RecyclerAdapter<Merchandising> {

    private Context mContext;

    public MerchandisingAdapter(OnItemClickListener listener,
                                Context context,
                                ArrayList<Merchandising> merchandisings) {
        super();
        mItemClickListener = listener;
        mList = merchandisings;
        mContext = context;
    }


    @Override
    public ItemMerchandidingViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        View view = LayoutInflater.from(context).inflate(R.layout.item_merchandising, parent, false);

        final ItemMerchandidingViewHolder holder = new ItemMerchandidingViewHolder(view);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mItemClickListener != null) mItemClickListener.onItemClick(mList.get(holder.getAdapterPosition()),holder.getAdapterPosition());
            }
        });
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder != null) {
           int totalItem =  getItemCount();
            Merchandising merchandising = mList.get(position);

            if (merchandising != null) {
                ((ItemMerchandidingViewHolder)holder).merchandisingTextView.setText(merchandising.getName());

                if (Constants.MERCHANDISING_LIST != null){
                    for (int j = 0; j <  Constants.MERCHANDISING_LIST.size(); j++) {
                        if (Constants.MERCHANDISING_LIST.get(j).getMerchandisingId() == merchandising.getMerchandisingId()){
                            ((ItemMerchandidingViewHolder)holder).countMerchandisingEditText.setText(Constants.MERCHANDISING_LIST.get(j).getStock());
                        }else{
                            ((ItemMerchandidingViewHolder)holder).countMerchandisingEditText.setText("0");

                        }
                    }
                }


                ((ItemMerchandidingViewHolder)holder).countMerchandisingEditText.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        if (((ItemMerchandidingViewHolder)holder).countMerchandisingEditText.getText().toString().isEmpty()){
                            ((ItemMerchandidingViewHolder)holder).countMerchandisingEditText.setText("0");
                            if (Constants.MERCHANDISING_LIST != null){
                                for (int j = 0; j <  Constants.MERCHANDISING_LIST.size(); j++) {
                                    if (Constants.MERCHANDISING_LIST.get(j).getMerchandisingId() == merchandising.getMerchandisingId()){
                                        Constants.MERCHANDISING_LIST.remove(Constants.MERCHANDISING_LIST.remove(j));
                                    }
                                }
                            }
                        }else{
                            if (!((ItemMerchandidingViewHolder)holder).countMerchandisingEditText.getText().toString().equals("0") ){
                                Merchandising merchandising1 = new Merchandising();
                                merchandising1.setMerchandisingId(merchandising.getMerchandisingId());
                                merchandising1.setName(merchandising.getName());
                                merchandising1.setStock(Integer.parseInt(((ItemMerchandidingViewHolder)holder).countMerchandisingEditText.getText().toString()));

                                if (Constants.MERCHANDISING_LIST != null){
                                    int index = -1;
                                    for (int j = 0; j <  Constants.MERCHANDISING_LIST.size(); j++) {
                                        if (Constants.MERCHANDISING_LIST.get(j).getMerchandisingId() == merchandising.getMerchandisingId()){
                                            index = j;
                                        }
                                    }

                                    if (index != -1){
                                        Constants.MERCHANDISING_LIST.remove(Constants.MERCHANDISING_LIST.remove(index));
                                        Constants.MERCHANDISING_LIST.add(merchandising1);
                                    }else{
                                        Constants.MERCHANDISING_LIST.add(merchandising1);
                                    }
                                }else{
                                    Constants.MERCHANDISING_LIST.add(merchandising1);
                                }
                            }else{
                                if (Constants.MERCHANDISING_LIST != null){
                                    for (int j = 0; j <  Constants.MERCHANDISING_LIST.size(); j++) {
                                        if (Constants.MERCHANDISING_LIST.get(j).getMerchandisingId() == merchandising.getMerchandisingId()){
                                            Constants.MERCHANDISING_LIST.remove(Constants.MERCHANDISING_LIST.remove(j));
                                        }
                                    }
                                }
                            }
                        }



                    }
                });

            }
        }
    }


    public static class ItemMerchandidingViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txt_merchandising) TextView merchandisingTextView;
        @BindView(R.id.et_count_merchandising) EditText countMerchandisingEditText;

        public ItemMerchandidingViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
