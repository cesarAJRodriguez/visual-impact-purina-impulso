package com.visual.purina_impulso.view;

import android.content.Context;
import android.util.AttributeSet;

import com.visual.purina_impulso.util.ViewUtil;


public class AutoCompleteOpenSansSemiBold extends android.support.v7.widget.AppCompatAutoCompleteTextView {

    public AutoCompleteOpenSansSemiBold(Context context) {
        super(context);
        setupTypeFace();
    }

    public AutoCompleteOpenSansSemiBold(Context context, AttributeSet attrs) {
        super(context, attrs);
        setupTypeFace();
    }

    public AutoCompleteOpenSansSemiBold(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setupTypeFace();
    }

    public void setupTypeFace() {
        setTypeface(ViewUtil.openSansSemiBold());
    }
}
