package com.visual.purina_impulso.view;

import android.content.Context;
import android.util.AttributeSet;

import com.visual.purina_impulso.util.ViewUtil;


public class EditTextOpenSansBold extends android.support.design.widget.TextInputEditText {

    public EditTextOpenSansBold(Context context) {
        super(context);
        setupTypeFace();
    }

    public EditTextOpenSansBold(Context context, AttributeSet attrs) {
        super(context, attrs);
        setupTypeFace();
    }

    public EditTextOpenSansBold(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setupTypeFace();
    }

    public void setupTypeFace() {
        setTypeface(ViewUtil.openSansBold());
    }
}
