package com.visual.purina_impulso.view.dialog;

import com.visual.purina_impulso.domain.Client;

public interface IDialogClient {
    void onVisit(Client client);
}
