package com.visual.purina_impulso.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.visual.purina_impulso.R;
import com.visual.purina_impulso.domain.Quiz;
import com.visual.purina_impulso.util.widget.RecyclerAdapter;
import com.visual.purina_impulso.view.dialog.IDialogQuiz;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class QuizAdapter extends RecyclerAdapter<Quiz> {

    private Context mContext;
    private IDialogQuiz iDialog;


    public QuizAdapter(OnItemClickListener listener,
                       IDialogQuiz iDialogQuiz,
                       Context context,
                       ArrayList<Quiz> quizzes) {
        super();
        mItemClickListener = listener;
        iDialog = iDialogQuiz;
        mList = quizzes;
        mContext = context;
    }


    @Override
    public ItemClientViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        View view = LayoutInflater.from(context).inflate(R.layout.item_quiz, parent, false);

        final ItemClientViewHolder holder = new ItemClientViewHolder(view);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mItemClickListener != null) mItemClickListener.onItemClick(mList.get(holder.getAdapterPosition()),holder.getAdapterPosition());
            }
        });
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder != null) {
           int totalItem =  getItemCount();
            Quiz quiz = mList.get(position);

            if (quiz != null) {
                ((ItemClientViewHolder)holder).quizQuestionButton.setText(quiz.getQuizName());
                ((ItemClientViewHolder)holder).quizQuestionButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        iDialog.onQuiz(quiz);
                    }
                });
            }
        }
    }


    public static class ItemClientViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.btn_quiz_question) Button quizQuestionButton;

        public ItemClientViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
