package com.visual.purina_impulso.view.dialog;

public interface IDialogSync {
    void onSyncSuccess();
    void onSyncConfirmAccept();

    interface clearDialog{
        void onClearDB();
    }
}

