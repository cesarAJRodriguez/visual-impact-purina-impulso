package com.visual.purina_impulso.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.visual.purina_impulso.Constants;
import com.visual.purina_impulso.R;
import com.visual.purina_impulso.domain.Alternative;
import com.visual.purina_impulso.domain.Question;
import com.visual.purina_impulso.util.widget.RecyclerAdapter;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;



public class QuestionAdapter extends RecyclerAdapter<Question>  {

    private Context mContext;
    private ArrayList<Alternative> mAlternatives;



    public QuestionAdapter(OnItemClickListener listener,
                           Context context,
                           ArrayList<Question> questions,
                           ArrayList<Alternative> alternatives) {
        super();
        mItemClickListener = listener;

        mList = questions;
        mContext = context;
        mAlternatives = alternatives;

    }


    @Override
    public ItemQuestionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        View view = LayoutInflater.from(context).inflate(R.layout.item_question_quiz, parent, false);



        final ItemQuestionViewHolder holder = new ItemQuestionViewHolder(view);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mItemClickListener != null) mItemClickListener.onItemClick(mList.get(holder.getAdapterPosition()),holder.getAdapterPosition());
            }
        });
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder != null) {
           int totalItem =  getItemCount();
            Question question = mList.get(position);

            if (question != null) {

                    ((ItemQuestionViewHolder)holder).questionTextView.setText(question.getQuestionName());
                    if (question.getTypeQuestion() == Constants.FLAG_TYPE_QUESTION_OPTION){
                        ((ItemQuestionViewHolder)holder).questionAnswerEditText.setVisibility(View.GONE);
                        ((ItemQuestionViewHolder)holder).alternativeRadioGroup.setVisibility(View.VISIBLE);
                        ((ItemQuestionViewHolder)holder).alternativeRadioGroup.removeAllViews();
                        for (int i= 0; i<mAlternatives.size();i++){
                            if (mAlternatives.get(i).getQuestionId() == question.getQuestionId()  ){
                                RadioButton rb = new RadioButton(QuestionAdapter.this.mContext);
                                rb.setId(mAlternatives.get(i).getAlternativeId());
                                rb.setText(mAlternatives.get(i).getAlternativeName()) ;
                                rb.setChecked(false);

                                for (int j=0;j<Constants.ALTERNATIVES_QUESTION_CHECKD.size();j++){
                                    if (Constants.ALTERNATIVES_QUESTION_CHECKD.get(j).getQuestionId() == question.getQuestionId()){
                                        if (Constants.ALTERNATIVES_QUESTION_CHECKD.get(j).getAlternativeId() != 0){
                                            if (Constants.ALTERNATIVES_QUESTION_CHECKD.get(j).getAlternativeId() == rb.getId()){
                                                rb.setChecked(true);
                                            }
                                        }
                                    }
                                }

                                ((ItemQuestionViewHolder)holder).alternativeRadioGroup.addView(rb);



                                rb.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        for (int i=0;i<Constants.ALTERNATIVES_QUESTION_CHECKD.size();i++){
                                            if (Constants.ALTERNATIVES_QUESTION_CHECKD.get(i).getQuestionId() == question.getQuestionId()){
                                                Constants.ALTERNATIVES_QUESTION_CHECKD.get(i).setAlternativeId(rb.getId());
                                            }
                                        }
                                    }
                                });
                            }
                        }
                    }else{
                        ((ItemQuestionViewHolder)holder).alternativeRadioGroup.setVisibility(View.GONE);
                        ((ItemQuestionViewHolder)holder).questionAnswerEditText.setVisibility(View.VISIBLE);

                        for (int i=0;i<Constants.ALTERNATIVES_QUESTION_CHECKD.size();i++){
                            if (Constants.ALTERNATIVES_QUESTION_CHECKD.get(i).getQuestionId() == question.getQuestionId()){
                                    ((ItemQuestionViewHolder)holder).questionAnswerEditText.setText(Constants.ALTERNATIVES_QUESTION_CHECKD.get(i).getAnswer());
                            }
                        }


                        ((ItemQuestionViewHolder)holder).questionAnswerEditText.addTextChangedListener(new TextWatcher() {
                            @Override
                            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                            }

                            @Override
                            public void onTextChanged(CharSequence s, int start, int before, int count) {

                            }

                            @Override
                            public void afterTextChanged(Editable editable) {
                                for (int i=0;i<Constants.ALTERNATIVES_QUESTION_CHECKD.size();i++){
                                    if (Constants.ALTERNATIVES_QUESTION_CHECKD.get(i).getQuestionId() == question.getQuestionId()){
                                        Constants.ALTERNATIVES_QUESTION_CHECKD.get(i).setAnswer( ((ItemQuestionViewHolder)holder).questionAnswerEditText.getText().toString());
                                    }
                                }
                            }
                        });
                    }
            }
        }
    }


    public static class ItemQuestionViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txt_question) TextView questionTextView;
        @BindView(R.id.et_question_answer) EditText questionAnswerEditText;
        @BindView(R.id.rg_alternative) RadioGroup alternativeRadioGroup;

        public ItemQuestionViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }




}
