package com.visual.purina_impulso.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Button;

import com.visual.purina_impulso.util.ViewUtil;



public class ButtonOpenSansBold extends Button {
    public ButtonOpenSansBold(Context context) {
        super(context);
        setupTextView();
    }

    public ButtonOpenSansBold(Context context, AttributeSet attrs) {
        super(context, attrs);
        setupTextView();
    }

    public ButtonOpenSansBold(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setupTextView();
    }

    public void setupTextView() {
        setTypeface(ViewUtil.openSansBold());
    }
}
