package com.visual.purina_impulso.domain;

public class User {

    private String assistance;
    private int employeeId;
    private int systemId;
    private int typeId;
    private String names;
    private String userChannel;
    private String userType;


    public User(String assistance, int employeeId, int systemId, int typeId, String names, String userChannel,String userType){
        this.assistance = assistance;
        this.employeeId = employeeId;
        this.systemId = systemId;
        this.typeId = typeId;
        this.names = names;
        this.userChannel = userChannel;
        this.userType = userType;
    }

    public User(){
        this.assistance = "";
        this.employeeId = 0;
        this.systemId = 0;
        this.typeId = 0;
        this.names = "";
        this.userChannel = "";
        this.userType = "";
    }


    public String getAssistance() {
        return assistance;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public int getSystemId() {
        return systemId;
    }

    public int getTypeId() {
        return typeId;
    }

    public String getNames() {
        return names;
    }

    public String getUserChannel() {
        return userChannel;
    }

    public String getUserType() {
        return userType;
    }
}
