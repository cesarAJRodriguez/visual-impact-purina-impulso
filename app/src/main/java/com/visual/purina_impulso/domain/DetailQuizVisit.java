package com.visual.purina_impulso.domain;

import android.os.Parcel;
import android.os.Parcelable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;


public class DetailQuizVisit extends RealmObject implements Parcelable {

    @PrimaryKey
    private String id;

    private String quizVisitId;
    private int questionId;

    private String visitId;

    private int employeeId;
    private String date;
    private int clientId;
    private int quizId;
    private int alternativeId;
    private String answer;
    private String pending;

    public DetailQuizVisit(String id,String quizVisitId, int questionId, String visitId, int employeeId, String date, int clientId, int quizId, int alternativeId, String answer, String pending) {
        this.id = id;
        this.quizVisitId = quizVisitId;
        this.questionId = questionId;
        this.visitId = visitId;
        this.employeeId = employeeId;
        this.date = date;
        this.clientId = clientId;
        this.quizId = quizId;
        this.alternativeId = alternativeId;
        this.answer = answer;
        this.pending = pending;
    }

    public DetailQuizVisit(){
        this.id = "";
        this.quizVisitId = "";
        this.questionId = 0;
        this.visitId = "";
        this.employeeId = 0;
        this.date = "";
        this.clientId = 0;
        this.quizId = 0;
        this.alternativeId = 0;
        this.answer = "";
        this.pending = "";
    }

    public String getQuizVisitId() {
        return quizVisitId;
    }

    public void setQuizVisitId(String quizVisitId) {
        this.quizVisitId = quizVisitId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getQuestionId() {
        return questionId;
    }

    public void setQuestionId(int questionId) {
        this.questionId = questionId;
    }

    public String getVisitId() {
        return visitId;
    }

    public void setVisitId(String visitId) {
        this.visitId = visitId;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public int getQuizId() {
        return quizId;
    }

    public void setQuizId(int quizId) {
        this.quizId = quizId;
    }

    public int getAlternativeId() {
        return alternativeId;
    }

    public void setAlternativeId(int alternativeId) {
        this.alternativeId = alternativeId;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getPending() {
        return pending;
    }

    public void setPending(String pending) {
        this.pending = pending;
    }

    protected DetailQuizVisit(Parcel in) {
        id = in.readString();;
        quizVisitId = in.readString();
        questionId = in.readInt();
        visitId = in.readString();
        employeeId = in.readInt();
        date = in.readString();
        clientId = in.readInt();
        quizId = in.readInt();
        questionId = in.readInt();
        alternativeId = in.readInt();
        answer = in.readString();
        pending = in.readString();
    }

    public static final Creator<DetailQuizVisit> CREATOR = new Creator<DetailQuizVisit>() {
        @Override
        public DetailQuizVisit createFromParcel(Parcel in) {
            return new DetailQuizVisit(in);
        }

        @Override
        public DetailQuizVisit[] newArray(int size) {
            return new DetailQuizVisit[size];
        }
    };



    @Override
    public int describeContents() {
        return 0;
    }


    @Override
    public void writeToParcel(Parcel parcel, int i) {

        parcel.writeString(id);
        parcel.writeString(quizVisitId);
        parcel.writeInt(questionId);
        parcel.writeString(visitId);
        parcel.writeInt(employeeId);
        parcel.writeString(date);
        parcel.writeInt(clientId);
        parcel.writeInt(quizId);
        parcel.writeInt(questionId);
        parcel.writeInt(alternativeId);
        parcel.writeString(answer);
        parcel.writeString(pending);

    }
}
