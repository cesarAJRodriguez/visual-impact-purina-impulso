package com.visual.purina_impulso.domain;

import android.os.Parcel;
import android.os.Parcelable;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Sync extends RealmObject implements Parcelable{

    @PrimaryKey
    private String id;

    private RealmList<Client> client;
    private RealmList<Module> module;
    private RealmList<Photo> photo;
    private RealmList<Incidence> incidence;
    private RealmList<Quiz> quiz;
    private RealmList<Question> question;
    private RealmList<Alternative> alternative;
    private RealmList<Price> price;
    private RealmList<ProductCategory> productCategories;
    private RealmList<Merchandising> merchandisings;

    private RealmList<SubCategoryProduct> subCategoryProducts ;
    private RealmList<BrandProduct> brandProducts ;
    private RealmList<BaseProduct> baseProducts ;
    private RealmList<Product> products ;

    private RealmList<ListAdvanceDay> listAdvanceDays ;
    private RealmList<ListBusinessDay> listBusinessDays ;
    private RealmList<ListFee> listFees ;
    private RealmList<ListSale> listSales ;
    private RealmList<ListVisit> listVisits ;
    private RealmList<ListFeeImpulso> listFeeImpulsos ;



    public Sync( RealmList<Client> client,RealmList<Module> module,RealmList<Photo> photo,RealmList<Incidence> incidence,
                RealmList<Quiz> quiz,RealmList<Question> question,RealmList<Alternative> alternative,RealmList<Price> price,RealmList<ProductCategory> productCategories,
                 RealmList<Merchandising> merchandisings,
                 RealmList<SubCategoryProduct> subCategoryProducts, RealmList<BrandProduct> brandProducts,
                 RealmList<BaseProduct> baseProducts, RealmList<Product> products,
                 RealmList<ListAdvanceDay> listAdvanceDays, RealmList<ListBusinessDay> listBusinessDays,
                 RealmList<ListFee> listFees, RealmList<ListSale> listSales,
                 RealmList<ListVisit> listVisits, RealmList<ListFeeImpulso> listFeeImpulsos
                 ) {
        this.client = client;
        this.module = module;
        this.photo = photo;
        this.incidence = incidence;
        this.quiz = quiz;
        this.question = question;
        this.alternative = alternative;
        this.price = price;
        this.productCategories = productCategories;
        this.merchandisings = merchandisings;

        this.subCategoryProducts = subCategoryProducts;
        this.brandProducts = brandProducts;
        this.baseProducts = baseProducts;
        this.products = products;


        this.listAdvanceDays = listAdvanceDays;
        this.listBusinessDays = listBusinessDays;
        this.listFees = listFees;
        this.listSales = listSales;
        this.listVisits = listVisits;
        this.listFeeImpulsos = listFeeImpulsos;
    }



    public Sync(){
        this.client = new RealmList<Client>();
        this.module = new RealmList<Module>();
        this.photo = new RealmList<Photo>();
        this.incidence = new RealmList<Incidence>();
        this.quiz = new RealmList<Quiz>();
        this.question = new RealmList<Question>();
        this.alternative = new RealmList<Alternative>();
        this.price = new RealmList<Price>();
        this.productCategories = new RealmList<ProductCategory>();
        this.merchandisings = new RealmList<Merchandising>();

        this.subCategoryProducts = new RealmList<SubCategoryProduct>();
        this.brandProducts = new RealmList<BrandProduct>();
        this.baseProducts = new RealmList<BaseProduct>();
        this.products = new RealmList<Product>();

        this.listAdvanceDays = new RealmList<ListAdvanceDay>();
        this.listBusinessDays = new RealmList<ListBusinessDay>();
        this.listFees = new RealmList<ListFee>();
        this.listSales = new RealmList<ListSale>();
        this.listVisits = new RealmList<ListVisit>();
        this.listFeeImpulsos = new RealmList<ListFeeImpulso>();

    }


    public static final Creator<Sync> CREATOR = new Creator<Sync>() {
        @Override
        public Sync createFromParcel(Parcel in) {
            return new Sync(in);
        }

        @Override
        public Sync[] newArray(int size) {
            return new Sync[size];
        }
    };

    public RealmList<Client> getClient() {
        return client;
    }

    public void setClient(RealmList<Client> client) {
        this.client = client;
    }

    protected Sync(Parcel source) {
        client = new RealmList<>();
        source.readList(client,Client.class.getClassLoader());

        module = new RealmList<>();
        source.readList(module,Module.class.getClassLoader());

        photo = new RealmList<>();
        source.readList(photo,Photo.class.getClassLoader());

        incidence = new RealmList<>();
        source.readList(incidence,Incidence.class.getClassLoader());

        quiz = new RealmList<>();
        source.readList(quiz,Quiz.class.getClassLoader());

        question = new RealmList<>();
        source.readList(question,Question.class.getClassLoader());

        alternative = new RealmList<>();
        source.readList(alternative,Alternative.class.getClassLoader());

        price = new RealmList<>();
        source.readList(price,Price.class.getClassLoader());

        productCategories = new RealmList<>();
        source.readList(productCategories,ProductCategory.class.getClassLoader());

        merchandisings = new RealmList<>();
        source.readList(merchandisings,Merchandising.class.getClassLoader());

        subCategoryProducts = new RealmList<>();
        source.readList(subCategoryProducts,SubCategoryProduct.class.getClassLoader());

        brandProducts = new RealmList<>();
        source.readList(brandProducts,BrandProduct.class.getClassLoader());

        baseProducts = new RealmList<>();
        source.readList(baseProducts,BaseProduct.class.getClassLoader());

        products = new RealmList<>();
        source.readList(products,Product.class.getClassLoader());

        listAdvanceDays = new RealmList<>();
        source.readList(listAdvanceDays,ListAdvanceDay.class.getClassLoader());

        listBusinessDays = new RealmList<>();
        source.readList(listBusinessDays,ListBusinessDay.class.getClassLoader());

        listFees = new RealmList<>();
        source.readList(listFees,ListFee.class.getClassLoader());

        listSales = new RealmList<>();
        source.readList(listSales,ListSale.class.getClassLoader());

        listVisits = new RealmList<>();
        source.readList(listVisits,ListVisit.class.getClassLoader());

        listFeeImpulsos = new RealmList<>();
        source.readList(listFeeImpulsos,ListFeeImpulso.class.getClassLoader());


    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(client);
        dest.writeList(module);
        dest.writeList(photo);
        dest.writeList(incidence);
        dest.writeList(quiz);
        dest.writeList(question);
        dest.writeList(alternative);
        dest.writeList(price);
        dest.writeList(productCategories);
        dest.writeList(merchandisings);

        dest.writeList(subCategoryProducts);
        dest.writeList(brandProducts);
        dest.writeList(baseProducts);
        dest.writeList(products);

        dest.writeList(listAdvanceDays);
        dest.writeList(listBusinessDays);
        dest.writeList(listFees);
        dest.writeList(listSales);
        dest.writeList(listVisits);
        dest.writeList(listFeeImpulsos);
    }

}
