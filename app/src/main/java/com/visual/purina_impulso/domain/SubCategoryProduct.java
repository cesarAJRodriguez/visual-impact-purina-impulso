package com.visual.purina_impulso.domain;

import android.os.Parcel;
import android.os.Parcelable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;


public class SubCategoryProduct extends RealmObject implements Parcelable {

    @PrimaryKey
    private String id;
    private int subCategoryProductId;

    private String subCategoryProduct;

    public SubCategoryProduct(String id,int subCategoryProductId, String subCategoryProduct) {
        this.id = id;
        this.subCategoryProductId = subCategoryProductId;
        this.subCategoryProduct = subCategoryProduct;
    }

    public SubCategoryProduct(){
        this.id = "";
        this.subCategoryProductId = 0;
        this.subCategoryProduct = "";

    }

    public int getSubCategoryProductId() {
        return subCategoryProductId;
    }

    public void setSubCategoryProductId(int subCategoryProductId) {
        this.subCategoryProductId = subCategoryProductId;
    }

    public String getSubCategoryProduct() {
        return subCategoryProduct;
    }

    public void setSubCategoryProduct(String subCategoryProduct) {
        this.subCategoryProduct = subCategoryProduct;
    }

    public String getId() {
        return id;
    }

    public static Creator<SubCategoryProduct> getCREATOR() {
        return CREATOR;
    }

    protected SubCategoryProduct(Parcel source) {
        id = source.readString();
        subCategoryProductId = source.readInt();
        subCategoryProduct = source.readString();


    }

    public static final Creator<SubCategoryProduct> CREATOR = new Creator<SubCategoryProduct>() {
        @Override
        public SubCategoryProduct createFromParcel(Parcel in) {
            return new SubCategoryProduct(in);
        }

        @Override
        public SubCategoryProduct[] newArray(int size) {
            return new SubCategoryProduct[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeInt(subCategoryProductId);
        dest.writeString(subCategoryProduct);

    }

    @Override
    public String toString() {
        return subCategoryProduct;
    }
}
