package com.visual.purina_impulso.domain;

import android.os.Parcel;
import android.os.Parcelable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;



public class ListBusinessDay extends RealmObject implements Parcelable {

    @PrimaryKey
    private String id;
    private String timeId;

    public ListBusinessDay(String id, String timeId) {
        this.id = id;
        this.timeId = timeId;
    }

    public ListBusinessDay() {
        this.id = "";
        this.timeId = "";
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTimeId() {
        return timeId;
    }

    public void setTimeId(String timeId) {
        this.timeId = timeId;
    }

    public static Creator<ListBusinessDay> getCREATOR() {
        return CREATOR;
    }

    protected ListBusinessDay(Parcel source) {
        id = source.readString();
        timeId = source.readString();

    }

    public static final Creator<ListBusinessDay> CREATOR = new Creator<ListBusinessDay>() {
        @Override
        public ListBusinessDay createFromParcel(Parcel in) {
            return new ListBusinessDay(in);
        }

        @Override
        public ListBusinessDay[] newArray(int size) {
            return new ListBusinessDay[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(timeId);
    }

}
