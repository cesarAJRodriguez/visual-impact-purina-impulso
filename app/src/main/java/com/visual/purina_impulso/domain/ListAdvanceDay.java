package com.visual.purina_impulso.domain;

import android.os.Parcel;
import android.os.Parcelable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;



public class ListAdvanceDay extends RealmObject implements Parcelable {

    @PrimaryKey
    private String id;
    private String timeId;

    public ListAdvanceDay(String id, String timeId) {
        this.id = id;
        this.timeId = timeId;
    }

    public ListAdvanceDay() {
        this.id = "";
        this.timeId = "";
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTimeId() {
        return timeId;
    }

    public void setTimeId(String timeId) {
        this.timeId = timeId;
    }

    public static Creator<ListAdvanceDay> getCREATOR() {
        return CREATOR;
    }

    protected ListAdvanceDay(Parcel source) {
        id = source.readString();
        timeId = source.readString();

    }

    public static final Creator<ListAdvanceDay> CREATOR = new Creator<ListAdvanceDay>() {
        @Override
        public ListAdvanceDay createFromParcel(Parcel in) {
            return new ListAdvanceDay(in);
        }

        @Override
        public ListAdvanceDay[] newArray(int size) {
            return new ListAdvanceDay[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(timeId);
    }

}
