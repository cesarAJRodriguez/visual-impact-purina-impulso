package com.visual.purina_impulso.domain;

import android.os.Parcel;
import android.os.Parcelable;


import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;



public class Price extends RealmObject implements Parcelable {

    @PrimaryKey
    private int productId;

    private String productName;
    private String type;
    private int clientId;
    private int bannerId;
    private String typePrice;


    private int distribution;
    private String price;
    private String pricePromotion;

    private int priceBreak;
    private String priceSuggested;
    private String photoPath = "";
    private String photoBase64 = "";


    public Price(int productId, String productName , String type, int clientId, int bannerId,String typePrice){
        this.productId = productId;
        this.productName = productName;
        this.type = type;
        this.clientId = clientId;
        this.bannerId = bannerId;
        this.typePrice = typePrice;
    }

    public Price(int productId, String productName,String typePrice ){
        this.productId = productId;
        this.productName = productName;
        this.typePrice = typePrice;
    }

    public Price(int productId, int distribution, String price , String pricePromotion){
        this.productId = productId;
        this.distribution = distribution;
        this.price = price;
        this.pricePromotion = pricePromotion;
    }



    public Price(){
        this.productId = 0;
        this.productName = "";
        this.type = "";
        this.clientId = 0;
        this.bannerId = 0;
        this.typePrice = "";
        this.distribution = 0;
        this.price = "";
        this.pricePromotion = "";
        this.priceBreak = 0;
        this.priceSuggested = "";
        this.photoPath = "";
        this.photoBase64 = "";
    }


    public int getProductId() {
        return productId;
    }

    public String getProductName() {
        return productName;
    }

    public String getType() {
        return type;
    }

    public int getClientId() {
        return clientId;
    }

    public int getBannerId() {
        return bannerId;
    }

    public String getTypePrice() {
        return typePrice;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public void setBannerId(int bannerId) {
        this.bannerId = bannerId;
    }

    public void setTypePrice(String typePrice) {
        this.typePrice = typePrice;
    }

    public int getDistribution() {
        return distribution;
    }

    public void setDistribution(int distribution) {
        this.distribution = distribution;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPricePromotion() {
        return pricePromotion;
    }

    public void setPricePromotion(String pricePromotion) {
        this.pricePromotion = pricePromotion;
    }

    public int getPriceBreak() {
        return priceBreak;
    }

    public void setPriceBreak(int priceBreak) {
        this.priceBreak = priceBreak;
    }

    public String getPriceSuggested() {
        return priceSuggested;
    }

    public void setPriceSuggested(String priceSuggested) {
        this.priceSuggested = priceSuggested;
    }

    public String getPhotoPath() {
        return photoPath;
    }

    public void setPhotoPath(String photoPath) {
        this.photoPath = photoPath;
    }

    public String getPhotoBase64() {
        return photoBase64;
    }

    public void setPhotoBase64(String photoBase64) {
        this.photoBase64 = photoBase64;
    }

    public static Creator<Price> getCREATOR() {
        return CREATOR;
    }

    protected Price(Parcel source) {
        productId = source.readInt();
        productName = source.readString();
        type = source.readString();
        clientId = source.readInt();
        bannerId = source.readInt();
        typePrice = source.readString();
    }

    public static final Creator<Price> CREATOR = new Creator<Price>() {
        @Override
        public Price createFromParcel(Parcel in) {
            return new Price(in);
        }

        @Override
        public Price[] newArray(int size) {
            return new Price[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(productId);
        dest.writeString(productName);
        dest.writeString(type);
        dest.writeInt(clientId);
        dest.writeInt(bannerId);
        dest.writeString(typePrice);
    }
}
