package com.visual.purina_impulso.domain;

import android.os.Parcel;
import android.os.Parcelable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;


public class ActionVisit extends RealmObject implements Parcelable {

    @PrimaryKey
    private String id;

    private String visitId;

    private int employeeId;
    private String date;
    private int clientId;
    private int productCategoryId;
    private String product;
    private String mechanics;
    private int price;
    private String expirationDate;
    private String photo;
    private String hour;
    private String pending;

    public ActionVisit(String id, String visitId, int employeeId, String date, int clientId, int productCategoryId, String product, String mechanics, int price, String expirationDate, String photo, String hour,String pending) {
        this.id = id;
        this.visitId = visitId;
        this.employeeId = employeeId;
        this.date = date;
        this.clientId = clientId;
        this.productCategoryId = productCategoryId;
        this.product = product;
        this.mechanics = mechanics;
        this.price = price;
        this.expirationDate = expirationDate;
        this.photo = photo;
        this.hour = hour;
        this.pending = pending;
    }

    public ActionVisit(){
        this.id = "";
        this.visitId = "";
        this.employeeId = 0;
        this.date = "";
        this.clientId = 0;
        this.productCategoryId = 0;
        this.product = "";
        this.mechanics = "";
        this.price = 0;
        this.expirationDate = "";
        this.photo = "";
        this.hour = "";
        this.pending = "";
    }

    protected ActionVisit(Parcel in) {
        id = in.readString();
        visitId = in.readString();
        employeeId = in.readInt();
        date = in.readString();
        clientId = in.readInt();
        productCategoryId = in.readInt();
        product = in.readString();
        mechanics = in.readString();
        price = in.readInt();
        expirationDate = in.readString();
        photo = in.readString();
        hour = in.readString();
        pending = in.readString();
    }

    public static final Creator<ActionVisit> CREATOR = new Creator<ActionVisit>() {
        @Override
        public ActionVisit createFromParcel(Parcel in) {
            return new ActionVisit(in);
        }

        @Override
        public ActionVisit[] newArray(int size) {
            return new ActionVisit[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getVisitId() {
        return visitId;
    }

    public void setVisitId(String visitId) {
        this.visitId = visitId;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public int getProductCategoryId() {
        return productCategoryId;
    }

    public void setProductCategoryId(int productCategoryId) {
        this.productCategoryId = productCategoryId;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getMechanics() {
        return mechanics;
    }

    public void setMechanics(String mechanics) {
        this.mechanics = mechanics;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    public String getPending() {
        return pending;
    }

    public void setPending(String pending) {
        this.pending = pending;
    }

    @Override
    public int describeContents() {
        return 0;
    }


    @Override
    public void writeToParcel(Parcel parcel, int i) {

        parcel.writeString(id);
        parcel.writeString(visitId);
        parcel.writeInt(employeeId);
        parcel.writeString(date);
        parcel.writeInt(clientId);
        parcel.writeInt(productCategoryId);
        parcel.writeString(product);
        parcel.writeString(mechanics);
        parcel.writeInt(price);
        parcel.writeString(expirationDate);
        parcel.writeString(photo);
        parcel.writeString(hour);
        parcel.writeString(pending);

    }
}
