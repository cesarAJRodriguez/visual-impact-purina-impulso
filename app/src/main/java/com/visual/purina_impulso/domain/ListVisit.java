package com.visual.purina_impulso.domain;

import android.os.Parcel;
import android.os.Parcelable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;


public class ListVisit extends RealmObject implements Parcelable {

    @PrimaryKey
    private String id;

    private int clientId;
    private int viProg;
    private int viExc;
    private int viHab;
    private int viEfec;
    private int viIn;


    public ListVisit(String id, int clientId, int viProg, int viExc, int viHab, int viEfec, int viIn) {
        this.id = id;
        this.clientId = clientId;
        this.viProg = viProg;
        this.viExc = viExc;
        this.viHab = viHab;
        this.viEfec = viEfec;
        this.viIn = viIn;

    }

    public ListVisit() {
        this.id = "";
        this.clientId = 0;
        this.viProg = 0;
        this.viExc = 0;
        this.viHab = 0;
        this.viEfec = 0;
        this.viIn = 0;

    }

    public static Creator<ListVisit> getCREATOR() {
        return CREATOR;
    }

    protected ListVisit(Parcel source) {
        id = source.readString();
        clientId = source.readInt();
        viProg = source.readInt();
        viExc = source.readInt();
        viHab = source.readInt();
        viEfec = source.readInt();
        viIn = source.readInt();


    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public int getViProg() {
        return viProg;
    }

    public void setViProg(int viProg) {
        this.viProg = viProg;
    }

    public int getViExc() {
        return viExc;
    }

    public void setViExc(int viExc) {
        this.viExc = viExc;
    }

    public int getViHab() {
        return viHab;
    }

    public void setViHab(int viHab) {
        this.viHab = viHab;
    }

    public int getViEfec() {
        return viEfec;
    }

    public void setViEfec(int viEfec) {
        this.viEfec = viEfec;
    }

    public int getViIn() {
        return viIn;
    }

    public void setViIn(int viIn) {
        this.viIn = viIn;
    }




    public static final Creator<ListVisit> CREATOR = new Creator<ListVisit>() {
        @Override
        public ListVisit createFromParcel(Parcel in) {
            return new ListVisit(in);
        }

        @Override
        public ListVisit[] newArray(int size) {
            return new ListVisit[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeInt(clientId);
        dest.writeInt(viProg);
        dest.writeInt(viExc);
        dest.writeInt(viHab);
        dest.writeInt(viEfec);
        dest.writeInt(viIn);


    }

}
