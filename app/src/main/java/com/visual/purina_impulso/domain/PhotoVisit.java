package com.visual.purina_impulso.domain;

import android.os.Parcel;
import android.os.Parcelable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;


public class PhotoVisit extends RealmObject implements Parcelable {

    @PrimaryKey
    private String id;

    private String visitId;

    private int employeeId;
    private String date;
    private int clientId;
    private int typePhotoId;
    private String photo;
    private String comment;
    private String hour;
    private String pending;


    public PhotoVisit(String id,String visitId, int employeeId, String date, int clientId, int typePhotoId, String photo, String comment, String hour,String pending) {
        this.id = id;
        this.visitId = visitId;
        this.employeeId = employeeId;
        this.date = date;
        this.clientId = clientId;
        this.typePhotoId = typePhotoId;
        this.photo = photo;
        this.comment = comment;
        this.hour = hour;
        this.pending = pending;
    }
    public PhotoVisit(){
        this.id = "";
        this.visitId = "";
        this.employeeId = 0;
        this.date = "";
        this.clientId = 0;
        this.typePhotoId = 0;
        this.photo = "";
        this.comment = "";
        this.hour = "";
        this.pending = "";
    }



    protected PhotoVisit(Parcel in) {
        id = in.readString();
        visitId = in.readString();
        employeeId = in.readInt();
        date = in.readString();
        clientId = in.readInt();
        typePhotoId = in.readInt();
        photo = in.readString();
        comment = in.readString();
        hour = in.readString();
        pending = in.readString();
    }

    public static final Creator<PhotoVisit> CREATOR = new Creator<PhotoVisit>() {
        @Override
        public PhotoVisit createFromParcel(Parcel in) {
            return new PhotoVisit(in);
        }

        @Override
        public PhotoVisit[] newArray(int size) {
            return new PhotoVisit[size];
        }
    };

    public String getId() {
        return id;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public String getDate() {
        return date;
    }

    public int getClientId() {
        return clientId;
    }

    public int getTypePhotoId() {
        return typePhotoId;
    }

    public String getPhoto() {
        return photo;
    }

    public String getComment() {
        return comment;
    }

    public String getHour() {
        return hour;
    }

    public String getPending() {
        return pending;
    }

    public String getVisitId() {
        return visitId;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setVisitId(String visitId) {
        this.visitId = visitId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public void setTypePhotoId(int typePhotoId) {
        this.typePhotoId = typePhotoId;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    public void setPending(String pending) {
        this.pending = pending;
    }

    @Override
    public int describeContents() {
        return 0;
    }


    @Override
    public void writeToParcel(Parcel parcel, int i) {

        parcel.writeString(id);
        parcel.writeString(visitId);
        parcel.writeInt(employeeId);
        parcel.writeString(date);
        parcel.writeInt(clientId);
        parcel.writeInt(typePhotoId);
        parcel.writeString(photo);
        parcel.writeString(comment);
        parcel.writeString(hour);
        parcel.writeString(pending);
    }
}
