package com.visual.purina_impulso.domain;

import android.os.Parcel;
import android.os.Parcelable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;



public class DetailPriceCompetitionVisit extends RealmObject implements Parcelable {

    @PrimaryKey
    private String id ;

    private int productId;

    private String priceCompetitionVisitId;
    private int employeeId;
    private String date;
    private int clientId;
    private int distribution;
    private String price;
    private String pricePromotion;
    private String visitId;
    private String pending;

    public DetailPriceCompetitionVisit(String id,int productId,String priceCompetitionVisitId, int employeeId, String date, int clientId, int distribution, String price, String pricePromotion, String visitId, String pending) {
        this.id = id;
        this.productId = productId;
        this.priceCompetitionVisitId = priceCompetitionVisitId;
        this.employeeId = employeeId;
        this.date = date;
        this.clientId = clientId;
        this.distribution = distribution;
        this.price = price;
        this.pricePromotion = pricePromotion;
        this.visitId = visitId;
        this.pending = pending;
    }

    public DetailPriceCompetitionVisit(){
        this.id = "";
        this.productId = 0;
        this.priceCompetitionVisitId = "";
        this.employeeId = 0;
        this.date = "";
        this.clientId = 0;
        this.distribution = 0;
        this.price = "";
        this.pricePromotion = "";
        this.visitId = "";
        this.pending = "";
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPriceCompetitionVisitId() {
        return priceCompetitionVisitId;
    }

    public void setPriceCompetitionVisitId(String priceCompetitionVisitId) {
        this.priceCompetitionVisitId = priceCompetitionVisitId;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public int getDistribution() {
        return distribution;
    }

    public void setDistribution(int distribution) {
        this.distribution = distribution;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPricePromotion() {
        return pricePromotion;
    }

    public void setPricePromotion(String pricePromotion) {
        this.pricePromotion = pricePromotion;
    }

    public String getVisitId() {
        return visitId;
    }

    public void setVisitId(String visitId) {
        this.visitId = visitId;
    }

    public String getPending() {
        return pending;
    }

    public void setPending(String pending) {
        this.pending = pending;
    }

    public static Creator<DetailPriceCompetitionVisit> getCREATOR() {
        return CREATOR;
    }

    protected DetailPriceCompetitionVisit(Parcel source) {

        id = source.readString();
        productId = source.readInt();
        priceCompetitionVisitId = source.readString();
        employeeId = source.readInt();
        date = source.readString();
        clientId = source.readInt();
        distribution = source.readInt();
        price = source.readString();
        pricePromotion = source.readString();
        visitId = source.readString();
        pending = source.readString();



    }

    public static final Creator<DetailPriceCompetitionVisit> CREATOR = new Creator<DetailPriceCompetitionVisit>() {
        @Override
        public DetailPriceCompetitionVisit createFromParcel(Parcel in) {
            return new DetailPriceCompetitionVisit(in);
        }

        @Override
        public DetailPriceCompetitionVisit[] newArray(int size) {
            return new DetailPriceCompetitionVisit[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeInt(productId);
        dest.writeString(priceCompetitionVisitId);
        dest.writeInt(employeeId);
        dest.writeString(date);
        dest.writeInt(clientId);
        dest.writeInt(distribution);
        dest.writeString(price);
        dest.writeString(pricePromotion);
        dest.writeString(visitId);
        dest.writeString(pending);
    }
}
