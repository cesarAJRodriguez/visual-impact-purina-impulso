package com.visual.purina_impulso.domain;

import android.os.Parcel;
import android.os.Parcelable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;



public class Incidence extends RealmObject implements Parcelable {

    @PrimaryKey
    private int typeIncidenceId;

    private int channelId;
    private String typeIncidenceName;

    public Incidence(int typeIncidenceId,int channelId, String typeIncidenceName){
        this.typeIncidenceId = typeIncidenceId;
        this.channelId = channelId;
        this.typeIncidenceName = typeIncidenceName;
    }


    public Incidence(){
        this.typeIncidenceId = 0;
        this.channelId = 0;
        this.typeIncidenceName = "";
    }

    public int getTypeIncidenceId() {
        return typeIncidenceId;
    }

    public int getChannelId() {
        return channelId;
    }

    public String getTypeIncidenceName() {
        return typeIncidenceName;
    }

    public static Creator<Incidence> getCREATOR() {
        return CREATOR;
    }

    protected Incidence(Parcel source) {
        typeIncidenceId = source.readInt();
        channelId = source.readInt();
        typeIncidenceName = source.readString();
    }

    public static final Creator<Incidence> CREATOR = new Creator<Incidence>() {
        @Override
        public Incidence createFromParcel(Parcel in) {
            return new Incidence(in);
        }

        @Override
        public Incidence[] newArray(int size) {
            return new Incidence[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(typeIncidenceId);
        dest.writeInt(channelId);
        dest.writeString(typeIncidenceName);
    }

    @Override
    public String toString() {
        return typeIncidenceName;
    }
}
