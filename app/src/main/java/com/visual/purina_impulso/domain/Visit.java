package com.visual.purina_impulso.domain;

import android.os.Parcel;
import android.os.Parcelable;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;


public class Visit extends RealmObject implements Parcelable {

    @PrimaryKey
    private String id;

    private int employeeId;
    private String date;
    private int clientId;
    private String systemId;

    private RealmList<StartVisit> startVisit;
    private RealmList<QuizVisit> quizVisit;
    private RealmList<DetailQuizVisit> detailQuizVisit;
    private RealmList<PhotoVisit> photoVisit;
    private RealmList<ActionVisit> actionVisit;
    private RealmList<CommentShooperVisit> commentShooperVisit;
    private RealmList<PriceCompetitionVisit> priceCompetitionVisit;
    private RealmList<DetailPriceCompetitionVisit> detailPriceCompetitionVisit;
    private RealmList<StockSampligVisit> stockSampligVisit;
    private RealmList<DetailStockSampligVisit> detailStockSampligVisit;
    private RealmList<DeliveryDayVisit> deliveryDayVisit;
    private RealmList<PriceBreakVisit> priceBreakVisit;
    private RealmList<DetailPriceBreakVisit> detailPriceBreakVisit;
    private RealmList<StockSamplingFinalVisit> stockSamplingFinalVisit;
    private RealmList<DetailStockSamplingFinalVisit> detailStockSamplingFinalVisit;


       public Visit(String id, int employeeId, String date, int clientId,String systemId, RealmList<StartVisit> startVisit, RealmList<QuizVisit> quizVisit, RealmList<DetailQuizVisit> detailQuizVisit, RealmList<PhotoVisit> photoVisit, RealmList<ActionVisit> actionVisit, RealmList<CommentShooperVisit> commentShooperVisit,RealmList<PriceCompetitionVisit> priceCompetitionVisit , RealmList<DetailPriceCompetitionVisit> detailPriceCompetitionVisit,
                 RealmList<StockSampligVisit> stockSampligVisit,RealmList<DetailStockSampligVisit> detailStockSampligVisit,RealmList<DeliveryDayVisit> deliveryDayVisit,RealmList<PriceBreakVisit> priceBreakVisit,
                 RealmList<DetailPriceBreakVisit> detailPriceBreakVisit,
                    RealmList<StockSamplingFinalVisit> stockSamplingFinalVisit,
                    RealmList<DetailStockSamplingFinalVisit> detailStockSamplingFinalVisit
                    ) {
        this.id = id;
        this.employeeId = employeeId;
        this.date = date;
        this.clientId = clientId;
        this.systemId = systemId;
        this.startVisit = startVisit;
        this.quizVisit = quizVisit;
        this.detailQuizVisit = detailQuizVisit;
        this.photoVisit = photoVisit;
        this.actionVisit = actionVisit;
        this.commentShooperVisit = commentShooperVisit;
        this.priceCompetitionVisit = priceCompetitionVisit;
        this.detailPriceCompetitionVisit = detailPriceCompetitionVisit;
        this.stockSampligVisit = stockSampligVisit;
        this.detailStockSampligVisit = detailStockSampligVisit;
        this.deliveryDayVisit = deliveryDayVisit;
        this.priceBreakVisit = priceBreakVisit;
        this.detailPriceBreakVisit = detailPriceBreakVisit;
        this.stockSamplingFinalVisit = stockSamplingFinalVisit;
        this.detailStockSamplingFinalVisit = detailStockSamplingFinalVisit;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public String getSystemId() {
        return systemId;
    }

    public void setSystemId(String systemId) {
        this.systemId = systemId;
    }

    public RealmList<StartVisit> getStartVisit() {
        return startVisit;
    }

    public void setStartVisit(RealmList<StartVisit> startVisit) {
        this.startVisit = startVisit;
    }

    public RealmList<QuizVisit> getQuizVisit() {
        return quizVisit;
    }

    public void setQuizVisit(RealmList<QuizVisit> quizVisit) {
        this.quizVisit = quizVisit;
    }

    public RealmList<DetailQuizVisit> getDetailQuizVisit() {
        return detailQuizVisit;
    }

    public void setDetailQuizVisit(RealmList<DetailQuizVisit> detailQuizVisit) {
        this.detailQuizVisit = detailQuizVisit;
    }

    public RealmList<PhotoVisit> getPhotoVisit() {
        return photoVisit;
    }

    public void setPhotoVisit(RealmList<PhotoVisit> photoVisit) {
        this.photoVisit = photoVisit;
    }

    public RealmList<ActionVisit> getActionVisit() {
        return actionVisit;
    }

    public void setActionVisit(RealmList<ActionVisit> actionVisit) {
        this.actionVisit = actionVisit;
    }

    public RealmList<CommentShooperVisit> getCommentShooperVisit() {
        return commentShooperVisit;
    }

    public void setCommentShooperVisit(RealmList<CommentShooperVisit> commentShooperVisit) {
        this.commentShooperVisit = commentShooperVisit;
    }

    public RealmList<PriceCompetitionVisit> getPriceCompetitionVisit() {
        return priceCompetitionVisit;
    }

    public void setPriceCompetitionVisit(RealmList<PriceCompetitionVisit> priceCompetitionVisit) {
        this.priceCompetitionVisit = priceCompetitionVisit;
    }

    public RealmList<DetailPriceCompetitionVisit> getDetailPriceCompetitionVisit() {
        return detailPriceCompetitionVisit;
    }

    public void setDetailPriceCompetitionVisit(RealmList<DetailPriceCompetitionVisit> detailPriceCompetitionVisit) {
        this.detailPriceCompetitionVisit = detailPriceCompetitionVisit;
    }

    public RealmList<StockSampligVisit> getStockSampligVisit() {
        return stockSampligVisit;
    }

    public void setStockSampligVisit(RealmList<StockSampligVisit> stockSampligVisit) {
        this.stockSampligVisit = stockSampligVisit;
    }

    public RealmList<DetailStockSampligVisit> getDetailStockSampligVisit() {
        return detailStockSampligVisit;
    }

    public void setDetailStockSampligVisit(RealmList<DetailStockSampligVisit> detailStockSampligVisit) {
        this.detailStockSampligVisit = detailStockSampligVisit;
    }

    public RealmList<DeliveryDayVisit> getDeliveryDayVisit() {
        return deliveryDayVisit;
    }

    public void setDeliveryDayVisit(RealmList<DeliveryDayVisit> deliveryDayVisit) {
        this.deliveryDayVisit = deliveryDayVisit;
    }

    public RealmList<PriceBreakVisit> getPriceBreakVisit() {
        return priceBreakVisit;
    }

    public void setPriceBreakVisit(RealmList<PriceBreakVisit> priceBreakVisit) {
        this.priceBreakVisit = priceBreakVisit;
    }

    public RealmList<DetailPriceBreakVisit> getDetailPriceBreakVisit() {
        return detailPriceBreakVisit;
    }

    public void setDetailPriceBreakVisit(RealmList<DetailPriceBreakVisit> detailPriceBreakVisit) {
        this.detailPriceBreakVisit = detailPriceBreakVisit;
    }

    public RealmList<StockSamplingFinalVisit> getStockSamplingFinalVisit() {
        return stockSamplingFinalVisit;
    }

    public void setStockSamplingFinalVisit(RealmList<StockSamplingFinalVisit> stockSamplingFinalVisit) {
        this.stockSamplingFinalVisit = stockSamplingFinalVisit;
    }

    public RealmList<DetailStockSamplingFinalVisit> getDetailStockSamplingFinalVisit() {
        return detailStockSamplingFinalVisit;
    }

    public void setDetailStockSamplingFinalVisit(RealmList<DetailStockSamplingFinalVisit> detailStockSamplingFinalVisit) {
        this.detailStockSamplingFinalVisit = detailStockSamplingFinalVisit;
    }

    public Visit() {
        this.id = "";
        this.employeeId = 0;
        this.date = "";
        this.clientId = 0;
        this.systemId = "";
        this.startVisit = new RealmList<StartVisit>();
        this.quizVisit = new RealmList<QuizVisit>();
        this.detailQuizVisit = new RealmList<DetailQuizVisit>();
        this.photoVisit = new RealmList<PhotoVisit>();
        this.actionVisit = new RealmList<ActionVisit>();
        this.commentShooperVisit = new RealmList<CommentShooperVisit>();
        this.priceCompetitionVisit = new RealmList<PriceCompetitionVisit>();
        this.detailPriceCompetitionVisit = new RealmList<DetailPriceCompetitionVisit>();
        this.stockSampligVisit = new RealmList<StockSampligVisit>();
        this.detailStockSampligVisit = new RealmList<DetailStockSampligVisit>();
        this.deliveryDayVisit = new RealmList<DeliveryDayVisit>();
        this.priceBreakVisit = new RealmList<PriceBreakVisit>();
        this.detailPriceBreakVisit = new RealmList<DetailPriceBreakVisit>();

        this.stockSamplingFinalVisit = new RealmList<StockSamplingFinalVisit>();
        this.detailStockSamplingFinalVisit = new RealmList<DetailStockSamplingFinalVisit>();
    }

    protected Visit(Parcel source) {
        id = source.readString();
        employeeId = source.readInt();
        date = source.readString();
        clientId = source.readInt();
        systemId = source.readString();


        startVisit = new RealmList<>();
        source.readList(startVisit,StartVisit.class.getClassLoader());

        quizVisit = new RealmList<>();
        source.readList(quizVisit,QuizVisit.class.getClassLoader());

        detailQuizVisit = new RealmList<>();
        source.readList(detailQuizVisit,DetailQuizVisit.class.getClassLoader());

        photoVisit = new RealmList<>();
        source.readList(photoVisit,PhotoVisit.class.getClassLoader());

        actionVisit = new RealmList<>();
        source.readList(actionVisit,ActionVisit.class.getClassLoader());

        commentShooperVisit = new RealmList<>();
        source.readList(commentShooperVisit,CommentShooperVisit.class.getClassLoader());

        priceCompetitionVisit = new RealmList<>();
        source.readList(priceCompetitionVisit,PriceCompetitionVisit.class.getClassLoader());

        detailPriceCompetitionVisit = new RealmList<>();
        source.readList(detailPriceCompetitionVisit,DetailPriceCompetitionVisit.class.getClassLoader());

        stockSampligVisit = new RealmList<>();
        source.readList(stockSampligVisit,StockSampligVisit.class.getClassLoader());

        detailStockSampligVisit = new RealmList<>();
        source.readList(detailStockSampligVisit,DetailStockSampligVisit.class.getClassLoader());

        deliveryDayVisit = new RealmList<>();
        source.readList(deliveryDayVisit,DeliveryDayVisit.class.getClassLoader());

        priceBreakVisit = new RealmList<>();
        source.readList(priceBreakVisit,PriceBreakVisit.class.getClassLoader());

        detailPriceBreakVisit = new RealmList<>();
        source.readList(detailPriceBreakVisit,DetailPriceBreakVisit.class.getClassLoader());

        stockSamplingFinalVisit = new RealmList<>();
        source.readList(stockSamplingFinalVisit,StockSamplingFinalVisit.class.getClassLoader());

        detailStockSamplingFinalVisit = new RealmList<>();
        source.readList(detailStockSamplingFinalVisit,DetailStockSamplingFinalVisit.class.getClassLoader());

    }

   public static final Creator<Visit> CREATOR = new Creator<Visit>() {
        @Override
        public Visit createFromParcel(Parcel in) {
            return new Visit(in);
        }

        @Override
        public Visit[] newArray(int size) {
            return new Visit[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeInt(employeeId);
        dest.writeString(date);
        dest.writeInt(clientId);
        dest.writeString(systemId);
        dest.writeList(startVisit);
        dest.writeList(quizVisit);
        dest.writeList(detailQuizVisit);
        dest.writeList(photoVisit);
        dest.writeList(actionVisit);
        dest.writeList(commentShooperVisit);
        dest.writeList(priceCompetitionVisit);
        dest.writeList(detailPriceCompetitionVisit);
        dest.writeList(stockSampligVisit);
        dest.writeList(detailStockSampligVisit);
        dest.writeList(deliveryDayVisit);
        dest.writeList(priceBreakVisit);
        dest.writeList(detailPriceBreakVisit);
        dest.writeList(stockSamplingFinalVisit);
        dest.writeList(detailStockSamplingFinalVisit);
    }
}
