package com.visual.purina_impulso.domain;

import android.os.Parcel;
import android.os.Parcelable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;



public class ListFee extends RealmObject implements Parcelable {

    @PrimaryKey
    private String id;
    private int clientId;
    private String fee;

    public ListFee(String id, int clientId, String fee) {
        this.id = id;
        this.clientId = clientId;
        this.fee = fee;
    }

    public ListFee() {
        this.id = "";
        this.clientId = 0;
        this.fee = "";
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public String getFee() {
        return fee;
    }

    public void setFee(String fee) {
        this.fee = fee;
    }

    public static Creator<ListFee> getCREATOR() {
        return CREATOR;
    }

    protected ListFee(Parcel source) {
        id = source.readString();
        clientId = source.readInt();
        fee = source.readString();

    }

    public static final Creator<ListFee> CREATOR = new Creator<ListFee>() {
        @Override
        public ListFee createFromParcel(Parcel in) {
            return new ListFee(in);
        }

        @Override
        public ListFee[] newArray(int size) {
            return new ListFee[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeInt(clientId);
        dest.writeString(fee);
    }

}
