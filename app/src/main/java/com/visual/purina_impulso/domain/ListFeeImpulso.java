package com.visual.purina_impulso.domain;

import android.os.Parcel;
import android.os.Parcelable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;



public class ListFeeImpulso extends RealmObject implements Parcelable {

    @PrimaryKey
    private String id;
    private int clientId;
    private int fee;
    private int range;
    private int saleDay;

    public ListFeeImpulso(String id, int clientId, int fee, int range, int saleDay) {
        this.id = id;
        this.clientId = clientId;
        this.fee = fee;
        this.range = range;
        this.saleDay = saleDay;
    }

    public ListFeeImpulso() {
        this.id = "";
        this.clientId = 0;
        this.fee = 0;
        this.range = 0;
        this.saleDay = 0;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public int getFee() {
        return fee;
    }

    public void setFee(int fee) {
        this.fee = fee;
    }

    public int getRange() {
        return range;
    }

    public void setRange(int range) {
        this.range = range;
    }

    public int getSaleDay() {
        return saleDay;
    }

    public void setSaleDay(int saleDay) {
        this.saleDay = saleDay;
    }

    public static Creator<ListFeeImpulso> getCREATOR() {
        return CREATOR;
    }

    protected ListFeeImpulso(Parcel source) {
        id = source.readString();
        clientId = source.readInt();
        fee = source.readInt();
        range = source.readInt();
        saleDay = source.readInt();
    }

    public static final Creator<ListFeeImpulso> CREATOR = new Creator<ListFeeImpulso>() {
        @Override
        public ListFeeImpulso createFromParcel(Parcel in) {
            return new ListFeeImpulso(in);
        }

        @Override
        public ListFeeImpulso[] newArray(int size) {
            return new ListFeeImpulso[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeInt(clientId);
        dest.writeInt(fee);
        dest.writeInt(range);
        dest.writeInt(saleDay);
    }

}
