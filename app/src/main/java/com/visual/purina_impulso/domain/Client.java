package com.visual.purina_impulso.domain;

import android.os.Parcel;
import android.os.Parcelable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;



public class Client extends RealmObject implements Parcelable {

    @PrimaryKey
    private int clientId;

    private String employeeId;
    private String dni;
    private String ruc;
    private String code;
    private String address;
    private int bannerId;
    private String banner;
    private int clusterId;
    private String cluster;
    private String type;

    private String visitId;

    private String visit;
    private String startHour;
    private String exitHour;
    private String statusClient;

    private String pending;

    public Client (int clientId, String employeeId , String dni, String  ruc,String code, String address, int bannerId, String banner, int clusterId, String cluster,String type){
        this.clientId = clientId;
        this.employeeId = employeeId;
        this.dni = dni;
        this.ruc = ruc;
        this.code = code;
        this.address = address;
        this.bannerId = bannerId;
        this.banner = banner;
        this.clusterId = clusterId;
        this.cluster = cluster;
        this.type = type;
    }

    public Client (int clientId, String employeeId , String dni, String  ruc,String code, String address, int bannerId, String banner, int clusterId, String cluster,String type,String visitId,String visit,String startHour,String exitHour,String statusClient,String pending){
        this.clientId = clientId;
        this.employeeId = employeeId;
        this.dni = dni;
        this.ruc = ruc;
        this.code = code;
        this.address = address;
        this.bannerId = bannerId;
        this.banner = banner;
        this.clusterId = clusterId;
        this.cluster = cluster;
        this.type = type;
        this.visitId = visitId;
        this.visit = visit;

        this.startHour = startHour;
        this.exitHour = exitHour;
        this.statusClient = statusClient;
        this.pending = pending;
    }

    public Client (){
        this.clientId = 0;
        this.employeeId = "";
        this.dni = "";
        this.ruc = "";
        this.code = "";
        this.address = "";
        this.bannerId = 0;
        this.banner = "";
        this.clusterId = 0;
        this.cluster = "";
        this.type = "";
        this.visitId = "";
        this.visit = "";

        this.startHour = "";
        this.exitHour = "";
        this.statusClient = "";
        this.pending = "";
    }

    public int getClientId() {
        return clientId;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public String getDni() {
        return dni;
    }

    public String getRuc() {
        return ruc;
    }

    public String getCode() {
        return code;
    }

    public String getAddress() {
        return address;
    }

    public int getBannerId() {
        return bannerId;
    }

    public String getBanner() {
        return banner;
    }

    public int getClusterId() {
        return clusterId;
    }

    public String getCluster() {
        return cluster;
    }

    public String getType() {
        return type;
    }

    public String getVisitId() {
        return visitId;
    }

    public String getVisit() {
        return visit;
    }

    public String getStartHour() {
        return startHour;
    }

    public String getStatusClient() {
        return statusClient;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setBannerId(int bannerId) {
        this.bannerId = bannerId;
    }

    public void setBanner(String banner) {
        this.banner = banner;
    }

    public void setClusterId(int clusterId) {
        this.clusterId = clusterId;
    }

    public void setCluster(String cluster) {
        this.cluster = cluster;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setVisitId(String visitId) {
        this.visitId = visitId;
    }

    public void setVisit(String visit) {
        this.visit = visit;
    }

    public void setStartHour(String startHour) {
        this.startHour = startHour;
    }

    public String getExitHour() {
        return exitHour;
    }

    public void setExitHour(String exitHour) {
        this.exitHour = exitHour;
    }

    public void setStatusClient(String statusClient) {
        this.statusClient = statusClient;
    }

    public String getPending() {
        return pending;
    }

    public void setPending(String pending) {
        this.pending = pending;
    }

    protected Client (Parcel source) {
        clientId = source.readInt();
        employeeId = source.readString();
        dni = source.readString();
        ruc = source.readString();
        code = source.readString();
        address = source.readString();
        bannerId = source.readInt();
        banner = source.readString();
        clusterId = source.readInt();
        cluster = source.readString();
        type = source.readString();
        visitId = source.readString();
        visit = source.readString();
        startHour = source.readString();
        exitHour = source.readString();
        statusClient = source.readString();
        pending = source.readString();
    }

    public static final Creator<Client> CREATOR = new Creator<Client>() {
        @Override
        public Client createFromParcel(Parcel in) {
            return new Client(in);
        }

        @Override
        public Client[] newArray(int size) {
            return new Client[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(clientId);
        dest.writeString(employeeId);
        dest.writeString(dni);
        dest.writeString(ruc);
        dest.writeString(code);
        dest.writeString(address);
        dest.writeInt(bannerId);
        dest.writeString(banner);
        dest.writeInt(clusterId);
        dest.writeString(cluster);
        dest.writeString(type);
        dest.writeString(visitId);
        dest.writeString(visit);
        dest.writeString(startHour);
        dest.writeString(exitHour);
        dest.writeString(statusClient);
        dest.writeString(pending);
    }
}
