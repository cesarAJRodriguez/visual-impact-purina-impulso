package com.visual.purina_impulso.domain;

import android.os.Parcel;
import android.os.Parcelable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;


public class Merchandising extends RealmObject implements Parcelable {

    @PrimaryKey
    private int merchandisingId;

    private String name;
    private int status;

    private int stock;

    public Merchandising(int merchandisingId, String name, int status) {
        this.merchandisingId = merchandisingId;
        this.name = name;
        this.status = status;
    }

    public Merchandising() {
        this.merchandisingId = 0;
        this.name = "";
        this.status = 0;
        this.stock = 0;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public int getMerchandisingId() {
        return merchandisingId;
    }

    public void setMerchandisingId(int merchandisingId) {
        this.merchandisingId = merchandisingId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public static Creator<Merchandising> getCREATOR() {
        return CREATOR;
    }

    protected Merchandising(Parcel source) {
        merchandisingId = source.readInt();
        name = source.readString();
        status = source.readInt();
    }



    public static final Creator<Merchandising> CREATOR = new Creator<Merchandising>() {
        @Override
        public Merchandising createFromParcel(Parcel in) {
            return new Merchandising(in);
        }

        @Override
        public Merchandising[] newArray(int size) {
            return new Merchandising[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(merchandisingId);
        dest.writeString(name);
        dest.writeInt(status);
    }
}
