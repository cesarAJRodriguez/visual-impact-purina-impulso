package com.visual.purina_impulso.domain;
import android.os.Parcel;
import android.os.Parcelable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;



public class QuizVisit extends RealmObject implements Parcelable {

    @PrimaryKey
    private String id;
    private int quizId;

    private String visitId;
    private int employeeId;
    private String date;
    private int clientId;

    private String photo;
    private String hour;
    private String pending;

    public QuizVisit(String id, int quizId, String visitId, int employeeId, String date, int clientId, String photo, String hour, String pending) {
        this.id = id;
        this.quizId = quizId;
        this.visitId = visitId;
        this.employeeId = employeeId;
        this.date = date;
        this.clientId = clientId;
        this.photo = photo;
        this.hour = hour;
        this.pending = pending;
    }

    public QuizVisit(){
        this.id = "";
        this.quizId = 0;
        this.visitId = "";
        this.employeeId = 0;
        this.date = "";
        this.clientId = 0;
        this.photo = "";
        this.hour = "";
        this.pending = "";
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getQuizId() {
        return quizId;
    }

    public void setQuizId(int quizId) {
        this.quizId = quizId;
    }

    public String getVisitId() {
        return visitId;
    }

    public void setVisitId(String visitId) {
        this.visitId = visitId;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    public String getPending() {
        return pending;
    }

    public void setPending(String pending) {
        this.pending = pending;
    }

    protected QuizVisit(Parcel in) {
        id = in.readString();
        quizId = in.readInt();
        visitId = in.readString();
        employeeId = in.readInt();
        date = in.readString();
        clientId = in.readInt();
        quizId = in.readInt();
        photo = in.readString();
        hour = in.readString();
        pending = in.readString();
    }

    public static final Creator<QuizVisit> CREATOR = new Creator<QuizVisit>() {
        @Override
        public QuizVisit createFromParcel(Parcel in) {
            return new QuizVisit(in);
        }

        @Override
        public QuizVisit[] newArray(int size) {
            return new QuizVisit[size];
        }
    };



    @Override
    public int describeContents() {
        return 0;
    }


    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeInt(quizId);
        parcel.writeString(visitId);
        parcel.writeInt(employeeId);
        parcel.writeString(date);
        parcel.writeInt(clientId);
        parcel.writeInt(quizId);
        parcel.writeString(photo);
        parcel.writeString(hour);
        parcel.writeString(pending);
    }
}
