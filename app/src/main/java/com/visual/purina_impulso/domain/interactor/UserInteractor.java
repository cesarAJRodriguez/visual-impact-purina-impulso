package com.visual.purina_impulso.domain.interactor;

import com.visual.purina_impulso.base.BaseInteractor;
import com.visual.purina_impulso.data.source.Source;
import com.visual.purina_impulso.data.source.SourceFactory;
import com.visual.purina_impulso.data.source.helper.PreferencesHelper;


public class UserInteractor extends BaseInteractor {

    public interface SuccessLoginListener {
        void onSuccessLogin();
    }

    public interface SuccessRegisterTokenPushListener {
        void onSuccessRegisterTokenPush();
    }

    public interface SuccessCheckVersionListener {
        void onSuccessCheckVersion();
    }

    private static UserInteractor INSTANCE = null;


    public UserInteractor(){}

    public synchronized static UserInteractor getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new UserInteractor();
        }
        return INSTANCE;
    }

    public void login(String dni, String password, final SuccessLoginListener successCallback, final ErrorCallback errorCallback) {

        SourceFactory.createUserDataSource(Source.REMOTE).login(dni,password,
                ((user) -> {
                    PreferencesHelper.setUser(user);
                    successCallback.onSuccessLogin();
                }),
                errorCallback::onError
        );


    }

    public void checkVersion(String version, final SuccessCheckVersionListener successCallback, final ErrorCallback errorCallback) {

        SourceFactory.createUserDataSource(Source.REMOTE).checkVersion(version,
                (() -> {
                    successCallback.onSuccessCheckVersion();
                }),
                errorCallback::onError
        );


    }

}
