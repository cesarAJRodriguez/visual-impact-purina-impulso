package com.visual.purina_impulso.domain.interactor;

import com.visual.purina_impulso.base.BaseInteractor;
import com.visual.purina_impulso.data.source.Source;
import com.visual.purina_impulso.data.source.SourceFactory;
import com.visual.purina_impulso.domain.Alternative;
import com.visual.purina_impulso.domain.BaseProduct;
import com.visual.purina_impulso.domain.BrandProduct;
import com.visual.purina_impulso.domain.Client;
import com.visual.purina_impulso.domain.Incidence;
import com.visual.purina_impulso.domain.IncidenceVisit;
import com.visual.purina_impulso.domain.ListAdvanceDay;
import com.visual.purina_impulso.domain.ListBusinessDay;
import com.visual.purina_impulso.domain.ListFee;
import com.visual.purina_impulso.domain.ListFeeImpulso;
import com.visual.purina_impulso.domain.ListSale;
import com.visual.purina_impulso.domain.ListVisit;
import com.visual.purina_impulso.domain.Merchandising;
import com.visual.purina_impulso.domain.Photo;
import com.visual.purina_impulso.domain.Price;
import com.visual.purina_impulso.domain.Product;
import com.visual.purina_impulso.domain.ProductCategory;
import com.visual.purina_impulso.domain.Question;
import com.visual.purina_impulso.domain.Quiz;
import com.visual.purina_impulso.domain.StartVisit;
import com.visual.purina_impulso.domain.SubCategoryProduct;
import com.visual.purina_impulso.domain.Visit;

import java.util.ArrayList;


public class VisitInteractor extends BaseInteractor {

    public interface SuccessAssignVisitListener {
        void onSuccessAssignVisit();
    }

    public interface SuccessGetQuizVisitListener {
        void onSuccessGetQuizVisit(ArrayList<Quiz> quizzes);
    }

    public interface SuccessGetQuizQuestionsListener {
        void onSuccessGetQuizQuestions(ArrayList<Question> questions);
    }

    public interface SuccessGetQuizQuestionAlternativesListener {
        void onSuccessGetQuizQuestionAlternatives(ArrayList<Alternative> alternatives);
    }

    public interface SuccessGetPricesListener {
        void onSuccessGetPrices(ArrayList<Price> prices);
    }

    public interface SuccessGetTypePhotosListener {
        void onSuccessGetTypePhotos(ArrayList<Photo> photos);
    }

    public interface SuccessGetTypeIncidencesListener {
        void onSuccessGetTypeIncidences(ArrayList<Incidence> incidences);
    }

    public interface SuccessRegisterStartVisitListener {
        void onSuccessRegisterStartVisit(Visit visit);
    }

    public interface SuccessRegisterVisitListener {
        void onSuccessRegisterVisit();
    }

    public interface SuccessGetCategoryProductsVisitListener {
        void onSuccessGetCategoryProducts(ArrayList<ProductCategory> productCategories);
    }

    public interface SuccessGetClientVisitListener {
        void onSuccessGetClientVisit(ArrayList<Visit> visits);
    }


    public interface SuccessGetMerchandisingListener {
        void onSuccessGetMerchandising(ArrayList<Merchandising> merchandisings);
    }

    public interface SuccessGetSubCategoryProductListener {
        void onSuccessGetSubCategoryProduct(ArrayList<SubCategoryProduct> subCategoryProducts);
    }

    public interface SuccessGetBrandProductListener {
        void onSuccessGetBrandProduct(ArrayList<BrandProduct> brandProducts);
    }

    public interface SuccessGetBaseProductListener {
        void onSuccessGetBaseProduct(ArrayList<BaseProduct> baseProducts);
    }


    public interface SuccessGetProductsListener {
        void onSuccessGetProducts(ArrayList<Product> products);
    }

    public interface SuccessGetListAdvanceDayListener {
        void onSuccessGetListAdvanceDay(ArrayList<ListAdvanceDay> listAdvanceDays);
    }

    public interface SuccessGetListBusinessDayListener {
        void onSuccessGetListBusinessDay(ArrayList<ListBusinessDay> listBusinessDays);
    }

    public interface SuccessGetListFeeListener {
        void onSuccessGetListFee(ArrayList<ListFee> listFees);
    }

    public interface SuccessGetListSaleListener {
        void onSuccessGetListSale(ArrayList<ListSale> listSales);
    }

    public interface SuccessGetListVisitListener {
        void onSuccessGetListVisit(ArrayList<ListVisit> listVisits);
    }

    public interface SuccessGetListFeeImpulsoListener {
        void onSuccessGetListFeeImpulso(ArrayList<ListFeeImpulso> listFeeImpulsos);
    }

    public interface SuccessRegisterIncidenceVisitListener {
        void onSuccessRegisterIncidenceVisit();
    }


    private static VisitInteractor INSTANCE = null;


    public VisitInteractor(){}

    public synchronized static VisitInteractor getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new VisitInteractor();
        }
        return INSTANCE;
    }

    public void assignVisit(String currentDate,String[] clients, final SuccessAssignVisitListener successCallback, final ErrorCallback errorCallback) {

        SourceFactory.createVisitDataSource(Source.REMOTE).assignVisit(currentDate,clients,
                (() -> {
                    successCallback.onSuccessAssignVisit();
                }),
                errorCallback::onError
        );
    }

    public void getQuizVisit(int channelId, final SuccessGetQuizVisitListener successCallback, final ErrorCallback errorCallback) {

        SourceFactory.createVisitDataSource(Source.LOCAL).getQuizzesVisit(channelId,
                ((quizzes) -> {
                    successCallback.onSuccessGetQuizVisit(quizzes);
                }),
                errorCallback::onError
        );
    }

    public void getQuizQuestions(int channelId,int quizId, final SuccessGetQuizQuestionsListener successCallback, final ErrorCallback errorCallback) {

        SourceFactory.createVisitDataSource(Source.LOCAL).getQuizQuestions(channelId,quizId,
                ((questions) -> {
                    successCallback.onSuccessGetQuizQuestions(questions);
                }),
                errorCallback::onError
        );
    }

    public void getQuizQuestionAlternatives(int channelId,int quizId, final SuccessGetQuizQuestionAlternativesListener successCallback, final ErrorCallback errorCallback) {

        SourceFactory.createVisitDataSource(Source.LOCAL).getQuizQuestionAlternatives(channelId,quizId,
                ((alternatives) -> {
                    successCallback.onSuccessGetQuizQuestionAlternatives(alternatives);
                }),
                errorCallback::onError
        );
    }

    public void getPrices(int clientId,String typePrice,String search, final SuccessGetPricesListener successCallback, final ErrorCallback errorCallback) {

        SourceFactory.createVisitDataSource(Source.LOCAL).getPrices(clientId,typePrice,search,
                ((prices) -> {
                    successCallback.onSuccessGetPrices(prices);
                }),
                errorCallback::onError
        );
    }

    public void getTypePhotos(int channelId, final SuccessGetTypePhotosListener successCallback, final ErrorCallback errorCallback) {

        SourceFactory.createVisitDataSource(Source.LOCAL).getTypePhotos(channelId,
                ((photos) -> {
                    successCallback.onSuccessGetTypePhotos(photos);
                }),
                errorCallback::onError
        );
    }

    public void getTypeIncidences(int channelId, final SuccessGetTypeIncidencesListener successCallback, final ErrorCallback errorCallback) {

        SourceFactory.createVisitDataSource(Source.LOCAL).getTypeIncidences(channelId,
                ((incidences) -> {
                    successCallback.onSuccessGetTypeIncidences(incidences);
                }),
                errorCallback::onError
        );
    }

    public void registerStartVisit(StartVisit startVisit, Visit visit, boolean isOnline , final SuccessRegisterStartVisitListener successCallback, final ErrorCallback errorCallback) {

        Source source;

        if (isOnline){
            source = Source.REMOTE;
        }else{
            source = Source.LOCAL;
        }


        SourceFactory.createVisitDataSource(source).registerStartVisit(startVisit,visit,
                ((visitResponse) -> {
                    successCallback.onSuccessRegisterStartVisit(visitResponse);
                }),
                errorCallback::onError
        );
    }

    public void registerVisit( Visit visit,String moduleVisit, boolean isOnline , final SuccessRegisterVisitListener successCallback, final ErrorCallback errorCallback) {

        Source source;

        if (isOnline){
            source = Source.REMOTE;
        }else{
            source = Source.LOCAL;
        }


        SourceFactory.createVisitDataSource(source).registerVisit(visit,moduleVisit,
                (() -> {
                    successCallback.onSuccessRegisterVisit();
                }),
                errorCallback::onError
        );
    }

    public void getCategoryProducts(int channelId, final SuccessGetCategoryProductsVisitListener successCallback, final ErrorCallback errorCallback) {

        SourceFactory.createVisitDataSource(Source.LOCAL).getCategoriesProduct(channelId,
                ((categories) -> {
                    successCallback.onSuccessGetCategoryProducts(categories);
                }),
                errorCallback::onError
        );
    }

    public void getVisitClient(String visitId, final SuccessGetClientVisitListener successCallback, final ErrorCallback errorCallback) {

        SourceFactory.createVisitDataSource(Source.LOCAL).getVisitClient(visitId,
                ((visits) -> {
                    successCallback.onSuccessGetClientVisit(visits);
                }),
                errorCallback::onError
        );
    }

    public void getMerchandising(final SuccessGetMerchandisingListener successCallback, final ErrorCallback errorCallback) {

        SourceFactory.createVisitDataSource(Source.LOCAL).getMerchandising(
                ((merchandisings) -> {
                    successCallback.onSuccessGetMerchandising(merchandisings);
                }),
                errorCallback::onError
        );
    }

    public void getSubCategoryProduct(final SuccessGetSubCategoryProductListener successCallback, final ErrorCallback errorCallback) {

        SourceFactory.createVisitDataSource(Source.LOCAL).getSubCategoryProduct(
                ((subCategoryProducts) -> {
                    successCallback.onSuccessGetSubCategoryProduct(subCategoryProducts);
                }),
                errorCallback::onError
        );
    }

    public void getBrandProduct(int subCategoryProduct,final SuccessGetBrandProductListener successCallback, final ErrorCallback errorCallback) {

        SourceFactory.createVisitDataSource(Source.LOCAL).getBrandProduct(subCategoryProduct,
                ((brandProducts) -> {
                    successCallback.onSuccessGetBrandProduct(brandProducts);
                }),
                errorCallback::onError
        );
    }

    public void getBaseProduct(int subCategoryProduct,int brandProduct,final SuccessGetBaseProductListener successCallback, final ErrorCallback errorCallback) {

        SourceFactory.createVisitDataSource(Source.LOCAL).getBaseProduct(subCategoryProduct,brandProduct,
                ((baseProducts) -> {
                    successCallback.onSuccessGetBaseProduct(baseProducts);
                }),
                errorCallback::onError
        );
    }

    public void getProducs(String baseProduct,final SuccessGetProductsListener successCallback, final ErrorCallback errorCallback) {

        SourceFactory.createVisitDataSource(Source.LOCAL).getProducts(baseProduct,
                ((products) -> {
                    successCallback.onSuccessGetProducts(products);
                }),
                errorCallback::onError
        );
    }



    public void getListAdvanceDay(final SuccessGetListAdvanceDayListener successCallback, final ErrorCallback errorCallback) {

        SourceFactory.createVisitDataSource(Source.LOCAL).getListAdvanceDay(
                ((listAdvanceDays) -> {
                    successCallback.onSuccessGetListAdvanceDay(listAdvanceDays);
                }),
                errorCallback::onError
        );
    }

    public void getListBusinessDay(final SuccessGetListBusinessDayListener successCallback, final ErrorCallback errorCallback) {

        SourceFactory.createVisitDataSource(Source.LOCAL).getListBusinessDay(
                ((listBusinessDays) -> {
                    successCallback.onSuccessGetListBusinessDay(listBusinessDays);
                }),
                errorCallback::onError
        );
    }

    public void getListFee(Client client,final SuccessGetListFeeListener successCallback, final ErrorCallback errorCallback) {

        SourceFactory.createVisitDataSource(Source.LOCAL).getListFee(client,
                ((listFees) -> {
                    successCallback.onSuccessGetListFee(listFees);
                }),
                errorCallback::onError
        );
    }

    public void getListSale(Client client, final SuccessGetListSaleListener successCallback, final ErrorCallback errorCallback) {

        SourceFactory.createVisitDataSource(Source.LOCAL).getListSale(client,
                ((listSales) -> {
                    successCallback.onSuccessGetListSale(listSales);
                }),
                errorCallback::onError
        );
    }


    public void getListVisit(Client client, final SuccessGetListVisitListener successCallback, final ErrorCallback errorCallback) {

        SourceFactory.createVisitDataSource(Source.LOCAL).getListVisit(client,
                ((listVisits) -> {
                    successCallback.onSuccessGetListVisit(listVisits);
                }),
                errorCallback::onError
        );
    }


    public void getListFeeImpulso(Client client, final SuccessGetListFeeImpulsoListener successCallback, final ErrorCallback errorCallback) {

        SourceFactory.createVisitDataSource(Source.LOCAL).getListFeeImpulso(client,
                ((listFeeImpulsos) -> {
                    successCallback.onSuccessGetListFeeImpulso(listFeeImpulsos);
                }),
                errorCallback::onError
        );
    }

    public void registerIncidenceVist(StartVisit startVisit, IncidenceVisit incidenceVisit, final SuccessRegisterIncidenceVisitListener successCallback, final ErrorCallback errorCallback) {

        SourceFactory.createVisitDataSource(Source.REMOTE).registerIncidenceVisit(startVisit,incidenceVisit,
                (() -> {
                    successCallback.onSuccessRegisterIncidenceVisit();
                }),
                errorCallback::onError
        );
    }




}
