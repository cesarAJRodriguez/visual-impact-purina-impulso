package com.visual.purina_impulso.domain;

import android.os.Parcel;
import android.os.Parcelable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;



public class BrandProduct extends RealmObject implements Parcelable {

    @PrimaryKey
    private String id;
    private int brandProductId;

    private String brandProduct;

    private int subProductCategoryId;


    public BrandProduct(String id,int brandProductId, String brandProduct,int subProductCategoryId) {

        this.id = id;
        this.brandProductId = brandProductId;
        this.brandProduct = brandProduct;
        this.subProductCategoryId = subProductCategoryId;

    }

    public BrandProduct() {
        this.id = "";
        this.brandProductId = 0;
        this.brandProduct = "";
        this.subProductCategoryId = 0;

    }

    public int getBrandProductId() {
        return brandProductId;
    }

    public String getBrandProduct() {
        return brandProduct;
    }

    public String getId() {
        return id;
    }

    public int getSubProductCategoryId() {
        return subProductCategoryId;
    }

    public static Creator<BrandProduct> getCREATOR() {
        return CREATOR;
    }

    protected BrandProduct(Parcel source) {
        id = source.readString();
        brandProductId = source.readInt();
        brandProduct = source.readString();
        subProductCategoryId = source.readInt();

    }

    public static final Creator<BrandProduct> CREATOR = new Creator<BrandProduct>() {
        @Override
        public BrandProduct createFromParcel(Parcel in) {
            return new BrandProduct(in);
        }

        @Override
        public BrandProduct[] newArray(int size) {
            return new BrandProduct[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeInt(brandProductId);
        dest.writeString(brandProduct);
        dest.writeInt(subProductCategoryId);

    }

    @Override
    public String toString() {
        return brandProduct;
    }
}
