package com.visual.purina_impulso.domain;

import android.os.Parcel;
import android.os.Parcelable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;



public class ListSale extends RealmObject implements Parcelable {

    @PrimaryKey
    private String id;
    private int clientId;
    private String sale;

    public ListSale(String id, int clientId, String sale) {
        this.id = id;
        this.clientId = clientId;
        this.sale = sale;
    }

    public ListSale() {
        this.id = "";
        this.clientId = 0;
        this.sale = "";
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public String getSale() {
        return sale;
    }

    public void setSale(String sale) {
        this.sale = sale;
    }

    public static Creator<ListSale> getCREATOR() {
        return CREATOR;
    }

    protected ListSale(Parcel source) {
        id = source.readString();
        clientId = source.readInt();
        sale = source.readString();

    }

    public static final Creator<ListSale> CREATOR = new Creator<ListSale>() {
        @Override
        public ListSale createFromParcel(Parcel in) {
            return new ListSale(in);
        }

        @Override
        public ListSale[] newArray(int size) {
            return new ListSale[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeInt(clientId);
        dest.writeString(sale);
    }

}
