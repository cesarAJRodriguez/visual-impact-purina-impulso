package com.visual.purina_impulso.domain.interactor;

import com.visual.purina_impulso.base.BaseInteractor;
import com.visual.purina_impulso.data.source.Source;
import com.visual.purina_impulso.data.source.SourceFactory;
import com.visual.purina_impulso.domain.Sync;

public class SyncInteractor extends BaseInteractor {

    public interface SuccessSyncListener {
        void onSuccessSync(Sync sync);
    }

    public interface SuccessSaveSyncListener {
        void onSuccessSaveSync(String message);
    }

    public interface SuccessClearSyncListener {
        void onSuccessClearSync();
    }

    public interface SuccessClearPartialSyncListener {
        void onSuccessClearPartialSync();
    }



    private static SyncInteractor INSTANCE = null;


    public SyncInteractor(){}

    public synchronized static SyncInteractor getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new SyncInteractor();
        }
        return INSTANCE;
    }

    public void sync(String syncTraditional, String syncModern,String syncSpecialized, final SuccessSyncListener successCallback, final ErrorCallback errorCallback) {

        SourceFactory.createSyncDataSource(Source.REMOTE).sync(syncTraditional,syncModern,syncSpecialized,
                ((sync) -> {
                    successCallback.onSuccessSync(sync);
                }),
                errorCallback::onError
        );
    }


    public void saveSync(Sync sync,boolean typeSync, final SuccessSaveSyncListener successCallback, final ErrorCallback errorCallback) {

        SourceFactory.createSyncDataSource(Source.LOCAL).saveSync(sync,typeSync,
                ((message) -> {
                    successCallback.onSuccessSaveSync(message);
                }),
                errorCallback::onError
        );
    }

    public void clearSync( final SuccessClearSyncListener successCallback, final ErrorCallback errorCallback) {

        SourceFactory.createSyncDataSource(Source.LOCAL).clearSync(
                (() -> {
                    successCallback.onSuccessClearSync();
                }),
                errorCallback::onError
        );
    }

    public void clearPartialSync( final SuccessClearPartialSyncListener successCallback, final ErrorCallback errorCallback) {

        SourceFactory.createSyncDataSource(Source.LOCAL).clearPartialSync(
                (() -> {
                    successCallback.onSuccessClearPartialSync();
                }),
                errorCallback::onError
        );
    }



}
