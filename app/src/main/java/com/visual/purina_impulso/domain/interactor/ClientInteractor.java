package com.visual.purina_impulso.domain.interactor;

import com.visual.purina_impulso.base.BaseInteractor;
import com.visual.purina_impulso.data.source.Source;
import com.visual.purina_impulso.data.source.SourceFactory;
import com.visual.purina_impulso.domain.Client;

import java.util.ArrayList;


public class ClientInteractor extends BaseInteractor {

    public interface SuccessGetClientsListener {
        void onSuccessGetClients(ArrayList<Client> clients);
    }


    public interface SuccessRegisterClientListener {
        void onSuccessRegisterClient();
    }

    public interface SuccessGetClientListener {
        void onSuccessGetClient(ArrayList<Client> clients);
    }


    private static ClientInteractor INSTANCE = null;


    public ClientInteractor(){}

    public synchronized static ClientInteractor getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new ClientInteractor();
        }
        return INSTANCE;
    }

    public void getClients(String employeeId,String type,String search, final SuccessGetClientsListener successCallback, final ErrorCallback errorCallback) {

        SourceFactory.createClientDataSource(Source.LOCAL).getClients(employeeId,type,search,
                ((clients) -> {
                    successCallback.onSuccessGetClients(clients);
                }),
                errorCallback::onError
        );
    }

    public void registerClient(Client client, final SuccessRegisterClientListener successCallback, final ErrorCallback errorCallback) {

        SourceFactory.createClientDataSource(Source.LOCAL).registerClient(client,
                (() -> {
                    successCallback.onSuccessRegisterClient();
                }),
                errorCallback::onError
        );
    }

    public void getClient(Client client, final SuccessGetClientListener successCallback, final ErrorCallback errorCallback) {

        SourceFactory.createClientDataSource(Source.LOCAL).getClient(client,
                ((clients) -> {
                    successCallback.onSuccessGetClient(clients);
                }),
                errorCallback::onError
        );
    }




}
