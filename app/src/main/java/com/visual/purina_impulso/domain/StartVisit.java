package com.visual.purina_impulso.domain;

import android.os.Parcel;
import android.os.Parcelable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;


public class StartVisit extends RealmObject implements Parcelable {

    @PrimaryKey
    private String id;

    private int employeeId;
    private String date;
    private int clientId;
    private String startHour;
    private String exitHour;
    private String startLongitude;
    private String startLatitude;
    private String exitLongitude;
    private String exitLatitude;
    private String statusFlag;

    private String visitId;
    private String pending;

    public StartVisit(String id, int employeeId, String date, int clientId, String startHour, String exitHour, String startLongitude, String startLatitude, String exitLongitude, String exitLatitude, String statusFlag, String visitId,String pending) {
        this.id = id;
        this.employeeId = employeeId;
        this.date = date;
        this.clientId = clientId;
        this.startHour = startHour;
        this.exitHour = exitHour;
        this.startLongitude = startLongitude;
        this.startLatitude = startLatitude;
        this.exitLongitude = exitLongitude;
        this.exitLatitude = exitLatitude;
        this.statusFlag = statusFlag;
        this.visitId = visitId;
        this.pending = pending;
    }

    public StartVisit() {
        this.id = "";
        this.employeeId = 0;
        this.date = "";
        this.clientId = 0;
        this.startHour = "";
        this.exitHour = "";
        this.startLongitude = "";
        this.startLatitude = "";
        this.exitLongitude = "";
        this.exitLatitude = "";
        this.statusFlag = "";
        this.visitId = "";
        this.pending = "";
    }

    public String getId() {
        return id;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public String getDate() {
        return date;
    }

    public int getClientId() {
        return clientId;
    }

    public String getStartHour() {
        return startHour;
    }

    public String getExitHour() {
        return exitHour;
    }

    public String getStartLongitude() {
        return startLongitude;
    }

    public String getStartLatitude() {
        return startLatitude;
    }

    public String getExitLongitude() {
        return exitLongitude;
    }

    public String getExitLatitude() {
        return exitLatitude;
    }

    public String getStatusFlag() {
        return statusFlag;
    }

    public String getVisitId() {
        return visitId;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public void setStartHour(String startHour) {
        this.startHour = startHour;
    }

    public void setExitHour(String exitHour) {
        this.exitHour = exitHour;
    }

    public void setStartLongitude(String startLongitude) {
        this.startLongitude = startLongitude;
    }

    public void setStartLatitude(String startLatitude) {
        this.startLatitude = startLatitude;
    }

    public void setExitLongitude(String exitLongitude) {
        this.exitLongitude = exitLongitude;
    }

    public void setExitLatitude(String exitLatitude) {
        this.exitLatitude = exitLatitude;
    }

    public void setStatusFlag(String statusFlag) {
        this.statusFlag = statusFlag;
    }

    public void setVisitId(String visitId) {
        this.visitId = visitId;
    }

    public String getPending() {
        return pending;
    }

    public void setPending(String pending) {
        this.pending = pending;
    }

    protected StartVisit(Parcel source) {
        id = source.readString();
        employeeId = source.readInt();
        date = source.readString();
        clientId = source.readInt();
        startHour = source.readString();
        exitHour = source.readString();
        startLongitude = source.readString();
        startLatitude = source.readString();
        exitLongitude = source.readString();
        exitLatitude = source.readString();
        statusFlag = source.readString();
        visitId = source.readString();
        pending = source.readString();

    }

    public static final Creator<StartVisit> CREATOR = new Creator<StartVisit>() {
        @Override
        public StartVisit createFromParcel(Parcel in) {
            return new StartVisit(in);
        }

        @Override
        public StartVisit[] newArray(int size) {
            return new StartVisit[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeInt(employeeId);
        dest.writeString(date);
        dest.writeInt(clientId);
        dest.writeString(startHour);
        dest.writeString(exitHour);
        dest.writeString(startLongitude);
        dest.writeString(startLatitude);
        dest.writeString(exitLongitude);
        dest.writeString(exitLatitude);
        dest.writeString(statusFlag);
        dest.writeString(visitId);
        dest.writeString(pending);
    }
}
