package com.visual.purina_impulso.domain;

import android.os.Parcel;
import android.os.Parcelable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;



public class IncidenceVisit extends RealmObject implements Parcelable {

    @PrimaryKey
    private String id;

    private int employeeId;
    private String date;
    private int clientId;
    private int typeIncidenceId;
    private String photo;
    private String comment;
    private String hour;

    private String visitId;

    public IncidenceVisit(String id, int employeeId, String date, int clientId, int typeIncidenceId, String photo, String comment, String hour, String visitId) {
        this.id = id;
        this.employeeId = employeeId;
        this.date = date;
        this.clientId = clientId;
        this.typeIncidenceId = typeIncidenceId;
        this.photo = photo;
        this.comment = comment;
        this.hour = hour;
        this.visitId = visitId;
    }
    public IncidenceVisit(){
        this.id = "";
        this.employeeId = 0;
        this.date = "";
        this.clientId = 0;
        this.typeIncidenceId = 0;
        this.photo = "";
        this.comment = "";
        this.hour = "";
        this.visitId = "";
    }

    public String getId() {
        return id;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public String getDate() {
        return date;
    }

    public int getClientId() {
        return clientId;
    }

    public int getTypeIncidenceId() {
        return typeIncidenceId;
    }

    public String getPhoto() {
        return photo;
    }

    public String getComment() {
        return comment;
    }

    public String getHour() {
        return hour;
    }

    public String getVisitId() {
        return visitId;
    }

    protected IncidenceVisit(Parcel source) {
        id = source.readString();
        employeeId = source.readInt();
        date = source.readString();
        clientId = source.readInt();
        typeIncidenceId = source.readInt();
        photo = source.readString();
        comment = source.readString();
        hour = source.readString();
        visitId = source.readString();
    }

    public static final Creator<IncidenceVisit> CREATOR = new Creator<IncidenceVisit>() {
        @Override
        public IncidenceVisit createFromParcel(Parcel in) {
            return new IncidenceVisit(in);
        }

        @Override
        public IncidenceVisit[] newArray(int size) {
            return new IncidenceVisit[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeInt(employeeId);
        dest.writeString(date);
        dest.writeInt(clientId);
        dest.writeInt(typeIncidenceId);
        dest.writeString(photo);
        dest.writeString(comment);
        dest.writeString(hour);
        dest.writeString(visitId);
    }
}
