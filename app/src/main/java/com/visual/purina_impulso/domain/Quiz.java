package com.visual.purina_impulso.domain;

import android.os.Parcel;
import android.os.Parcelable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;


public class Quiz extends RealmObject implements Parcelable {

    @PrimaryKey
    private int quizId;

    private int channelId;
    private String quizName;

    public Quiz(int quizId,int channelId, String quizName){
        this.quizId = quizId;
        this.channelId = channelId;
        this.quizName = quizName;
    }


    public Quiz(){
        this.quizId = 0;
        this.channelId = 0;
        this.quizName = "";
    }


    public int getQuizId() {
        return quizId;
    }

    public int getChannelId() {
        return channelId;
    }

    public String getQuizName() {
        return quizName;
    }

    public static Creator<Quiz> getCREATOR() {
        return CREATOR;
    }

    protected Quiz(Parcel source) {
        quizId = source.readInt();
        channelId = source.readInt();
        quizName = source.readString();
    }

    public static final Creator<Quiz> CREATOR = new Creator<Quiz>() {
        @Override
        public Quiz createFromParcel(Parcel in) {
            return new Quiz(in);
        }

        @Override
        public Quiz[] newArray(int size) {
            return new Quiz[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(quizId);
        dest.writeInt(channelId);
        dest.writeString(quizName);
    }
}
