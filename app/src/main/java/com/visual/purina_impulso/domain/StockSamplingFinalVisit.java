package com.visual.purina_impulso.domain;

import android.os.Parcel;
import android.os.Parcelable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;


public class StockSamplingFinalVisit extends RealmObject implements Parcelable {

    @PrimaryKey
    private String id;

    private int employeeId;
    private String date;
    private int clientId;
    private String photo;
    private String comment;
    private String hour;
    private String visitId;
    private String pending;

    public StockSamplingFinalVisit(String id, int employeeId, String date, int clientId, String photo, String comment, String hour, String visitId, String pending) {
        this.id = id;
        this.employeeId = employeeId;
        this.date = date;
        this.clientId = clientId;
        this.photo = photo;
        this.comment = comment;
        this.hour = hour;
        this.visitId = visitId;
        this.pending = pending;
    }

    public StockSamplingFinalVisit(){
        this.id = "";
        this.employeeId = 0;
        this.date = "";
        this.clientId = 0;
        this.photo = "";
        this.comment = "";
        this.hour = "";
        this.visitId = "";
        this.pending = "";
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    public String getVisitId() {
        return visitId;
    }

    public void setVisitId(String visitId) {
        this.visitId = visitId;
    }

    public String getPending() {
        return pending;
    }

    public void setPending(String pending) {
        this.pending = pending;
    }

    public static Creator<StockSamplingFinalVisit> getCREATOR() {
        return CREATOR;
    }

    protected StockSamplingFinalVisit(Parcel source) {
        id = source.readString();
        employeeId = source.readInt();
        date = source.readString();
        clientId = source.readInt();
        photo = source.readString();
        comment = source.readString();
        hour = source.readString();
        visitId = source.readString();
        pending = source.readString();

    }

    public static final Creator<StockSamplingFinalVisit> CREATOR = new Creator<StockSamplingFinalVisit>() {
        @Override
        public StockSamplingFinalVisit createFromParcel(Parcel in) {
            return new StockSamplingFinalVisit(in);
        }

        @Override
        public StockSamplingFinalVisit[] newArray(int size) {
            return new StockSamplingFinalVisit[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeInt(employeeId);
        dest.writeString(date);
        dest.writeInt(clientId);
        dest.writeString(photo);
        dest.writeString(comment);
        dest.writeString(hour);
        dest.writeString(visitId);
        dest.writeString(pending);

    }
}
