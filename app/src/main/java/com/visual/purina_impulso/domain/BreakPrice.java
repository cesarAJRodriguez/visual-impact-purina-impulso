package com.visual.purina_impulso.domain;

import android.os.Parcel;
import android.os.Parcelable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;


public class BreakPrice extends RealmObject implements Parcelable {

    @PrimaryKey
    private String id;

    private String visitId;
    private int employeeId;
    private String date;
    private int clientId;
    private String hour;
    private String pending;

    public BreakPrice(String id, String visitId, int employeeId, String date, int clientId, String hour, String pending) {
        this.id = id;
        this.visitId = visitId;
        this.employeeId = employeeId;
        this.date = date;
        this.clientId = clientId;
        this.hour = hour;
        this.pending = pending;
    }

    public BreakPrice(){
        this.id = "";
        this.visitId = "";
        this.employeeId = 0;
        this.date = "";
        this.clientId = 0;
        this.hour = "";
        this.pending = "";
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getVisitId() {
        return visitId;
    }

    public void setVisitId(String visitId) {
        this.visitId = visitId;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    public String getPending() {
        return pending;
    }

    public void setPending(String pending) {
        this.pending = pending;
    }

    public static Creator<BreakPrice> getCREATOR() {
        return CREATOR;
    }

    protected BreakPrice(Parcel source) {
        id = source.readString();
        visitId = source.readString();
        employeeId = source.readInt();
        date = source.readString();
        clientId = source.readInt();
        hour = source.readString();
        pending = source.readString();


    }

    public static final Creator<BreakPrice> CREATOR = new Creator<BreakPrice>() {
        @Override
        public BreakPrice createFromParcel(Parcel in) {
            return new BreakPrice(in);
        }

        @Override
        public BreakPrice[] newArray(int size) {
            return new BreakPrice[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(visitId);
        dest.writeInt(employeeId);
        dest.writeString(date);
        dest.writeInt(clientId);
        dest.writeString(hour);
        dest.writeString(pending);
    }
}
