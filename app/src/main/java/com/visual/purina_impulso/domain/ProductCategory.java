package com.visual.purina_impulso.domain;

import android.os.Parcel;
import android.os.Parcelable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;



public class ProductCategory extends RealmObject implements Parcelable {

    @PrimaryKey
    private int productCategoryId;

    private int channelId;
    private String productCategory;



    public ProductCategory(int productCategoryId, int channelId, String productCategory){
        this.productCategoryId = productCategoryId;
        this.channelId = channelId;
        this.productCategory = productCategory;
    }




    public ProductCategory(){
        this.productCategoryId = 0;
        this.channelId = 0;
        this.productCategory = "";

    }

    public int getProductCategoryId() {
        return productCategoryId;
    }

    public int getChannelId() {
        return channelId;
    }

    public String getProductCategory() {
        return productCategory;
    }


    public static Creator<ProductCategory> getCREATOR() {
        return CREATOR;
    }

    protected ProductCategory(Parcel source) {
        productCategoryId = source.readInt();
        channelId = source.readInt();
        productCategory = source.readString();


    }

    public static final Creator<ProductCategory> CREATOR = new Creator<ProductCategory>() {
        @Override
        public ProductCategory createFromParcel(Parcel in) {
            return new ProductCategory(in);
        }

        @Override
        public ProductCategory[] newArray(int size) {
            return new ProductCategory[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(productCategoryId);
        dest.writeInt(channelId);
        dest.writeString(productCategory);

    }

    @Override
    public String toString() {
        return productCategory;
    }
}
