package com.visual.purina_impulso.domain;

import android.os.Parcel;
import android.os.Parcelable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;



public class DetailPriceBreakVisit extends RealmObject implements Parcelable {

    @PrimaryKey
    private String id;

    private int productId;

    private String priceBreakId;
    private int employeeId;
    private String date;
    private int clientId;
    private String priceBreak;
    private String price;
    private String pricePromotion;
    private String priceSuggested;
    private String  photo;
    private String visitId;
    private String pending;

    public DetailPriceBreakVisit(String id, int productId,String priceBreakId, int employeeId, String date, int clientId, String priceBreak, String price, String pricePromotion, String priceSuggested, String photo, String visitId, String pending) {
        this.id = id;
        this.productId = productId;
        this.priceBreakId = priceBreakId;
        this.employeeId = employeeId;
        this.date = date;
        this.clientId = clientId;
        this.priceBreak = priceBreak;
        this.price = price;
        this.pricePromotion = pricePromotion;
        this.priceSuggested = priceSuggested;
        this.photo = photo;
        this.visitId = visitId;
        this.pending = pending;
    }


    public DetailPriceBreakVisit() {
        this.id = id;
        this.productId = productId;
        this.priceBreakId = priceBreakId;
        this.employeeId = employeeId;
        this.date = date;
        this.clientId = clientId;
        this.priceBreak = priceBreak;
        this.price = price;
        this.pricePromotion = pricePromotion;
        this.priceSuggested = priceSuggested;
        this.photo = photo;
        this.visitId = visitId;
        this.pending = pending;
    }

    public String getPriceBreakId() {
        return priceBreakId;
    }

    public void setPriceBreakId(String priceBreakId) {
        this.priceBreakId = priceBreakId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public String getPriceBreak() {
        return priceBreak;
    }

    public void setPriceBreak(String priceBreak) {
        this.priceBreak = priceBreak;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPricePromotion() {
        return pricePromotion;
    }

    public void setPricePromotion(String pricePromotion) {
        this.pricePromotion = pricePromotion;
    }

    public String getPriceSuggested() {
        return priceSuggested;
    }

    public void setPriceSuggested(String priceSuggested) {
        this.priceSuggested = priceSuggested;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getVisitId() {
        return visitId;
    }

    public void setVisitId(String visitId) {
        this.visitId = visitId;
    }

    public String getPending() {
        return pending;
    }

    public void setPending(String pending) {
        this.pending = pending;
    }

    public static Creator<DetailPriceBreakVisit> getCREATOR() {
        return CREATOR;
    }

    protected DetailPriceBreakVisit(Parcel source) {
        id = source.readString();
        productId = source.readInt();
        priceBreakId = source.readString();
        employeeId = source.readInt();
        date = source.readString();
        clientId = source.readInt();
        priceBreak = source.readString();
        price = source.readString();
        pricePromotion = source.readString();
        priceSuggested = source.readString();
        photo = source.readString();
        visitId = source.readString();
        pending = source.readString();

    }

    public static final Creator<DetailPriceBreakVisit> CREATOR = new Creator<DetailPriceBreakVisit>() {
        @Override
        public DetailPriceBreakVisit createFromParcel(Parcel in) {
            return new DetailPriceBreakVisit(in);
        }

        @Override
        public DetailPriceBreakVisit[] newArray(int size) {
            return new DetailPriceBreakVisit[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeInt(productId);
        dest.writeString(priceBreakId);
        dest.writeInt(employeeId);
        dest.writeString(date);
        dest.writeInt(clientId);
        dest.writeString(priceBreak);
        dest.writeString(price);
        dest.writeString(pricePromotion);
        dest.writeString(priceSuggested);
        dest.writeString(photo);
        dest.writeString(visitId);
        dest.writeString(pending);

    }
}
