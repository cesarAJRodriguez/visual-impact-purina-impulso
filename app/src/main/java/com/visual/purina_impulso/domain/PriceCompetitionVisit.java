package com.visual.purina_impulso.domain;

import android.os.Parcel;
import android.os.Parcelable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;



public class PriceCompetitionVisit extends RealmObject implements Parcelable {

    @PrimaryKey
    private String id;

    private String hour;
    private String date;
    private int clientId;
    private int employeeId;
    private String visitId;
    private String pending;



    public PriceCompetitionVisit(String id, String hour, String date, int clientId, int employeeId, String visitId, String pending) {
        this.id = id;
        this.hour = hour;
        this.date = date;
        this.clientId = clientId;
        this.employeeId = employeeId;
        this.visitId = visitId;
        this.pending = pending;

    }

    public PriceCompetitionVisit() {
        this.id = "";
        this.hour = "";
        this.date = "";
        this.clientId = 0;
        this.employeeId = 0;
        this.visitId = "";
        this.pending = "";
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public String getPending() {
        return pending;
    }

    public void setPending(String pending) {
        this.pending = pending;
    }

    public String getVisitId() {
        return visitId;
    }

    public void setVisitId(String visitId) {
        this.visitId = visitId;
    }

    public static Creator<PriceCompetitionVisit> getCREATOR() {
        return CREATOR;
    }

    protected PriceCompetitionVisit(Parcel source) {
        id = source.readString();
        hour = source.readString();
        date = source.readString();
        clientId = source.readInt();
        employeeId = source.readInt();
        visitId = source.readString();
        pending = source.readString();
    }

    public static final Creator<PriceCompetitionVisit> CREATOR = new Creator<PriceCompetitionVisit>() {
        @Override
        public PriceCompetitionVisit createFromParcel(Parcel in) {
            return new PriceCompetitionVisit(in);
        }

        @Override
        public PriceCompetitionVisit[] newArray(int size) {
            return new PriceCompetitionVisit[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(hour);
        dest.writeString(date);
        dest.writeInt(clientId);
        dest.writeInt(employeeId);
        dest.writeString(visitId);
        dest.writeString(pending);
    }
}
