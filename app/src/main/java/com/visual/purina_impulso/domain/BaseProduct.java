package com.visual.purina_impulso.domain;

import android.os.Parcel;
import android.os.Parcelable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;



public class BaseProduct extends RealmObject implements Parcelable {

    @PrimaryKey
    private String id;
    private String baseProductId;

    private String baseProduct;
    private int subCategoryProductId;
    private int brandProductId;

    public BaseProduct(String id, String baseProductId, String baseProduct, int subCategoryProductId, int brandProductId) {
        this.id = id;
        this.baseProductId = baseProductId;
        this.baseProduct = baseProduct;
        this.subCategoryProductId = subCategoryProductId;
        this.brandProductId = brandProductId;
    }

    public BaseProduct(){
        this.id = "";
        this.baseProductId = "";
        this.baseProduct = "";
        this.subCategoryProductId = 0;
        this.brandProductId = 0;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBaseProductId() {
        return baseProductId;
    }

    public void setBaseProductId(String baseProductId) {
        this.baseProductId = baseProductId;
    }

    public String getBaseProduct() {
        return baseProduct;
    }

    public void setBaseProduct(String baseProduct) {
        this.baseProduct = baseProduct;
    }

    public int getSubCategoryProductId() {
        return subCategoryProductId;
    }

    public void setSubCategoryProductId(int subCategoryProductId) {
        this.subCategoryProductId = subCategoryProductId;
    }

    public int getBrandProductId() {
        return brandProductId;
    }

    public void setBrandProductId(int brandProductId) {
        this.brandProductId = brandProductId;
    }

    public static Creator<BaseProduct> getCREATOR() {
        return CREATOR;
    }

    protected BaseProduct(Parcel source) {
        id = source.readString();
        baseProductId = source.readString();
        baseProduct = source.readString();
        subCategoryProductId = source.readInt();
        brandProductId = source.readInt();
    }

    public static final Creator<BaseProduct> CREATOR = new Creator<BaseProduct>() {
        @Override
        public BaseProduct createFromParcel(Parcel in) {
            return new BaseProduct(in);
        }

        @Override
        public BaseProduct[] newArray(int size) {
            return new BaseProduct[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(baseProductId);
        dest.writeString(baseProduct);
        dest.writeInt(subCategoryProductId);
        dest.writeInt(brandProductId);
    }
    @Override
    public String toString() {
        return baseProduct;
    }
}
