package com.visual.purina_impulso.domain.interactor;

import com.visual.purina_impulso.base.BaseInteractor;
import com.visual.purina_impulso.data.source.Source;
import com.visual.purina_impulso.data.source.SourceFactory;
import com.visual.purina_impulso.domain.Assistance;

import java.util.ArrayList;

public class AssistanceInteractor extends BaseInteractor {

    public interface SuccessAssistanceListener {
        void onSuccessAssistance();
    }

    public interface SuccessCheckAssistanceListener {
        void onSuccessCheckAssistance(ArrayList<Assistance> assistances);
    }

    private static AssistanceInteractor INSTANCE = null;


    public AssistanceInteractor() {
    }

    public synchronized static AssistanceInteractor getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new AssistanceInteractor();
        }
        return INSTANCE;
    }



    public void assistance(Assistance assistance,ArrayList<Assistance> assistancesPending,String pending,boolean isOnline, final SuccessAssistanceListener successCallback, final ErrorCallback errorCallback) {
        Source type;
        if (isOnline){
            type = Source.REMOTE;
        }else{
            type = Source.LOCAL;
        }


        SourceFactory.createAssistanceDataSource(type).assistance(assistance,assistancesPending,pending,
                (() -> {
                    successCallback.onSuccessAssistance();
                }),
                errorCallback::onError
        );
    }


    public void checkAssistance(Assistance assistance, final SuccessCheckAssistanceListener successCallback, final ErrorCallback errorCallback) {

        SourceFactory.createAssistanceDataSource(Source.LOCAL).checkAssistance(assistance,
                ((assistances) -> {
                    successCallback.onSuccessCheckAssistance(assistances);
                }),
                errorCallback::onError
        );
    }

}
