package com.visual.purina_impulso.domain;

import android.os.Parcel;
import android.os.Parcelable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;



public class DetailStockSampligVisit extends RealmObject implements Parcelable {

    @PrimaryKey
    private String id;

    private String stockSampligVisitId;
    private int employeeId;
    private String date;
    private int clientId;
    private int merchandisingId;
    private String merchandisingName;
    private int stock;
    private int stockRest;
    private String visitId;
    private String pending;

    public DetailStockSampligVisit(String id,String stockSampligVisitId, int employeeId, String date, int clientId, int merchandisingId,String merchandisingName, int stock,int stockRest, String visitId, String pending) {
        this.id = id;
        this.stockSampligVisitId = stockSampligVisitId;
        this.employeeId = employeeId;
        this.date = date;
        this.clientId = clientId;
        this.merchandisingId = merchandisingId;
        this.merchandisingName = merchandisingName;
        this.stock = stock;
        this.stockRest = stockRest;
        this.visitId = visitId;
        this.pending = pending;
    }

    public DetailStockSampligVisit() {
        this.id = "";
        this.stockSampligVisitId = "";
        this.employeeId = 0;
        this.date = "";
        this.clientId = 0;
        this.merchandisingId = 0;
        this.merchandisingName = "";
        this.stock = 0;
        this.stockRest = 0;
        this.visitId = "";
        this.pending = "";
    }

    public String getStockSampligVisitId() {
        return stockSampligVisitId;
    }

    public void setStockSampligVisitId(String stockSampligVisitId) {
        this.stockSampligVisitId = stockSampligVisitId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public int getMerchandisingId() {
        return merchandisingId;
    }

    public void setMerchandisingId(int merchandisingId) {
        this.merchandisingId = merchandisingId;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public String getVisitId() {
        return visitId;
    }

    public void setVisitId(String visitId) {
        this.visitId = visitId;
    }

    public String getPending() {
        return pending;
    }

    public void setPending(String pending) {
        this.pending = pending;
    }

    public String getMerchandisingName() {
        return merchandisingName;
    }

    public void setMerchandisingName(String merchandisingName) {
        this.merchandisingName = merchandisingName;
    }

    public int getStockRest() {
        return stockRest;
    }

    public void setStockRest(int stockRest) {
        this.stockRest = stockRest;
    }

    public static Creator<DetailStockSampligVisit> getCREATOR() {
        return CREATOR;
    }

    protected DetailStockSampligVisit(Parcel source) {
        id = source.readString();
        stockSampligVisitId = source.readString();
        employeeId = source.readInt();
        date = source.readString();
        clientId = source.readInt();
        merchandisingId = source.readInt();
        merchandisingName = source.readString();
        stock = source.readInt();
        stockRest = source.readInt();
        visitId = source.readString();
        pending = source.readString();

    }

    public static final Creator<DetailStockSampligVisit> CREATOR = new Creator<DetailStockSampligVisit>() {
        @Override
        public DetailStockSampligVisit createFromParcel(Parcel in) {
            return new DetailStockSampligVisit(in);
        }

        @Override
        public DetailStockSampligVisit[] newArray(int size) {
            return new DetailStockSampligVisit[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(stockSampligVisitId);
        dest.writeInt(employeeId);
        dest.writeString(date);
        dest.writeInt(clientId);
        dest.writeInt(merchandisingId);
        dest.writeString(merchandisingName);
        dest.writeInt(stock);
        dest.writeInt(stockRest);
        dest.writeString(visitId);
        dest.writeString(pending);
    }

    @Override
    public String toString() {
        return merchandisingName;
    }
}
