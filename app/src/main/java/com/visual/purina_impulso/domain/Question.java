package com.visual.purina_impulso.domain;

import android.os.Parcel;
import android.os.Parcelable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;



public class Question extends RealmObject implements Parcelable {

    @PrimaryKey
    private int questionId;

    private int channelId;
    private int quizId;
    private int typeQuestion;
    private String questionName;

    public Question(int questionId, int channelId,int quizId,int typeQuestion, String questionName){
        this.questionId = questionId;
        this.channelId = channelId;
        this.quizId = quizId;
        this.typeQuestion = typeQuestion;
        this.questionName = questionName;
    }


    public Question(){
        this.questionId = 0;
        this.channelId = 0;
        this.quizId = 0;
        this.typeQuestion = 0;
        this.questionName = "";
    }


    public int getQuestionId() {
        return questionId;
    }

    public int getChannelId() {
        return channelId;
    }

    public int getQuizId() {
        return quizId;
    }

    public int getTypeQuestion() {
        return typeQuestion;
    }

    public String getQuestionName() {
        return questionName;
    }

    public static Creator<Question> getCREATOR() {
        return CREATOR;
    }

    protected Question(Parcel source) {
        questionId = source.readInt();
        channelId = source.readInt();
        quizId = source.readInt();
        typeQuestion = source.readInt();
        questionName = source.readString();
    }

    public static final Creator<Question> CREATOR = new Creator<Question>() {
        @Override
        public Question createFromParcel(Parcel in) {
            return new Question(in);
        }

        @Override
        public Question[] newArray(int size) {
            return new Question[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(questionId);
        dest.writeInt(channelId);
        dest.writeInt(quizId);
        dest.writeInt(typeQuestion);
        dest.writeString(questionName);
    }
}
