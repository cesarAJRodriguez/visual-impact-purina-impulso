package com.visual.purina_impulso.domain;

import android.os.Parcel;
import android.os.Parcelable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Alternative extends RealmObject implements Parcelable {

    @PrimaryKey
    private int alternativeId;

    private int channelId;
    private int quizId;
    private int questionId;
    private String alternativeName;

    public Alternative(int alternativeId, int channelId, int quizId, int questionId, String alternativeName){
        this.alternativeId = alternativeId;
        this.channelId = channelId;
        this.quizId = quizId;
        this.questionId = questionId;
        this.alternativeName = alternativeName;
    }


    public Alternative(){
        this.alternativeId = 0;
        this.channelId = 0;
        this.quizId = 0;
        this.questionId = 0;
        this.alternativeName = "";
    }

    public int getAlternativeId() {
        return alternativeId;
    }

    public int getChannelId() {
        return channelId;
    }

    public int getQuizId() {
        return quizId;
    }

    public int getQuestionId() {
        return questionId;
    }

    public String getAlternativeName() {
        return alternativeName;
    }

    public static Creator<Alternative> getCREATOR() {
        return CREATOR;
    }

    protected Alternative(Parcel source) {
        alternativeId = source.readInt();
        channelId = source.readInt();
        quizId = source.readInt();
        questionId = source.readInt();
        alternativeName = source.readString();
    }

    public static final Creator<Alternative> CREATOR = new Creator<Alternative>() {
        @Override
        public Alternative createFromParcel(Parcel in) {
            return new Alternative(in);
        }

        @Override
        public Alternative[] newArray(int size) {
            return new Alternative[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(alternativeId);
        dest.writeInt(channelId);
        dest.writeInt(quizId);
        dest.writeInt(questionId);
        dest.writeString(alternativeName);
    }
}
