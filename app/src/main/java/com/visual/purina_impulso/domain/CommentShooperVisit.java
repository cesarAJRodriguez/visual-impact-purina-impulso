package com.visual.purina_impulso.domain;

import android.os.Parcel;
import android.os.Parcelable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;



public class CommentShooperVisit extends RealmObject implements Parcelable {

    @PrimaryKey
    private String id;

    private String visitId;

    private int employeeId;
    private String date;
    private int clientId;
    private String comment;
    private String opportunities;
    private String photo;
    private String typeShooper;
    private String hour;
    private String pending;


    public CommentShooperVisit(String id, String visitId, int employeeId, String date, int clientId, String comment, String opportunities, String photo, String typeShooper, String hour, String pending) {
        this.id = id;
        this.visitId = visitId;
        this.employeeId = employeeId;
        this.date = date;
        this.clientId = clientId;
        this.comment = comment;
        this.opportunities = opportunities;
        this.photo = photo;
        this.typeShooper = typeShooper;
        this.hour = hour;
        this.pending = pending;
    }

    public CommentShooperVisit(){
        this.id = "";
        this.visitId = "";
        this.employeeId = 0;
        this.date = "";
        this.clientId = 0;
        this.comment = "";
        this.opportunities = "";
        this.photo = "";
        this.typeShooper = "";
        this.hour = "";
        this.pending = "";


    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getVisitId() {
        return visitId;
    }

    public void setVisitId(String visitId) {
        this.visitId = visitId;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getOpportunities() {
        return opportunities;
    }

    public void setOpportunities(String opportunities) {
        this.opportunities = opportunities;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getTypeShooper() {
        return typeShooper;
    }

    public void setTypeShooper(String typeShooper) {
        this.typeShooper = typeShooper;
    }

    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    public String getPending() {
        return pending;
    }

    public void setPending(String pending) {
        this.pending = pending;
    }

    protected CommentShooperVisit(Parcel in) {

        id = in.readString();
        visitId = in.readString();
        employeeId = in.readInt();
        date = in.readString();
        clientId = in.readInt();
        comment = in.readString();
        opportunities = in.readString();
        photo = in.readString();
        typeShooper = in.readString();
        hour = in.readString();
        pending = in.readString();

    }

    public static final Creator<CommentShooperVisit> CREATOR = new Creator<CommentShooperVisit>() {
        @Override
        public CommentShooperVisit createFromParcel(Parcel in) {
            return new CommentShooperVisit(in);
        }

        @Override
        public CommentShooperVisit[] newArray(int size) {
            return new CommentShooperVisit[size];
        }
    };



    @Override
    public int describeContents() {
        return 0;
    }


    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(visitId);
        parcel.writeInt(employeeId);
        parcel.writeString(date);
        parcel.writeInt(clientId);
        parcel.writeString(comment);
        parcel.writeString(opportunities);
        parcel.writeString(photo);
        parcel.writeString(typeShooper);
        parcel.writeString(hour);
        parcel.writeString(pending);

    }
}
