package com.visual.purina_impulso.domain;

import android.os.Parcel;
import android.os.Parcelable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;



public class DetailStockSamplingFinalVisit extends RealmObject implements Parcelable {

    @PrimaryKey
    private String id;

    private String stockSamplingFinalVisitId;
    private int employeeId;
    private String date;
    private int clientId;
    private int merchandisingId;
    private int stockFinal;
    private String visitId;
    private String pending;

    public DetailStockSamplingFinalVisit(String id, String stockSamplingFinalVisitId, int employeeId, String date, int clientId, int merchandisingId, int stockFinal, String visitId, String pending) {
        this.id = id;
        this.stockSamplingFinalVisitId = stockSamplingFinalVisitId;
        this.employeeId = employeeId;
        this.date = date;
        this.clientId = clientId;
        this.merchandisingId = merchandisingId;
        this.stockFinal = stockFinal;
        this.visitId = visitId;
        this.pending = pending;
    }

    public DetailStockSamplingFinalVisit(){
        this.id = "";
        this.stockSamplingFinalVisitId = "";
        this.employeeId = 0;
        this.date = "";
        this.clientId = 0;
        this.merchandisingId = 0;
        this.stockFinal = 0;
        this.visitId = "";
        this.pending = "";
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStockSamplingFinalVisitId() {
        return stockSamplingFinalVisitId;
    }

    public void setStockSamplingFinalVisitId(String stockSamplingFinalVisitId) {
        this.stockSamplingFinalVisitId = stockSamplingFinalVisitId;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public int getMerchandisingId() {
        return merchandisingId;
    }

    public void setMerchandisingId(int merchandisingId) {
        this.merchandisingId = merchandisingId;
    }

    public int getStockFinal() {
        return stockFinal;
    }

    public void setStockFinal(int stockFinal) {
        this.stockFinal = stockFinal;
    }

    public String getVisitId() {
        return visitId;
    }

    public void setVisitId(String visitId) {
        this.visitId = visitId;
    }

    public String getPending() {
        return pending;
    }

    public void setPending(String pending) {
        this.pending = pending;
    }

    public static Creator<DetailStockSamplingFinalVisit> getCREATOR() {
        return CREATOR;
    }

    protected DetailStockSamplingFinalVisit(Parcel source) {
        id = source.readString();
        stockSamplingFinalVisitId = source.readString();
        employeeId = source.readInt();
        date = source.readString();
        clientId = source.readInt();
        merchandisingId = source.readInt();
        stockFinal = source.readInt();
        visitId = source.readString();
        pending = source.readString();

    }

    public static final Creator<DetailStockSamplingFinalVisit> CREATOR = new Creator<DetailStockSamplingFinalVisit>() {
        @Override
        public DetailStockSamplingFinalVisit createFromParcel(Parcel in) {
            return new DetailStockSamplingFinalVisit(in);
        }

        @Override
        public DetailStockSamplingFinalVisit[] newArray(int size) {
            return new DetailStockSamplingFinalVisit[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(stockSamplingFinalVisitId);
        dest.writeInt(employeeId);
        dest.writeString(date);
        dest.writeInt(clientId);
        dest.writeInt(merchandisingId);
        dest.writeInt(stockFinal);
        dest.writeString(visitId);
        dest.writeString(pending);

    }
}
