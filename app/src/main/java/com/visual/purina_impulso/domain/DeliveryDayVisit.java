package com.visual.purina_impulso.domain;

import android.os.Parcel;
import android.os.Parcelable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;


public class DeliveryDayVisit extends RealmObject implements Parcelable {

    @PrimaryKey
    private String id;

    private int employeeId;
    private String date;
    private int clientId;
    private int merchandisingId;
    private int amountMerchandising;
    private int subCategoryId;
    private int categoryProductId;
    private int baseProductId;
    private int productId;
    private int amountProduct;
    private String hour;
    private String photo;
    private String visitId;
    private String pending;

    public DeliveryDayVisit(String id, int employeeId, String date, int clientId, int merchandisingId, int amountMerchandising, int subCategoryId, int categoryProductId, int baseProductId, int productId, int amountProduct, String hour, String photo, String visitId, String pending) {
        this.id = id;
        this.employeeId = employeeId;
        this.date = date;
        this.clientId = clientId;
        this.merchandisingId = merchandisingId;
        this.amountMerchandising = amountMerchandising;
        this.subCategoryId = subCategoryId;
        this.categoryProductId = categoryProductId;
        this.baseProductId = baseProductId;
        this.productId = productId;
        this.amountProduct = amountProduct;
        this.hour = hour;
        this.photo = photo;
        this.visitId = visitId;
        this.pending = pending;
    }

    public DeliveryDayVisit() {
        this.id = "";
        this.employeeId = 0;
        this.date = "";
        this.clientId = 0;
        this.merchandisingId = 0;
        this.amountMerchandising = 0;
        this.subCategoryId = 0;
        this.categoryProductId = 0;
        this.baseProductId = 0;
        this.productId = 0;
        this.amountProduct = 0;
        this.hour = "";
        this.photo = "";
        this.visitId = "";
        this.pending = "";
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public int getMerchandisingId() {
        return merchandisingId;
    }

    public void setMerchandisingId(int merchandisingId) {
        this.merchandisingId = merchandisingId;
    }

    public int getAmountMerchandising() {
        return amountMerchandising;
    }

    public void setAmountMerchandising(int amountMerchandising) {
        this.amountMerchandising = amountMerchandising;
    }

    public int getSubCategoryId() {
        return subCategoryId;
    }

    public void setSubCategoryId(int subCategoryId) {
        this.subCategoryId = subCategoryId;
    }

    public int getCategoryProductId() {
        return categoryProductId;
    }

    public void setCategoryProductId(int categoryProductId) {
        this.categoryProductId = categoryProductId;
    }

    public int getBaseProductId() {
        return baseProductId;
    }

    public void setBaseProductId(int baseProductId) {
        this.baseProductId = baseProductId;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getAmountProduct() {
        return amountProduct;
    }

    public void setAmountProduct(int amountProduct) {
        this.amountProduct = amountProduct;
    }

    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getVisitId() {
        return visitId;
    }

    public void setVisitId(String visitId) {
        this.visitId = visitId;
    }

    public String getPending() {
        return pending;
    }

    public void setPending(String pending) {
        this.pending = pending;
    }

    public static Creator<DeliveryDayVisit> getCREATOR() {
        return CREATOR;
    }

    protected DeliveryDayVisit(Parcel source) {
        id = source.readString();
        employeeId = source.readInt();
        date = source.readString();
        clientId = source.readInt();
        merchandisingId = source.readInt();
        amountMerchandising = source.readInt();
        subCategoryId = source.readInt();
        categoryProductId = source.readInt();
        baseProductId = source.readInt();
        productId = source.readInt();
        amountProduct = source.readInt();
        hour = source.readString();
        photo = source.readString();
        visitId = source.readString();
        pending = source.readString();



    }

    public static final Creator<DeliveryDayVisit> CREATOR = new Creator<DeliveryDayVisit>() {
        @Override
        public DeliveryDayVisit createFromParcel(Parcel in) {
            return new DeliveryDayVisit(in);
        }

        @Override
        public DeliveryDayVisit[] newArray(int size) {
            return new DeliveryDayVisit[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeInt(employeeId);
        dest.writeString(date);
        dest.writeInt(clientId);
        dest.writeInt(merchandisingId);
        dest.writeInt(amountMerchandising);
        dest.writeInt(subCategoryId);
        dest.writeInt(categoryProductId);
        dest.writeInt(baseProductId);
        dest.writeInt(productId);
        dest.writeInt(amountProduct);
        dest.writeString(hour);
        dest.writeString(photo);
        dest.writeString(visitId);
        dest.writeString(pending);



    }


}
