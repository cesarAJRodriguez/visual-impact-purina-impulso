package com.visual.purina_impulso.domain;

import android.os.Parcel;
import android.os.Parcelable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;


public class Product extends RealmObject implements Parcelable {

    @PrimaryKey
    private String id;
    private int productId;

    private String product;

    private String baseProductId;

    public Product(String id, int productId, String product, String baseProductId) {
        this.id = id;
        this.productId = productId;
        this.product = product;
        this.baseProductId = baseProductId;
    }


    public Product() {
        this.id = "";
        this.productId = 0;
        this.product = "";
        this.baseProductId = "";
    }

    public String getId() {
        return id;
    }

    public int getProductId() {
        return productId;
    }

    public String getProduct() {
        return product;
    }

    public String getBaseProductId() {
        return baseProductId;
    }

    public static Creator<Product> getCREATOR() {
        return CREATOR;
    }

    protected Product(Parcel source) {

        id = source.readString();
        productId = source.readInt();
        product = source.readString();
        baseProductId = source.readString();


    }

    public static final Creator<Product> CREATOR = new Creator<Product>() {
        @Override
        public Product createFromParcel(Parcel in) {
            return new Product(in);
        }

        @Override
        public Product[] newArray(int size) {
            return new Product[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeInt(productId);
        dest.writeString(product);
        dest.writeString(baseProductId);
    }

    @Override
    public String toString() {
        return product;
    }
}
