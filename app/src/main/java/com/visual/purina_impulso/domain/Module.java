package com.visual.purina_impulso.domain;

import android.os.Parcel;
import android.os.Parcelable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;



public class Module extends RealmObject implements Parcelable {

    @PrimaryKey
    private int moduleId;

    private String moduleName;

    public Module(int moduleId, String moduleName){
        this.moduleId = moduleId;
        this.moduleName = moduleName;
    }


    public Module(){
        this.moduleId = 0;
        this.moduleName = "";
    }


    public int getModuleId() {
        return moduleId;
    }

    public String getModuleName() {
        return moduleName;
    }

    public static Creator<Module> getCREATOR() {
        return CREATOR;
    }

    protected Module(Parcel source) {
        moduleId = source.readInt();
        moduleName = source.readString();
    }

    public static final Creator<Module> CREATOR = new Creator<Module>() {
        @Override
        public Module createFromParcel(Parcel in) {
            return new Module(in);
        }

        @Override
        public Module[] newArray(int size) {
            return new Module[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(moduleId);
        dest.writeString(moduleName);
    }
}
