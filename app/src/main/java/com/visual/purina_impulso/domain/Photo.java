package com.visual.purina_impulso.domain;

import android.os.Parcel;
import android.os.Parcelable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;


public class Photo extends RealmObject implements Parcelable {

    @PrimaryKey
    private int typePhotoId;

    private int channelId;
    private String typePhotoName;

    public Photo(int typePhotoId,int channelId , String typePhotoName){
        this.typePhotoId = typePhotoId;
        this.channelId = channelId;
        this.typePhotoName = typePhotoName;
    }


    public Photo(){
        this.typePhotoId = 0;
        this.channelId = 0;
        this.typePhotoName = "";
    }

    public int getTypePhotoId() {
        return typePhotoId;
    }

    public int getChannelId() {
        return channelId;
    }

    public String getTypePhotoName() {
        return typePhotoName;
    }

    public static Creator<Photo> getCREATOR() {
        return CREATOR;
    }

    protected Photo(Parcel source) {
        typePhotoId = source.readInt();
        channelId = source.readInt();
        typePhotoName = source.readString();
    }

    public static final Creator<Photo> CREATOR = new Creator<Photo>() {
        @Override
        public Photo createFromParcel(Parcel in) {
            return new Photo(in);
        }

        @Override
        public Photo[] newArray(int size) {
            return new Photo[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(typePhotoId);
        dest.writeInt(channelId);
        dest.writeString(typePhotoName);
    }

    @Override
    public String toString() {
        return typePhotoName;
    }
}
