package com.visual.purina_impulso.domain;

import android.os.Parcel;
import android.os.Parcelable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;


public class Assistance extends RealmObject implements Parcelable {

    @PrimaryKey
    private String id;

    private String date;
    private String startPhoto;
    private String exitPhoto;
    private String employeeId;
    private String startHour;
    private String exitHour;
    private String startLongitude;
    private String startLatitude;
    private String exitLongitude;
    private String exitLatitude;
    private String startComment;
    private String exitComment;
    private String pending;
    private String type;
    private int amountPending;


    public Assistance( String date,String startPhoto, String exitPhoto, String employeeId, String startHour, String exitHour, String startLongitude, String startLatitude, String exitLongitude, String exitLatitude, String startComment, String exitComment) {
        this.date = date;
        this.startPhoto = startPhoto;
        this.exitPhoto = exitPhoto;
        this.employeeId = employeeId;
        this.startHour = startHour;
        this.exitHour = exitHour;
        this.startLongitude = startLongitude;
        this.startLatitude = startLatitude;
        this.exitLongitude = exitLongitude;
        this.exitLatitude = exitLatitude;
        this.startComment = startComment;
        this.exitComment = exitComment;
    }

    public Assistance() {
        this.date = "";
        this.startPhoto = "";
        this.exitPhoto = "";
        this.employeeId = "";
        this.startHour = "";
        this.exitHour = "";
        this.startLongitude = "";
        this.startLatitude = "";
        this.exitLongitude = "";
        this.exitLatitude = "";
        this.startComment = "";
        this.exitComment = "";
        this.type = "";
        this.amountPending = 0;
    }

    protected Assistance(Parcel in) {
        id = in.readString();
        date = in.readString();
        startPhoto = in.readString();
        exitPhoto = in.readString();
        employeeId = in.readString();
        startHour = in.readString();
        exitHour = in.readString();
        startLongitude = in.readString();
        startLatitude = in.readString();
        exitLongitude = in.readString();
        exitLatitude = in.readString();
        startComment = in.readString();
        exitComment = in.readString();
        pending = in.readString();
        type = in.readString();
        amountPending = in.readInt();
    }

    public static final Creator<Assistance> CREATOR = new Creator<Assistance>() {
        @Override
        public Assistance createFromParcel(Parcel in) {
            return new Assistance(in);
        }

        @Override
        public Assistance[] newArray(int size) {
            return new Assistance[size];
        }
    };

    public String getDate() {
        return date;
    }

    public String getStartPhoto() {
        return startPhoto;
    }

    public String getExitPhoto() {
        return exitPhoto;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public String getStartHour() {
        return startHour;
    }

    public String getExitHour() {
        return exitHour;
    }

    public String getStartLongitude() {
        return startLongitude;
    }

    public String getStartLatitude() {
        return startLatitude;
    }

    public String getExitLongitude() {
        return exitLongitude;
    }

    public String getExitLatitude() {
        return exitLatitude;
    }

    public String getStartComment() {
        return startComment;
    }

    public String getExitComment() {
        return exitComment;
    }

    public String getId() {
        return id;
    }

    public String getPending() {
        return pending;
    }


    public String getType() {
        return type;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setStartPhoto(String startPhoto) {
        this.startPhoto = startPhoto;
    }

    public void setExitPhoto(String exitPhoto) {
        this.exitPhoto = exitPhoto;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public void setStartHour(String startHour) {
        this.startHour = startHour;
    }

    public void setExitHour(String exitHour) {
        this.exitHour = exitHour;
    }

    public void setStartLongitude(String startLongitude) {
        this.startLongitude = startLongitude;
    }

    public void setStartLatitude(String startLatitude) {
        this.startLatitude = startLatitude;
    }

    public void setExitLongitude(String exitLongitude) {
        this.exitLongitude = exitLongitude;
    }

    public void setExitLatitude(String exitLatitude) {
        this.exitLatitude = exitLatitude;
    }

    public void setStartComment(String startComment) {
        this.startComment = startComment;
    }

    public void setExitComment(String exitComment) {
        this.exitComment = exitComment;
    }

    public void setPending(String pending) {
        this.pending = pending;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getAmountPending() {
        return amountPending;
    }

    public void setAmountPending(int amountPending) {
        this.amountPending = amountPending;
    }

    @Override
    public int describeContents() {
        return 0;
    }


    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(date);
        parcel.writeString(startPhoto);
        parcel.writeString(exitPhoto);
        parcel.writeString(employeeId);
        parcel.writeString(startHour);
        parcel.writeString(exitHour);
        parcel.writeString(startLongitude);
        parcel.writeString(startLatitude);
        parcel.writeString(exitLongitude);
        parcel.writeString(exitLatitude);
        parcel.writeString(startComment);
        parcel.writeString(exitComment);
        parcel.writeString(pending);
        parcel.writeString(type);
    }
}
