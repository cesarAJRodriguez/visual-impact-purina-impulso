package com.visual.purina_impulso.presentation.menu.clients.visit.menu;

import android.support.annotation.NonNull;

import com.visual.purina_impulso.App;
import com.visual.purina_impulso.Constants;
import com.visual.purina_impulso.R;
import com.visual.purina_impulso.base.BaseInteractor;
import com.visual.purina_impulso.domain.ActionVisit;
import com.visual.purina_impulso.domain.Alternative;
import com.visual.purina_impulso.domain.BaseProduct;
import com.visual.purina_impulso.domain.BrandProduct;
import com.visual.purina_impulso.domain.Client;
import com.visual.purina_impulso.domain.CommentShooperVisit;
import com.visual.purina_impulso.domain.DeliveryDayVisit;
import com.visual.purina_impulso.domain.DetailPriceBreakVisit;
import com.visual.purina_impulso.domain.DetailPriceCompetitionVisit;
import com.visual.purina_impulso.domain.DetailQuizVisit;
import com.visual.purina_impulso.domain.DetailStockSampligVisit;
import com.visual.purina_impulso.domain.DetailStockSamplingFinalVisit;
import com.visual.purina_impulso.domain.ListAdvanceDay;
import com.visual.purina_impulso.domain.ListBusinessDay;
import com.visual.purina_impulso.domain.ListFee;
import com.visual.purina_impulso.domain.ListFeeImpulso;
import com.visual.purina_impulso.domain.ListSale;
import com.visual.purina_impulso.domain.ListVisit;
import com.visual.purina_impulso.domain.Merchandising;
import com.visual.purina_impulso.domain.Photo;
import com.visual.purina_impulso.domain.PhotoVisit;
import com.visual.purina_impulso.domain.Price;
import com.visual.purina_impulso.domain.PriceBreakVisit;
import com.visual.purina_impulso.domain.PriceCompetitionVisit;
import com.visual.purina_impulso.domain.Product;
import com.visual.purina_impulso.domain.ProductCategory;
import com.visual.purina_impulso.domain.Question;
import com.visual.purina_impulso.domain.Quiz;
import com.visual.purina_impulso.domain.QuizVisit;
import com.visual.purina_impulso.domain.StockSampligVisit;
import com.visual.purina_impulso.domain.StockSamplingFinalVisit;
import com.visual.purina_impulso.domain.SubCategoryProduct;
import com.visual.purina_impulso.domain.Visit;
import com.visual.purina_impulso.domain.interactor.ClientInteractor;
import com.visual.purina_impulso.domain.interactor.VisitInteractor;
import com.visual.purina_impulso.util.DeviceUtil;

import java.util.ArrayList;

import io.realm.RealmList;

public class MenuVisitPresenter implements MenuVisitContract.Presenter {

    private final MenuVisitContract.View mView;

    public MenuVisitPresenter(@NonNull MenuVisitContract.View view) {
        mView = view;
        mView.setPresenter(this);
    }

    @Override
    public void start() {

    }

    @Override
    public void getQuizzes() {
        mView.startLoading();
        int channelId = Integer.parseInt("2");

        VisitInteractor.getInstance().getQuizVisit(channelId, new VisitInteractor.SuccessGetQuizVisitListener() {
            @Override
            public void onSuccessGetQuizVisit(ArrayList<Quiz> quizzes) {
                mView.stopLoading();
                ((MenuVisitContract.QuizView)mView).getQuizzesSuccess(quizzes);
        }

        }, new BaseInteractor.ErrorCallback() {
            @Override
            public void onError(Exception exception) {
                mView.stopLoading();
                mView.showErrorMessage(exception);
            }
        });


    }

    @Override
    public void getQuizQuestions(int quizId) {
        mView.startLoading();
        int channelId = Integer.parseInt("2");

        VisitInteractor.getInstance().getQuizQuestions(channelId,quizId, new VisitInteractor.SuccessGetQuizQuestionsListener() {
            @Override
            public void onSuccessGetQuizQuestions(ArrayList<Question> questions) {
                getQuizQuestionAlternatives(quizId,channelId,questions);
            }

        }, new BaseInteractor.ErrorCallback() {
            @Override
            public void onError(Exception exception) {
                mView.stopLoading();
                mView.showErrorMessage(exception);
            }
        });
    }

    @Override
    public void getPrices(int clientId,String typePrice,String search) {
        VisitInteractor.getInstance().getPrices(clientId,typePrice,search, new VisitInteractor.SuccessGetPricesListener() {
            @Override
            public void onSuccessGetPrices(ArrayList<Price> prices) {
                mView.stopLoading();
                 ((MenuVisitContract.PriceView)mView).getPricesSuccess(prices);
            }
        }, new BaseInteractor.ErrorCallback() {
            @Override
            public void onError(Exception exception) {
                mView.stopLoading();
                mView.showErrorMessage(exception);
            }
        });
    }

    @Override
    public void getTypePhotos() {
        mView.startLoading();
        int channelId = Integer.parseInt("2");
        VisitInteractor.getInstance().getTypePhotos(channelId, new VisitInteractor.SuccessGetTypePhotosListener() {
            @Override
            public void onSuccessGetTypePhotos(ArrayList<Photo> photos) {
                mView.stopLoading();
                ((MenuVisitContract.PhotoView)mView).getTypePhotosSuccess(photos);

            }

        }, new BaseInteractor.ErrorCallback() {
            @Override
            public void onError(Exception exception) {
                mView.stopLoading();
                mView.showErrorMessage(exception);
            }
        });
    }

    @Override
    public void getCategoryProducts() {
        mView.startLoading();
        int channelId = Integer.parseInt("2");
        VisitInteractor.getInstance().getCategoryProducts(channelId, new VisitInteractor.SuccessGetCategoryProductsVisitListener() {
            @Override
            public void onSuccessGetCategoryProducts(ArrayList<ProductCategory> productCategories) {
                mView.stopLoading();
                ((MenuVisitContract.ActionView)mView).getCategoryProductsSuccess(productCategories);
            }


        }, new BaseInteractor.ErrorCallback() {
            @Override
            public void onError(Exception exception) {
                mView.stopLoading();
                mView.showErrorMessage(exception);
            }
        });
    }

    @Override
    public void getMerchandising() {
        mView.startLoading();

        VisitInteractor.getInstance().getMerchandising( new VisitInteractor.SuccessGetMerchandisingListener() {
            @Override
            public void onSuccessGetMerchandising(ArrayList<Merchandising> merchandisings) {
                mView.stopLoading();
                ((MenuVisitContract.StockSamplingView)mView).getMerchandisingSuccess(merchandisings);
            }

        }, new BaseInteractor.ErrorCallback() {
            @Override
            public void onError(Exception exception) {
                mView.stopLoading();
                mView.showErrorMessage(exception);
            }
        });
    }

    @Override
    public void onRegisterVisit(Visit visit,String moduleVisit,Client client) {
        mView.startLoading();

        if (DeviceUtil.hasInternetConnection()){
            VisitInteractor.getInstance().registerVisit(visit,moduleVisit,true, new VisitInteractor.SuccessRegisterVisitListener() {
                @Override
                public void onSuccessRegisterVisit() {
                    if (moduleVisit.equals(Constants.FLAG_VISIT_T)){
                        client.setVisit(Constants.FLAG_CLIENT_VISIT_EXIT);
                    }

                    onSaveRegisterClient( client, true, moduleVisit, visit);
                }

            }, new BaseInteractor.ErrorCallback() {
                @Override
                public void onError(Exception exception) {
                    mView.stopLoading();
                    if (moduleVisit.equals(Constants.FLAG_VISIT_T)){
                        client.setVisit(Constants.FLAG_CLIENT_VISIT_EXIT);
                    }
                    onSaveRegisterClient( client, false, moduleVisit, visit);
                }
            });
        }else{
            if (moduleVisit.equals(Constants.FLAG_VISIT_T)){
                client.setVisit(Constants.FLAG_CLIENT_VISIT_EXIT);
            }
            onSaveRegisterClient( client, false, moduleVisit, visit);
        }
    }

    @Override
    public void getSubCategoryProduct() {
        mView.stopLoading();
        VisitInteractor.getInstance().getSubCategoryProduct( new VisitInteractor.SuccessGetSubCategoryProductListener() {
            @Override
            public void onSuccessGetSubCategoryProduct(ArrayList<SubCategoryProduct> subCategoryProducts) {
                mView.stopLoading();
                ((MenuVisitContract.DeliveryProductView)mView).getSubCategoryProductSuccess(subCategoryProducts);
            }

        }, new BaseInteractor.ErrorCallback() {
            @Override
            public void onError(Exception exception) {
                mView.stopLoading();
                mView.showErrorMessage(exception);
            }
        });
    }

    @Override
    public void getBrandProduct(int subCategoryProduct) {
        mView.stopLoading();
        VisitInteractor.getInstance().getBrandProduct(subCategoryProduct, new VisitInteractor.SuccessGetBrandProductListener() {
            @Override
            public void onSuccessGetBrandProduct(ArrayList<BrandProduct> brandProducts) {
                mView.stopLoading();
                ((MenuVisitContract.DeliveryProductView)mView).getBrandProductSuccess(brandProducts);
            }

        }, new BaseInteractor.ErrorCallback() {
            @Override
            public void onError(Exception exception) {
                mView.stopLoading();
                mView.showErrorMessage(exception);
            }
        });
    }

    @Override
    public void getBaseProduct(int subCategoryProduct, int brandProduct) {
        mView.stopLoading();
        VisitInteractor.getInstance().getBaseProduct(subCategoryProduct,brandProduct, new VisitInteractor.SuccessGetBaseProductListener() {
            @Override
            public void onSuccessGetBaseProduct(ArrayList<BaseProduct> baseProducts) {
                mView.stopLoading();
                ((MenuVisitContract.DeliveryProductView)mView).getBaseProductSuccess(baseProducts);
            }


        }, new BaseInteractor.ErrorCallback() {
            @Override
            public void onError(Exception exception) {
                mView.stopLoading();
                mView.showErrorMessage(exception);
            }
        });
    }

    @Override
    public void getProducts(String baseProduct) {
        mView.stopLoading();
        VisitInteractor.getInstance().getProducs(baseProduct, new VisitInteractor.SuccessGetProductsListener() {
            @Override
            public void onSuccessGetProducts(ArrayList<Product> products) {
                mView.stopLoading();
                ((MenuVisitContract.DeliveryProductView)mView).getProductsSuccess(products);
            }
        }, new BaseInteractor.ErrorCallback() {
            @Override
            public void onError(Exception exception) {
                mView.stopLoading();
                mView.showErrorMessage(exception);
            }
        });
    }

    @Override
    public void getValuesDaySale(Client client) {
        VisitInteractor.getInstance().getListAdvanceDay( new VisitInteractor.SuccessGetListAdvanceDayListener() {
            @Override
            public void onSuccessGetListAdvanceDay(ArrayList<ListAdvanceDay> listAdvanceDays) {
                getListBusiness(client,listAdvanceDays);
            }
        }, new BaseInteractor.ErrorCallback() {
            @Override
            public void onError(Exception exception) {
                mView.stopLoading();
                mView.showErrorMessage(exception);
            }
        });
    }

    private void getListBusiness(Client client,ArrayList<ListAdvanceDay> listAdvanceDays){
        VisitInteractor.getInstance().getListBusinessDay( new VisitInteractor.SuccessGetListBusinessDayListener() {
            @Override
            public void onSuccessGetListBusinessDay(ArrayList<ListBusinessDay> listBusinessDays) {
                getListFee(client,listAdvanceDays,listBusinessDays);
            }

        }, new BaseInteractor.ErrorCallback() {
            @Override
            public void onError(Exception exception) {
                mView.stopLoading();
                mView.showErrorMessage(exception);
            }
        });
    }

    private void getListFee(Client client,ArrayList<ListAdvanceDay> listAdvanceDays,
                            ArrayList<ListBusinessDay> listBusinessDays){
        VisitInteractor.getInstance().getListFee(client, new VisitInteractor.SuccessGetListFeeListener() {
            @Override
            public void onSuccessGetListFee(ArrayList<ListFee> listFees) {
                getListSale(client,listAdvanceDays,listBusinessDays,listFees);
            }
        }, new BaseInteractor.ErrorCallback() {
            @Override
            public void onError(Exception exception) {
                mView.stopLoading();
                mView.showErrorMessage(exception);
            }
        });
    }

    private void getListSale(Client client,ArrayList<ListAdvanceDay> listAdvanceDays,
                             ArrayList<ListBusinessDay> listBusinessDays,ArrayList<ListFee> listFees){
        VisitInteractor.getInstance().getListSale(client, new VisitInteractor.SuccessGetListSaleListener() {
            @Override
            public void onSuccessGetListSale(ArrayList<ListSale> listSales) {
                getListVisit(client,listAdvanceDays,listBusinessDays,listFees,listSales);
            }
        }, new BaseInteractor.ErrorCallback() {
            @Override
            public void onError(Exception exception) {
                mView.stopLoading();
                mView.showErrorMessage(exception);
            }
        });
    }

    private void getListVisit(Client client,ArrayList<ListAdvanceDay> listAdvanceDays,
                              ArrayList<ListBusinessDay> listBusinessDays,
                             ArrayList<ListFee> listFees,ArrayList<ListSale> listSales){
        VisitInteractor.getInstance().getListVisit(client, new VisitInteractor.SuccessGetListVisitListener() {
            @Override
            public void onSuccessGetListVisit(ArrayList<ListVisit> listVisits) {
                mView.stopLoading();
                getListFeeImpuslo(client,listAdvanceDays,
                        listBusinessDays,listFees,listSales,listVisits);
            }


        }, new BaseInteractor.ErrorCallback() {
            @Override
            public void onError(Exception exception) {
                mView.stopLoading();
                mView.showErrorMessage(exception);
            }
        });
    }

    private void getListFeeImpuslo(Client client,ArrayList<ListAdvanceDay> listAdvanceDays,
                              ArrayList<ListBusinessDay> listBusinessDays,
                              ArrayList<ListFee> listFees,ArrayList<ListSale> listSales,
                                   ArrayList<ListVisit> listVisits){
        VisitInteractor.getInstance().getListFeeImpulso(client, new VisitInteractor.SuccessGetListFeeImpulsoListener() {
            @Override
            public void onSuccessGetListFeeImpulso(ArrayList<ListFeeImpulso> listFeeImpulsos) {
                mView.stopLoading();
                ((MenuVisitContract.SaleDayView)mView).getValuesDaySaleSuccess(listAdvanceDays,
                        listBusinessDays,listFees,listSales,listVisits,listFeeImpulsos);
            }

        }, new BaseInteractor.ErrorCallback() {
            @Override
            public void onError(Exception exception) {
                mView.stopLoading();
                mView.showErrorMessage(exception);
            }
        });
    }




    public void getQuizQuestionAlternatives(int quizId,int channelId,ArrayList<Question> questions) {
        VisitInteractor.getInstance().getQuizQuestionAlternatives(channelId,quizId, new VisitInteractor.SuccessGetQuizQuestionAlternativesListener() {
            @Override
            public void onSuccessGetQuizQuestionAlternatives(ArrayList<Alternative> alternatives) {
                mView.stopLoading();
                ((MenuVisitContract.QuizQuestionView)mView).getQuizQuestionsAlternativesSuccess(questions,alternatives);
            }

        }, new BaseInteractor.ErrorCallback() {
            @Override
            public void onError(Exception exception) {
                mView.stopLoading();
                mView.showErrorMessage(exception);
            }
        });
    }

    public void saveRegisterVisit(Visit visit,boolean isSuccessOnline,String moduleVisit){

         String message;

        if (isSuccessOnline){
            if (moduleVisit.equals(Constants.FLAG_VISIT_PHOTO)){
                RealmList<PhotoVisit> photoVisitRealmList = new RealmList<>();
                for (int i = 0; i<visit.getPhotoVisit().size();i++){
                    if (i == visit.getPhotoVisit().size()-1){
                        PhotoVisit photoVisit =  visit.getPhotoVisit().get(visit.getPhotoVisit().size()-1);
                        photoVisit.setPending(Constants.FLAG_DATA_PENDING_NO);
                        photoVisitRealmList.add(photoVisit);
                    }else{
                        photoVisitRealmList.add(visit.getPhotoVisit().get(i));
                    }
                }
                visit.setPhotoVisit(photoVisitRealmList);

            }

            if (moduleVisit.equals(Constants.FLAG_VISIT_ACTION)){
                RealmList<ActionVisit> actionVisitRealmList = new RealmList<>();
                for (int i = 0; i<visit.getActionVisit().size();i++){
                    if (i == visit.getActionVisit().size()-1){
                        ActionVisit actionVisit=  visit.getActionVisit().get(visit.getActionVisit().size()-1);
                        actionVisit.setPending(Constants.FLAG_DATA_PENDING_NO);
                        actionVisitRealmList.add(actionVisit);
                    }else{
                        actionVisitRealmList.add(visit.getActionVisit().get(i));
                    }

                }
                visit.setActionVisit(actionVisitRealmList);
            }

            if (moduleVisit.equals(Constants.FLAG_VISIT_QUIZ)){
                String quizVisitId = "";
                RealmList<QuizVisit> quizVisitRealmList = new RealmList<>();
                for (int i = 0; i<visit.getQuizVisit().size();i++){
                    if (i == visit.getQuizVisit().size()-1){
                        QuizVisit quizVisit=  visit.getQuizVisit().get(visit.getQuizVisit().size()-1);
                         quizVisitId = visit.getQuizVisit().get(visit.getQuizVisit().size()-1).getId();
                        quizVisit.setPending(Constants.FLAG_DATA_PENDING_NO);
                        quizVisitRealmList.add(quizVisit);
                    }else{
                        quizVisitRealmList.add(visit.getQuizVisit().get(i));
                    }

                }
                visit.setQuizVisit(quizVisitRealmList);

                if (visit.getDetailQuizVisit() != null && visit.getDetailQuizVisit().size()>0){
                    RealmList<DetailQuizVisit> detailQuizVisits = new RealmList<>();
                    for (int i = 0; i<visit.getDetailQuizVisit().size();i++){
                        if (visit.getDetailQuizVisit().get(i).getQuizVisitId().equals(quizVisitId)){
                            DetailQuizVisit detailQuizVisit = visit.getDetailQuizVisit().get(i);
                            detailQuizVisit.setPending(Constants.FLAG_DATA_PENDING_NO);
                            detailQuizVisits.add(detailQuizVisit);
                        }else{
                            detailQuizVisits.add(visit.getDetailQuizVisit().get(i));
                        }
                    }
                    visit.setDetailQuizVisit(detailQuizVisits);
                }
            }


            if (moduleVisit.equals(Constants.FLAG_VISIT_SHOOPER)){

                RealmList<CommentShooperVisit> commentShooperVisitRealmList = new RealmList<>();
                for (int i = 0; i<visit.getCommentShooperVisit().size();i++){
                    if (i == visit.getCommentShooperVisit().size()-1){
                        CommentShooperVisit commentShooperVisit=  visit.getCommentShooperVisit().get(visit.getCommentShooperVisit().size()-1);
                        commentShooperVisit.setPending(Constants.FLAG_DATA_PENDING_NO);
                        commentShooperVisitRealmList.add(commentShooperVisit);
                    }else{
                        commentShooperVisitRealmList.add(visit.getCommentShooperVisit().get(i));
                    }

                }
                visit.setCommentShooperVisit(commentShooperVisitRealmList);
            }


            if (moduleVisit.equals(Constants.FLAG_VISIT_T)){
                message = App.context.getResources().getString(R.string.message_finish_visit_correct);

               if (visit.getPhotoVisit() != null){
                   RealmList<PhotoVisit> photoVisitRealmList = new RealmList<>();
                   for (int i = 0 ; i<visit.getPhotoVisit().size();i++){
                       if (visit.getPhotoVisit().get(i).getPending().equals(Constants.FLAG_DATA_PENDING_YES)){
                           PhotoVisit photoVisit=  visit.getPhotoVisit().get(i);
                           photoVisit.setPending(Constants.FLAG_DATA_PENDING_NO);
                           photoVisitRealmList.add(photoVisit);
                       }
                   }
                   visit.setPhotoVisit(photoVisitRealmList);
               }

                if (visit.getActionVisit() != null){
                    RealmList<ActionVisit> actionVisitRealmList = new RealmList<>();
                    for (int i = 0 ; i<visit.getActionVisit().size();i++){
                        if (visit.getActionVisit().get(i).getPending().equals(Constants.FLAG_DATA_PENDING_YES)){
                            ActionVisit actionVisit=  visit.getActionVisit().get(i);
                            actionVisit.setPending(Constants.FLAG_DATA_PENDING_NO);
                            actionVisitRealmList.add(actionVisit);

                        }
                    }
                    visit.setActionVisit(actionVisitRealmList);
                }


                if (visit.getQuizVisit() != null){
                    RealmList<QuizVisit> quizVisitRealmList = new RealmList<>();
                    for (int i = 0 ; i<visit.getQuizVisit().size();i++){
                        if (visit.getQuizVisit().get(i).getPending().equals(Constants.FLAG_DATA_PENDING_YES)){
                            QuizVisit quizVisit =  visit.getQuizVisit().get(i);
                            quizVisit.setPending(Constants.FLAG_DATA_PENDING_NO);
                            quizVisitRealmList.add(quizVisit);

                        }
                    }
                    visit.setQuizVisit(quizVisitRealmList);
                }

                if (visit.getDetailQuizVisit() != null){
                    if (visit.getDetailQuizVisit() != null && visit.getDetailQuizVisit().size()>0){
                        RealmList<DetailQuizVisit> detailQuizVisits = new RealmList<>();
                        for (int i = 0; i<visit.getDetailQuizVisit().size();i++){
                            if (visit.getDetailQuizVisit().get(i).getPending().equals(Constants.FLAG_DATA_PENDING_YES)){
                                DetailQuizVisit detailQuizVisit = visit.getDetailQuizVisit().get(i);
                                detailQuizVisit.setPending(Constants.FLAG_DATA_PENDING_NO);
                                detailQuizVisits.add(detailQuizVisit);
                            }
                        }
                        visit.setDetailQuizVisit(detailQuizVisits);
                    }
                }

                if (visit.getCommentShooperVisit() != null){
                    RealmList<CommentShooperVisit> commentShooperVisitRealmList = new RealmList<>();
                    for (int i = 0 ; i<visit.getCommentShooperVisit().size();i++){
                        if (visit.getCommentShooperVisit().get(i).getPending().equals(Constants.FLAG_DATA_PENDING_YES)){
                            CommentShooperVisit commentShooperVisit=  visit.getCommentShooperVisit().get(i);
                            commentShooperVisit.setPending(Constants.FLAG_DATA_PENDING_NO);
                            commentShooperVisitRealmList.add(commentShooperVisit);

                        }
                    }
                    visit.setCommentShooperVisit(commentShooperVisitRealmList);
                }

                if (visit.getDeliveryDayVisit() != null){
                    RealmList<DeliveryDayVisit> deliveryDayVisits = new RealmList<>();
                    for (int i = 0 ; i<visit.getDeliveryDayVisit().size();i++){
                        if (visit.getDeliveryDayVisit().get(i).getPending().equals(Constants.FLAG_DATA_PENDING_YES)){
                            DeliveryDayVisit deliveryDayVisit=  visit.getDeliveryDayVisit().get(i);
                            deliveryDayVisit.setPending(Constants.FLAG_DATA_PENDING_NO);
                            deliveryDayVisits.add(deliveryDayVisit);

                        }
                    }
                    visit.setDeliveryDayVisit(deliveryDayVisits);
                }


                if (visit.getStockSampligVisit() != null){
                    RealmList<StockSampligVisit> stockSampligVisits = new RealmList<>();
                    for (int i = 0 ; i<visit.getStockSampligVisit().size();i++){
                        if (visit.getStockSampligVisit().get(i).getPending().equals(Constants.FLAG_DATA_PENDING_YES)){
                            StockSampligVisit stockSampligVisit=  visit.getStockSampligVisit().get(i);
                            stockSampligVisit.setPending(Constants.FLAG_DATA_PENDING_NO);
                            stockSampligVisits.add(stockSampligVisit);
                        }
                    }
                    visit.setStockSampligVisit(stockSampligVisits);
                }

                if (visit.getDetailStockSampligVisit() != null && visit.getDetailStockSampligVisit().size()>0){
                    RealmList<DetailStockSampligVisit> detailStockSampligVisits = new RealmList<>();
                    for (int i = 0; i<visit.getDetailStockSampligVisit().size();i++){
                        if ( visit.getDetailStockSampligVisit().get(i).getPending().equals(Constants.FLAG_DATA_PENDING_YES)){
                            DetailStockSampligVisit detailStockSampligVisit = visit.getDetailStockSampligVisit().get(i);
                            detailStockSampligVisit.setPending(Constants.FLAG_DATA_PENDING_NO);
                            detailStockSampligVisits.add(detailStockSampligVisit);
                        }
                    }
                    visit.setDetailStockSampligVisit(detailStockSampligVisits);
                }

                if (visit.getPriceCompetitionVisit() != null){
                    RealmList<PriceCompetitionVisit> priceCompetitionVisits = new RealmList<>();
                    for (int i = 0 ; i<visit.getPriceCompetitionVisit().size();i++){
                        if (visit.getPriceCompetitionVisit().get(i).getPending().equals(Constants.FLAG_DATA_PENDING_YES)){
                            PriceCompetitionVisit priceCompetitionVisit=  visit.getPriceCompetitionVisit().get(i);
                            priceCompetitionVisit.setPending(Constants.FLAG_DATA_PENDING_NO);
                            priceCompetitionVisits.add(priceCompetitionVisit);
                        }
                    }
                    visit.setPriceCompetitionVisit(priceCompetitionVisits);
                }

                if (visit.getDetailPriceCompetitionVisit() != null && visit.getDetailPriceCompetitionVisit().size()>0){
                    RealmList<DetailPriceCompetitionVisit> detailPriceCompetitionVisits = new RealmList<>();
                    for (int i = 0; i<visit.getDetailPriceCompetitionVisit().size();i++){
                        if (visit.getDetailPriceCompetitionVisit().get(i).getPending().equals(Constants.FLAG_DATA_PENDING_YES)){
                            DetailPriceCompetitionVisit detailPriceCompetitionVisit = visit.getDetailPriceCompetitionVisit().get(i);
                            detailPriceCompetitionVisit.setPending(Constants.FLAG_DATA_PENDING_NO);
                            detailPriceCompetitionVisits.add(detailPriceCompetitionVisit);
                        }
                    }
                    visit.setDetailPriceCompetitionVisit(detailPriceCompetitionVisits);
                }

                if (visit.getPriceBreakVisit() != null){
                    RealmList<PriceBreakVisit> priceBreakVisits = new RealmList<>();
                    for (int i = 0 ; i<visit.getPriceBreakVisit().size();i++){
                        if (visit.getPriceBreakVisit().get(i).getPending().equals(Constants.FLAG_DATA_PENDING_YES)){
                            PriceBreakVisit priceBreakVisit=  visit.getPriceBreakVisit().get(i);
                            priceBreakVisit.setPending(Constants.FLAG_DATA_PENDING_NO);
                            priceBreakVisits.add(priceBreakVisit);
                        }
                    }
                    visit.setPriceBreakVisit(priceBreakVisits);
                }

                if (visit.getDetailPriceBreakVisit() != null && visit.getDetailPriceBreakVisit().size()>0){
                    RealmList<DetailPriceBreakVisit> detailPriceBreakVisits = new RealmList<>();
                    for (int i = 0; i<visit.getDetailPriceBreakVisit().size();i++){
                        if (visit.getDetailPriceBreakVisit().get(i).getPending().equals(Constants.FLAG_DATA_PENDING_YES)){
                            DetailPriceBreakVisit detailPriceBreakVisit = visit.getDetailPriceBreakVisit().get(i);
                            detailPriceBreakVisit.setPending(Constants.FLAG_DATA_PENDING_NO);
                            detailPriceBreakVisits.add(detailPriceBreakVisit);
                        }
                    }
                    visit.setDetailPriceBreakVisit(detailPriceBreakVisits);
                }


                if (visit.getStockSamplingFinalVisit() != null ) {
                    RealmList<StockSamplingFinalVisit> stockSamplingFinalVisits = new RealmList<>();
                    for (int i = 0; i<visit.getStockSamplingFinalVisit().size();i++){
                        if (visit.getStockSamplingFinalVisit().get(i).getPending().equals(Constants.FLAG_DATA_PENDING_YES)){
                            StockSamplingFinalVisit stockSamplingFinalVisit =  visit.getStockSamplingFinalVisit().get(i);
                            stockSamplingFinalVisit.setPending(Constants.FLAG_DATA_PENDING_NO);
                            stockSamplingFinalVisits.add(stockSamplingFinalVisit);
                        }
                    }
                    visit.setStockSamplingFinalVisit(stockSamplingFinalVisits);
                }


                if (visit.getDetailStockSamplingFinalVisit() != null  && visit.getDetailStockSamplingFinalVisit().size()>0){
                    RealmList<DetailStockSamplingFinalVisit> detailStockSamplingFinalVisits = new RealmList<>();
                    for (int i = 0 ; i<visit.getDetailStockSamplingFinalVisit().size();i++){
                        if (visit.getDetailStockSamplingFinalVisit().get(i).getPending().equals(Constants.FLAG_DATA_PENDING_YES)){
                            DetailStockSamplingFinalVisit detailStockSamplingFinalVisit =  visit.getDetailStockSamplingFinalVisit().get(i);
                            detailStockSamplingFinalVisit.setPending(Constants.FLAG_DATA_PENDING_NO);
                            detailStockSamplingFinalVisits.add(detailStockSamplingFinalVisit);
                        }
                    }
                    visit.setDetailStockSamplingFinalVisit(detailStockSamplingFinalVisits);
                }

            }else{
                message = App.context.getResources().getString(R.string.message_register_visit_success);
            }

            if (moduleVisit.equals(Constants.FLAG_VISIT_PRICE_COMPETITION)){
                String priceCompetitionVisitId = "";
                RealmList<PriceCompetitionVisit> priceCompetitionVisits = new RealmList<>();
                for (int i = 0; i<visit.getPriceCompetitionVisit().size();i++){
                    if (i == visit.getPriceCompetitionVisit().size()-1){
                        PriceCompetitionVisit priceCompetitionVisit =  visit.getPriceCompetitionVisit().get(visit.getPriceCompetitionVisit().size()-1);
                        priceCompetitionVisitId = visit.getPriceCompetitionVisit().get(visit.getPriceCompetitionVisit().size()-1).getId();
                        priceCompetitionVisit.setPending(Constants.FLAG_DATA_PENDING_NO);
                        priceCompetitionVisits.add(priceCompetitionVisit);
                    }else{
                        priceCompetitionVisits.add(visit.getPriceCompetitionVisit().get(i));
                    }

                }
                visit.setPriceCompetitionVisit(priceCompetitionVisits);

                if (visit.getDetailPriceCompetitionVisit() != null && visit.getDetailPriceCompetitionVisit().size()>0){
                    RealmList<DetailPriceCompetitionVisit> detailPriceCompetitionVisits = new RealmList<>();
                    for (int i = 0; i<visit.getDetailPriceCompetitionVisit().size();i++){
                        if (visit.getDetailPriceCompetitionVisit().get(i).getPriceCompetitionVisitId().equals(priceCompetitionVisitId)){
                            DetailPriceCompetitionVisit detailPriceCompetitionVisit = visit.getDetailPriceCompetitionVisit().get(i);
                            detailPriceCompetitionVisit.setPending(Constants.FLAG_DATA_PENDING_NO);
                            detailPriceCompetitionVisits.add(detailPriceCompetitionVisit);
                        }else{
                            detailPriceCompetitionVisits.add(visit.getDetailPriceCompetitionVisit().get(i));
                        }

                    }
                    visit.setDetailPriceCompetitionVisit(detailPriceCompetitionVisits);
                }
            }

            if (moduleVisit.equals(Constants.FLAG_VISIT_PRICE_BREAK)){

                String priceBreakVisitId = "";
                RealmList<PriceBreakVisit> priceBreakVisits = new RealmList<>();
                for (int i = 0; i<visit.getPriceBreakVisit().size();i++){
                    if (i == visit.getPriceBreakVisit().size()-1){
                        PriceBreakVisit priceBreakVisit=  visit.getPriceBreakVisit().get(visit.getPriceBreakVisit().size()-1);
                        priceBreakVisitId = visit.getPriceBreakVisit().get(visit.getPriceBreakVisit().size()-1).getId();
                        priceBreakVisit.setPending(Constants.FLAG_DATA_PENDING_NO);
                        priceBreakVisits.add(priceBreakVisit);
                    }else{
                        priceBreakVisits.add(visit.getPriceBreakVisit().get(i));
                    }

                }
                visit.setPriceBreakVisit(priceBreakVisits);

                if (visit.getDetailPriceBreakVisit() != null && visit.getDetailPriceBreakVisit().size()>0){
                    RealmList<DetailPriceBreakVisit> detailPriceBreakVisits = new RealmList<>();
                    for (int i = 0; i<visit.getDetailPriceBreakVisit().size();i++){
                        if (visit.getDetailPriceBreakVisit().get(i).getPriceBreakId().equals(priceBreakVisitId)){
                            DetailPriceBreakVisit detailPriceBreakVisit = visit.getDetailPriceBreakVisit().get(i);
                            detailPriceBreakVisit.setPending(Constants.FLAG_DATA_PENDING_NO);
                            detailPriceBreakVisits.add(detailPriceBreakVisit);
                        }else{
                            detailPriceBreakVisits.add(visit.getDetailPriceBreakVisit().get(i));
                        }
                    }
                    visit.setDetailPriceBreakVisit(detailPriceBreakVisits);
                }

            }

            if (moduleVisit.equals(Constants.FLAG_VISIT_STOCK)){

                String stockSampligVisitId = "";
                RealmList<StockSampligVisit> stockSampligVisits = new RealmList<>();
                for (int i = 0; i<visit.getStockSampligVisit().size();i++){
                    if (i == visit.getStockSampligVisit().size()-1){
                        StockSampligVisit stockSampligVisit= visit.getStockSampligVisit().get(visit.getStockSampligVisit().size()-1);
                        stockSampligVisitId = visit.getStockSampligVisit().get(visit.getStockSampligVisit().size()-1).getId();
                        stockSampligVisit.setPending(Constants.FLAG_DATA_PENDING_NO);
                        stockSampligVisits.add(stockSampligVisit);
                    }else{
                        stockSampligVisits.add(visit.getStockSampligVisit().get(i));
                    }

                }
                visit.setStockSampligVisit(stockSampligVisits);


                if (visit.getDetailStockSampligVisit() != null && visit.getDetailStockSampligVisit().size()>0){
                    RealmList<DetailStockSampligVisit> detailStockSampligVisits = new RealmList<>();
                    for (int i = 0; i<visit.getDetailStockSampligVisit().size();i++){
                        if ( visit.getDetailStockSampligVisit().get(i).getStockSampligVisitId().equals(stockSampligVisitId)){
                            DetailStockSampligVisit detailStockSampligVisit = visit.getDetailStockSampligVisit().get(i);
                            detailStockSampligVisit.setPending(Constants.FLAG_DATA_PENDING_NO);
                            detailStockSampligVisits.add(detailStockSampligVisit);
                        }else{
                            detailStockSampligVisits.add(visit.getDetailStockSampligVisit().get(i));
                        }
                    }
                    visit.setDetailStockSampligVisit(detailStockSampligVisits);
                }
            }

            if (moduleVisit.equals(Constants.FLAG_VISIT_STOCK_FINAL)){

                String stockSamplingFinalVisitId = "";
                RealmList<StockSamplingFinalVisit> stockSamplingFinalVisits = new RealmList<>();
                for (int i = 0; i<visit.getStockSamplingFinalVisit().size();i++){
                    if (i == visit.getStockSamplingFinalVisit().size()-1){
                        StockSamplingFinalVisit stockSampligFinalVisit = visit.getStockSamplingFinalVisit().get(visit.getStockSamplingFinalVisit().size()-1);
                        stockSamplingFinalVisitId = visit.getStockSamplingFinalVisit().get(visit.getStockSamplingFinalVisit().size()-1).getId();
                        stockSampligFinalVisit.setPending(Constants.FLAG_DATA_PENDING_NO);
                        stockSamplingFinalVisits.add(stockSampligFinalVisit);
                    }else{
                        stockSamplingFinalVisits.add(visit.getStockSamplingFinalVisit().get(i));
                    }

                }
                visit.setStockSamplingFinalVisit(stockSamplingFinalVisits);


                if (visit.getDetailStockSamplingFinalVisit() != null && visit.getDetailStockSamplingFinalVisit().size()>0){
                    RealmList<DetailStockSamplingFinalVisit> detailStockSamplingFinalVisits = new RealmList<>();
                    for (int i = 0; i<visit.getDetailStockSamplingFinalVisit().size();i++){
                        if ( visit.getDetailStockSamplingFinalVisit().get(i).getStockSamplingFinalVisitId().equals(stockSamplingFinalVisitId)){
                            DetailStockSamplingFinalVisit detailStockSamplingFinalVisit = visit.getDetailStockSamplingFinalVisit().get(i);
                            detailStockSamplingFinalVisit.setPending(Constants.FLAG_DATA_PENDING_NO);
                            detailStockSamplingFinalVisits.add(detailStockSamplingFinalVisit);
                        }else{
                            detailStockSamplingFinalVisits.add(visit.getDetailStockSamplingFinalVisit().get(i));
                        }
                    }
                    visit.setDetailStockSamplingFinalVisit(detailStockSamplingFinalVisits);
                }
            }

            if (moduleVisit.equals(Constants.FLAG_VISIT_DELIVERY)){

                RealmList<DeliveryDayVisit> deliveryDayVisits = new RealmList<>();
                for (int i = 0; i<visit.getDeliveryDayVisit().size();i++){
                    if (i == visit.getDeliveryDayVisit().size()-1){
                        DeliveryDayVisit deliveryDayVisit = visit.getDeliveryDayVisit().get(visit.getDeliveryDayVisit().size()-1);
                        deliveryDayVisit.setPending(Constants.FLAG_DATA_PENDING_NO);
                        deliveryDayVisits.add(deliveryDayVisit);
                    }else{
                        deliveryDayVisits.add(visit.getDeliveryDayVisit().get(i));
                    }

                }
                visit.setDeliveryDayVisit(deliveryDayVisits);
            }

        }else{
            message = App.context.getResources().getString(R.string.message_register_visit_error);
        }


        VisitInteractor.getInstance().registerVisit(visit,moduleVisit,false, new VisitInteractor.SuccessRegisterVisitListener() {
            @Override
            public void onSuccessRegisterVisit() {
                mView.stopLoading();
                if (moduleVisit.equals(Constants.FLAG_VISIT_PHOTO)){
                    ((MenuVisitContract.PhotoView)mView).onRegisterPhotoSuccess( message,visit);
                }

                if (moduleVisit.equals(Constants.FLAG_VISIT_QUIZ)){
                    ((MenuVisitContract.QuizQuestionView)mView).onRegisterQuizSuccess( message,visit);
                }

                if (moduleVisit.equals(Constants.FLAG_VISIT_ACTION)){
                    ((MenuVisitContract.ActionView)mView).onRegisterActionSuccess( message,visit);
                }

                if (moduleVisit.equals(Constants.FLAG_VISIT_ACTION)){
                    ((MenuVisitContract.QuizQuestionView)mView).onRegisterQuizSuccess( message,visit);
                }

                if (moduleVisit.equals(Constants.FLAG_VISIT_SHOOPER)){
                    ((MenuVisitContract.CommentShooperView)mView).onRegisterCommentShooperSuccess( message,visit);
                }

                if (moduleVisit.equals(Constants.FLAG_VISIT_T)){
                    ((MenuVisitContract.FinishView)mView).onRegisterSuccess(message);
                }
                if (moduleVisit.equals(Constants.FLAG_VISIT_PRICE_COMPETITION) || moduleVisit.equals(Constants.FLAG_VISIT_PRICE_BREAK)){
                    ((MenuVisitContract.PriceView)mView).onRegisterPriceSuccess(message,visit);
                }

                if (moduleVisit.equals(Constants.FLAG_VISIT_STOCK)){
                    ((MenuVisitContract.StockSamplingView)mView).onRegisterMerchandisingSuccess(message,visit);
                }

                if (moduleVisit.equals(Constants.FLAG_VISIT_STOCK_FINAL)){
                    ((MenuVisitContract.StockSamplingFinalView)mView).onRegisterMerchandisingStockFinalSuccess(message,visit);
                }

                if (moduleVisit.equals(Constants.FLAG_VISIT_DELIVERY)){
                    ((MenuVisitContract.DeliveryProductView)mView).onRegisterDeliveryProductSuccess(message,visit);
                }
            }

        }, new BaseInteractor.ErrorCallback() {
            @Override
            public void onError(Exception exception) {
                mView.stopLoading();
                mView.showErrorMessage(exception);
            }
        });
    }

    private void onSaveRegisterClient(Client client, boolean isOnline, String  moduleVisit, Visit visit){

        if (!isOnline){
            client.setPending(Constants.FLAG_DATA_PENDING_YES);
        }

        if (isOnline && moduleVisit.equals(Constants.FLAG_VISIT_T)){
            client.setPending(Constants.FLAG_DATA_PENDING_NO);
        }

        ClientInteractor.getInstance().registerClient(client, new ClientInteractor.SuccessRegisterClientListener() {
            @Override
            public void onSuccessRegisterClient() {
                saveRegisterVisit(visit,isOnline,moduleVisit);
            }
        }, new BaseInteractor.ErrorCallback() {
            @Override
            public void onError(Exception exception) {
                mView.stopLoading();
                mView.showErrorMessage(exception);
            }
        });
    }

}


