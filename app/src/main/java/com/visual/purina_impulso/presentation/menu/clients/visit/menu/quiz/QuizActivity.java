package com.visual.purina_impulso.presentation.menu.clients.visit.menu.quiz;

import android.content.Intent;
import android.os.Bundle;

import android.support.annotation.NonNull;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import com.visual.purina_impulso.Constants;
import com.visual.purina_impulso.R;
import com.visual.purina_impulso.base.BaseActivity;

import com.visual.purina_impulso.domain.Client;
import com.visual.purina_impulso.domain.Visit;
import com.visual.purina_impulso.presentation.menu.clients.visit.menu.MenuVisitActivity;
import com.visual.purina_impulso.presentation.menu.clients.visit.menu.MenuVisitContract;
import com.visual.purina_impulso.presentation.menu.clients.visit.menu.MenuVisitPresenter;
import com.visual.purina_impulso.util.ActivityUtils;

import butterknife.BindView;


public class QuizActivity extends BaseActivity implements MenuVisitContract.View {

    @BindView(R.id.toolbar_main) Toolbar mToolbar;
    @BindView(R.id.toolbar_title_text_view) TextView mToolbarTitleText;

    public  MenuVisitPresenter mPresenter;
    @Override
    protected int getResLayout() {
        return R.layout.activity_visit_quiz;
    }

    @Override
    protected void setupView(Bundle savedInstanceState) {
        QuizFragment quizFragment = (QuizFragment) getSupportFragmentManager().findFragmentById(R.id.contentFrameVisitQuiz);

        if (quizFragment == null) {
            quizFragment = QuizFragment.newInstance(getClient(),getVisit());
            ActivityUtils.addFragmentToActivity(getSupportFragmentManager(), quizFragment, R.id.contentFrameVisitQuiz);
        }

        mPresenter =  new MenuVisitPresenter(quizFragment);

        ActivityUtils.setToolBarBack(this,mToolbar,true);
    }

    public void setTitleToolbar(String mTitleToolbar) {
        mToolbarTitleText.setText(mTitleToolbar);
    }

    @Override
    public void setPresenter(@NonNull MenuVisitContract.Presenter presenter) {

    }

    @Override
    public Client getClient() {
        return getIntent().getParcelableExtra(Constants.BUNDLE_CLIENT_VISIT);
    }

    @Override
    public Visit getVisit() {
        return getIntent().getParcelableExtra(Constants.BUNDLE_CLIENT_VISIT_DETAIL);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home) {

            Intent intent = new Intent(this, MenuVisitActivity.class);
            intent.putExtra(Constants.BUNDLE_CLIENT_VISIT,getClient());
            intent.putExtra(Constants.BUNDLE_CLIENT_VISIT_DETAIL,getVisit());
            startActivity(intent);
            finish();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(this, MenuVisitActivity.class);
        intent.putExtra(Constants.BUNDLE_CLIENT_VISIT,getClient());
        intent.putExtra(Constants.BUNDLE_CLIENT_VISIT_DETAIL,getVisit());
        startActivity(intent);
        finish();
    }
}