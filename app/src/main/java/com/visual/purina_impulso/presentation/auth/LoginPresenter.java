package com.visual.purina_impulso.presentation.auth;

import android.support.annotation.NonNull;

import com.visual.purina_impulso.App;
import com.visual.purina_impulso.Constants;
import com.visual.purina_impulso.R;
import com.visual.purina_impulso.base.BaseInteractor;
import com.visual.purina_impulso.data.source.helper.PreferencesHelper;
import com.visual.purina_impulso.domain.Sync;
import com.visual.purina_impulso.domain.interactor.SyncInteractor;
import com.visual.purina_impulso.domain.interactor.UserInteractor;


class LoginPresenter implements LoginContract.Presenter {

    private final LoginContract.View mView;

    LoginPresenter(@NonNull LoginContract.View view) {
        mView = view;
        mView.setPresenter(this);
    }

    @Override
    public void start() {

    }

    @Override
    public void onCheckVersion() {
        String version = "1.7.3";
        UserInteractor.getInstance().checkVersion(version, new UserInteractor.SuccessCheckVersionListener() {
            @Override
            public void onSuccessCheckVersion() {

            }
        }, new BaseInteractor.ErrorCallback() {
            @Override
            public void onError(Exception exception) {
                mView.showErrorMessage(exception);
            }
        });
    }

    @Override
    public void login(String email, String password) {
        onLogin( email, password);
    }

    @Override
    public void clearDB() {
        mView.startLoading();
        SyncInteractor.getInstance().clearSync(new SyncInteractor.SuccessClearSyncListener() {
            @Override
            public void onSuccessClearSync() {
                mView.stopLoading();
                PreferencesHelper.setUser(null);
                PreferencesHelper.setSystemId(null);
                mView.onClearDBSuccess(App.context.getResources().getString(R.string.message_success_delete));
            }

        }, new BaseInteractor.ErrorCallback() {
            @Override
            public void onError(Exception exception) {
                mView.stopLoading();
                mView.showErrorMessage(exception);
            }
        });
    }

    private void onLogin(String email, String password){
        if (isValidInput(email,password)) {
            mView.startLoading();

            UserInteractor.getInstance().login(email, password, new UserInteractor.SuccessLoginListener() {
                @Override
                public void onSuccessLogin() {
                    mView.stopLoading();
                        PreferencesHelper.setSystemId(Constants.FLAG_CHANNEL_MODERN);
                      //  onClearDB();
                    onSync(String.valueOf(PreferencesHelper.getUser().getSystemId()));

                }
            }, new BaseInteractor.ErrorCallback() {
                @Override
                public void onError(Exception exception) {
                    mView.stopLoading();
                    mView.showErrorMessage(exception);
                }
            });
        }
    }

    private void onClearDB(){

        SyncInteractor.getInstance().clearSync(new SyncInteractor.SuccessClearSyncListener() {
            @Override
            public void onSuccessClearSync() {

                onSync(String.valueOf(PreferencesHelper.getUser().getSystemId()));
            }

        }, new BaseInteractor.ErrorCallback() {
            @Override
            public void onError(Exception exception) {
                mView.stopLoading();
                mView.showErrorMessage(exception);
            }
        });
    }



    private boolean isValidInput(@NonNull String email , @NonNull String password) {

        if (email.isEmpty() ){
            mView.showErrorMessage(new Exception(App.context.getString(R.string.message_login_user_empty)));
            return false;
        }

        if (password.isEmpty()){
            mView.showErrorMessage(new Exception(App.context.getString(R.string.message_login_user_empty)));
            return false;
        }

        return true;
    }


    public void onSync(String type) {
        mView.startLoadingText(App.context.getResources().getString(R.string.dialog_sync_message));
        String syncTraditional = "false";
        String syncModern = "false";
        String syncSpecialized = "false";
        if (type.equals("1")){
            syncTraditional = "true";
        }else if(type.equals("2") || type.equals("4")){
            syncModern = "true";
        }else{
            syncSpecialized = "true";
        }

        SyncInteractor.getInstance().sync(syncTraditional,syncModern,syncSpecialized, new SyncInteractor.SuccessSyncListener() {
            @Override
            public void onSuccessSync(Sync sync) {
                saveSync(sync);
            }
        }, new BaseInteractor.ErrorCallback() {
            @Override
            public void onError(Exception exception) {
                mView.stopLoading();
                mView.showErrorMessage(exception);
            }
        });
    }


    private void saveSync(Sync sync){

        SyncInteractor.getInstance().saveSync(sync,true, new SyncInteractor.SuccessSaveSyncListener() {
            @Override
            public void onSuccessSaveSync(String message) {
                mView.stopLoading();
                mView.onSyncSuccess(message);
            }
        }, new BaseInteractor.ErrorCallback() {
            @Override
            public void onError(Exception exception) {
                mView.stopLoading();
                mView.showErrorMessage(exception);
            }
        });
    }

}