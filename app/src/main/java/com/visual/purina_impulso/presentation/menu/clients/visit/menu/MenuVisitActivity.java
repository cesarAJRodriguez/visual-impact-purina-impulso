package com.visual.purina_impulso.presentation.menu.clients.visit.menu;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.visual.purina_impulso.Constants;
import com.visual.purina_impulso.R;
import com.visual.purina_impulso.base.BaseActivity;
import com.visual.purina_impulso.domain.Client;
import com.visual.purina_impulso.domain.Visit;
import com.visual.purina_impulso.util.ActivityUtils;

import butterknife.BindView;

public class MenuVisitActivity extends BaseActivity implements MenuVisitContract.View{

    @BindView(R.id.toolbar_main) Toolbar mToolbar;
    @BindView(R.id.toolbar_title_text_view) TextView mToolbarTitleText;


    @Override
    protected int getResLayout() {
        return R.layout.activity_menu_visit;
    }

    @Override
    protected void setupView(Bundle savedInstanceState) {
        MenuVisitFragment menuVisitFragment = (MenuVisitFragment) getSupportFragmentManager().findFragmentById(R.id.contentFrameMenuVisit);

        if (menuVisitFragment == null) {
            menuVisitFragment = MenuVisitFragment.newInstance(getClient(),getVisit());
            ActivityUtils.addFragmentToActivity(getSupportFragmentManager(), menuVisitFragment, R.id.contentFrameMenuVisit);
        }
    }

    public void setTitleToolbar(String mTitleToolbar) {
        mToolbarTitleText.setText(mTitleToolbar);
    }


    @Override
    public void setPresenter(@NonNull MenuVisitContract.Presenter presenter) {

    }

    @Override
    public Client getClient() {
        return getIntent().getParcelableExtra(Constants.BUNDLE_CLIENT_VISIT);
    }

    @Override
    public Visit getVisit() {
        return getIntent().getParcelableExtra(Constants.BUNDLE_CLIENT_VISIT_DETAIL);
    }

    @Override
    public void onBackPressed() {}
}