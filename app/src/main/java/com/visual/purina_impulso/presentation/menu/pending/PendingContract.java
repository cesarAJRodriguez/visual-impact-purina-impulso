package com.visual.purina_impulso.presentation.menu.pending;

import com.visual.purina_impulso.base.BasePresenter;
import com.visual.purina_impulso.base.BaseViewPresenter;
import com.visual.purina_impulso.domain.Assistance;
import com.visual.purina_impulso.domain.Client;

import java.util.ArrayList;



interface PendingContract {

    interface Presenter extends BasePresenter {
        void onCheckCountPendingAssistance(Assistance assistance);
        void onRegisterAssistance(Assistance assistance,ArrayList<Assistance> assistances);
        void onCheckVisitPending();
        void onSendPendingVisit(Client client);

    }

    interface View extends BaseViewPresenter<Presenter> {
        void onSuccessCheckCountPendingAssistance(ArrayList<Assistance> assistances, ArrayList<Client> clients);
        void onSuccessCheckVisitPending(ArrayList<Client> clients);
        void onRegisterAssistanceSuccess(String message);
        void onSendPendingVisitSuccess(String message);
    }
}
