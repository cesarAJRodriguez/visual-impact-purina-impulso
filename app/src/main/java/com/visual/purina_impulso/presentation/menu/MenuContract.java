package com.visual.purina_impulso.presentation.menu;

import com.visual.purina_impulso.base.BasePresenter;
import com.visual.purina_impulso.base.BaseViewPresenter;
import com.visual.purina_impulso.domain.Assistance;
import com.visual.purina_impulso.domain.Client;

import java.util.ArrayList;


interface MenuContract {

    interface Presenter extends BasePresenter {
        void onSync(String type);
        void onCheckAssistance(Assistance assistance);

    }

    interface View extends BaseViewPresenter<Presenter> {
        void onSuccessSyncMenu(String message);
        void onSuccessCheckAssistance(String message,ArrayList<Assistance> assistances,ArrayList<Client> clients);


    }
}
