package com.visual.purina_impulso.presentation.menu.clients.visit.menu.quiz;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import com.visual.purina_impulso.Constants;
import com.visual.purina_impulso.R;
import com.visual.purina_impulso.base.BaseFragment;
import com.visual.purina_impulso.domain.Alternative;
import com.visual.purina_impulso.domain.Client;
import com.visual.purina_impulso.domain.Question;
import com.visual.purina_impulso.domain.Quiz;
import com.visual.purina_impulso.domain.Visit;
import com.visual.purina_impulso.presentation.menu.clients.visit.menu.MenuVisitContract;
import com.visual.purina_impulso.presentation.menu.clients.visit.menu.MenuVisitPresenter;
import com.visual.purina_impulso.view.adapter.QuestionAdapter;

import java.util.ArrayList;

import butterknife.BindView;


public class QuizQuesionFragment extends BaseFragment implements MenuVisitContract.QuizQuestionView,QuestionAdapter.OnItemClickListener{

    @BindView(R.id.txt_title_quiz) TextView titleQuizTextView;
    @BindView(R.id.rv_quiz_question) RecyclerView questionRecyclerView;

    private FragmentManager mFragmentManager;
    private MenuVisitContract.Presenter mPresenter;

    public static QuizQuesionFragment newInstance(Quiz quiz) {
        Bundle arg = new Bundle();
        arg.putParcelable(Constants.BUNDLE_CLIENT_QUIZ_VISIT, quiz);
        QuizQuesionFragment fragment = new QuizQuesionFragment();
        fragment.setArguments(arg);
        return fragment;
    }

    @Override
    protected int getResLayout() {
        return R.layout.fragment_quiz_question;
    }

    @Override
    public void onResume() {
        super.onResume();
        ((QuizActivity)mContext).setTitleToolbar(getResources().getString(R.string.menu_visit_quiz));
        if (getQuiz() != null){
            titleQuizTextView.setText(getQuiz().getQuizName());
        }else{
            titleQuizTextView.setText("");
        }

        if (mPresenter == null){
            mPresenter = new MenuVisitPresenter(this);
        }

        mPresenter.getQuizQuestions(getQuiz().getQuizId());
    }

    @Override
    protected void setupView(Bundle savedInstanceState) {
    }

    @Override
    protected void onRestoreView(Bundle savedInstanceState) {

    }

    @Override
    public void setPresenter(@NonNull MenuVisitContract.Presenter presenter) {
        mPresenter = presenter;
    }


    private void setupRecycler(ArrayList<Question> questions,ArrayList<Alternative> alternatives) {
       /* QuestionAdapter quizAdapter = new QuestionAdapter(this,mContext,questions,alternatives);
        questionRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        questionRecyclerView.setAdapter(quizAdapter);*/
    }

    @Override
    public Client getClient() {
        return null;
    }

    @Override
    public Visit getVisit() {
        return null;
    }

    @Override
    public Quiz getQuiz() {
        Bundle args = getArguments();
        if (args != null) {
            return args.getParcelable(Constants.BUNDLE_CLIENT_QUIZ_VISIT);
        }
        return null;
    }

    @Override
    public void getQuizQuestionsAlternativesSuccess(ArrayList<Question> questions, ArrayList<Alternative> alternatives) {
        setupRecycler(questions,alternatives);
    }

    @Override
    public void onRegisterQuizSuccess(String message, Visit visit) {

    }

    @Override
    public void onItemClick(Object object, int position) {

    }

    public void onRegisterQuiz(){
        Visit visit = new Visit();

    }
}