package com.visual.purina_impulso.presentation.menu.clients.visit.menu.price;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.visual.purina_impulso.Constants;
import com.visual.purina_impulso.R;
import com.visual.purina_impulso.base.BaseActivity;
import com.visual.purina_impulso.domain.Client;
import com.visual.purina_impulso.domain.DetailPriceBreakVisit;
import com.visual.purina_impulso.domain.DetailPriceCompetitionVisit;
import com.visual.purina_impulso.domain.Price;
import com.visual.purina_impulso.domain.PriceBreakVisit;
import com.visual.purina_impulso.domain.PriceCompetitionVisit;
import com.visual.purina_impulso.domain.Visit;
import com.visual.purina_impulso.presentation.menu.clients.visit.menu.MenuVisitActivity;
import com.visual.purina_impulso.presentation.menu.clients.visit.menu.MenuVisitContract;
import com.visual.purina_impulso.presentation.menu.clients.visit.menu.MenuVisitPresenter;
import com.visual.purina_impulso.util.ActivityUtils;
import com.visual.purina_impulso.util.DialogUtil;
import com.visual.purina_impulso.util.FileUtil;
import com.visual.purina_impulso.util.ImageProcessAsynTask;
import com.visual.purina_impulso.view.adapter.PriceBreakAdapter;
import com.visual.purina_impulso.view.adapter.PriceCompetitionAdapter;
import com.visual.purina_impulso.view.dialog.IDialogCameraView;
import com.visual.purina_impulso.view.dialog.IDialogVisit;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;
import java.util.UUID;

import butterknife.BindView;
import butterknife.OnClick;
import io.realm.RealmList;



public class PriceTypeActivity extends BaseActivity implements MenuVisitContract.PriceView, PriceBreakAdapter.OnItemClickListener,IDialogVisit.VisitPriceDialog,IDialogCameraView{

    @BindView(R.id.toolbar_main) Toolbar mToolbar;
    @BindView(R.id.toolbar_title_text_view) TextView mToolbarTitleText;

    private String typePrice;
    private int positionRv;
    private int priceIdItem;
    private boolean totalPriceBreak = false;

    public  MenuVisitPresenter mPresenter;

    @BindView(R.id.rv_search_product) RecyclerView productRecyclerView;
    @BindView(R.id.txt_message_price) TextView messagePriceTextView;


    @BindView(R.id.et_search_product) EditText searchProductEditText;
    @BindView(R.id.btn_save_price) Button savePriceButton;

    @BindView(R.id.ll_search_product) LinearLayout searchProductLinearLayout;

    @BindView(R.id.ll_search_product_title) LinearLayout titleLinearLayout;
    @BindView(R.id.ll_search_product_et) LinearLayout searchProductEtLinearLayout;
    @BindView(R.id.ll_price_rv) LinearLayout priceRvLinearLayout;
    @BindView(R.id.ll_buttons) LinearLayout buttonsLinearLayout;


    private FragmentManager mFragmentManager;
    public Visit visitBundle;

    private static final int CAMERA_REQUEST_CODE = 102;
    private static final int REQUEST_IMAGE_CAMERA = 555;

    private String prefixFileName = "report-image-p";
    private String tempName = "";



    public static String pathPhotos="";
    public static Uri uriPhotos;
    public static String nameFiles="";
    public static String base64Photo="";

    @Override
    protected int getResLayout() {
        return R.layout.activity_visit_price_type;
    }

    @Override
    protected void setupView(Bundle savedInstanceState) {

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        mPresenter =  new MenuVisitPresenter(this);

            if (mPresenter == null){
                mPresenter = new MenuVisitPresenter(this);
            }

            if (Constants.FLAG_PRICE_TYPE.equals(Constants.FLAG_PRICE_TYPE_BREAK)){
                setTitleToolbar(getResources().getString(R.string.menu_visit_precio_type_break));
                mPresenter.getPrices(getClient().getClientId(),Constants.FLAG_PRICE_TYPE,null);
                totalPriceBreak = true;
            }else{
               setTitleToolbar(getResources().getString(R.string.menu_visit_precio_type_competition));
                mPresenter.getPrices(0,Constants.FLAG_PRICE_TYPE,null);
            }

      /*  messagePriceTextView.setVisibility(View.GONE);
        priceRvLinearLayout.setVisibility(View.GONE);
        searchProductLinearLayout.setVisibility(View.GONE);
       savePriceButton.setVisibility(View.GONE);*/
        setupListener();
        ActivityUtils.setToolBarBack(this,mToolbar,true);
        setupKeyboard();
    }


    private void setupListener(){


                searchProductEditText.addTextChangedListener(new TextWatcher() {

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }


            @Override
            public void afterTextChanged(Editable s) {

            }

            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                String search = searchProductEditText.getText().toString();

                if (Constants.FLAG_PRICE_TYPE.equals(Constants.FLAG_PRICE_TYPE_BREAK)){
                    totalPriceBreak = false;
                    mPresenter.getPrices(getClient().getClientId(),Constants.FLAG_PRICE_TYPE,search);
                }else{

                    mPresenter.getPrices(0,Constants.FLAG_PRICE_TYPE,search);
                }


            }

        });
    }

    private void requestPermissionCamera(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.CAMERA},
                        CAMERA_REQUEST_CODE);
            }else{
                callCamera(this);
            }
        }else{
            callCamera(this);
        }

    }

    @Override
    public void  onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case CAMERA_REQUEST_CODE: {
                if (grantResults.length == 0 || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                } else {

                    callCamera(this);
                }
                return;
            }
        }
    }

    public void setTitleToolbar(String mTitleToolbar) {
        mToolbarTitleText.setText(mTitleToolbar);
    }

    @Override
    public void setPresenter(@NonNull MenuVisitContract.Presenter presenter) {

    }

    @Override
    public Client getClient() {
        return getIntent().getParcelableExtra(Constants.BUNDLE_CLIENT_VISIT);
    }

    @Override
    public Visit getVisit() {
        return getIntent().getParcelableExtra(Constants.BUNDLE_CLIENT_VISIT_DETAIL);
    }

    @Override
    public void onItemClick(Object object, int position) {

    }

    @Override
    public String getTypePrice() {
        return getIntent().getStringExtra(Constants.FLAG_PRICE_TYPE);
    }

    @Override
    public void getPricesSuccess(ArrayList<Price> prices) {
        if(prices != null && prices.size() > 0){
            setupRecycler(prices);
            messagePriceTextView.setVisibility(View.GONE);
            priceRvLinearLayout.setVisibility(View.VISIBLE);
            searchProductLinearLayout.setVisibility(View.VISIBLE);
            savePriceButton.setVisibility(View.VISIBLE);
        }else{
           /* messagePriceTextView.setVisibility(View.VISIBLE);
            priceRvLinearLayout.setVisibility(View.GONE);
            searchProductLinearLayout.setVisibility(View.GONE);
            savePriceButton.setVisibility(View.GONE);*/
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home) {
            finish();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRegisterPriceSuccess(String message, Visit visit) {
        visitBundle  = visit;
        DialogUtil.showDialogMessageRegisterPriceVisit(this,message,this);
    }

    private void setupRecycler(ArrayList<Price> prices) {
        if (Constants.FLAG_PRICE_TYPE.equals(Constants.FLAG_PRICE_TYPE_BREAK)){
            if (totalPriceBreak){
                Constants.SEARCH_PRICE_BREAK = prices;
            }

            PriceBreakAdapter adapter = new PriceBreakAdapter(this,this,prices,this);
            productRecyclerView.setLayoutManager(new LinearLayoutManager(this));
            productRecyclerView.setAdapter(adapter);
        }else{

            PriceCompetitionAdapter adapter = new PriceCompetitionAdapter(this,this,prices);
            productRecyclerView.setLayoutManager(new LinearLayoutManager(this));
            productRecyclerView.setAdapter(adapter);
        }
    }


    @OnClick(R.id.btn_save_price)
    public void onRegisterPrice(){
        if (Constants.FLAG_PRICE_TYPE.equals(Constants.FLAG_PRICE_TYPE_BREAK)){
            if (Constants.SEARCH_PRICE_BREAK != null && Constants.SEARCH_PRICE_BREAK.size()>0){

                Visit visit = getVisit();

                String priceBreakId = UUID.randomUUID().toString();
                RealmList<PriceBreakVisit> priceBreakVisits = new RealmList<>();
                RealmList<DetailPriceBreakVisit> detailPriceBreakVisits = new RealmList<>();

                if (visit.getPriceBreakVisit() != null ){
                    if (visit.getPriceBreakVisit().size() > 0){
                        priceBreakVisits.addAll(visit.getPriceBreakVisit());
                    }
                }

                Calendar calendar = Calendar.getInstance();
                SimpleDateFormat formatDate = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
                String date =  formatDate.format(calendar.getTime());

                SimpleDateFormat formatHour = new SimpleDateFormat("HH:mm:ss");
                String hour =   formatHour.format(calendar.getTime());

                PriceBreakVisit priceBreakVisit = new PriceBreakVisit(priceBreakId,
                        hour,
                        date,
                        getClient().getClientId(),
                        Integer.parseInt(getClient().getEmployeeId()),
                        getVisit().getId(),
                        Constants.FLAG_DATA_PENDING_YES);

                priceBreakVisits.add(priceBreakVisit);

                if (visit.getDetailPriceBreakVisit() != null ){
                    if (visit.getDetailPriceBreakVisit().size() > 0){
                        detailPriceBreakVisits.addAll(visit.getDetailPriceBreakVisit());
                    }
                }

                for (int i = 0 ; i< Constants.SEARCH_PRICE_BREAK.size();i++){
                    String detailPriceBreakId = UUID.randomUUID().toString();
                    DetailPriceBreakVisit detailPriceBreakVisit = new DetailPriceBreakVisit(
                            detailPriceBreakId,
                            Constants.SEARCH_PRICE_BREAK.get(i).getProductId(),
                            priceBreakId,
                            Integer.parseInt(getClient().getEmployeeId()),
                            date,
                            getClient().getClientId(),
                            String.valueOf(Constants.SEARCH_PRICE_BREAK.get(i).getPriceBreak()),
                            Constants.SEARCH_PRICE_BREAK.get(i).getPrice(),
                            Constants.SEARCH_PRICE_BREAK.get(i).getPricePromotion(),
                            Constants.SEARCH_PRICE_BREAK.get(i).getPriceSuggested(),
                            Constants.SEARCH_PRICE_BREAK.get(i).getPhotoBase64(),
                            getVisit().getId(),
                            Constants.FLAG_DATA_PENDING_YES);

                    detailPriceBreakVisits.add(detailPriceBreakVisit);
                }




                visit.setPriceBreakVisit(priceBreakVisits);
                visit.setDetailPriceBreakVisit(detailPriceBreakVisits);

                mPresenter.onRegisterVisit(visit,Constants.FLAG_VISIT_PRICE_COMPETITION,getClient());



            }else{
                DialogUtil.showDialogMessage(this,getResources().getString(R.string.message_product_competition_empty));
            }
        }else{
            if (Constants.SEARCH_PRICE_COMPETITION != null && Constants.SEARCH_PRICE_COMPETITION.size()>0){

                Visit visit = getVisit();

                String priceCompetitionId = UUID.randomUUID().toString();
                RealmList<PriceCompetitionVisit> priceCompetitionVisits = new RealmList<>();
                RealmList<DetailPriceCompetitionVisit> detailPriceCompetitionVisits = new RealmList<>();

                if (visit.getPriceCompetitionVisit() != null ){
                    if (visit.getPriceCompetitionVisit().size() > 0){
                        priceCompetitionVisits.addAll(visit.getPriceCompetitionVisit());
                    }
                }

                Calendar calendar = Calendar.getInstance();
                SimpleDateFormat formatDate = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
                String date =  formatDate.format(calendar.getTime());

                SimpleDateFormat formatHour = new SimpleDateFormat("HH:mm:ss");
                String hour =   formatHour.format(calendar.getTime());

                PriceCompetitionVisit priceCompetitionVisit = new PriceCompetitionVisit(priceCompetitionId,
                        hour,
                        date,
                        getClient().getClientId(),
                        Integer.parseInt(getClient().getEmployeeId()),
                        getVisit().getId(),
                        Constants.FLAG_DATA_PENDING_YES);

                priceCompetitionVisits.add(priceCompetitionVisit);

                if (visit.getDetailPriceCompetitionVisit() != null ){
                    if (visit.getDetailPriceCompetitionVisit().size() > 0){
                        detailPriceCompetitionVisits.addAll(visit.getDetailPriceCompetitionVisit());
                    }
                }

                for (int i = 0 ; i< Constants.SEARCH_PRICE_COMPETITION.size();i++){
                    String detailPriceCompetitionId = UUID.randomUUID().toString();
                    DetailPriceCompetitionVisit detailPriceCompetitionVisit = new DetailPriceCompetitionVisit(
                            detailPriceCompetitionId,
                            Constants.SEARCH_PRICE_COMPETITION.get(i).getProductId(),
                            priceCompetitionId,
                            Integer.parseInt(getClient().getEmployeeId()),
                            date,
                            getClient().getClientId(),
                            Constants.SEARCH_PRICE_COMPETITION.get(i).getDistribution(),
                            Constants.SEARCH_PRICE_COMPETITION.get(i).getPrice(),
                            Constants.SEARCH_PRICE_COMPETITION.get(i).getPricePromotion(),
                            getVisit().getId(),
                            Constants.FLAG_DATA_PENDING_YES);
                    detailPriceCompetitionVisits.add(detailPriceCompetitionVisit);
                }

                visit.setPriceCompetitionVisit(priceCompetitionVisits);
                visit.setDetailPriceCompetitionVisit(detailPriceCompetitionVisits);

                mPresenter.onRegisterVisit(visit,Constants.FLAG_VISIT_PRICE_COMPETITION,getClient());
            }else{
                DialogUtil.showDialogMessage(this,getResources().getString(R.string.message_product_competition_empty));
            }
        }
    }

    @OnClick(R.id.btn_cancel_price)
    public void onCancel()  {
        finish();
    }


    @Override
    public void onConfirmPriceVisit() {
        Intent intent = new Intent(this, MenuVisitActivity.class);
        intent.putExtra(Constants.BUNDLE_CLIENT_VISIT,getClient());
        intent.putExtra(Constants.BUNDLE_CLIENT_VISIT_DETAIL,visitBundle);
        startActivity(intent);
        finish();
    }

    @Override
    public void onOpenCameraPriceBreak(int position,int priceId) {
        positionRv = position;
        priceIdItem = priceId;
        requestPermissionCamera();
    }

        @Override
        public void callCamera(Context context) {
            tempName = prefixFileName + System.currentTimeMillis();
            Intent takeCameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

            takeCameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, FileUtil.getPhotoFileUri(this, tempName));

            if (takeCameraIntent.resolveActivity(this.getPackageManager()) != null) {
                startActivityForResult(takeCameraIntent, REQUEST_IMAGE_CAMERA);
            } else {
                DialogUtil.showDialogMessage(this, getString(R.string.alert_error_to_camera));
            }
        }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            System.gc();
            if (requestCode == REQUEST_IMAGE_CAMERA) {
                startLoading();
                ImageProcessAsynTask imageProcessAsynTask = new ImageProcessAsynTask(this, tempName,this, true);
                imageProcessAsynTask.execute();
                nameFiles = tempName;
            }
        }

    }

        @Override
        public void callGallery() {

        }

        @Override
        public void removeItem(int position) {

        }

        @Override
        public void showMessageError() {
            stopLoading();
            DialogUtil.showDialogMessage(this, this.getResources().getString(R.string.message_error_image_memory));

        }

        @Override
        public void SuccessUpload(String size, String path) {
            stopLoading();
            if (path!= null || path != ""){
                int pricePosition = 0;
                for (int i= 0 ;i < Constants.SEARCH_PRICE_BREAK.size();i++){
                    if (Constants.SEARCH_PRICE_BREAK.get(i).getProductId() == priceIdItem){
                        pricePosition = i;
                    }
                }

                Constants.SEARCH_PRICE_BREAK.get(pricePosition).setPhotoPath(path);
                File file = new File(path);
                Bitmap myBitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                myBitmap.compress(Bitmap.CompressFormat.JPEG, 70, stream);
                byte[] byteFormat = stream.toByteArray();
                String imgString = Base64.encodeToString(byteFormat, Base64.NO_WRAP);
                base64Photo = "data:image/jpeg;base64," + imgString;
                Constants.SEARCH_PRICE_BREAK.get(pricePosition).setPhotoBase64(base64Photo);
                setupRecycler(Constants.SEARCH_PRICE_BREAK);

            }
        }

    private void setupKeyboard(){
        searchProductLinearLayout.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                Rect r = new Rect();
                searchProductLinearLayout.getWindowVisibleDisplayFrame(r);
                int screenHeight = searchProductLinearLayout.getRootView().getHeight();

                int keypadHeight = screenHeight - r.bottom;

                if (keypadHeight > screenHeight * 0.15) {
                    buttonsLinearLayout.setVisibility(View.GONE);
                    LinearLayout.LayoutParams paramTitle = new LinearLayout.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT,
                            0,
                            1.0f
                    );
                    titleLinearLayout.setLayoutParams(paramTitle);

                    LinearLayout.LayoutParams paramEt = new LinearLayout.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT,
                            0,
                            1.5f
                    );
                    searchProductEtLinearLayout.setLayoutParams(paramEt);

                    LinearLayout.LayoutParams paramRv = new LinearLayout.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT,
                            0,
                            7.5f
                    );

                    priceRvLinearLayout.setLayoutParams(paramRv);

                } else {

                    LinearLayout.LayoutParams paramTitle = new LinearLayout.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT,
                            0,
                            1.0f
                    );
                    titleLinearLayout.setLayoutParams(paramTitle);

                    LinearLayout.LayoutParams paramEt = new LinearLayout.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT,
                            0,
                            1.0f
                    );
                    searchProductEtLinearLayout.setLayoutParams(paramEt);

                    LinearLayout.LayoutParams paramRv = new LinearLayout.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT,
                            0,
                            5.0f
                    );

                    priceRvLinearLayout.setLayoutParams(paramRv);
                    buttonsLinearLayout.setVisibility(View.VISIBLE);
                }
            }
        });
    }


}