package com.visual.purina_impulso.presentation.menu.clients.visit.menu.shooper;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.view.MenuItem;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.visual.purina_impulso.Constants;
import com.visual.purina_impulso.R;
import com.visual.purina_impulso.base.BaseActivity;
import com.visual.purina_impulso.domain.Client;
import com.visual.purina_impulso.domain.CommentShooperVisit;
import com.visual.purina_impulso.domain.Visit;
import com.visual.purina_impulso.presentation.menu.clients.visit.menu.MenuVisitActivity;
import com.visual.purina_impulso.presentation.menu.clients.visit.menu.MenuVisitContract;
import com.visual.purina_impulso.presentation.menu.clients.visit.menu.MenuVisitPresenter;
import com.visual.purina_impulso.util.ActivityUtils;
import com.visual.purina_impulso.util.DialogUtil;
import com.visual.purina_impulso.util.FileUtil;
import com.visual.purina_impulso.util.ImageProcessAsynTask;
import com.visual.purina_impulso.view.dialog.IDialogCameraView;
import com.visual.purina_impulso.view.dialog.IDialogVisit;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.UUID;

import butterknife.BindView;
import butterknife.OnClick;
import io.realm.RealmList;



public class CommentShooperActivity extends BaseActivity implements MenuVisitContract.View,MenuVisitContract.CommentShooperView,IDialogCameraView,IDialogVisit.VisitMenu {

    @BindView(R.id.toolbar_main) Toolbar mToolbar;
    @BindView(R.id.toolbar_title_text_view) TextView mToolbarTitleText;
    @BindView(R.id.rb_purina) RadioButton purinaRadioButton;
    @BindView(R.id.rb_competition) RadioButton competitionRadioButton;
    @BindView(R.id.et_comment) EditText commentEditText;
    @BindView(R.id.et_opportunities) EditText oportunitiesEditText;
    @BindView(R.id.iv_photo) ImageView photoImageView;


    public  MenuVisitContract.Presenter mPresenter;
    public Visit visitBundle;

    private String prefixFileName = "report-image-";
    private String tempName = "";
    private static String type;
    private static final int CAMERA_REQUEST_CODE = 102;
    private static final int REQUEST_IMAGE_CAMERA = 555;


    public static String pathPhotos="";
    public static Uri uriPhotos;
    public static String nameFiles="";
    public static String base64Photo="";
    @Override
    protected int getResLayout() {
        return R.layout.activity_comment_shooper;
    }

    @Override
    protected void setupView(Bundle savedInstanceState) {

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        mPresenter =  new MenuVisitPresenter(this);

      setTitleToolbar(getResources().getString(R.string.menu_visit_comment_shooper));

        if (mPresenter == null){
            mPresenter = new MenuVisitPresenter(this);
        }

        setupListerner();
        ActivityUtils.setToolBarBack(this,mToolbar,true);
    }

    public void setTitleToolbar(String mTitleToolbar) {
        mToolbarTitleText.setText(mTitleToolbar);
    }

    @Override
    public void setPresenter(@NonNull MenuVisitContract.Presenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public Client getClient() {
        return getIntent().getParcelableExtra(Constants.BUNDLE_CLIENT_VISIT);
    }

    @Override
    public Visit getVisit() {
        return getIntent().getParcelableExtra(Constants.BUNDLE_CLIENT_VISIT_DETAIL);
    }

    @OnClick(R.id.btn_take_photo)
    public void onTakePhoto(){
        requestPermissionCamera();
    }

    private void requestPermissionCamera(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.CAMERA},
                        CAMERA_REQUEST_CODE);
            }else{
                callCamera();
            }
        }else{
            callCamera();
        }

    }

    public void callCamera(){
        tempName = prefixFileName + System.currentTimeMillis();
        Intent takeCameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        takeCameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, FileUtil.getPhotoFileUri(this, tempName));

        if (takeCameraIntent.resolveActivity(this.getPackageManager()) != null) {
            startActivityForResult(takeCameraIntent, REQUEST_IMAGE_CAMERA);
        } else {
            DialogUtil.showDialogMessage(this, getString(R.string.alert_error_to_camera));
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            System.gc();
            if (requestCode == REQUEST_IMAGE_CAMERA) {
                startLoading();
                ImageProcessAsynTask imageProcessAsynTask = new ImageProcessAsynTask(this, tempName,this, true);
                imageProcessAsynTask.execute();
                nameFiles = tempName;
            }
        }

    }

    @Override
    public void  onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case CAMERA_REQUEST_CODE: {
                if (grantResults.length == 0 || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                } else {

                    callCamera();
                }
                return;
            }
        }
    }


    @Override
    public void callCamera(Context context) {

    }

    @Override
    public void callGallery() {

    }

    @Override
    public void removeItem(int position) {

    }

    @Override
    public void showMessageError() {
        stopLoading();
        DialogUtil.showDialogMessage(this, this.getResources().getString(R.string.message_error_image_memory));

    }

    @Override
    public void SuccessUpload(String size, String path) {
        stopLoading();
        if (path!= null || path != ""){
            File file = new File(path);
            Picasso.with(this).load(file).placeholder(android.R.drawable.progress_horizontal).into(photoImageView);
            pathPhotos = path;
            Bitmap myBitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            myBitmap.compress(Bitmap.CompressFormat.JPEG, 70, stream);
            byte[] byteFormat = stream.toByteArray();
            String imgString = Base64.encodeToString(byteFormat, Base64.NO_WRAP);
            base64Photo = "data:image/jpeg;base64," + imgString;
        }
    }

    @OnClick(R.id.btn_save_register)
    public void onRegisterPhotoVisit(){
        if (formValidate()){
            Calendar calendar = Calendar.getInstance();
            SimpleDateFormat formatDate = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
            String date =  formatDate.format(calendar.getTime());
            Visit visit = getVisit();

            RealmList<CommentShooperVisit> commentShooperVisits = new RealmList<>();

            String commentShooperId = UUID.randomUUID().toString();


            SimpleDateFormat formatHour = new SimpleDateFormat("HH:mm:ss");
            String hour =   formatHour.format(calendar.getTime());
            String comment =   commentEditText.getText().toString();
            String opportunities =   oportunitiesEditText.getText().toString();
            String type = "";



            if (purinaRadioButton.isChecked()){
                type = getResources().getString(R.string.text_shooper_radio_purina_value);
            }

            if (competitionRadioButton.isChecked()){
                type = getResources().getString(R.string.text_shooper_radio_competition_value);
            }

            if (visit.getCommentShooperVisit() != null ){
                if (visit.getCommentShooperVisit().size() > 0){
                    commentShooperVisits.addAll(visit.getCommentShooperVisit());
                }
            }

            CommentShooperVisit commentShooperVisit = new CommentShooperVisit(commentShooperId,getVisit().getId(),
                    Integer.parseInt(getClient().getEmployeeId()),
                    date,
                    getClient().getClientId(),
                    comment,
                    opportunities,
                    base64Photo,
                    type,
                    hour,
                    Constants.FLAG_DATA_PENDING_YES);



            commentShooperVisits.add(commentShooperVisit);



            visit.setCommentShooperVisit(commentShooperVisits);

            mPresenter.onRegisterVisit(visit,Constants.FLAG_VISIT_SHOOPER,getClient());

        }
    }

    public void setupListerner(){
        purinaRadioButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
               if (b){
                   competitionRadioButton.setChecked(false);
               }
            }
        });

        competitionRadioButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b){
                    purinaRadioButton.setChecked(false);
                }
            }
        });
    }

    @Override
    public void onRegisterVisitSuccess() {
        Intent intent = new Intent(this, MenuVisitActivity.class);
        intent.putExtra(Constants.BUNDLE_CLIENT_VISIT,getClient());
        intent.putExtra(Constants.BUNDLE_CLIENT_VISIT_DETAIL,visitBundle);
        startActivity(intent);
        finish();
    }

    @OnClick(R.id.btn_cancel)
    public void onCancelRegister(){
        Intent intent = new Intent(this, MenuVisitActivity.class);
        intent.putExtra(Constants.BUNDLE_CLIENT_VISIT,getClient());
        intent.putExtra(Constants.BUNDLE_CLIENT_VISIT_DETAIL,getVisit());
        startActivity(intent);
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home) {

            Intent intent = new Intent(this, MenuVisitActivity.class);
            intent.putExtra(Constants.BUNDLE_CLIENT_VISIT,getClient());
            intent.putExtra(Constants.BUNDLE_CLIENT_VISIT_DETAIL,getVisit());
            startActivity(intent);
            finish();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(this, MenuVisitActivity.class);
        intent.putExtra(Constants.BUNDLE_CLIENT_VISIT,getClient());
        intent.putExtra(Constants.BUNDLE_CLIENT_VISIT_DETAIL,getVisit());
        startActivity(intent);
        finish();
    }



    @Override
    public void onRegisterCommentShooperSuccess(String message, Visit visit) {
        visitBundle = visit;
        DialogUtil.showDialogMessageRegisterVisit(this,message,this);
    }


    private boolean formValidate(){


        if (!competitionRadioButton.isChecked() && !purinaRadioButton.isChecked()){
            DialogUtil.showDialogMessage(this, getResources().getString(R.string.message_option_comments_empty));
            return false;
        }

        if (commentEditText.getText().toString().isEmpty()){
            DialogUtil.showDialogMessage(this, getResources().getString(R.string.message_comments_empty));
            return false;
        }

        if (oportunitiesEditText.getText().toString().isEmpty()){
            DialogUtil.showDialogMessage(this, getResources().getString(R.string.message_opportunities_empty));
            return false;
        }



        return true;
    }
}