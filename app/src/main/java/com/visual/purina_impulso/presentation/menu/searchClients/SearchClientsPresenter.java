package com.visual.purina_impulso.presentation.menu.searchClients;

import android.support.annotation.NonNull;

import com.visual.purina_impulso.App;
import com.visual.purina_impulso.Constants;
import com.visual.purina_impulso.R;
import com.visual.purina_impulso.base.BaseInteractor;
import com.visual.purina_impulso.data.source.helper.PreferencesHelper;
import com.visual.purina_impulso.domain.Client;
import com.visual.purina_impulso.domain.Sync;
import com.visual.purina_impulso.domain.interactor.ClientInteractor;
import com.visual.purina_impulso.domain.interactor.SyncInteractor;
import com.visual.purina_impulso.domain.interactor.VisitInteractor;

import java.util.ArrayList;

import io.realm.RealmList;


class SearchClientsPresenter implements SearchClientsContract.Presenter {

    private final SearchClientsContract.View mView;


    SearchClientsPresenter(@NonNull SearchClientsContract.View view) {
        mView = view;
        mView.setPresenter(this);
    }

    @Override
    public void start() {

    }


    @Override
    public void getClients(String search) {
        String employeeId = String.valueOf(PreferencesHelper.getUser().getEmployeeId());
        String type = Constants.FLAG_CLIENT_ASSIGNED;

        if (search ==  null){
            mView.startLoading();
            Constants.SEARCH_CLIENTS_CHECK.clear();
        }

        ClientInteractor.getInstance().getClients(employeeId,type,search, new ClientInteractor.SuccessGetClientsListener() {
            @Override
            public void onSuccessGetClients(ArrayList<Client> clients) {
                if (search ==  null){
                    mView.stopLoading();
                }
                mView.getClientsSuccess(clients);
            }

        }, new BaseInteractor.ErrorCallback() {
            @Override
            public void onError(Exception exception) {
                if (search ==  null){
                    mView.stopLoading();
                }
                mView.showErrorMessage(exception);
            }
        });
    }

    @Override
    public void assignVisit(String currentDate, String[] clients) {
        mView.startLoading();
        VisitInteractor.getInstance().assignVisit(currentDate,clients, new VisitInteractor.SuccessAssignVisitListener() {
            @Override
            public void onSuccessAssignVisit() {
                mView.stopLoading();
                mView.assignVisitSuccess();
            }
        }, new BaseInteractor.ErrorCallback() {
            @Override
            public void onError(Exception exception) {
                mView.stopLoading();
                mView.showErrorMessage(exception);
            }
        });
    }


    public void onSync(String type) {
        mView.startLoadingText(App.context.getResources().getString(R.string.dialog_sync_message));
        String syncTraditional = "false";
        String syncModern = "false";
        String syncSpecialized = "false";
        if (type.equals("1")){
            syncTraditional = "true";
        }else if(type.equals("2") || type.equals("4")){
            syncModern = "true";
        }else{
            syncSpecialized = "true";
        }

        SyncInteractor.getInstance().sync(syncTraditional,syncModern,syncSpecialized, new SyncInteractor.SuccessSyncListener() {
            @Override
            public void onSuccessSync(Sync sync) {
               // saveSync(sync);

                getClients(sync);
            }
        }, new BaseInteractor.ErrorCallback() {
            @Override
            public void onError(Exception exception) {
                mView.stopLoading();
                mView.showErrorMessage(exception);
            }
        });
    }



    private void getClients(Sync sync){
        String employeeId = String.valueOf(PreferencesHelper.getUser().getEmployeeId());
        String type = Constants.FLAG_CLIENT;
        String search =  null;

        ClientInteractor.getInstance().getClients(employeeId,type,search, new ClientInteractor.SuccessGetClientsListener() {
            @Override
            public void onSuccessGetClients(ArrayList<Client> clients) {
                RealmList<Client> clientTotal =  new RealmList<>();
                RealmList<Client> clientsSync =  sync.getClient();
                if (clients != null && clients.size()>0){
                    if (clients.size() != clientsSync.size()){
                        clientTotal.addAll(clients);
                        for (int i = 0;i<clientsSync.size();i++){
                            boolean clientSame = false;
                            if (clientsSync.get(i).getType().equals(Constants.FLAG_CLIENT)){
                                for (int j = 0;j<clients.size();j++){
                                    if (clients.get(j).getClientId() == clientsSync.get(i).getClientId()){
                                        clientSame = true;
                                    }
                                }

                                if (!clientSame){
                                    clientTotal.add(clientsSync.get(i));
                                }
                            }
                        }
                    }else{
                        for (int i = 0;i<clientsSync.size();i++){
                            if (clientsSync.get(i).getType().equals(Constants.FLAG_CLIENT)){
                                clientTotal.add(clientsSync.get(i));
                            }
                        }
                    }


                }else{
                    for (int i = 0;i<clientsSync.size();i++){
                        if (clientsSync.get(i).getType().equals(Constants.FLAG_CLIENT)){
                            clientTotal.add(clientsSync.get(i));
                        }
                    }
                }



                for (int i = 0;i<clientsSync.size();i++){
                    if (clientsSync.get(i).getType().equals(Constants.FLAG_CLIENT_ASSIGNED)){
                        clientTotal.add(clientsSync.get(i));
                    }
                }

                sync.setClient(clientTotal);
                saveSync(sync);
                //clearPartialDB(sync);

            }

        }, new BaseInteractor.ErrorCallback() {
            @Override
            public void onError(Exception exception) {
                mView.stopLoading();
                mView.showErrorMessage(exception);
            }
        });
    }


    private void clearPartialDB(Sync sync){

        SyncInteractor.getInstance().clearPartialSync(new SyncInteractor.SuccessClearPartialSyncListener() {
            @Override
            public void onSuccessClearPartialSync() {

                saveSync(sync);
            }
        }, new BaseInteractor.ErrorCallback() {
            @Override
            public void onError(Exception exception) {
                mView.stopLoading();
                mView.showErrorMessage(exception);
            }
        });
    }



    private void saveSync(Sync sync){

        SyncInteractor.getInstance().saveSync(sync,false, new SyncInteractor.SuccessSaveSyncListener() {
            @Override
            public void onSuccessSaveSync(String message) {
                mView.stopLoading();
                mView.onSyncSuccessClients(message);
            }
        }, new BaseInteractor.ErrorCallback() {
            @Override
            public void onError(Exception exception) {
                mView.stopLoading();
                mView.showErrorMessage(exception);
            }
        });
    }
}


