package com.visual.purina_impulso.presentation.channel;

import android.support.annotation.NonNull;

import com.visual.purina_impulso.App;
import com.visual.purina_impulso.R;
import com.visual.purina_impulso.base.BaseInteractor;
import com.visual.purina_impulso.data.source.helper.PreferencesHelper;
import com.visual.purina_impulso.domain.Sync;
import com.visual.purina_impulso.domain.User;
import com.visual.purina_impulso.domain.interactor.SyncInteractor;



class ChannelPresenter implements  ChannelContract.Presenter {

    private final ChannelContract.View mView;

    ChannelPresenter(@NonNull ChannelContract.View view) {
        mView = view;
        mView.setPresenter(this);
    }

    @Override
    public void start() {

    }

    @Override
    public void onSync(String type) {
        mView.startLoading();
        String syncTraditional = "false";
        String syncModern = "false";
        String syncSpecialized = "false";
        if (type.equals("1")){
            syncTraditional = "true";
        }else if(type.equals("2")){
            syncModern = "true";
        }else{
            syncSpecialized = "true";
        }

        SyncInteractor.getInstance().sync(syncTraditional,syncModern,syncSpecialized, new SyncInteractor.SuccessSyncListener() {
            @Override
            public void onSuccessSync(Sync sync) {
                saveSync(sync,type);
            }
        }, new BaseInteractor.ErrorCallback() {
            @Override
            public void onError(Exception exception) {
                mView.stopLoading();
                mView.showErrorMessage(exception);
            }
        });
    }


    private void saveSync(Sync sync,String type){

        SyncInteractor.getInstance().saveSync(sync,true, new SyncInteractor.SuccessSaveSyncListener() {
            @Override
            public void onSuccessSaveSync(String message) {
                mView.stopLoading();
                String channel = "";
                if (type.equals("1")){
                    channel = App.context.getString(R.string.name_channel_traditional);
                }else if(type.equals("2")){
                    channel = App.context.getString(R.string.name_channel_modern);
                }else{
                    channel = App.context.getString(R.string.name_channel_specialized);
                }

                User user = new User(PreferencesHelper.getUser().getAssistance(),
                        PreferencesHelper.getUser().getEmployeeId(),
                        PreferencesHelper.getUser().getSystemId(),
                        PreferencesHelper.getUser().getTypeId(),
                        PreferencesHelper.getUser().getNames(),
                        channel,
                        PreferencesHelper.getUser().getUserType());

                PreferencesHelper.setUser(user);
                mView.onSyncSuccess(message);
            }
        }, new BaseInteractor.ErrorCallback() {
            @Override
            public void onError(Exception exception) {
                mView.stopLoading();
                mView.showErrorMessage(exception);
            }
        });
    }
}