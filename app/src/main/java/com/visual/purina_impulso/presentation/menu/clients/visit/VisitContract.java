package com.visual.purina_impulso.presentation.menu.clients.visit;

import com.visual.purina_impulso.base.BasePresenter;
import com.visual.purina_impulso.base.BaseViewPresenter;
import com.visual.purina_impulso.domain.Client;
import com.visual.purina_impulso.domain.Incidence;
import com.visual.purina_impulso.domain.IncidenceVisit;
import com.visual.purina_impulso.domain.StartVisit;
import com.visual.purina_impulso.domain.Visit;

import java.util.ArrayList;


interface VisitContract {

    interface Presenter extends BasePresenter {
        void getTypeIncidences();
        void registerStartVisit(StartVisit startVisit, Visit visit,Client client);
        void registerIncidence(StartVisit visit, IncidenceVisit incidenceVisit);
    }


    interface View extends BaseViewPresenter<Presenter> {
        void getTypeIncidencesSuccess(ArrayList<Incidence> incidences);
        void onRegisterStartVisitSuccess(String message,Visit visit);
        void onRegisterIncidenceVisitSuccess(String message);
        Client getClient();
        Visit getVisit();
    }


}
