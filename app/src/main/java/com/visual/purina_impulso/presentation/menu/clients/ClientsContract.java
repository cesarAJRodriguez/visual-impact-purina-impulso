package com.visual.purina_impulso.presentation.menu.clients;

import com.visual.purina_impulso.base.BasePresenter;
import com.visual.purina_impulso.base.BaseViewPresenter;
import com.visual.purina_impulso.domain.Client;
import com.visual.purina_impulso.domain.Visit;

import java.util.ArrayList;



interface ClientsContract {

    interface Presenter extends BasePresenter {
        void getClients();
        void getClientInternal(Client client);
    }

    interface View extends BaseViewPresenter<Presenter> {
        void getClientsSuccess(ArrayList<Client> clients);
        void getClientInternalSuccess(Client client, Visit visit);
    }


}