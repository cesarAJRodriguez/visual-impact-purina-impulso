package com.visual.purina_impulso.presentation.menu.assistance;

import com.visual.purina_impulso.base.BasePresenter;
import com.visual.purina_impulso.base.BaseViewPresenter;
import com.visual.purina_impulso.domain.Assistance;


interface AssistanceContract {

    interface Presenter extends BasePresenter {
        void onRegisterAssistance(Assistance assistance);
    }

    interface View extends BaseViewPresenter<Presenter> {
        void onRegisterAssistanceSuccess(String message);
    }
}
