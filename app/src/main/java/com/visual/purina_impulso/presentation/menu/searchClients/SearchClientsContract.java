package com.visual.purina_impulso.presentation.menu.searchClients;

import com.visual.purina_impulso.base.BasePresenter;
import com.visual.purina_impulso.base.BaseViewPresenter;
import com.visual.purina_impulso.domain.Client;

import java.util.ArrayList;


interface SearchClientsContract {

    interface Presenter extends BasePresenter {
        void getClients(String Search);
        void assignVisit(String currentDate,String[] clients);
        void onSync(String type);
    }

    interface View extends BaseViewPresenter<Presenter> {
        void getClientsSuccess(ArrayList<Client> clients);
        void assignVisitSuccess();
        void onSyncSuccessClients(String message);
    }
}
