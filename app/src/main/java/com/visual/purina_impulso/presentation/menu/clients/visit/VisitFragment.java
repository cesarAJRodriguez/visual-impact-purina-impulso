package com.visual.purina_impulso.presentation.menu.clients.visit;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.widget.TextView;

import com.visual.purina_impulso.Constants;
import com.visual.purina_impulso.R;
import com.visual.purina_impulso.base.BaseFragment;
import com.visual.purina_impulso.domain.Client;
import com.visual.purina_impulso.domain.Incidence;
import com.visual.purina_impulso.domain.StartVisit;
import com.visual.purina_impulso.domain.Visit;
import com.visual.purina_impulso.presentation.menu.clients.visit.menu.MenuVisitActivity;
import com.visual.purina_impulso.util.DialogUtil;
import com.visual.purina_impulso.util.MapUtil;
import com.visual.purina_impulso.view.dialog.IDialogVisit;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;
import java.util.UUID;

import butterknife.BindView;
import butterknife.OnClick;
import io.realm.RealmList;



public class VisitFragment extends BaseFragment implements VisitContract.View,IDialogVisit.StartVisit{

    @BindView(R.id.txt_confirm_start_visit) TextView confirmStartVisitTextView;

    private FragmentManager mFragmentManager;
    private VisitContract.Presenter mPresenter;
    private Visit visitDB;
    private boolean continueVisit = false;


    public static VisitFragment newInstance(Client client,Visit visit) {
        Bundle arg = new Bundle();
        arg.putParcelable(Constants.BUNDLE_CLIENT_VISIT, client);
        arg.putParcelable(Constants.BUNDLE_CLIENT_VISIT_DETAIL, visit);
        VisitFragment fragment = new VisitFragment();
        fragment.setArguments(arg);
        return fragment;
    }

    @Override
    protected int getResLayout() {
        return R.layout.fragment_visit;
    }

    @Override
    public void onResume() {
        super.onResume();
        Constants.STEP_POSITION =  Constants.FLAG_VISIT_FRAGMENT;
        mPresenter.start();
        ((VisitActivity)mContext).setTitleToolbar(getResources().getString(R.string.toolbar_start_visit));
        if (getClient() !=null){
            if (!getClient().getStartHour().isEmpty()){
                continueVisit = true;
                confirmStartVisitTextView.setText(getResources().getString(R.string.visit_text_confirm_visit_continue) + " "+ getClient().getCode() + " " +"?");

            }else{
                continueVisit = false;
                confirmStartVisitTextView.setText(getResources().getString(R.string.visit_text_confirm_visit) + " "+ getClient().getCode() + " " +"?");

            }

        }else{
            confirmStartVisitTextView.setText("");
        }

    }

    @Override
    protected void setupView(Bundle savedInstanceState) {

    }

    @Override
    protected void onRestoreView(Bundle savedInstanceState) {

    }

    @Override
    public void setPresenter(@NonNull VisitContract.Presenter presenter) {
        mPresenter = presenter;
    }




    @OnClick(R.id.btn_register_incidence)
    public void onRegisterIncidenceView(){
        Intent intent = new Intent(mContext,VisitIncidenceActivity.class);
        intent.putExtra(Constants.BUNDLE_CLIENT_VISIT,getClient());
        intent.putExtra(Constants.BUNDLE_CLIENT_VISIT_DETAIL,getVisit());
        startActivity(intent);
    }

    @OnClick(R.id.btn_accept)
    public void onStartVisit(){
        if (continueVisit){

            Intent intent = new Intent(mContext, MenuVisitActivity.class);
            intent.putExtra(Constants.BUNDLE_CLIENT_VISIT,getClient());
            intent.putExtra(Constants.BUNDLE_CLIENT_VISIT_DETAIL,getVisit());
            startActivity(intent);
        }else{
            final MapUtil gps = new MapUtil(mContext);


            String latitude =  String.valueOf(gps.getLatitude());
            String longitude =   String.valueOf(gps.getLongitude());

            Calendar calendar = Calendar.getInstance();
            SimpleDateFormat formatHour = new SimpleDateFormat("HH:mm:ss");
            String hour =   formatHour.format(calendar.getTime());
            SimpleDateFormat formatDate = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
            String date =  formatDate.format(calendar.getTime());

            String visitStartId = UUID.randomUUID().toString();
            StartVisit startVisit = new StartVisit(visitStartId,Integer.parseInt(getClient().getEmployeeId()),
                    date,getClient().getClientId(),hour,"",longitude,latitude,"","",
                    getClient().getStatusClient(),
                    getClient().getVisitId(),Constants.FLAG_DATA_PENDING_NO);


            RealmList<StartVisit> startVisits = new RealmList<>();
            startVisits.add(startVisit);


            Visit visit = new Visit();
            visit.setId(getClient().getVisitId());
            visit.setDate(date);
            visit.setClientId(getClient().getClientId());
            visit.setSystemId("2");
            visit.setStartVisit(startVisits);

            getClient().setStartHour(hour);
            getClient().setVisit(Constants.FLAG_CLIENT_VISIT_START);

            mPresenter.registerStartVisit(startVisit,visit,getClient());
        }

    }

    @OnClick(R.id.btn_cancel_visit)
    public void onCancelVisit(){
        ((VisitActivity)mContext).finish();
    }

    @Override
    public void getTypeIncidencesSuccess(ArrayList<Incidence> incidences) {

    }

    @Override
    public void onRegisterStartVisitSuccess(String message,Visit visit) {
        visitDB = visit;
        DialogUtil.showDialogMessageRegisterStartVisit(mContext,message,this);
    }

    @Override
    public void onRegisterIncidenceVisitSuccess(String message) {

    }


    @Override
    public Client getClient() {
        Bundle args = getArguments();
        if (args != null) {
            return args.getParcelable(Constants.BUNDLE_CLIENT_VISIT);
        }
        return null;
    }

    @Override
    public Visit getVisit() {
        Bundle args = getArguments();
        if (args != null) {
            return args.getParcelable(Constants.BUNDLE_CLIENT_VISIT_DETAIL);
        }
        return null;
    }

    public void replaceFragment(Client client) {
        if (mFragmentManager == null) {
            mFragmentManager = getFragmentManager();
        }
        FragmentTransaction transaction = mFragmentManager.beginTransaction();
        transaction.replace(R.id.contentFrameVisit, VisitIncidenceFragment.newInstance(client));
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    public void onConfirmStartVisit() {
        Intent intent = new Intent(mContext, MenuVisitActivity.class);
        intent.putExtra(Constants.BUNDLE_CLIENT_VISIT,getClient());
        intent.putExtra(Constants.BUNDLE_CLIENT_VISIT_DETAIL,visitDB);
        startActivity(intent);
    }
}