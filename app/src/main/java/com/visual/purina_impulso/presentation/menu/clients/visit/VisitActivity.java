package com.visual.purina_impulso.presentation.menu.clients.visit;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import com.visual.purina_impulso.Constants;
import com.visual.purina_impulso.R;
import com.visual.purina_impulso.base.BaseActivity;
import com.visual.purina_impulso.domain.Client;
import com.visual.purina_impulso.domain.Incidence;
import com.visual.purina_impulso.domain.Visit;
import com.visual.purina_impulso.util.ActivityUtils;

import java.util.ArrayList;

import butterknife.BindView;


public class VisitActivity extends BaseActivity implements VisitContract.View{

    @BindView(R.id.toolbar_main) Toolbar mToolbar;
    @BindView(R.id.toolbar_title_text_view) TextView mToolbarTitleText;
    private VisitPresenter mPresenter;
    private FragmentManager mFragmentManager;

    @Override
    protected int getResLayout() {
        return R.layout.activity_visit;
    }

    @Override
    protected void setupView(Bundle savedInstanceState) {
        mFragmentManager = getSupportFragmentManager();
        VisitFragment visitFragment = (VisitFragment) getSupportFragmentManager().findFragmentById(R.id.contentFrameVisit);

        if (visitFragment == null) {
            visitFragment = VisitFragment.newInstance(getClient(),getVisit());
            ActivityUtils.addFragmentToActivity(getSupportFragmentManager(), visitFragment, R.id.contentFrameVisit);
        }

        mPresenter = new VisitPresenter(visitFragment);
        ActivityUtils.setToolBarBack(this,mToolbar,true);
    }

    public void setTitleToolbar(String mTitleToolbar) {
        mToolbarTitleText.setText(mTitleToolbar);
    }

    @Override
    public void setPresenter(@NonNull VisitContract.Presenter presenter) {

    }



    @Override
    public void getTypeIncidencesSuccess(ArrayList<Incidence> incidences) {

    }

    @Override
    public void onRegisterStartVisitSuccess(String message, Visit visit) {

    }

    @Override
    public void onRegisterIncidenceVisitSuccess(String message) {

    }


    @Override
    public Client getClient() {
        return getIntent().getParcelableExtra(Constants.BUNDLE_CLIENT_VISIT);
    }

    @Override
    public Visit getVisit() {
        return getIntent().getParcelableExtra(Constants.BUNDLE_CLIENT_VISIT_DETAIL);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home) {
            if (Constants.FLAG_VISIT_FRAGMENT == Constants.STEP_POSITION) {
                finish();
            } else {
                mFragmentManager.popBackStack();
            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}