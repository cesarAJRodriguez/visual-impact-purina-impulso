package com.visual.purina_impulso.presentation.menu.clients.visit.menu;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.widget.TextView;

import com.visual.purina_impulso.Constants;
import com.visual.purina_impulso.R;
import com.visual.purina_impulso.base.BaseFragment;
import com.visual.purina_impulso.domain.Client;
import com.visual.purina_impulso.domain.Visit;
import com.visual.purina_impulso.presentation.menu.clients.ClientsActivity;
import com.visual.purina_impulso.presentation.menu.clients.visit.menu.SaleDay.SaleDayActivity;
import com.visual.purina_impulso.presentation.menu.clients.visit.menu.actionCompetence.ActionActivity;
import com.visual.purina_impulso.presentation.menu.clients.visit.menu.deliveryProduct.DeliveryProductActivity;
import com.visual.purina_impulso.presentation.menu.clients.visit.menu.photo.PhotoActivity;
import com.visual.purina_impulso.presentation.menu.clients.visit.menu.price.PriceActivity;
import com.visual.purina_impulso.presentation.menu.clients.visit.menu.quiz.QuizActivity;
import com.visual.purina_impulso.presentation.menu.clients.visit.menu.shooper.CommentShooperActivity;
import com.visual.purina_impulso.presentation.menu.clients.visit.menu.stockSampling.StockSamplingActivity;
import com.visual.purina_impulso.presentation.menu.clients.visit.menu.stockSamplingFinal.StockSamplingFinalActivity;
import com.visual.purina_impulso.util.DialogUtil;
import com.visual.purina_impulso.util.MapUtil;
import com.visual.purina_impulso.view.dialog.IDialogVisit;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.OnClick;


public class MenuVisitFragment extends BaseFragment implements MenuVisitContract.View,IDialogVisit.VisitFinish, MenuVisitContract.FinishView,IDialogVisit.VisitFinishDialog{

    @BindView(R.id.menu_visit_client) TextView clientVisitTextView;

    private FragmentManager mFragmentManager;
    private MenuVisitContract.Presenter mPresenter;

    public static MenuVisitFragment newInstance(Client client, Visit visit) {
        Bundle arg = new Bundle();
        arg.putParcelable(Constants.BUNDLE_CLIENT_VISIT, client);
        arg.putParcelable(Constants.BUNDLE_CLIENT_VISIT_DETAIL, visit);
        MenuVisitFragment fragment = new MenuVisitFragment();
        fragment.setArguments(arg);
        return fragment;
    }

    @Override
    protected int getResLayout() {
        return R.layout.fragment_menu_visit;
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MenuVisitActivity)mContext).setTitleToolbar(getResources().getString(R.string.toolbar_title_menu_client));
        if (getClient()!= null){
            clientVisitTextView.setText(getClient().getCode());
        }else{
            clientVisitTextView.setText("");
        }
    }

    @OnClick(R.id.btn_visit_quiz)
    public void onVisitQuiz(){
        Intent intent = new Intent(mContext, QuizActivity.class);
        intent.putExtra(Constants.BUNDLE_CLIENT_VISIT_DETAIL,getVisit());
        intent.putExtra(Constants.BUNDLE_CLIENT_VISIT,getClient());
        startActivity(intent);
    }

    @OnClick(R.id.btn_visit_price)
    public void onVisitPrice(){
        Intent intent = new Intent(mContext, PriceActivity.class);
        intent.putExtra(Constants.BUNDLE_CLIENT_VISIT,getClient());
        intent.putExtra(Constants.BUNDLE_CLIENT_VISIT_DETAIL,getVisit());
        startActivity(intent);
    }

    @OnClick(R.id.btn_visit_photo)
    public void onVisitPhoto(){
        ((MenuVisitActivity)mContext).finish();
        Intent intent = new Intent(mContext, PhotoActivity.class);
        intent.putExtra(Constants.BUNDLE_CLIENT_VISIT,getClient());
        intent.putExtra(Constants.BUNDLE_CLIENT_VISIT_DETAIL,getVisit());
        startActivity(intent);
    }

    @OnClick(R.id.btn_action)
    public void onAction(){
        ((MenuVisitActivity)mContext).finish();
        Intent intent = new Intent(mContext, ActionActivity.class);
        intent.putExtra(Constants.BUNDLE_CLIENT_VISIT,getClient());
        intent.putExtra(Constants.BUNDLE_CLIENT_VISIT_DETAIL,getVisit());
        startActivity(intent);
    }

    @OnClick(R.id.btn_comment_shooper)
    public void onShooper(){
        ((MenuVisitActivity)mContext).finish();
        Intent intent = new Intent(mContext, CommentShooperActivity.class);
        intent.putExtra(Constants.BUNDLE_CLIENT_VISIT,getClient());
        intent.putExtra(Constants.BUNDLE_CLIENT_VISIT_DETAIL,getVisit());
        startActivity(intent);
    }

    @OnClick(R.id.btn_stock)
    public void onStock(){
        ((MenuVisitActivity)mContext).finish();
        Intent intent = new Intent(mContext, StockSamplingActivity.class);
        intent.putExtra(Constants.BUNDLE_CLIENT_VISIT,getClient());
        intent.putExtra(Constants.BUNDLE_CLIENT_VISIT_DETAIL,getVisit());
        startActivity(intent);
    }

    @OnClick(R.id.btn_stock_final)
    public void onStockFinal(){
        ((MenuVisitActivity)mContext).finish();
        Intent intent = new Intent(mContext, StockSamplingFinalActivity.class);
        intent.putExtra(Constants.BUNDLE_CLIENT_VISIT,getClient());
        intent.putExtra(Constants.BUNDLE_CLIENT_VISIT_DETAIL,getVisit());
        startActivity(intent);
    }

    @OnClick(R.id.btn_delivery)
    public void onDelivery(){
        ((MenuVisitActivity)mContext).finish();
        Intent intent = new Intent(mContext, DeliveryProductActivity.class);
        intent.putExtra(Constants.BUNDLE_CLIENT_VISIT,getClient());
        intent.putExtra(Constants.BUNDLE_CLIENT_VISIT_DETAIL,getVisit());
        startActivity(intent);
    }

    @OnClick(R.id.btn_sale)
    public void onSale(){
        ((MenuVisitActivity)mContext).finish();
        Intent intent = new Intent(mContext, SaleDayActivity.class);
        intent.putExtra(Constants.BUNDLE_CLIENT_VISIT,getClient());
        intent.putExtra(Constants.BUNDLE_CLIENT_VISIT_DETAIL,getVisit());
        startActivity(intent);
    }

    @OnClick(R.id.btn_close)
    public void onClose(){
        ((MenuVisitActivity)mContext).finish();
        Intent intent = new Intent(mContext, ClientsActivity.class);
        startActivity(intent);
    }


    @OnClick(R.id.btn_finish_visit)
    public void onFinishVisit(){
        DialogUtil.showDialogConfirmFinishVisit(mContext,getResources().getString(R.string.message_dialog_finish_visit) + " "+ getClient().getCode() + " ?",this);
    }


    @Override
    protected void setupView(Bundle savedInstanceState) {
    }

    @Override
    protected void onRestoreView(Bundle savedInstanceState) {

    }


    @Override
    public void setPresenter(@NonNull MenuVisitContract.Presenter presenter) {
          mPresenter = presenter;
    }

    @Override
    public Client getClient() {
        Bundle args = getArguments();
        if (args != null) {
            return args.getParcelable(Constants.BUNDLE_CLIENT_VISIT);
        }
        return null;
    }

    @Override
    public Visit getVisit() {
        Bundle args = getArguments();
        if (args != null) {
            return args.getParcelable(Constants.BUNDLE_CLIENT_VISIT_DETAIL);
        }
        return null;
    }

    @Override
    public void onConfirmFinishVisit() {
        if (mPresenter == null){
            mPresenter = new MenuVisitPresenter(this);
        }

        final MapUtil gps = new MapUtil(mContext);

        String latitude =  String.valueOf(gps.getLatitude());
        String longitude =   String.valueOf(gps.getLongitude());

        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat formatHour = new SimpleDateFormat("HH:mm:ss");
        String hour =   formatHour.format(calendar.getTime());

        getVisit().getStartVisit().get(0).setExitHour(hour);
        getVisit().getStartVisit().get(0).setExitLatitude(latitude);
        getVisit().getStartVisit().get(0).setExitLongitude(longitude);

        getClient().setExitHour(hour);
        getClient().setVisit(Constants.FLAG_CLIENT_VISIT_EXIT);

        mPresenter.onRegisterVisit(getVisit(),Constants.FLAG_VISIT_T,getClient());
    }

    @Override
    public void onRegisterSuccess(String message) {
        DialogUtil.showDialogMessageFinishVisit(mContext,message,this);
    }

    @Override
    public void onFinishVisitDialog() {
        startActivity(new Intent(mContext, ClientsActivity.class));
    }
}