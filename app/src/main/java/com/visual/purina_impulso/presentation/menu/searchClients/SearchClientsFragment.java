package com.visual.purina_impulso.presentation.menu.searchClients;

import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.visual.purina_impulso.Constants;
import com.visual.purina_impulso.R;
import com.visual.purina_impulso.base.BaseFragment;
import com.visual.purina_impulso.data.source.helper.PreferencesHelper;
import com.visual.purina_impulso.domain.Client;
import com.visual.purina_impulso.util.DeviceUtil;
import com.visual.purina_impulso.util.DialogUtil;
import com.visual.purina_impulso.view.dialog.IDialogSync;
import com.visual.purina_impulso.view.dialog.IDialogVisit;
import com.visual.purina_impulso.view.adapter.SearchClientAdapter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import butterknife.BindView;
import butterknife.OnClick;


public class SearchClientsFragment extends BaseFragment implements SearchClientsContract.View,SearchClientAdapter.OnItemClickListener,IDialogVisit.Visit,IDialogSync {

    @BindView(R.id.rv_search_clients) RecyclerView searchClientsRecyclerView;
    @BindView(R.id.et_search_client) EditText searchClientEditText;
    @BindView(R.id.ll_search_clients) LinearLayout searchClientLinearLayout;
    @BindView(R.id.ll_search_clients_et) LinearLayout searchClientEtLinearLayout;
    @BindView(R.id.ll_buttons) LinearLayout buttonsLinearLayout;
    @BindView(R.id.ll_search_clients_rv) LinearLayout rvLinearLayout;

    private FragmentManager mFragmentManager;

    private SearchClientsContract.Presenter mPresenter;
    private boolean clientsEmpty = true;


    public static SearchClientsFragment newInstance() {
        return new SearchClientsFragment();
    }

    @Override
    protected int getResLayout() {
        return R.layout.fragment_search_clients;
    }

    @Override
    public void onResume() {
        super.onResume();
        Constants.STEP_POSITION =  Constants.FLAG_SEARCH_CLIENTS_FRAGMENT;
        ((SearchClientsActivity)mContext).setTitleToolbar(getResources().getText(R.string.toolbar_title_search_clients).toString());

        if (mPresenter == null){
            mPresenter =  new SearchClientsPresenter(this);
        }

        mPresenter.getClients(null);

        searchClient();
        setupKeyboard();
    }

    @Override
    protected void setupView(Bundle savedInstanceState) {

    }

    @Override
    protected void onRestoreView(Bundle savedInstanceState) {

    }

    @Override
    public void setPresenter(@NonNull SearchClientsContract.Presenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public void getClientsSuccess(ArrayList<Client> clients) {
       if (clients!=null && clients.size() > 0){
           clientsEmpty = false;
           setupRecycler(clients);
       }

    }

    @OnClick(R.id.btn_assign_visit)
    public void onAssignVisit(){
        if (Constants.SEARCH_CLIENTS_CHECK.size() > 0){
            DialogUtil.showDialogConfirmAssignedVisit(mContext,getResources().getText(R.string.message_visit_assign_confirm).toString(),this);
        }else{
            DialogUtil.showDialogMessage(mContext,getResources().getText(R.string.message_visit_assign_clients).toString());
        }

    }

    @Override
    public void assignVisitSuccess() {
        DialogUtil.showDialogSyncVisit(mContext,getResources().getText(R.string.message_visit_assign_success).toString(),this);
    }

    @Override
    public void onSyncSuccessClients(String message) {
        DialogUtil.showDialogSync(mContext,message,this);
    }

    private void searchClient(){
        searchClientEditText.addTextChangedListener(new TextWatcher() {

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }


            @Override
            public void afterTextChanged(Editable s) {

            }

            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                String search = searchClientEditText.getText().toString();
               if (!clientsEmpty){
                   mPresenter.getClients(search);
               }
            }

        });
    }

    private void setupRecycler(ArrayList<Client> clients) {
        SearchClientAdapter searchClientAdapter = new SearchClientAdapter(this,mContext,clients);
        searchClientsRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        searchClientsRecyclerView.setAdapter(searchClientAdapter);
    }

    @Override
    public void onItemClick(Object object, int position) {

    }

    @Override
    public void onConfirmAssignVisit() {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat formatDate = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        String date =  formatDate.format(calendar.getTime());
            if (DeviceUtil.hasInternetConnection()){
                String[] clients = new String[Constants.SEARCH_CLIENTS_CHECK.size()];
                for (int i = 0; i < Constants.SEARCH_CLIENTS_CHECK.size(); i++) {
                    clients[i] = Constants.SEARCH_CLIENTS_CHECK.get(i);
                }
                if (mPresenter == null){
                    mPresenter = new SearchClientsPresenter(this);
                }
                mPresenter.assignVisit(date, clients);
            }else{
                DialogUtil.showDialogMessage(mContext,getResources().getText(R.string.message_not_internet).toString());
            }
    }

    @Override
    public void onSyncVisitAccept() {
        mPresenter.onSync(PreferencesHelper.getSystemId());
    }

    @Override
    public void onSyncVisitCancel() {
        ((SearchClientsActivity)mContext).finish();
    }

    @OnClick(R.id.btn_cancel)
    public void onMenu(){
        ((SearchClientsActivity)mContext).finish();
    }

    @Override
    public void onSyncSuccess() {
        ((SearchClientsActivity)mContext).finish();
    }

    @Override
    public void onSyncConfirmAccept() {

    }

    private void setupKeyboard(){
        searchClientLinearLayout.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                Rect r = new Rect();
                searchClientLinearLayout.getWindowVisibleDisplayFrame(r);
                int screenHeight = searchClientLinearLayout.getRootView().getHeight();

                int keypadHeight = screenHeight - r.bottom;

                if (keypadHeight > screenHeight * 0.15) {
                    buttonsLinearLayout.setVisibility(View.GONE);
                    LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT,
                            0,
                            6.5f
                    );
                    rvLinearLayout.setLayoutParams(param);

                    LinearLayout.LayoutParams paramSc = new LinearLayout.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT,
                            0,
                            1.5f
                    );

                    searchClientEtLinearLayout.setLayoutParams(paramSc);

                } else {
                    LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT,
                            0,
                            5.0f
                    );
                    rvLinearLayout.setLayoutParams(param);

                    LinearLayout.LayoutParams paramSc = new LinearLayout.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT,
                            0,
                            1.0f
                    );

                    searchClientEtLinearLayout.setLayoutParams(paramSc);
                    buttonsLinearLayout.setVisibility(View.VISIBLE);
                }
            }
        });
    }

}