package com.visual.purina_impulso.presentation.menu.pending;

import android.support.annotation.NonNull;

import com.visual.purina_impulso.App;
import com.visual.purina_impulso.Constants;
import com.visual.purina_impulso.R;
import com.visual.purina_impulso.base.BaseInteractor;
import com.visual.purina_impulso.data.source.helper.PreferencesHelper;
import com.visual.purina_impulso.domain.ActionVisit;
import com.visual.purina_impulso.domain.Assistance;
import com.visual.purina_impulso.domain.Client;
import com.visual.purina_impulso.domain.CommentShooperVisit;
import com.visual.purina_impulso.domain.DeliveryDayVisit;
import com.visual.purina_impulso.domain.DetailPriceBreakVisit;
import com.visual.purina_impulso.domain.DetailPriceCompetitionVisit;
import com.visual.purina_impulso.domain.DetailQuizVisit;
import com.visual.purina_impulso.domain.DetailStockSampligVisit;
import com.visual.purina_impulso.domain.DetailStockSamplingFinalVisit;
import com.visual.purina_impulso.domain.PhotoVisit;
import com.visual.purina_impulso.domain.PriceBreakVisit;
import com.visual.purina_impulso.domain.PriceCompetitionVisit;
import com.visual.purina_impulso.domain.QuizVisit;
import com.visual.purina_impulso.domain.StockSampligVisit;
import com.visual.purina_impulso.domain.StockSamplingFinalVisit;
import com.visual.purina_impulso.domain.Visit;
import com.visual.purina_impulso.domain.interactor.AssistanceInteractor;
import com.visual.purina_impulso.domain.interactor.ClientInteractor;
import com.visual.purina_impulso.domain.interactor.VisitInteractor;
import com.visual.purina_impulso.util.DeviceUtil;

import java.util.ArrayList;

import io.realm.RealmList;


class PendingPresenter implements PendingContract.Presenter {

    private final PendingContract.View mView;

    PendingPresenter(@NonNull PendingContract.View view) {
        mView = view;
        mView.setPresenter(this);
    }

    @Override
    public void start() {

    }

    @Override
    public void onCheckCountPendingAssistance(Assistance assistance) {
        mView.startLoading();
        AssistanceInteractor.getInstance().checkAssistance(assistance, new AssistanceInteractor.SuccessCheckAssistanceListener() {
            @Override
            public void onSuccessCheckAssistance(ArrayList<Assistance> assistances) {
                mView.stopLoading();
                onCheckVisitPending(assistances);
               // mView.onSuccessCheckCountPendingAssistance(assistances);
            }
        }, new BaseInteractor.ErrorCallback() {
            @Override
            public void onError(Exception exception) {
                mView.stopLoading();
                onCheckVisitPending(null);
                //mView.showErrorMessage(exception);
            }
        });

    }

    @Override
    public void onRegisterAssistance(Assistance assistance,ArrayList<Assistance> assistances) {
        mView.startLoading();
        AssistanceInteractor.getInstance().assistance(assistance,null,Constants.FLAG_PENDING_ASSISTANCE_NO, true, new AssistanceInteractor.SuccessAssistanceListener() {
            @Override
            public void onSuccessAssistance() {
                for (int i=0;i<assistances.size();i++){
                    assistances.get(i).setPending(Constants.FLAG_PENDING_ASSISTANCE_NO);
                }
                onUpdateAssistancePending(assistances);

            }
        }, new BaseInteractor.ErrorCallback() {
            @Override
            public void onError(Exception exception) {
                mView.stopLoading();
                mView.showErrorMessage(exception);
            }
        });
    }

    @Override
    public void onCheckVisitPending() {
        String employeeId = String.valueOf(PreferencesHelper.getUser().getEmployeeId());
        String typeClient = Constants.FLAG_CLIENT;
        String search =  Constants.FLAG_PENDING_CLIENTS_VISITS;
        mView.startLoading();
        ClientInteractor.getInstance().getClients(employeeId,typeClient,search, new ClientInteractor.SuccessGetClientsListener() {
            @Override
            public void onSuccessGetClients(ArrayList<Client> clients) {
                mView.stopLoading();
                mView.onSuccessCheckVisitPending(clients);
                //  mView.onSuccessCheckAssistance(type,assistances,clients);
            }

        }, new BaseInteractor.ErrorCallback() {
            @Override
            public void onError(Exception exception) {
                mView.stopLoading();

                mView.showErrorMessage(exception);
            }
        });
    }

    @Override
    public void onSendPendingVisit(Client client) {
        if (DeviceUtil.hasInternetConnection()){
            mView.startLoading();
            VisitInteractor.getInstance().getVisitClient(client.getVisitId(), new VisitInteractor.SuccessGetClientVisitListener() {
                @Override
                public void onSuccessGetClientVisit(ArrayList<Visit> visits) {
                    if (visits!= null && visits.size()>0){
                        onSendOnlinePending(visits.get(0),client);
                    }else{
                        mView.stopLoading();
                        mView.showErrorMessage(new Exception(App.context.getResources().getString(R.string.message_error_general)));
                    }
                }
            }, new BaseInteractor.ErrorCallback() {
                @Override
                public void onError(Exception exception) {
                    mView.stopLoading();
                    mView.showErrorMessage(exception);
                }
            });
        }else{
            mView.showErrorMessage(new Exception(App.context.getResources().getString(R.string.message_not_internet)));
        }

    }

    public void onUpdateAssistancePending(ArrayList<Assistance> assistances){


        mView.startLoading();
        AssistanceInteractor.getInstance().assistance(null,assistances,Constants.FLAG_PENDING_ASSISTANCE_YES, false, new AssistanceInteractor.SuccessAssistanceListener() {
            @Override
            public void onSuccessAssistance() {
                mView.stopLoading();
                mView.onRegisterAssistanceSuccess(App.context.getResources().getString(R.string.message_assistance_register_success));
            }
        }, new BaseInteractor.ErrorCallback() {
            @Override
            public void onError(Exception exception) {
                mView.stopLoading();
                mView.showErrorMessage(exception);
            }
        });
    }


    public void onCheckVisitPending(ArrayList<Assistance> assistances) {
        String employeeId = String.valueOf(PreferencesHelper.getUser().getEmployeeId());
        String typeClient = Constants.FLAG_CLIENT;
        String search =  Constants.FLAG_PENDING_CLIENTS_VISITS;
        mView.startLoading();
        ClientInteractor.getInstance().getClients(employeeId,typeClient,search, new ClientInteractor.SuccessGetClientsListener() {
            @Override
            public void onSuccessGetClients(ArrayList<Client> clients) {
                mView.stopLoading();
                mView.onSuccessCheckCountPendingAssistance(assistances,clients);
              //  mView.onSuccessCheckAssistance(type,assistances,clients);
            }

        }, new BaseInteractor.ErrorCallback() {
            @Override
            public void onError(Exception exception) {
                mView.stopLoading();
                mView.onSuccessCheckCountPendingAssistance(assistances,null);
               // mView.onSuccessCheckAssistance(type,assistances,null);
            }
        });
    }


    public void saveRegisterVisit(Visit visit,boolean isSuccessOnline,String moduleVisit){





                if (visit.getPhotoVisit() != null){
                    RealmList<PhotoVisit> photoVisitRealmList = new RealmList<>();
                    for (int i = 0 ; i<visit.getPhotoVisit().size();i++){
                        if (visit.getPhotoVisit().get(i).getPending().equals(Constants.FLAG_DATA_PENDING_YES)){
                            PhotoVisit photoVisit=  visit.getPhotoVisit().get(i);
                            photoVisit.setPending(Constants.FLAG_DATA_PENDING_NO);
                            photoVisitRealmList.add(photoVisit);
                        }
                    }
                    visit.setPhotoVisit(photoVisitRealmList);
                }

                if (visit.getActionVisit() != null){
                    RealmList<ActionVisit> actionVisitRealmList = new RealmList<>();
                    for (int i = 0 ; i<visit.getActionVisit().size();i++){
                        if (visit.getActionVisit().get(i).getPending().equals(Constants.FLAG_DATA_PENDING_YES)){
                            ActionVisit actionVisit=  visit.getActionVisit().get(i);
                            actionVisit.setPending(Constants.FLAG_DATA_PENDING_NO);
                            actionVisitRealmList.add(actionVisit);

                        }
                    }
                    visit.setActionVisit(actionVisitRealmList);
                }


                if (visit.getQuizVisit() != null){
                    RealmList<QuizVisit> quizVisitRealmList = new RealmList<>();
                    for (int i = 0 ; i<visit.getQuizVisit().size();i++){
                        if (visit.getQuizVisit().get(i).getPending().equals(Constants.FLAG_DATA_PENDING_YES)){
                            QuizVisit quizVisit =  visit.getQuizVisit().get(i);
                            quizVisit.setPending(Constants.FLAG_DATA_PENDING_NO);
                            quizVisitRealmList.add(quizVisit);

                        }
                    }
                    visit.setQuizVisit(quizVisitRealmList);
                }

                if (visit.getDetailQuizVisit() != null){
                    if (visit.getDetailQuizVisit() != null && visit.getDetailQuizVisit().size()>0){
                        RealmList<DetailQuizVisit> detailQuizVisits = new RealmList<>();
                        for (int i = 0; i<visit.getDetailQuizVisit().size();i++){
                            if (visit.getDetailQuizVisit().get(i).getPending().equals(Constants.FLAG_DATA_PENDING_YES)){
                                DetailQuizVisit detailQuizVisit = visit.getDetailQuizVisit().get(i);
                                detailQuizVisit.setPending(Constants.FLAG_DATA_PENDING_NO);
                                detailQuizVisits.add(detailQuizVisit);
                            }
                        }
                        visit.setDetailQuizVisit(detailQuizVisits);
                    }
                }

                if (visit.getCommentShooperVisit() != null){
                    RealmList<CommentShooperVisit> commentShooperVisitRealmList = new RealmList<>();
                    for (int i = 0 ; i<visit.getCommentShooperVisit().size();i++){
                        if (visit.getCommentShooperVisit().get(i).getPending().equals(Constants.FLAG_DATA_PENDING_YES)){
                            CommentShooperVisit commentShooperVisit=  visit.getCommentShooperVisit().get(i);
                            commentShooperVisit.setPending(Constants.FLAG_DATA_PENDING_NO);
                            commentShooperVisitRealmList.add(commentShooperVisit);

                        }
                    }
                    visit.setCommentShooperVisit(commentShooperVisitRealmList);
                }

                if (visit.getDeliveryDayVisit() != null){
                    RealmList<DeliveryDayVisit> deliveryDayVisits = new RealmList<>();
                    for (int i = 0 ; i<visit.getDeliveryDayVisit().size();i++){
                        if (visit.getDeliveryDayVisit().get(i).getPending().equals(Constants.FLAG_DATA_PENDING_YES)){
                            DeliveryDayVisit deliveryDayVisit=  visit.getDeliveryDayVisit().get(i);
                            deliveryDayVisit.setPending(Constants.FLAG_DATA_PENDING_NO);
                            deliveryDayVisits.add(deliveryDayVisit);

                        }
                    }
                    visit.setDeliveryDayVisit(deliveryDayVisits);
                }


                if (visit.getStockSampligVisit() != null){
                    RealmList<StockSampligVisit> stockSampligVisits = new RealmList<>();
                    for (int i = 0 ; i<visit.getStockSampligVisit().size();i++){
                        if (visit.getStockSampligVisit().get(i).getPending().equals(Constants.FLAG_DATA_PENDING_YES)){
                            StockSampligVisit stockSampligVisit=  visit.getStockSampligVisit().get(i);
                            stockSampligVisit.setPending(Constants.FLAG_DATA_PENDING_NO);
                            stockSampligVisits.add(stockSampligVisit);
                        }
                    }
                    visit.setStockSampligVisit(stockSampligVisits);
                }

                if (visit.getDetailStockSampligVisit() != null && visit.getDetailStockSampligVisit().size()>0){
                    RealmList<DetailStockSampligVisit> detailStockSampligVisits = new RealmList<>();
                    for (int i = 0; i<visit.getDetailStockSampligVisit().size();i++){
                        if ( visit.getDetailStockSampligVisit().get(i).getPending().equals(Constants.FLAG_DATA_PENDING_YES)){
                            DetailStockSampligVisit detailStockSampligVisit = visit.getDetailStockSampligVisit().get(i);
                            detailStockSampligVisit.setPending(Constants.FLAG_DATA_PENDING_NO);
                            detailStockSampligVisits.add(detailStockSampligVisit);
                        }
                    }
                    visit.setDetailStockSampligVisit(detailStockSampligVisits);
                }

                if (visit.getPriceCompetitionVisit() != null){
                    RealmList<PriceCompetitionVisit> priceCompetitionVisits = new RealmList<>();
                    for (int i = 0 ; i<visit.getPriceCompetitionVisit().size();i++){
                        if (visit.getPriceCompetitionVisit().get(i).getPending().equals(Constants.FLAG_DATA_PENDING_YES)){
                            PriceCompetitionVisit priceCompetitionVisit=  visit.getPriceCompetitionVisit().get(i);
                            priceCompetitionVisit.setPending(Constants.FLAG_DATA_PENDING_NO);
                            priceCompetitionVisits.add(priceCompetitionVisit);
                        }
                    }
                    visit.setPriceCompetitionVisit(priceCompetitionVisits);
                }

                if (visit.getDetailPriceCompetitionVisit() != null && visit.getDetailPriceCompetitionVisit().size()>0){
                    RealmList<DetailPriceCompetitionVisit> detailPriceCompetitionVisits = new RealmList<>();
                    for (int i = 0; i<visit.getDetailPriceCompetitionVisit().size();i++){
                        if (visit.getDetailPriceCompetitionVisit().get(i).getPending().equals(Constants.FLAG_DATA_PENDING_YES)){
                            DetailPriceCompetitionVisit detailPriceCompetitionVisit = visit.getDetailPriceCompetitionVisit().get(i);
                            detailPriceCompetitionVisit.setPending(Constants.FLAG_DATA_PENDING_NO);
                            detailPriceCompetitionVisits.add(detailPriceCompetitionVisit);
                        }
                    }
                    visit.setDetailPriceCompetitionVisit(detailPriceCompetitionVisits);
                }

                if (visit.getPriceBreakVisit() != null){
                    RealmList<PriceBreakVisit> priceBreakVisits = new RealmList<>();
                    for (int i = 0 ; i<visit.getPriceBreakVisit().size();i++){
                        if (visit.getPriceBreakVisit().get(i).getPending().equals(Constants.FLAG_DATA_PENDING_YES)){
                            PriceBreakVisit priceBreakVisit=  visit.getPriceBreakVisit().get(i);
                            priceBreakVisit.setPending(Constants.FLAG_DATA_PENDING_NO);
                            priceBreakVisits.add(priceBreakVisit);
                        }
                    }
                    visit.setPriceBreakVisit(priceBreakVisits);
                }

                if (visit.getDetailPriceBreakVisit() != null && visit.getDetailPriceBreakVisit().size()>0){
                    RealmList<DetailPriceBreakVisit> detailPriceBreakVisits = new RealmList<>();
                    for (int i = 0; i<visit.getDetailPriceBreakVisit().size();i++){
                        if (visit.getDetailPriceBreakVisit().get(i).getPending().equals(Constants.FLAG_DATA_PENDING_YES)){
                            DetailPriceBreakVisit detailPriceBreakVisit = visit.getDetailPriceBreakVisit().get(i);
                            detailPriceBreakVisit.setPending(Constants.FLAG_DATA_PENDING_NO);
                            detailPriceBreakVisits.add(detailPriceBreakVisit);
                        }
                    }
                    visit.setDetailPriceBreakVisit(detailPriceBreakVisits);
                }


        if (visit.getStockSamplingFinalVisit() != null ) {
            RealmList<StockSamplingFinalVisit> stockSamplingFinalVisits = new RealmList<>();
            for (int i = 0; i<visit.getStockSamplingFinalVisit().size();i++){
                if (visit.getStockSamplingFinalVisit().get(i).getPending().equals(Constants.FLAG_DATA_PENDING_YES)){
                    StockSamplingFinalVisit stockSamplingFinalVisit =  visit.getStockSamplingFinalVisit().get(i);
                    stockSamplingFinalVisit.setPending(Constants.FLAG_DATA_PENDING_NO);
                    stockSamplingFinalVisits.add(stockSamplingFinalVisit);
                }
            }
            visit.setStockSamplingFinalVisit(stockSamplingFinalVisits);
        }


        if (visit.getDetailStockSamplingFinalVisit() != null  && visit.getDetailStockSamplingFinalVisit().size()>0){
            RealmList<DetailStockSamplingFinalVisit> detailStockSamplingFinalVisits = new RealmList<>();
            for (int i = 0 ; i<visit.getDetailStockSamplingFinalVisit().size();i++){
                if (visit.getDetailStockSamplingFinalVisit().get(i).getPending().equals(Constants.FLAG_DATA_PENDING_YES)){
                    DetailStockSamplingFinalVisit detailStockSamplingFinalVisit =  visit.getDetailStockSamplingFinalVisit().get(i);
                    detailStockSamplingFinalVisit.setPending(Constants.FLAG_DATA_PENDING_NO);
                    detailStockSamplingFinalVisits.add(detailStockSamplingFinalVisit);
                }
            }
            visit.setDetailStockSamplingFinalVisit(detailStockSamplingFinalVisits);
        }

               String message = App.context.getResources().getString(R.string.message_register_visit_success);



        VisitInteractor.getInstance().registerVisit(visit,moduleVisit,false, new VisitInteractor.SuccessRegisterVisitListener() {
            @Override
            public void onSuccessRegisterVisit() {
                mView.stopLoading();
                mView.onSendPendingVisitSuccess(message);
            }

        }, new BaseInteractor.ErrorCallback() {
            @Override
            public void onError(Exception exception) {
                mView.stopLoading();
                mView.showErrorMessage(exception);
            }
        });
    }

    private void onSaveRegisterClient(Client client, boolean isOnline, String  moduleVisit, Visit visit){

        client.setPending(Constants.FLAG_DATA_PENDING_NO);

        ClientInteractor.getInstance().registerClient(client, new ClientInteractor.SuccessRegisterClientListener() {
            @Override
            public void onSuccessRegisterClient() {
                saveRegisterVisit(visit,isOnline,moduleVisit);
            }
        }, new BaseInteractor.ErrorCallback() {
            @Override
            public void onError(Exception exception) {
                mView.stopLoading();
                mView.showErrorMessage(exception);
            }
        });
    }


    public void onSendOnlinePending(Visit visit,Client client){
        String moduleVisit = Constants.FLAG_VISIT_T;
        VisitInteractor.getInstance().registerVisit(visit,moduleVisit,true, new VisitInteractor.SuccessRegisterVisitListener() {
            @Override
            public void onSuccessRegisterVisit() {
                onSaveRegisterClient( client, false, moduleVisit, visit);
            }

        }, new BaseInteractor.ErrorCallback() {
            @Override
            public void onError(Exception exception) {
                mView.stopLoading();
                mView.showErrorMessage(exception);
            }
        });
    }

}


