package com.visual.purina_impulso.presentation.auth;

import com.visual.purina_impulso.base.BasePresenter;
import com.visual.purina_impulso.base.BaseViewPresenter;


interface LoginContract {

    interface Presenter extends BasePresenter {

        void onCheckVersion();
        void login(String dni, String password);
        void clearDB();

    }

    interface View extends BaseViewPresenter<Presenter> {

        void onSyncSuccess(String message);
        void onClearDBSuccess(String message);

    }
}
