package com.visual.purina_impulso.presentation;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.visual.purina_impulso.App;
import com.visual.purina_impulso.data.source.helper.PreferencesHelper;
import com.visual.purina_impulso.presentation.auth.AuthActivity;
import com.visual.purina_impulso.presentation.menu.MenuActivity;


public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //obteneos al usuario de las preferncias, dependiendo de eso se va al login o al menu general
       if ( PreferencesHelper.getUser() != null){
           App.log("id empleado " + PreferencesHelper.getUser().getEmployeeId());
           navigateToMenuActivity();
        }else{
            navigateToAuthActivity();
        }
    }

    // al login
    private void navigateToAuthActivity() {
        Intent intent = new Intent(this, AuthActivity.class);
        startActivity(intent);
        finish();
    }


    // al menu general
    private void navigateToMenuActivity() {
        Intent intent = new Intent(this, MenuActivity.class);
        startActivity(intent);
        finish();
    }
}
