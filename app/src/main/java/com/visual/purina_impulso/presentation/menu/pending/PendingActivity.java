package com.visual.purina_impulso.presentation.menu.pending;


import android.os.Bundle;
import android.os.StrictMode;

import android.support.v4.app.FragmentManager;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import com.visual.purina_impulso.Constants;
import com.visual.purina_impulso.R;
import com.visual.purina_impulso.base.BaseActivity;

import com.visual.purina_impulso.util.ActivityUtils;

import butterknife.BindView;


public class PendingActivity extends BaseActivity {

    @BindView(R.id.toolbar_main) Toolbar mToolbar;
    @BindView(R.id.toolbar_title_text_view) TextView mToolbarTitleText;

    private PendingPresenter mPresenter;
    private static final int CAMERA_REQUEST_CODE = 102;
    private static final int REQUEST_IMAGE_CAMERA = 555;

    private FragmentManager mFragmentManager;

    @Override
    protected int getResLayout() {
        return R.layout.activity_pending;
    }

    @Override
    protected void setupView(Bundle savedInstanceState) {
        mFragmentManager = getSupportFragmentManager();
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());


        PendingFragment pendingFragment = (PendingFragment) getSupportFragmentManager().findFragmentById(R.id.contentFramePending);

        if (pendingFragment == null) {
            pendingFragment = PendingFragment.newInstance();
            ActivityUtils.addFragmentToActivity(getSupportFragmentManager(), pendingFragment, R.id.contentFramePending);
        }

        mPresenter = new PendingPresenter(pendingFragment);

        ActivityUtils.setToolBarBack(this,mToolbar,true);
    }

    public void setTitleToolbar(String mTitleToolbar) {
        mToolbarTitleText.setText(mTitleToolbar);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home) {
            if (Constants.FLAG_PENDING_FRAGMENT == Constants.STEP_POSITION) {
                finish();
            } else {
                mFragmentManager.popBackStack();
            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void backStack(){
        mFragmentManager.popBackStack();
    }



}