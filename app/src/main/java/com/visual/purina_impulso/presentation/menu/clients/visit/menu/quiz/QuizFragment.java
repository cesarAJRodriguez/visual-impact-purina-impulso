package com.visual.purina_impulso.presentation.menu.clients.visit.menu.quiz;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.visual.purina_impulso.Constants;
import com.visual.purina_impulso.R;
import com.visual.purina_impulso.base.BaseFragment;
import com.visual.purina_impulso.domain.Client;
import com.visual.purina_impulso.domain.Quiz;
import com.visual.purina_impulso.domain.Visit;
import com.visual.purina_impulso.presentation.menu.clients.visit.menu.MenuVisitActivity;
import com.visual.purina_impulso.presentation.menu.clients.visit.menu.MenuVisitContract;
import com.visual.purina_impulso.presentation.menu.clients.visit.menu.MenuVisitPresenter;
import com.visual.purina_impulso.view.adapter.QuizAdapter;
import com.visual.purina_impulso.view.dialog.IDialogQuiz;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;


public class QuizFragment extends BaseFragment implements MenuVisitContract.QuizView,QuizAdapter.OnItemClickListener,IDialogQuiz{

    @BindView(R.id.rv_visit_quizzes) RecyclerView quizzesRecyclerView;
    @BindView(R.id.txt_message_quiz) TextView messageQuizTextView;
    @BindView(R.id.ll_quiz) LinearLayout quizLinearLayout;

    private FragmentManager mFragmentManager;
    private MenuVisitContract.Presenter mPresenter;

    public static QuizFragment newInstance(Client client,Visit visit) {
        Bundle arg = new Bundle();
        arg.putParcelable(Constants.BUNDLE_CLIENT_VISIT, client);
        arg.putParcelable(Constants.BUNDLE_CLIENT_VISIT_DETAIL, visit);
        QuizFragment fragment = new QuizFragment();
        fragment.setArguments(arg);
        return fragment;
    }

    @Override
    protected int getResLayout() {
        return R.layout.fragment_quiz;
    }

    @Override
    public void onResume() {
        super.onResume();
        ((QuizActivity)mContext).setTitleToolbar(getResources().getString(R.string.menu_visit_quiz));
        if (mPresenter == null){
            mPresenter = new MenuVisitPresenter(this);
        }
        mPresenter.getQuizzes();
        messageQuizTextView.setVisibility(View.GONE);
        quizLinearLayout.setVisibility(View.GONE);
    }

    @Override
    protected void setupView(Bundle savedInstanceState) {
    }

    @Override
    protected void onRestoreView(Bundle savedInstanceState) {

    }

    @Override
    public void setPresenter(@NonNull MenuVisitContract.Presenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public void getQuizzesSuccess(ArrayList<Quiz> quizzes) {
      if (quizzes != null && quizzes.size() > 0){
          setupRecycler(quizzes);
          messageQuizTextView.setVisibility(View.GONE);
          quizLinearLayout.setVisibility(View.VISIBLE);
      }else{
          messageQuizTextView.setVisibility(View.VISIBLE);
          quizLinearLayout.setVisibility(View.GONE);
      }
    }

    private void setupRecycler(ArrayList<Quiz> quizzes) {
        QuizAdapter searchClientAdapter = new QuizAdapter(this,this,mContext,quizzes);
        quizzesRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        quizzesRecyclerView.setAdapter(searchClientAdapter);
    }

    @Override
    public Client getClient() {
        Bundle args = getArguments();
        if (args != null) {
            return args.getParcelable(Constants.BUNDLE_CLIENT_VISIT);
        }
        return null;
    }

    @Override
    public Visit getVisit() {
        Bundle args = getArguments();
        if (args != null) {
            return args.getParcelable(Constants.BUNDLE_CLIENT_VISIT_DETAIL);
        }
        return null;
    }

    @Override
    public void onQuiz(Quiz quiz) {
        Intent intent = new Intent(mContext,QuizQuestionActivity.class);
        intent.putExtra(Constants.BUNDLE_CLIENT_VISIT,getClient());
        intent.putExtra(Constants.BUNDLE_CLIENT_VISIT_DETAIL,getVisit());
        intent.putExtra(Constants.BUNDLE_CLIENT_VISIT_QUIZ,quiz);
        startActivity(intent);
    }

    @Override
    public void onItemClick(Object object, int position) {

    }

    public void replaceFragment(Quiz quiz) {
        if (mFragmentManager == null) {
            mFragmentManager = getFragmentManager();
        }
        FragmentTransaction transaction = mFragmentManager.beginTransaction();
        transaction.replace(R.id.contentFrameVisitQuiz, QuizQuesionFragment.newInstance(quiz));
        transaction.addToBackStack(null);
        transaction.commit();
    }


    @OnClick(R.id.btn_close)
    public void onClose(){
        Intent intent = new Intent(mContext, MenuVisitActivity.class);
        intent.putExtra(Constants.BUNDLE_CLIENT_VISIT,getClient());
        intent.putExtra(Constants.BUNDLE_CLIENT_VISIT_DETAIL,getVisit());
        startActivity(intent);
        ((QuizActivity)mContext).finish();
    }


}