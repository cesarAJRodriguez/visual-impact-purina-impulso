package com.visual.purina_impulso.presentation.menu.clients.visit.menu.price;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;

import com.visual.purina_impulso.Constants;
import com.visual.purina_impulso.R;
import com.visual.purina_impulso.base.BaseFragment;
import com.visual.purina_impulso.domain.Client;
import com.visual.purina_impulso.domain.Visit;
import com.visual.purina_impulso.presentation.menu.clients.visit.menu.MenuVisitActivity;
import com.visual.purina_impulso.presentation.menu.clients.visit.menu.MenuVisitContract;

import butterknife.OnClick;

public class PriceTypeFragment extends BaseFragment implements MenuVisitContract.View{

    private FragmentManager mFragmentManager;

    public static PriceTypeFragment newInstance(Client client,Visit visit) {
        Bundle arg = new Bundle();
        arg.putParcelable(Constants.BUNDLE_CLIENT_VISIT, client);
        arg.putParcelable(Constants.BUNDLE_CLIENT_VISIT_DETAIL, visit);
        PriceTypeFragment fragment = new PriceTypeFragment();
        fragment.setArguments(arg);
        return fragment;
    }

    @Override
    protected int getResLayout() {
        return R.layout.fragment_type_price;
    }

    @Override
    public void onResume() {
        super.onResume();
        ((PriceActivity)mContext).setTitleToolbar(getResources().getString(R.string.menu_visit_price));
    }

    @Override
    protected void setupView(Bundle savedInstanceState) {
    }

    @Override
    protected void onRestoreView(Bundle savedInstanceState) {

    }

    @Override
    public void setPresenter(@NonNull MenuVisitContract.Presenter presenter) {
    }



    @OnClick(R.id.btn_price_break)
    public void onPriceBreak(){
        Constants.SEARCH_PRICE_BREAK.clear();
        Intent intent = new Intent(mContext,PriceTypeActivity.class);
        intent.putExtra(Constants.BUNDLE_CLIENT_VISIT,getClient());
        intent.putExtra(Constants.BUNDLE_CLIENT_VISIT_DETAIL,getVisit());
       // intent.putExtra(Constants.FLAG_PRICE_TYPE,Constants.FLAG_PRICE_TYPE_BREAK);
        Constants.FLAG_PRICE_TYPE =   Constants.FLAG_PRICE_TYPE_BREAK;
        startActivity(intent);
    }

    @OnClick(R.id.btn_price_competition)
    public void onPriceCompetition(){
        Constants.SEARCH_PRICE_COMPETITION.clear();
        Intent intent = new Intent(mContext,PriceTypeActivity.class);
        intent.putExtra(Constants.BUNDLE_CLIENT_VISIT,getClient());
        intent.putExtra(Constants.BUNDLE_CLIENT_VISIT_DETAIL,getVisit());
       // intent.putExtra(Constants.FLAG_PRICE_TYPE,Constants.FLAG_PRICE_TYPE_COMPETITION);
        Constants.FLAG_PRICE_TYPE =   Constants.FLAG_PRICE_TYPE_COMPETITION;
        startActivity(intent);
    }

    @OnClick(R.id.btn_close)
    public void onClose(){
        Intent intent = new Intent(mContext, MenuVisitActivity.class);
        intent.putExtra(Constants.BUNDLE_CLIENT_VISIT,getClient());
        intent.putExtra(Constants.BUNDLE_CLIENT_VISIT_DETAIL,getVisit());
        startActivity(intent);
        ((PriceActivity)mContext).finish();
    }


    public void replaceFragment(String typePrice) {
      /*  if (mFragmentManager == null) {
            mFragmentManager = getFragmentManager();
        }
        FragmentTransaction transaction = mFragmentManager.beginTransaction();
        transaction.replace(R.id.contentFrameVisitPrice, PriceFragment.newInstance(typePrice,getClient()));
        transaction.addToBackStack(null);
        transaction.commit();*/
    }


    @Override
    public Client getClient() {
        Bundle args = getArguments();
        if (args != null) {
            return args.getParcelable(Constants.BUNDLE_CLIENT_VISIT);
        }
        return null;
    }

    @Override
    public Visit getVisit() {
        Bundle args = getArguments();
        if (args != null) {
            return args.getParcelable(Constants.BUNDLE_CLIENT_VISIT_DETAIL);
        }
        return null;
    }

}