package com.visual.purina_impulso.presentation.auth;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.widget.EditText;

import butterknife.BindView;
import butterknife.OnClick;

import com.visual.purina_impulso.App;
import com.visual.purina_impulso.R;
import com.visual.purina_impulso.base.BaseFragment;

import com.visual.purina_impulso.data.source.helper.PreferencesHelper;
import com.visual.purina_impulso.presentation.channel.ChannelActivity;
import com.visual.purina_impulso.presentation.menu.MenuActivity;
import com.visual.purina_impulso.util.DeviceUtil;
import com.visual.purina_impulso.util.DialogUtil;
import com.visual.purina_impulso.view.dialog.IDialogSync;


public class LoginFragment extends BaseFragment implements LoginContract.View ,IDialogSync,IDialogSync.clearDialog{

    private LoginContract.Presenter mPresenter;

    @BindView(R.id.login_et_user) EditText mEmailTextInput;
    @BindView(R.id.login_et_password) EditText mPasswordTextInput;

    public static LoginFragment newInstance() {
        return new LoginFragment();
    }

    @Override
    protected int getResLayout() {
        return R.layout.fragment_login;
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.start();
        if (DeviceUtil.hasInternetConnection()){  // se compruab si se cuenta con internet
            mPresenter.onCheckVersion();  // servicio de la version
        }

    }

    @Override
    protected void setupView(Bundle savedInstanceState) {

    }

    @Override
    protected void onRestoreView(Bundle savedInstanceState) {

    }


    @Override
    public void onSyncSuccess(String message) {
        PreferencesHelper.setSystemId(String.valueOf(PreferencesHelper.getUser().getSystemId()));
        DialogUtil.showDialogSync(mContext,App.context.getString(R.string.dialog_welcome_message) + " "+ PreferencesHelper.getUser().getNames(),this);
    }



    @Override
    public void setPresenter(@NonNull LoginContract.Presenter presenter) {
        mPresenter = presenter;
    }


    @OnClick(R.id.btn_login)
    public void onLoginUser(){
        // click  para enviar la data del login
       if (DeviceUtil.hasInternetConnection()){
           PreferencesHelper.setUser(null);
           PreferencesHelper.setSystemId(null);
           mPresenter.login(mEmailTextInput.getText().toString(),mPasswordTextInput.getText().toString());
       }else{
           DialogUtil.showDialogMessage(mContext,getResources().getString(R.string.message_not_internet));
       }

    }

    // bprramos data , se muestra un mensaje de confirmacion
    @OnClick(R.id.btn_clear_data)
    public void onClearData(){
        DialogUtil.showDialogMessageConfirmClearDB(mContext,getResources().getString(R.string.message_confirm_delete_db),this);
    }

    @Override
    public void onSyncSuccess() {
        mContext.startActivity(new Intent(mContext,MenuActivity.class));
    }

    @Override
    public void onSyncConfirmAccept() {

    }

    // borra data
    @Override
    public void onClearDB() {
        mPresenter.clearDB();
    }

    @Override
    public void onClearDBSuccess(String message) {
        DialogUtil.showDialogMessage(mContext,message);
    }
}