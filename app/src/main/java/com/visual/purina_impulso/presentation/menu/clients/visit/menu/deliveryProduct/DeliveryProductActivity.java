package com.visual.purina_impulso.presentation.menu.clients.visit.menu.deliveryProduct;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.visual.purina_impulso.Constants;
import com.visual.purina_impulso.R;
import com.visual.purina_impulso.base.BaseActivity;
import com.visual.purina_impulso.domain.BaseProduct;
import com.visual.purina_impulso.domain.BrandProduct;
import com.visual.purina_impulso.domain.Client;
import com.visual.purina_impulso.domain.DeliveryDayVisit;
import com.visual.purina_impulso.domain.DetailStockSampligVisit;
import com.visual.purina_impulso.domain.Product;
import com.visual.purina_impulso.domain.SubCategoryProduct;
import com.visual.purina_impulso.domain.Visit;
import com.visual.purina_impulso.presentation.menu.clients.visit.menu.MenuVisitActivity;
import com.visual.purina_impulso.presentation.menu.clients.visit.menu.MenuVisitContract;
import com.visual.purina_impulso.presentation.menu.clients.visit.menu.MenuVisitPresenter;
import com.visual.purina_impulso.util.ActivityUtils;
import com.visual.purina_impulso.util.DialogUtil;
import com.visual.purina_impulso.util.FileUtil;
import com.visual.purina_impulso.util.ImageProcessAsynTask;
import com.visual.purina_impulso.util.widget.SpinnerViewItem;
import com.visual.purina_impulso.view.dialog.IDialogCameraView;
import com.visual.purina_impulso.view.dialog.IDialogVisit;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;
import java.util.UUID;

import butterknife.BindView;
import butterknife.OnClick;
import io.realm.RealmList;


public class DeliveryProductActivity extends BaseActivity implements MenuVisitContract.View,MenuVisitContract.DeliveryProductView,IDialogCameraView,IDialogVisit.VisitDeliveryDialog {

    @BindView(R.id.toolbar_main) Toolbar mToolbar;
    @BindView(R.id.toolbar_title_text_view) TextView mToolbarTitleText;
    @BindView(R.id.spn_merchandising) SpinnerViewItem<DetailStockSampligVisit> merchandisingSpinnerView;
    @BindView(R.id.spn_category) SpinnerViewItem<SubCategoryProduct> categorySpinnerView;
    @BindView(R.id.spn_brand) SpinnerViewItem<BrandProduct> brandSpinnerView;
    @BindView(R.id.spn_product) SpinnerViewItem<BaseProduct> baseProductSpinnerView;
    @BindView(R.id.spn_sku) SpinnerViewItem<Product> skuSpinnerView;
    @BindView(R.id.iv_photo) ImageView photoImageView;
    @BindView(R.id.et_amount_merchandising) EditText amountMerchandisingEditText;
    @BindView(R.id.et_amount_product) EditText amountProductEditText;

    public  MenuVisitContract.Presenter mPresenter;
    public Visit visitBundle;




    private String prefixFileName = "report-image-";
    private String tempName = "";
    private static String type;
    private static final int CAMERA_REQUEST_CODE = 102;
    private static final int REQUEST_IMAGE_CAMERA = 555;


    public static String pathPhotos="";
    public static Uri uriPhotos;
    public static String nameFiles="";
    public static String base64Photo="";
    private  int merchandisingId = -1;
    private  int merchandisingStockRes = 0;
    private  int subCategoryId = -1;
    private  int brandId = -1;
    private  int baseId = -1;
    private  int productId = -1;
    @Override
    protected int getResLayout() {
        return R.layout.activity_visit_delivery_product;
    }

    @Override
    protected void setupView(Bundle savedInstanceState) {

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        mPresenter =  new MenuVisitPresenter(this);

      setTitleToolbar(getResources().getString(R.string.menu_visit_delivery_product));

        if (mPresenter == null){
            mPresenter = new MenuVisitPresenter(this);
        }
        mPresenter.getSubCategoryProduct();
        setupListerner();
        ActivityUtils.setToolBarBack(this,mToolbar,true);

    }

    public void setTitleToolbar(String mTitleToolbar) {
        mToolbarTitleText.setText(mTitleToolbar);
    }

    @Override
    public void setPresenter(@NonNull MenuVisitContract.Presenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public Client getClient() {
        return getIntent().getParcelableExtra(Constants.BUNDLE_CLIENT_VISIT);
    }

    @Override
    public Visit getVisit() {
        return getIntent().getParcelableExtra(Constants.BUNDLE_CLIENT_VISIT_DETAIL);
    }

    @OnClick(R.id.btn_take_photo)
    public void onTakePhoto(){
        requestPermissionCamera();
    }

    private void requestPermissionCamera(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.CAMERA},
                        CAMERA_REQUEST_CODE);
            }else{
                callCamera();
            }
        }else{
            callCamera();
        }

    }

    public void callCamera(){
        tempName = prefixFileName + System.currentTimeMillis();
        Intent takeCameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        takeCameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, FileUtil.getPhotoFileUri(this, tempName));

        if (takeCameraIntent.resolveActivity(this.getPackageManager()) != null) {
            startActivityForResult(takeCameraIntent, REQUEST_IMAGE_CAMERA);
        } else {
            DialogUtil.showDialogMessage(this, getString(R.string.alert_error_to_camera));
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            System.gc();
            if (requestCode == REQUEST_IMAGE_CAMERA) {
                startLoading();
                ImageProcessAsynTask imageProcessAsynTask = new ImageProcessAsynTask(this, tempName,this, true);
                imageProcessAsynTask.execute();
                nameFiles = tempName;
            }
        }

    }

    @Override
    public void  onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case CAMERA_REQUEST_CODE: {
                if (grantResults.length == 0 || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                }else{
                    callCamera();
                }
                return;
            }
        }
    }






    @Override
    public void callCamera(Context context) {

    }

    @Override
    public void callGallery() {

    }

    @Override
    public void removeItem(int position) {

    }

    @Override
    public void showMessageError() {
        stopLoading();
        DialogUtil.showDialogMessage(this, this.getResources().getString(R.string.message_error_image_memory));

    }

    @Override
    public void SuccessUpload(String size, String path) {
        stopLoading();
        if (path!= null || path != ""){
            File file = new File(path);
            Picasso.with(this).load(file).placeholder(android.R.drawable.progress_horizontal).into(photoImageView);
            pathPhotos = path;
            Bitmap myBitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            myBitmap.compress(Bitmap.CompressFormat.JPEG, 70, stream);
            byte[] byteFormat = stream.toByteArray();
            String imgString = Base64.encodeToString(byteFormat, Base64.NO_WRAP);
            base64Photo = "data:image/jpeg;base64," + imgString;
        }
    }

    @OnClick(R.id.btn_save_register)
    public void onRegisterDeliveryVisit(){
            if (formValidate()){
                Calendar calendar = Calendar.getInstance();
                SimpleDateFormat formatDate = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
                String date =  formatDate.format(calendar.getTime());
                Visit visit = getVisit();

                RealmList<DeliveryDayVisit> deliveryDayVisits = new RealmList<>();

                String deliveryId = UUID.randomUUID().toString();


                SimpleDateFormat formatHour = new SimpleDateFormat("HH:mm:ss");
                String hour =   formatHour.format(calendar.getTime());
                int amountMerchandising =   Integer.parseInt(amountMerchandisingEditText.getText().toString());
                int amountProduct =   Integer.parseInt(amountProductEditText.getText().toString());


                if (visit.getDeliveryDayVisit() != null ){
                    if (visit.getDeliveryDayVisit().size() > 0){
                        deliveryDayVisits.addAll(visit.getDeliveryDayVisit());
                    }
                }

                String photo =  "";
                if (pathPhotos == ""){
                    photo="";
                }else{
                    photo=base64Photo;
                }

                DeliveryDayVisit deliveryDayVisit = new DeliveryDayVisit(
                        deliveryId,
                        Integer.parseInt(getClient().getEmployeeId()),
                        date,
                        getClient().getClientId(),
                        merchandisingId,
                        amountMerchandising,
                        subCategoryId,
                        brandId,
                        baseId,
                        productId,
                        amountProduct,
                        hour,
                        photo,
                        getVisit().getId(),
                        Constants.FLAG_DATA_PENDING_YES
                      );


                deliveryDayVisits.add(deliveryDayVisit);
                visit.setDeliveryDayVisit(deliveryDayVisits);

                for (int i = 0 ; i< getVisit().getDetailStockSampligVisit().size();i++) {
                    if ( getVisit().getDetailStockSampligVisit().get(i).getMerchandisingId() == merchandisingId){
                      int res = visit.getDetailStockSampligVisit().get(i).getStockRest();
                      int totalRes = res  - amountMerchandising;
                        visit.getDetailStockSampligVisit().get(i).setStockRest(totalRes);
                      break;
                    }
                }

                mPresenter.onRegisterVisit(visit,Constants.FLAG_VISIT_DELIVERY,getClient());
            }
    }

    public void setupListerner(){
        if (getVisit().getDetailStockSampligVisit() !=  null && getVisit().getDetailStockSampligVisit().size()>0){

            int total = 0;
            for (int i = 0 ; i< getVisit().getDetailStockSampligVisit().size();i++) {
                if ( getVisit().getDetailStockSampligVisit().get(i).getStockRest() > 0){
                    total++;
                }
            }

            DetailStockSampligVisit detailStockSampligVisits[] =  new DetailStockSampligVisit[total];

            int index = 0;
            for (int i = 0 ; i< getVisit().getDetailStockSampligVisit().size();i++) {
                if ( getVisit().getDetailStockSampligVisit().get(i).getStockRest() > 0){
                    detailStockSampligVisits[index] = getVisit().getDetailStockSampligVisit().get(i);
                    index++;
                }
            }
            merchandisingSpinnerView.setEntries(detailStockSampligVisits);
        }

        merchandisingSpinnerView.setOnItemSpinnerClick(new SpinnerViewItem.OnSpinnerClick() {
            @Override
            public void onItemClick(int position) {
                DetailStockSampligVisit detailStockSampligVisit = merchandisingSpinnerView.getSelectedItem();
                merchandisingId = detailStockSampligVisit.getMerchandisingId();
                merchandisingStockRes = detailStockSampligVisit.getStockRest();
            }

            @Override
            public void onRestoreItems() {

            }
        });


        categorySpinnerView.setOnItemSpinnerClick(new SpinnerViewItem.OnSpinnerClick() {
            @Override
            public void onItemClick(int position) {
               SubCategoryProduct subCategoryProduct = categorySpinnerView.getSelectedItem();
                subCategoryId = subCategoryProduct.getSubCategoryProductId();
                brandSpinnerView.setEntries(null);
                brandId = -1;
                brandSpinnerView.setText(getResources().getString(R.string.text_select_brand));
                baseProductSpinnerView.setEntries(null);
                baseId = -1;
                baseProductSpinnerView.setText(getResources().getString(R.string.text_select_product));
                skuSpinnerView.setEntries(null);
                productId=-1;
                skuSpinnerView.setText(getResources().getString(R.string.text_select_sku));
               mPresenter.getBrandProduct(subCategoryId);

            }

            @Override
            public void onRestoreItems() {

            }
        });

        brandSpinnerView.setOnItemSpinnerClick(new SpinnerViewItem.OnSpinnerClick() {
            @Override
            public void onItemClick(int position) {
                BrandProduct brandProduct = brandSpinnerView.getSelectedItem();
                 brandId = brandProduct.getBrandProductId();

                baseProductSpinnerView.setEntries(null);
                baseId = -1;
                baseProductSpinnerView.setText(getResources().getString(R.string.text_select_product));
                skuSpinnerView.setEntries(null);
                productId=-1;
                skuSpinnerView.setText(getResources().getString(R.string.text_select_sku));
                mPresenter.getBaseProduct(subCategoryId,brandId);
            }

            @Override
            public void onRestoreItems() {

            }
        });

        baseProductSpinnerView.setOnItemSpinnerClick(new SpinnerViewItem.OnSpinnerClick() {
            @Override
            public void onItemClick(int position) {
                BaseProduct baseProduct = baseProductSpinnerView.getSelectedItem();
                baseId = Integer.parseInt(baseProduct.getBaseProductId());
                skuSpinnerView.setEntries(null);
                productId=-1;
                skuSpinnerView.setText(getResources().getString(R.string.text_select_sku));
                mPresenter.getProducts(String.valueOf(baseId));
            }

            @Override
            public void onRestoreItems() {

            }
        });

        skuSpinnerView.setOnItemSpinnerClick(new SpinnerViewItem.OnSpinnerClick() {
            @Override
            public void onItemClick(int position) {
                Product product = skuSpinnerView.getSelectedItem();
                productId = product.getProductId();

            }

            @Override
            public void onRestoreItems() {

            }
        });
    }



    @OnClick(R.id.btn_cancel)
    public void onCancelRegister(){
        Intent intent = new Intent(this, MenuVisitActivity.class);
        intent.putExtra(Constants.BUNDLE_CLIENT_VISIT,getClient());
        intent.putExtra(Constants.BUNDLE_CLIENT_VISIT_DETAIL,getVisit());
        startActivity(intent);
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home) {

            Intent intent = new Intent(this, MenuVisitActivity.class);
            intent.putExtra(Constants.BUNDLE_CLIENT_VISIT,getClient());
            intent.putExtra(Constants.BUNDLE_CLIENT_VISIT_DETAIL,getVisit());
            startActivity(intent);
            finish();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(this, MenuVisitActivity.class);
        intent.putExtra(Constants.BUNDLE_CLIENT_VISIT,getClient());
        intent.putExtra(Constants.BUNDLE_CLIENT_VISIT_DETAIL,getVisit());
        startActivity(intent);
        finish();
    }



    private boolean formValidate(){


        if (merchandisingId == -1){
            DialogUtil.showDialogMessage(this,getResources().getString(R.string.message_merchandising_not_select));
            return false;
        }

        if (amountMerchandisingEditText.getText().toString().equals("0") || amountMerchandisingEditText.getText().toString().equals("0")){
            DialogUtil.showDialogMessage(this,getResources().getString(R.string.message_amount_merchandising_empty));
            return false;
        }else{
            if (Integer.parseInt(amountMerchandisingEditText.getText().toString()) > merchandisingStockRes){
                DialogUtil.showDialogMessage(this,getResources().getString(R.string.message_amount_merchandising_not_allow));
                return false;
            }
        }

        if (subCategoryId == -1){
            DialogUtil.showDialogMessage(this,getResources().getString(R.string.message_category_not_select));
            return false;
        }

        if (brandId == -1){
            DialogUtil.showDialogMessage(this,getResources().getString(R.string.message_brand_not_select));
            return false;
        }

        if (baseId == -1){
            DialogUtil.showDialogMessage(this,getResources().getString(R.string.message_product_not_select));
            return false;
        }

        if (productId == -1){
            DialogUtil.showDialogMessage(this,getResources().getString(R.string.message_sku_not_select));
            return false;
        }

        if (amountProductEditText.getText().toString().equals("0") || amountProductEditText.getText().toString().equals("0")){
            DialogUtil.showDialogMessage(this,getResources().getString(R.string.message_amount_product_empty));
            return false;
        }

        if (amountProductEditText.getText().toString().equals("0") || amountProductEditText.getText().toString().equals("0")){
            DialogUtil.showDialogMessage(this,getResources().getString(R.string.message_amount_product_empty));
            return false;
        }


            return true;
    }

    @Override
    public void onRegisterDeliveryProductSuccess(String message, Visit visit) {
        visitBundle = visit;
        DialogUtil.showDialogMessageRegisterDeliveryVisit(this,message,this);
    }

    @Override
    public void getSubCategoryProductSuccess(ArrayList<SubCategoryProduct> subCategoryProducts) {
        if (subCategoryProducts !=  null && subCategoryProducts.size()>0){
            int indexsub = 0;
          ArrayList<Integer> subCateProId = new ArrayList<>();

            for (int i = 0 ; i< subCategoryProducts.size();i++) {
                for (int j = 0 ; j< subCategoryProducts.size();j++) {
                    if (subCategoryProducts.get(j).getSubCategoryProductId() != subCategoryProducts.get(i).getSubCategoryProductId()){
                        if (!subCateProId.contains(subCategoryProducts.get(j).getSubCategoryProductId())){
                            subCateProId.add(subCategoryProducts.get(j).getSubCategoryProductId());
                            indexsub++;
                        }

                    }
                }
            }

            SubCategoryProduct subCategoryProducts1[] =  new SubCategoryProduct[indexsub];
            indexsub = 0;
            subCateProId.clear();
            for (int i = 0 ; i< subCategoryProducts.size();i++) {
                for (int j = 0 ; j< subCategoryProducts.size();j++) {
                    if (subCategoryProducts.get(j).getSubCategoryProductId() != subCategoryProducts.get(i).getSubCategoryProductId()){
                        if (!subCateProId.contains(subCategoryProducts.get(j).getSubCategoryProductId())){
                            subCateProId.add(subCategoryProducts.get(j).getSubCategoryProductId());
                            subCategoryProducts1[indexsub] = subCategoryProducts.get(j);
                            indexsub++;
                        }

                    }
                }
            }

            if (indexsub>0){
                categorySpinnerView.setEntries(subCategoryProducts1);
            }

        }
    }

    @Override
    public void getBrandProductSuccess(ArrayList<BrandProduct> brandProducts) {
        if (brandProducts !=  null && brandProducts.size()>0){
            int indexsBrand = 0;
            ArrayList<Integer> brandId = new ArrayList<>();
            for (int i = 0 ; i< brandProducts.size();i++) {
                for (int j = 0 ; j< brandProducts.size();j++) {
                    if (brandProducts.get(j).getBrandProductId() != brandProducts.get(i).getBrandProductId()){
                        if (!brandId.contains(brandProducts.get(j).getBrandProductId())){
                            brandId.add(brandProducts.get(j).getBrandProductId());
                            indexsBrand++;
                        }
                    }
                }
            }



            BrandProduct brandProducts1[] =  new BrandProduct[indexsBrand];
            indexsBrand = 0;
            brandId.clear();
            for (int i = 0 ; i< brandProducts.size();i++) {
                for (int j = 0 ; j< brandProducts.size();j++) {
                    if (brandProducts.get(j).getBrandProductId() != brandProducts.get(i).getBrandProductId()){
                        if (!brandId.contains(brandProducts.get(j).getBrandProductId())){
                            brandId.add(brandProducts.get(j).getBrandProductId());
                            brandProducts1[indexsBrand] = brandProducts.get(j);
                            indexsBrand++;
                        }
                    }
                }

            }

            if (indexsBrand>0){
                brandSpinnerView.setEntries(brandProducts1);
            }

        }
    }

    @Override
    public void getBaseProductSuccess(ArrayList<BaseProduct> baseProducts) {
        if (baseProducts !=  null && baseProducts.size()>0){
            int indexsBase = 0;
            ArrayList<String> baseId = new ArrayList<>();

            for (int i = 0 ; i< baseProducts.size();i++) {
                for (int j = 0 ; j< baseProducts.size();j++) {
                    if (baseProducts.get(j).getBaseProductId() != baseProducts.get(i).getBaseProductId()){
                        if (!baseId.contains(baseProducts.get(j).getBaseProductId())){
                            baseId.add(baseProducts.get(j).getBaseProductId());
                            indexsBase++;
                        }
                    }
                }
            }


            BaseProduct baseProducts1[] =  new BaseProduct[indexsBase];
             indexsBase = 0;
            baseId.clear();
            for (int i = 0 ; i< baseProducts.size();i++) {
                for (int j = 0 ; j< baseProducts.size();j++) {
                    if (baseProducts.get(j).getBaseProductId() != baseProducts.get(i).getBaseProductId()){
                        if (!baseId.contains(baseProducts.get(j).getBaseProductId())){
                            baseId.add(baseProducts.get(j).getBaseProductId());
                            baseProducts1[indexsBase] = baseProducts.get(j);
                            indexsBase++;
                        }
                    }
                }

            }

            if (indexsBase>0){
                baseProductSpinnerView.setEntries(baseProducts1);
            }

        }
    }

    @Override
    public void getProductsSuccess(ArrayList<Product> products) {
        if (products !=  null && products.size()>0){
            int indexsProduct = 0;
            ArrayList<Integer> productId = new ArrayList<>();
            for (int i = 0 ; i< products.size();i++) {
                for (int j = 0 ; j< products.size();j++) {
                    if (products.get(j).getProductId() != products.get(i).getProductId()){
                        if (!productId.contains(products.get(j).getProductId())){
                            productId.add(products.get(j).getProductId());
                            indexsProduct++;
                        }
                    }
                }

            }

            Product products1[] =  new Product[indexsProduct];
             indexsProduct = 0;
            productId.clear();
            for (int i = 0 ; i< products.size();i++) {
                for (int j = 0 ; j< products.size();j++) {
                    if (products.get(j).getProductId() != products.get(i).getProductId()){
                        if (!productId.contains(products.get(j).getProductId())){
                            productId.add(products.get(j).getProductId());
                            products1[indexsProduct] = products.get(j);
                            indexsProduct++;
                        }

                    }
                }

            }

            if (indexsProduct>0){
                skuSpinnerView.setEntries(products1);
            }


        }
    }

    @Override
    public void onConfirmDeliveryVisit() {
        Intent intent = new Intent(this, MenuVisitActivity.class);
        intent.putExtra(Constants.BUNDLE_CLIENT_VISIT,getClient());
        intent.putExtra(Constants.BUNDLE_CLIENT_VISIT_DETAIL,visitBundle);
        startActivity(intent);
        finish();
    }
}