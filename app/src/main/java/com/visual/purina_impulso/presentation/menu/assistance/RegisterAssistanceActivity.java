package com.visual.purina_impulso.presentation.menu.assistance;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.visual.purina_impulso.Constants;
import com.visual.purina_impulso.R;
import com.visual.purina_impulso.base.BaseActivity;
import com.visual.purina_impulso.data.source.helper.PreferencesHelper;
import com.visual.purina_impulso.domain.Assistance;
import com.visual.purina_impulso.util.ActivityUtils;
import com.visual.purina_impulso.util.DialogUtil;
import com.visual.purina_impulso.util.FileUtil;
import com.visual.purina_impulso.util.ImageProcessAsynTask;
import com.visual.purina_impulso.util.MapUtil;
import com.visual.purina_impulso.view.dialog.IDialogAssistance;
import com.visual.purina_impulso.view.dialog.IDialogCameraView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.UUID;

import butterknife.BindView;
import butterknife.OnClick;

public class RegisterAssistanceActivity extends BaseActivity implements AssistanceContract.View, IDialogCameraView,IDialogAssistance {

    @BindView(R.id.toolbar_main) Toolbar mToolbar;
    @BindView(R.id.toolbar_title_text_view) TextView mToolbarTitleText;
    @BindView(R.id.et_assistance_comment) EditText assistanceCommentEditText;
    @BindView(R.id.txt_assistance_type) TextView assistanceTypeTextView;
    @BindView(R.id.txt_assistance_date) TextView assistanceDateTextView;
    @BindView(R.id.txt_assistance_hour) TextView assistanceHourTextView;
    @BindView(R.id.iv_photo) ImageView photoImageView;

    private String prefixFileName = "report-image-";
    private String tempName = "";
    private static String type;
    private static final int CAMERA_REQUEST_CODE = 102;
    private static final int REQUEST_IMAGE_CAMERA = 555;


    public static String pathPhotos="";
    public static Uri uriPhotos;
    public static String nameFiles="";
    public static String base64Photo="";

    private AssistancePresenter mPresenter;


    @Override
    protected int getResLayout() {
        return R.layout.activity_register_assistance;
    }

    @Override
    protected void setupView(Bundle savedInstanceState) {

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        mPresenter = new AssistancePresenter(this);
        ActivityUtils.setToolBarBack(this, mToolbar, true);
        type = getIntent().getStringExtra(Constants.BUNDLE_TYPE_ASSISTANCE);
        setTimeAssistance(type);
        setTitleToolbar(getResources().getText(R.string.toolbar_title_assistance).toString());
        Constants.STEP_POSITION =  Constants.FLAG_REGISTER_ASSISTANCE_FRAGMENT;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home) {
                finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void setTitleToolbar(String mTitleToolbar) {
        mToolbarTitleText.setText(mTitleToolbar);
    }

    private void setTimeAssistance(String type){
        if (type.equals(Constants.FLAG_TYPE_ASSISTANCE_START)){
            assistanceTypeTextView.setText(getResources().getText(R.string.assistance_register_start).toString());
        }else{
            assistanceTypeTextView.setText(getResources().getText(R.string.assistance_register_exit).toString());
        }

        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat formatDate = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
        assistanceDateTextView.setText(formatDate.format(calendar.getTime()));
        SimpleDateFormat formatHour = new SimpleDateFormat("HH:mm:ss");
        assistanceHourTextView.setText(formatHour.format(calendar.getTime()));
    }

    @OnClick(R.id.btn_save_register)
    public void onRegisterAssistance(){

        final MapUtil gps = new MapUtil(this);

        String photo =  base64Photo;
        String employeeId = String.valueOf(PreferencesHelper.getUser().getEmployeeId());
        String latitude =  String.valueOf(gps.getLatitude());
        String longitude =   String.valueOf(gps.getLongitude());
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat formatHour = new SimpleDateFormat("HH:mm:ss");
        String hour =   formatHour.format(calendar.getTime());
        String comment =   assistanceCommentEditText.getText().toString();
        SimpleDateFormat formatDate = new SimpleDateFormat("yyyy-MM-dd",Locale.ENGLISH);
        String date =  formatDate.format(calendar.getTime());

        Assistance assistance =  new Assistance();
        String id = UUID.randomUUID().toString();
        if (type.equals(Constants.FLAG_TYPE_ASSISTANCE_START)){
            assistance.setId(id);
            assistance.setDate(date);
            assistance.setStartPhoto(photo);
            assistance.setEmployeeId(employeeId);
            assistance.setStartHour(hour);
            assistance.setStartLatitude(longitude);
            assistance.setStartLongitude(latitude);
            assistance.setStartComment(comment);
            assistance.setType(Constants.FLAG_TYPE_ASSISTANCE_START);
            assistance.setPending(Constants.FLAG_PENDING_ASSISTANCE_NO);

        }else{
            assistance.setId(id);
            assistance.setDate(date);
            assistance.setExitPhoto(photo);
            assistance.setEmployeeId(employeeId);
            assistance.setExitHour(hour);
            assistance.setExitLatitude(longitude);
            assistance.setExitLongitude(latitude);
            assistance.setExitComment(comment);
            assistance.setType(Constants.FLAG_TYPE_ASSISTANCE_EXIT);
            assistance.setPending(Constants.FLAG_PENDING_ASSISTANCE_NO);
        }

        if (validateAssistance()){
            if (mPresenter == null){
                mPresenter =  new AssistancePresenter(this);
            }
            mPresenter.onRegisterAssistance(assistance);
        }

    }


    private boolean validateAssistance(){
        if (base64Photo == "" || base64Photo == null ){
            DialogUtil.showDialogMessage(this,getResources().getText(R.string.message_assistance_incomplete_photo).toString());
            return false;
        }
        return true;
    }

    @OnClick(R.id.btn_take_photo)
    public void onTakePhoto(){
        requestPermissionCamera();
    }

    private void requestPermissionCamera(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.CAMERA},
                        CAMERA_REQUEST_CODE);
            }else{
                callCamera();
            }
        }else{
            callCamera();
        }

    }

    public void callCamera(){
        tempName = prefixFileName + System.currentTimeMillis();
        Intent takeCameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        takeCameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, FileUtil.getPhotoFileUri(this, tempName));

        if (takeCameraIntent.resolveActivity(this.getPackageManager()) != null) {
            startActivityForResult(takeCameraIntent, REQUEST_IMAGE_CAMERA);
        } else {
             DialogUtil.showDialogMessage(this, getString(R.string.alert_error_to_camera));
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            System.gc();
            if (requestCode == REQUEST_IMAGE_CAMERA) {
                startLoading();
                ImageProcessAsynTask imageProcessAsynTask = new ImageProcessAsynTask(this, tempName,this, true);
                imageProcessAsynTask.execute();
                nameFiles = tempName;
            }
        }

    }

    @Override
    public void  onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
       super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case CAMERA_REQUEST_CODE: {
                if (grantResults.length == 0 || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                } else {

                callCamera();
                }
                return;
            }
        }
    }



    @Override
    public void setPresenter(@NonNull AssistanceContract.Presenter presenter) {

    }

    @Override
    public void onRegisterAssistanceSuccess(String message) {
        DialogUtil.showDialogMessageRegisterAssistance(this,message,this);
    }

    @Override
    public void onRegisterSuccess() {
        finish();
    }

    @Override
    public void onConfirmSendAssistancePending() {
    }

    @OnClick(R.id.btn_cancel_register)
    public void onCancelRegisterAssistance(){
        finish();
    }

    @Override
    public void callCamera(Context context) {

    }

    @Override
    public void callGallery() {

    }

    @Override
    public void removeItem(int position) {

    }

    @Override
    public void showMessageError() {
        stopLoading();
        DialogUtil.showDialogMessage(this, this.getResources().getString(R.string.message_error_image_memory));
    }

    @Override
    public void SuccessUpload(String size, String path) {
        stopLoading();
        if (path!= null || path != ""){
            File file = new File(path);
            Picasso.with(this).load(file).placeholder(android.R.drawable.progress_horizontal).into(photoImageView);
            pathPhotos = path;
            Bitmap myBitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            myBitmap.compress(Bitmap.CompressFormat.JPEG, 70, stream);
            byte[] byteFormat = stream.toByteArray();
            String imgString = Base64.encodeToString(byteFormat, Base64.NO_WRAP);
            base64Photo = "data:image/jpeg;base64," + imgString;
        }

    }
}