package com.visual.purina_impulso.presentation.menu.assistance;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import com.visual.purina_impulso.Constants;
import com.visual.purina_impulso.R;
import com.visual.purina_impulso.base.BaseActivity;
import com.visual.purina_impulso.util.ActivityUtils;

import butterknife.BindView;



public class AssistanceActivity extends BaseActivity {

    @BindView(R.id.toolbar_main) Toolbar mToolbar;
    @BindView(R.id.toolbar_title_text_view) TextView mToolbarTitleText;

    private AssistancePresenter mPresenter;
    private static final int CAMERA_REQUEST_CODE = 102;
    private static final int REQUEST_IMAGE_CAMERA = 555;

    private FragmentManager mFragmentManager;

    @Override
    protected int getResLayout() {
        return R.layout.activity_assistance;
    }

    @Override
    protected void setupView(Bundle savedInstanceState) {



        mFragmentManager = getSupportFragmentManager();
        AssistanceFragment assistanceFragment = (AssistanceFragment) getSupportFragmentManager().findFragmentById(R.id.contentFrameAssistance);

        if (assistanceFragment == null) {
            assistanceFragment = AssistanceFragment.newInstance();
            ActivityUtils.addFragmentToActivity(getSupportFragmentManager(), assistanceFragment, R.id.contentFrameAssistance);
        }

        mPresenter = new AssistancePresenter(assistanceFragment);
        ActivityUtils.setToolBarBack(this,mToolbar,true);
    }

    public void setTitleToolbar(String mTitleToolbar) {
        mToolbarTitleText.setText(mTitleToolbar);
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.contentFrameAssistance);
        fragment.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home) {
            if (Constants.FLAG_ASSISTANCE_FRAGMENT == Constants.STEP_POSITION) {
                finish();
            } else {
                mFragmentManager.popBackStack();
            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}