package com.visual.purina_impulso.presentation.menu.clients.visit;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.visual.purina_impulso.Constants;
import com.visual.purina_impulso.R;
import com.visual.purina_impulso.base.BaseActivity;
import com.visual.purina_impulso.domain.Client;
import com.visual.purina_impulso.domain.Incidence;
import com.visual.purina_impulso.domain.IncidenceVisit;
import com.visual.purina_impulso.domain.StartVisit;
import com.visual.purina_impulso.domain.Visit;
import com.visual.purina_impulso.util.ActivityUtils;
import com.visual.purina_impulso.util.DialogUtil;
import com.visual.purina_impulso.util.FileUtil;
import com.visual.purina_impulso.util.ImageProcessAsynTask;
import com.visual.purina_impulso.util.MapUtil;
import com.visual.purina_impulso.util.widget.SpinnerViewItem;
import com.visual.purina_impulso.view.dialog.IDialogCameraView;
import com.visual.purina_impulso.view.dialog.IDialogVisit;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;
import java.util.UUID;

import butterknife.BindView;
import butterknife.OnClick;


public class VisitIncidenceActivity extends BaseActivity implements VisitContract.View,IDialogCameraView,IDialogVisit.VisitSendIncidenceSuccessDialog {

    @BindView(R.id.toolbar_main) Toolbar mToolbar;
    @BindView(R.id.toolbar_title_text_view) TextView mToolbarTitleText;
    @BindView(R.id.spn_type_incidence) SpinnerViewItem<Incidence> typeIncidenceSpinnerView;
    @BindView(R.id.iv_photo) ImageView photoImageView;
    @BindView(R.id.txt_comment) TextView commentTextView;


    private VisitContract.Presenter mPresenter;
    private Incidence incidenceSelect;
    public Visit visitBundle;
    public int incidenceTypeId = -1;

    private String prefixFileName = "report-image-";
    private String tempName = "";

    private static final int CAMERA_REQUEST_CODE = 102;
    private static final int REQUEST_IMAGE_CAMERA = 555;


    public static String pathPhotos="";
    public static Uri uriPhotos;
    public static String nameFiles="";
    public static String base64Photo="";
    @Override
    protected int getResLayout() {
        return R.layout.activity_incidence;
    }

    @Override
    protected void setupView(Bundle savedInstanceState) {

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        mPresenter =  new VisitPresenter(this);

        setTitleToolbar(getResources().getString(R.string.toolbar_register_incidence));

        if (mPresenter == null){
            mPresenter = new VisitPresenter(this);
        }
        mPresenter.getTypeIncidences();
        setupListerner();
        ActivityUtils.setToolBarBack(this,mToolbar,true);
    }

    public void setTitleToolbar(String mTitleToolbar) {
        mToolbarTitleText.setText(mTitleToolbar);
    }



    @Override
    public void getTypeIncidencesSuccess(ArrayList<Incidence> incidences) {
        if (incidences != null && incidences.size() > 0){
            Incidence typeIncidence[] =  new Incidence[incidences.size()];
            for (int i = 0 ; i< incidences.size();i++) {
                typeIncidence[i] = incidences.get(i);
            }
            typeIncidenceSpinnerView.setEntries(typeIncidence);
        }
    }

    @Override
    public void onRegisterStartVisitSuccess(String message, Visit visit) {

    }

    @Override
    public void onRegisterIncidenceVisitSuccess(String message) {
DialogUtil.showDialogMessageSendIncidenceSuccess(this,message,this);
    }

    @Override
    public Client getClient() {
        return getIntent().getParcelableExtra(Constants.BUNDLE_CLIENT_VISIT);
    }

    @Override
    public Visit getVisit() {
        return getIntent().getParcelableExtra(Constants.BUNDLE_CLIENT_VISIT_DETAIL);
    }

    @OnClick(R.id.btn_take_photo)
    public void onTakePhoto(){
        requestPermissionCamera();
    }

    private void requestPermissionCamera(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.CAMERA},
                        CAMERA_REQUEST_CODE);
            }else{
                callCamera();
            }
        }else{
            callCamera();
        }

    }

    public void callCamera(){
        tempName = prefixFileName + System.currentTimeMillis();
        Intent takeCameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        takeCameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, FileUtil.getPhotoFileUri(this, tempName));

        if (takeCameraIntent.resolveActivity(this.getPackageManager()) != null) {
            startActivityForResult(takeCameraIntent, REQUEST_IMAGE_CAMERA);
        } else {
            DialogUtil.showDialogMessage(this, getString(R.string.alert_error_to_camera));
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            System.gc();
            if (requestCode == REQUEST_IMAGE_CAMERA) {
                startLoading();
                ImageProcessAsynTask imageProcessAsynTask = new ImageProcessAsynTask(this, tempName,this, true);
                imageProcessAsynTask.execute();
                nameFiles = tempName;
            }
        }

    }

    @Override
    public void  onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case CAMERA_REQUEST_CODE: {
                if (grantResults.length == 0 || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                } else {

                    callCamera();
                }
                return;
            }
        }
    }


    @Override
    public void callCamera(Context context) {

    }

    @Override
    public void callGallery() {

    }

    @Override
    public void removeItem(int position) {

    }

    @Override
    public void showMessageError() {
        stopLoading();
        DialogUtil.showDialogMessage(this, this.getResources().getString(R.string.message_error_image_memory));

    }

    @Override
    public void SuccessUpload(String size, String path) {
        stopLoading();
        if (path!= null || path != ""){
            File file = new File(path);
            Picasso.with(this).load(file).placeholder(android.R.drawable.progress_horizontal).into(photoImageView);
            pathPhotos = path;
            Bitmap myBitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            myBitmap.compress(Bitmap.CompressFormat.JPEG, 70, stream);
            byte[] byteFormat = stream.toByteArray();
            String imgString = Base64.encodeToString(byteFormat, Base64.NO_WRAP);
            base64Photo = "data:image/jpeg;base64," + imgString;
        }
    }

    @OnClick(R.id.btn_save_register)
    public void onRegisterPhotoVisit(){
        if (formValidate()){


            final MapUtil gps = new MapUtil(this);

            String latitude =  String.valueOf(gps.getLatitude());
            String longitude =   String.valueOf(gps.getLongitude());
            Calendar calendar = Calendar.getInstance();
            SimpleDateFormat formatHour = new SimpleDateFormat("HH:mm:ss");
            String hour =   formatHour.format(calendar.getTime());
            SimpleDateFormat formatDate = new SimpleDateFormat("yyyy-MM-dd",Locale.ENGLISH);
            String date =  formatDate.format(calendar.getTime());


            String comment = commentTextView.getText().toString();

            String visitStartIncidenceId = UUID.randomUUID().toString();
            StartVisit startVisit = new StartVisit(
                    visitStartIncidenceId,
                    Integer.valueOf(getClient().getEmployeeId()),
                    date,
                    getClient().getClientId(),
                    hour,
                    "",
                    longitude,
                    latitude,
                    "",
                    "",
                    Constants.FLAG_CLIENT_VISIT_STATUS_INCIDENCE,
                    getClient().getVisitId(),
                    Constants.FLAG_DATA_PENDING_YES
            );

            String incidenceId = UUID.randomUUID().toString();
            IncidenceVisit incidenceVisit = new IncidenceVisit(
                    incidenceId,
                    Integer.valueOf(getClient().getEmployeeId()),
                    date,
                    getClient().getClientId(),
                    incidenceTypeId,
                    base64Photo,
                    comment,
                    hour,
                    getClient().getVisitId()

            );

            mPresenter.registerIncidence(startVisit,incidenceVisit);
        }
    }

    public void setupListerner(){
        typeIncidenceSpinnerView.setOnItemSpinnerClick(new SpinnerViewItem.OnSpinnerClick() {
            @Override
            public void onItemClick(int position) {
                incidenceSelect = typeIncidenceSpinnerView.getSelectedItem();
                incidenceTypeId = incidenceSelect.getTypeIncidenceId();
            }

            @Override
            public void onRestoreItems() {

            }
        });

    }




    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home) {

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }

    private boolean formValidate(){

        if (incidenceTypeId == -1){
            DialogUtil.showDialogMessage(this,getResources().getString(R.string.message_type_incidence_empty));
            return false;
        }

        if (pathPhotos.equals("")){
            DialogUtil.showDialogMessage(this,getResources().getString(R.string.message_photo_empty));
            return false;
        }
        return true;
    }

    @Override
    public void setPresenter(@NonNull VisitContract.Presenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public void onConfirmSendIncidenceSuccessVisit() {
        finish();
    }
}