package com.visual.purina_impulso.presentation.menu;

import android.support.annotation.NonNull;

import com.visual.purina_impulso.App;
import com.visual.purina_impulso.Constants;
import com.visual.purina_impulso.R;
import com.visual.purina_impulso.base.BaseInteractor;
import com.visual.purina_impulso.data.source.helper.PreferencesHelper;
import com.visual.purina_impulso.domain.Assistance;
import com.visual.purina_impulso.domain.Client;
import com.visual.purina_impulso.domain.Sync;
import com.visual.purina_impulso.domain.User;
import com.visual.purina_impulso.domain.interactor.AssistanceInteractor;
import com.visual.purina_impulso.domain.interactor.ClientInteractor;
import com.visual.purina_impulso.domain.interactor.SyncInteractor;

import java.util.ArrayList;


class MenuPresenter implements MenuContract.Presenter {

    private  static  MenuContract.View mView;


    MenuPresenter(@NonNull MenuContract.View view) {
        mView = view;
        mView.setPresenter(this);
    }

    @Override
    public void start() {
    }

    @Override
    public void onSync(String type) {
        mView.startLoadingText(App.context.getResources().getString(R.string.dialog_sync_message));
        String syncTraditional = "false";
        String syncModern = "false";
        String syncSpecialized = "false";
        if (type.equals("1")){
            syncTraditional = "true";
        }else if(type.equals("2") || type.equals("4")){
            syncModern = "true";
        }else{
            syncSpecialized = "true";
        }


        //llamando al servicio de sincronizar
        SyncInteractor.getInstance().sync(syncTraditional,syncModern,syncSpecialized, new SyncInteractor.SuccessSyncListener() {
            @Override
            public void onSuccessSync(Sync sync) {
                //servicio correcto
                saveSync(sync,type);
            }
        }, new BaseInteractor.ErrorCallback() {
            @Override
            public void onError(Exception exception) {
                //error
                mView.stopLoading();
                mView.showErrorMessage(exception);
            }
        });
    }

    private void onClearDB(Sync sync, String type){

        SyncInteractor.getInstance().clearSync(new SyncInteractor.SuccessClearSyncListener() {
            @Override
            public void onSuccessClearSync() {
                saveSync(sync,type);
            }
        }, new BaseInteractor.ErrorCallback() {
            @Override
            public void onError(Exception exception) {
                mView.stopLoading();
                mView.showErrorMessage(exception);
            }
        });
    }

    @Override
    public void onCheckAssistance(Assistance assistance) {

        AssistanceInteractor.getInstance().checkAssistance(assistance, new AssistanceInteractor.SuccessCheckAssistanceListener() {
            @Override
            public void onSuccessCheckAssistance(ArrayList<Assistance> assistances) {
                mView.stopLoading();

                    if (assistances != null && assistances.size() > 0  ){
                        onCheckVisitPending(Constants.FLAG_TYPE_ASSISTANCE_START,assistances);
                    }else{
                        onCheckVisitPending(Constants.FLAG_TYPE_ASSISTANCE_START_EMPTY,assistances);
                    }
            }
        }, new BaseInteractor.ErrorCallback() {
            @Override
            public void onError(Exception exception) {
                onCheckVisitPending(Constants.FLAG_TYPE_ASSISTANCE_START_EMPTY,null);
            }
        });
    }

    public void onCheckVisitPending(String type,ArrayList<Assistance> assistances) {
        String employeeId = String.valueOf(PreferencesHelper.getUser().getEmployeeId());
        String typeClient = Constants.FLAG_CLIENT;
        String search =  Constants.FLAG_PENDING_CLIENTS_VISITS;

        ClientInteractor.getInstance().getClients(employeeId,typeClient,search, new ClientInteractor.SuccessGetClientsListener() {
            @Override
            public void onSuccessGetClients(ArrayList<Client> clients) {
                mView.stopLoading();
                mView.onSuccessCheckAssistance(type,assistances,clients);
            }
        }, new BaseInteractor.ErrorCallback() {
            @Override
            public void onError(Exception exception) {
                mView.stopLoading();
                mView.onSuccessCheckAssistance(type,assistances,null);
            }
        });
    }

    //guardar la sincronizacio en la bd local
    private void saveSync(Sync sync,String type){

        SyncInteractor.getInstance().saveSync(sync,true, new SyncInteractor.SuccessSaveSyncListener() {
            @Override
            public void onSuccessSaveSync(String message) {
                mView.stopLoading();
                String channel = "";
                channel = App.context.getString(R.string.name_channel_modern);
                User user = new User(PreferencesHelper.getUser().getAssistance(),
                        PreferencesHelper.getUser().getEmployeeId(),
                        PreferencesHelper.getUser().getSystemId(),
                        PreferencesHelper.getUser().getTypeId(),
                        PreferencesHelper.getUser().getNames(),
                        channel,
                        PreferencesHelper.getUser().getUserType());

                PreferencesHelper.setUser(user);
                mView.onSuccessSyncMenu(message);
            }
        }, new BaseInteractor.ErrorCallback() {
            @Override
            public void onError(Exception exception) {
                mView.stopLoading();
                mView.showErrorMessage(exception);
            }
        });
    }

}


