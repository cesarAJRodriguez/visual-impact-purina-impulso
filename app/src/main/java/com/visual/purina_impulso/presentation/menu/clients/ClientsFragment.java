package com.visual.purina_impulso.presentation.menu.clients;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.visual.purina_impulso.Constants;
import com.visual.purina_impulso.R;
import com.visual.purina_impulso.base.BaseFragment;
import com.visual.purina_impulso.domain.Client;
import com.visual.purina_impulso.domain.Visit;
import com.visual.purina_impulso.presentation.menu.MenuActivity;
import com.visual.purina_impulso.presentation.menu.clients.visit.VisitActivity;
import com.visual.purina_impulso.util.DialogUtil;
import com.visual.purina_impulso.view.adapter.ClientAdapter;
import com.visual.purina_impulso.view.dialog.IDialogClient;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;


public class ClientsFragment extends BaseFragment implements ClientsContract.View,ClientAdapter.OnItemClickListener,IDialogClient {

    @BindView(R.id.btn_channel) Button channelButton;
    @BindView(R.id.ll_clients) LinearLayout clientsLinearLayout;
    @BindView(R.id.rv_clients) RecyclerView clientsRecyclerView;
    @BindView(R.id.txt_message_clients) TextView messageClientsTextView;

    private ClientsContract.Presenter mPresenter;

    private Client clientSelect;
    private Visit visitInternal;
    private static final int LOCATION_REQUEST_CODE = 101;
    private int amountVisitStart = 0;

    public static ClientsFragment newInstance() {
        return new ClientsFragment();
    }

    @Override
    protected int getResLayout() {
        return R.layout.fragment_clients;
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.start();
        Constants.STEP_POSITION =  Constants.FLAG_CLIENTS_FRAGMENT;
        ((ClientsActivity)mContext).setTitleToolbar(getResources().getText(R.string.toolbar_title_clients).toString());
        if (mPresenter == null){
            mPresenter =  new ClientsPresenter(this);
        }
        mPresenter.getClients();

    }

    @Override
    protected void setupView(Bundle savedInstanceState) {

    }

    @Override
    protected void onRestoreView(Bundle savedInstanceState) {

    }

    @Override
    public void setPresenter(@NonNull ClientsContract.Presenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public void getClientsSuccess(ArrayList<Client> clients) {


        if (clients!= null){

           ArrayList<Client> clientList = new ArrayList<>();

           for (int i = 0; i<clients.size(); i++){
               if (!clients.get(i).getVisit().equals(Constants.FLAG_CLIENT_VISIT_EXIT)){
                   if (clients.get(i).getVisit().equals(Constants.FLAG_CLIENT_VISIT_START)){
                       amountVisitStart++;
                   }
                   clientList.add(clients.get(i));
               }
           }

            if (clientList.size() >0){
                    channelButton.setText(getResources().getText(R.string.internal_change_channel_modern).toString() + " (" +clientList.size()+")" );
                    channelButton.setVisibility(View.VISIBLE);
                    clientsLinearLayout.setVisibility(View.GONE);
                    clientsRecyclerView.setVisibility(View.GONE);
                    messageClientsTextView.setVisibility(View.GONE);
                    setupRecycler(clientList);
                }else{
                    channelButton.setVisibility(View.GONE);
                    clientsLinearLayout.setVisibility(View.GONE);
                    clientsRecyclerView.setVisibility(View.GONE);
                    messageClientsTextView.setVisibility(View.VISIBLE);
                }
        }else{
            channelButton.setVisibility(View.GONE);
            clientsLinearLayout.setVisibility(View.GONE);
             clientsRecyclerView.setVisibility(View.GONE);
             messageClientsTextView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void getClientInternalSuccess(Client client, Visit visit) {
           onVisitClient(visit);
    }

    @OnClick(R.id.btn_channel)
    public void onClickChannel(){
        if (clientsRecyclerView.getVisibility() == View.VISIBLE) {
            channelButton.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_add_circle_outline_white, 0, 0, 0);
            clientsRecyclerView.setVisibility(View.GONE);
            clientsLinearLayout.setVisibility(View.GONE);
        } else {
            channelButton.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_remove_circle_outline_white, 0, 0, 0);
            clientsRecyclerView.setVisibility(View.VISIBLE);
            clientsLinearLayout.setVisibility(View.VISIBLE);
        }
    }

    private void setupRecycler(ArrayList<Client> clients) {
        ClientAdapter clientAdapter = new ClientAdapter(this,this,mContext,clients);
        clientsRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        clientsRecyclerView.setAdapter(clientAdapter);
    }

    @Override
    public void onItemClick(Object object, int position) {
    }

    @Override
    public void onVisit(Client client) {
        if (client.getVisit().equals(Constants.FLAG_CLIENT_VISIT_NO_START) && amountVisitStart >= 1){
            DialogUtil.showDialogMessage(mContext,getResources().getString(R.string.message_visit_start_error));
        }else{
            clientSelect =  client;
            if (client.getVisit().equals(Constants.FLAG_CLIENT_VISIT_START)){
                mPresenter.getClientInternal(client);
            }else{
                requestPermissionGps();
            }
        }

    }

    @OnClick(R.id.btn_cancel_clients)
    public void onCancelClients(){
        startActivity(new Intent(mContext,MenuActivity.class));
      //  ((ClientsActivity)mContext).finish();

    }

    private void requestPermissionGps(){
        if (ContextCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    LOCATION_REQUEST_CODE);
        }else{
            onVisitClient(null);
        }
    }

    public void onVisitClient(Visit visit){
        Intent intent  = new Intent(mContext, VisitActivity.class);
        intent.putExtra(Constants.BUNDLE_CLIENT_VISIT,clientSelect);

        if (visit !=  null){
            intent.putExtra(Constants.BUNDLE_CLIENT_VISIT_DETAIL,visit);
        }else{
            intent.putExtra(Constants.BUNDLE_CLIENT_VISIT_DETAIL,visitInternal);
        }
        mContext.startActivity(intent);
    }

}