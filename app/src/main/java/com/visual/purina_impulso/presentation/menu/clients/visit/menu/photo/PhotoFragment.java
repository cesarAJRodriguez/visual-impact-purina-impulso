package com.visual.purina_impulso.presentation.menu.clients.visit.menu.photo;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;

import com.visual.purina_impulso.Constants;
import com.visual.purina_impulso.R;
import com.visual.purina_impulso.base.BaseFragment;
import com.visual.purina_impulso.domain.Client;
import com.visual.purina_impulso.domain.Photo;
import com.visual.purina_impulso.domain.Visit;
import com.visual.purina_impulso.presentation.menu.clients.visit.menu.MenuVisitContract;
import com.visual.purina_impulso.presentation.menu.clients.visit.menu.MenuVisitPresenter;
import com.visual.purina_impulso.util.widget.SpinnerViewItem;

import java.util.ArrayList;

import butterknife.BindView;

public class PhotoFragment extends BaseFragment implements MenuVisitContract.View ,MenuVisitContract.PhotoView{

    @BindView(R.id.spn_type_photo) SpinnerViewItem<Photo> typePhotoSpinnerView;

    private FragmentManager mFragmentManager;
    private MenuVisitContract.Presenter mPresenter;

    public static PhotoFragment newInstance(Client client) {
        Bundle arg = new Bundle();
        arg.putParcelable(Constants.BUNDLE_CLIENT_VISIT, client);
        PhotoFragment fragment = new PhotoFragment();
        fragment.setArguments(arg);
        return fragment;
    }

    @Override
    protected int getResLayout() {
        return R.layout.fragment_photo;
    }

    @Override
    public void onResume() {
        super.onResume();
        ((PhotoActivity)mContext).setTitleToolbar(getResources().getString(R.string.menu_visit_photos));

        if (mPresenter == null){
            mPresenter = new MenuVisitPresenter(this);
        }
        mPresenter.getTypePhotos();
    }

    @Override
    protected void setupView(Bundle savedInstanceState) {
    }

    @Override
    protected void onRestoreView(Bundle savedInstanceState) {

    }

    @Override
    public void setPresenter(@NonNull MenuVisitContract.Presenter presenter) {
        mPresenter = presenter;
    }


    @Override
    public Client getClient() {
        Bundle args = getArguments();
        if (args != null) {
            return args.getParcelable(Constants.BUNDLE_CLIENT_VISIT);
        }
        return null;
    }

    @Override
    public Visit getVisit() {
        return null;
    }

    @Override
    public void getTypePhotosSuccess(ArrayList<Photo> photos) {
        if (photos !=  null && photos.size()>0){
            Photo typePhoto[] =  new Photo[photos.size()];
            for (int i = 0 ; i< photos.size();i++) {
                typePhoto[i] = photos.get(i);
            }
            typePhotoSpinnerView.setEntries(typePhoto);
        }
    }

    @Override
    public void onRegisterPhotoSuccess(String message, Visit visit) {

    }

}