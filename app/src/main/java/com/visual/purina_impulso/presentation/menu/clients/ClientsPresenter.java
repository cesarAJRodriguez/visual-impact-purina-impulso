package com.visual.purina_impulso.presentation.menu.clients;

import android.support.annotation.NonNull;

import com.visual.purina_impulso.Constants;
import com.visual.purina_impulso.base.BaseInteractor;
import com.visual.purina_impulso.data.source.helper.PreferencesHelper;
import com.visual.purina_impulso.domain.Client;
import com.visual.purina_impulso.domain.Visit;
import com.visual.purina_impulso.domain.interactor.ClientInteractor;
import com.visual.purina_impulso.domain.interactor.VisitInteractor;

import java.util.ArrayList;



class ClientsPresenter implements ClientsContract.Presenter {

    private final ClientsContract.View mView;


    ClientsPresenter(@NonNull ClientsContract.View view) {
        mView = view;
        mView.setPresenter(this);
    }

    @Override
    public void start() {

    }


    @Override
    public void getClients() {
        mView.startLoading();
        String employeeId = String.valueOf(PreferencesHelper.getUser().getEmployeeId());
        String type = Constants.FLAG_CLIENT;
        String search =  null;

        ClientInteractor.getInstance().getClients(employeeId,type,search, new ClientInteractor.SuccessGetClientsListener() {
            @Override
            public void onSuccessGetClients(ArrayList<Client> clients) {
                mView.stopLoading();
                mView.getClientsSuccess(clients);
            }

        }, new BaseInteractor.ErrorCallback() {
            @Override
            public void onError(Exception exception) {
                mView.stopLoading();
                mView.showErrorMessage(exception);
            }
        });
    }

    @Override
    public void getClientInternal(Client client) {
        VisitInteractor.getInstance().getVisitClient(client.getVisitId(), new VisitInteractor.SuccessGetClientVisitListener() {
            @Override
            public void onSuccessGetClientVisit(ArrayList<Visit> visits) {
                mView.stopLoading();

                if (visits!= null && visits.size()>0){
                    mView.getClientInternalSuccess(client,visits.get(0));
                }else{
                    mView.getClientInternalSuccess(null,null);
                }

            }


        }, new BaseInteractor.ErrorCallback() {
            @Override
            public void onError(Exception exception) {
                mView.stopLoading();
                mView.showErrorMessage(exception);
            }
        });

    }

    public void getVisitClient(Client client){

        VisitInteractor.getInstance().getVisitClient(client.getVisitId(), new VisitInteractor.SuccessGetClientVisitListener() {
            @Override
            public void onSuccessGetClientVisit(ArrayList<Visit> visits) {
               mView.stopLoading();

               if (visits!= null && visits.size()>0){
                   mView.getClientInternalSuccess(client,visits.get(0));
               }else{
                   mView.getClientInternalSuccess(null,null);
               }

            }


        }, new BaseInteractor.ErrorCallback() {
            @Override
            public void onError(Exception exception) {
                mView.stopLoading();
                mView.showErrorMessage(exception);
            }
        });
    }


}


