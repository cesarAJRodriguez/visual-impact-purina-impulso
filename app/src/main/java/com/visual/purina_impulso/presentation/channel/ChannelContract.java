package com.visual.purina_impulso.presentation.channel;

import com.visual.purina_impulso.base.BasePresenter;
import com.visual.purina_impulso.base.BaseViewPresenter;

interface ChannelContract {

    interface Presenter extends BasePresenter {
        void onSync(String type);
    }

    interface View extends BaseViewPresenter<Presenter> {
        void onSyncSuccess(String message);
    }
}
