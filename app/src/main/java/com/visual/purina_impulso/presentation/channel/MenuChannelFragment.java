package com.visual.purina_impulso.presentation.channel;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;

import com.visual.purina_impulso.R;
import com.visual.purina_impulso.base.BaseFragment;

import com.visual.purina_impulso.data.source.helper.PreferencesHelper;
import com.visual.purina_impulso.presentation.menu.MenuActivity;
import com.visual.purina_impulso.util.DialogUtil;
import com.visual.purina_impulso.view.dialog.IDialogSync;

import butterknife.OnClick;


public class MenuChannelFragment extends BaseFragment implements ChannelContract.View,IDialogSync{

    private ChannelContract.Presenter mPresenter;

    public static MenuChannelFragment newInstance() {
        return new MenuChannelFragment();
    }

    @Override
    protected int getResLayout() {
        return R.layout.fragment_menu_channel;
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.start();
        ((ChannelActivity)mContext).setTitleToolbar(getResources().getText(R.string.toolbar_title_change_channel).toString());
    }

    @Override
    protected void setupView(Bundle savedInstanceState) {

    }

    @Override
    protected void onRestoreView(Bundle savedInstanceState) {

    }

    @OnClick(R.id.btn_traditional)
    public void onSyncTraditional() {
        onSync("1");
    }

    @OnClick(R.id.btn_modern)
    public void onSyncModen() {
        onSync("2");
    }

    @OnClick(R.id.btn_specialized)
    public void onSyncSpecialized() {
        onSync("3");
    }

    private void onSync(String type){
        PreferencesHelper.setSystemId(type);
        mPresenter.onSync(type);
    }


    @Override
    public void setPresenter(@NonNull ChannelContract.Presenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public void onSyncSuccess(String message) {
        DialogUtil.showDialogSync(mContext,message,this);
    }

    @Override
    public void onSyncSuccess() {
        startActivity(new Intent(mContext, MenuActivity.class));
    }

    @Override
    public void onSyncConfirmAccept() {}
}