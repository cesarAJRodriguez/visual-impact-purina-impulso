package com.visual.purina_impulso.presentation.channel;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.visual.purina_impulso.R;
import com.visual.purina_impulso.base.BaseActivity;
import com.visual.purina_impulso.util.ActivityUtils;

import butterknife.BindView;


public class ChannelActivity extends BaseActivity {

    @BindView(R.id.toolbar_main) Toolbar mToolbar;
    @BindView(R.id.toolbar_title_text_view) TextView mToolbarTitleText;

    private ChannelPresenter mPresenter;

    @Override
    protected int getResLayout() {
        return R.layout.activity_channel;
    }

    @Override
    protected void setupView(Bundle savedInstanceState) {
        MenuChannelFragment menuChannelFragment = (MenuChannelFragment) getSupportFragmentManager().findFragmentById(R.id.contentFrameChannel);

        if (menuChannelFragment == null) {
            menuChannelFragment = MenuChannelFragment.newInstance();
            ActivityUtils.addFragmentToActivity(getSupportFragmentManager(), menuChannelFragment, R.id.contentFrameChannel);
        }

        mPresenter = new ChannelPresenter(menuChannelFragment);


    }

    public void setTitleToolbar(String mTitleToolbar) {
        mToolbarTitleText.setText(mTitleToolbar);
    }
}