package com.visual.purina_impulso.presentation.menu.clients.visit;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;

import com.visual.purina_impulso.Constants;
import com.visual.purina_impulso.R;
import com.visual.purina_impulso.base.BaseFragment;
import com.visual.purina_impulso.domain.Client;
import com.visual.purina_impulso.domain.Incidence;
import com.visual.purina_impulso.domain.Visit;
import com.visual.purina_impulso.util.widget.SpinnerViewItem;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;


public class VisitIncidenceFragment extends BaseFragment implements VisitContract.View {

    @BindView(R.id.spn_type_incidence) SpinnerViewItem<Incidence> typeIncidenceSpinnerView;


    private VisitContract.Presenter mPresenter;
    private FragmentManager mFragmentManager;

    public static VisitIncidenceFragment newInstance(Client client) {
        Bundle arg = new Bundle();
        arg.putParcelable(Constants.BUNDLE_CLIENT_VISIT, client);
        VisitIncidenceFragment fragment = new VisitIncidenceFragment();
        fragment.setArguments(arg);
        return fragment;
    }

    @Override
    protected int getResLayout() {
        return R.layout.fragment_register_incidence;
    }

    @Override
    public void onResume() {
        super.onResume();
        mFragmentManager = ((VisitActivity)mContext).getSupportFragmentManager();
        Constants.STEP_POSITION =  Constants.FLAG_VISIT_INCIDENCE_FRAGMENT;
        ((VisitActivity)mContext).setTitleToolbar(getResources().getString(R.string.toolbar_register_incidence));

        if (mPresenter == null){
            mPresenter = new VisitPresenter(this);
        }

        mPresenter.getTypeIncidences();
    }

    @Override
    protected void setupView(Bundle savedInstanceState) {

    }

    @Override
    protected void onRestoreView(Bundle savedInstanceState) {

    }

    @Override
    public void setPresenter(@NonNull VisitContract.Presenter presenter) {
        mPresenter = presenter;
    }



    @OnClick(R.id.btn_cancel_incidence)
    public void onCancelIncidence(){
        mFragmentManager.popBackStack();
    }



    @Override
    public void getTypeIncidencesSuccess(ArrayList<Incidence> incidences) {
        if (incidences != null && incidences.size() > 0){
            Incidence typeIncidence[] =  new Incidence[incidences.size()];
            for (int i = 0 ; i< incidences.size();i++) {
                typeIncidence[i] = incidences.get(i);
            }
            typeIncidenceSpinnerView.setEntries(typeIncidence);
        }
    }

    @Override
    public void onRegisterStartVisitSuccess(String message, Visit visit) {
    }

    @Override
    public void onRegisterIncidenceVisitSuccess(String message) {

    }


    @Override
    public Client getClient() {
        Bundle args = getArguments();
        if (args != null) {
            return args.getParcelable(Constants.BUNDLE_CLIENT_VISIT);
        }
        return null;
    }

    @Override
    public Visit getVisit() {
        return null;
    }
}