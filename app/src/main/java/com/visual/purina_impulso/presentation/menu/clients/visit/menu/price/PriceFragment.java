package com.visual.purina_impulso.presentation.menu.clients.visit.menu.price;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.visual.purina_impulso.Constants;
import com.visual.purina_impulso.R;
import com.visual.purina_impulso.base.BaseFragment;
import com.visual.purina_impulso.domain.Client;
import com.visual.purina_impulso.domain.Price;
import com.visual.purina_impulso.domain.Visit;
import com.visual.purina_impulso.presentation.menu.clients.visit.menu.MenuVisitContract;
import com.visual.purina_impulso.presentation.menu.clients.visit.menu.MenuVisitPresenter;
import com.visual.purina_impulso.view.adapter.PriceBreakAdapter;
import com.visual.purina_impulso.view.adapter.PriceCompetitionAdapter;

import java.util.ArrayList;

import butterknife.BindView;


public class PriceFragment extends BaseFragment implements MenuVisitContract.PriceView, PriceBreakAdapter.OnItemClickListener{

    @BindView(R.id.rv_search_product) RecyclerView productRecyclerView;

    private FragmentManager mFragmentManager;
    private MenuVisitContract.Presenter mPresenter;

    public static PriceFragment newInstance(String typePrice,Client client) {
        Bundle arg = new Bundle();
        arg.putString(Constants.BUNDLE_TYPE_PRICE, typePrice);
        arg.putParcelable(Constants.BUNDLE_CLIENT_VISIT, client);
        PriceFragment fragment = new PriceFragment();
        fragment.setArguments(arg);
        return fragment;
    }

    @Override
    protected int getResLayout() {
        return R.layout.fragment_price;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getTypePrice() != null || !getTypePrice().isEmpty()){
            if (mPresenter == null){
                mPresenter = new MenuVisitPresenter(this);
            }

          /*  if (getTypePrice().equals(Constants.FLAG_PRICE_TYPE_BREAK)){
                ((PriceActivity)mContext).setTitleToolbar(getResources().getString(R.string.menu_visit_precio_type_break));
                mPresenter.getPrices(getClient().getClientId(),getTypePrice());
            }else{
                ((PriceActivity)mContext).setTitleToolbar(getResources().getString(R.string.menu_visit_precio_type_competition));
                mPresenter.getPrices(0,getTypePrice());
            }*/

        }

    }

    @Override
    protected void setupView(Bundle savedInstanceState) {
    }

    @Override
    protected void onRestoreView(Bundle savedInstanceState) {

    }

    @Override
    public void setPresenter(@NonNull MenuVisitContract.Presenter presenter) {
        mPresenter = presenter;
    }



    @Override
    public Client getClient() {
        Bundle args = getArguments();
        if (args != null) {
            return args.getParcelable(Constants.BUNDLE_CLIENT_VISIT);
        }
        return null;
    }

    @Override
    public Visit getVisit() {
        Bundle args = getArguments();
        if (args != null) {
            return args.getParcelable(Constants.BUNDLE_CLIENT_VISIT_DETAIL);
        }
        return null;
    }


    @Override
    public String getTypePrice() {
        Bundle args = getArguments();
        if (args != null) {
            return args.getString(Constants.BUNDLE_TYPE_PRICE);
        }
        return null;
    }

    private void setupRecycler(ArrayList<Price> prices) {
        if (getTypePrice().equals(Constants.FLAG_PRICE_TYPE_BREAK)){
     /*       PriceBreakAdapter adapter = new PriceBreakAdapter(this,mContext,prices);
            productRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));
            productRecyclerView.setAdapter(adapter);*/
        }else{
            PriceCompetitionAdapter adapter = new PriceCompetitionAdapter(this,mContext,prices);
            productRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));
            productRecyclerView.setAdapter(adapter);
        }

    }

    @Override
    public void getPricesSuccess(ArrayList<Price> prices) {
        if(prices != null && prices.size() > 0){
            setupRecycler(prices);
        }
    }

    @Override
    public void onRegisterPriceSuccess(String message, Visit visit) {

    }

    @Override
    public void onItemClick(Object object, int position) {

    }
}