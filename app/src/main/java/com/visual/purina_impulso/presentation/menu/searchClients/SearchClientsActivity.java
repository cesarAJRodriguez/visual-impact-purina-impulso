package com.visual.purina_impulso.presentation.menu.searchClients;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import com.visual.purina_impulso.Constants;
import com.visual.purina_impulso.R;
import com.visual.purina_impulso.base.BaseActivity;
import com.visual.purina_impulso.util.ActivityUtils;

import butterknife.BindView;


public class SearchClientsActivity extends BaseActivity {

    @BindView(R.id.toolbar_main) Toolbar mToolbar;
    @BindView(R.id.toolbar_title_text_view) TextView mToolbarTitleText;

    private SearchClientsPresenter mPresenter;

    private FragmentManager mFragmentManager;

    @Override
    protected int getResLayout() {
        return R.layout.activity_search_clients;
    }

    @Override
    protected void setupView(Bundle savedInstanceState) {
        mFragmentManager = getSupportFragmentManager();
        SearchClientsFragment searchClientsFragment = (SearchClientsFragment) getSupportFragmentManager().findFragmentById(R.id.contentFrameSearchClients);

        if (searchClientsFragment == null) {
            searchClientsFragment = SearchClientsFragment.newInstance();
            ActivityUtils.addFragmentToActivity(getSupportFragmentManager(), searchClientsFragment, R.id.contentFrameSearchClients);
        }

        mPresenter = new SearchClientsPresenter(searchClientsFragment);

        ActivityUtils.setToolBarBack(this,mToolbar,true);
    }

    public void setTitleToolbar(String mTitleToolbar) {
        mToolbarTitleText.setText(mTitleToolbar);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home) {
            if (Constants.FLAG_SEARCH_CLIENTS_FRAGMENT == Constants.STEP_POSITION) {
                finish();
            } else {
                mFragmentManager.popBackStack();
            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}