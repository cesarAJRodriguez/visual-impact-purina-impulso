package com.visual.purina_impulso.presentation.menu.assistance;

import android.support.annotation.NonNull;

import com.visual.purina_impulso.App;
import com.visual.purina_impulso.Constants;
import com.visual.purina_impulso.R;
import com.visual.purina_impulso.base.BaseInteractor;
import com.visual.purina_impulso.domain.Assistance;
import com.visual.purina_impulso.domain.interactor.AssistanceInteractor;
import com.visual.purina_impulso.util.DeviceUtil;

import java.util.ArrayList;

class AssistancePresenter implements AssistanceContract.Presenter {

    private final AssistanceContract.View mView;
    private ArrayList<Assistance> assistancesResult= new ArrayList<>();

    AssistancePresenter(@NonNull AssistanceContract.View view) {
        mView = view;
        mView.setPresenter(this);
    }

    @Override
    public void start() {

    }


    @Override
    public void onRegisterAssistance(Assistance assistance) {
        mView.startLoading();
        checkAssistance(assistance);
    }

    private void onRegisterGeneralAssistance(Assistance assistance){

        if (DeviceUtil.hasInternetConnection()) {
            AssistanceInteractor.getInstance().assistance(assistance,null,Constants.FLAG_PENDING_ASSISTANCE_NO, true, new AssistanceInteractor.SuccessAssistanceListener() {
                @Override
                public void onSuccessAssistance() {

                    saveRegisterAssistance(assistance, true);
                }
            }, new BaseInteractor.ErrorCallback() {
                @Override
                public void onError(Exception exception) {
                    saveRegisterAssistance(assistance, false);
                }
            });
        } else {
            saveRegisterAssistance(assistance, false);
        }
    }


    private void saveRegisterAssistance(Assistance assistance, boolean registerOnline) {

        String message;
        if (registerOnline) {
            message = App.context.getResources().getText(R.string.message_assistance_register_success).toString();
        } else {
            assistance.setPending(Constants.FLAG_PENDING_ASSISTANCE_YES);
            message = App.context.getResources().getText(R.string.message_assistance_register_save).toString();
        }

        AssistanceInteractor.getInstance().assistance(assistance,null,Constants.FLAG_PENDING_ASSISTANCE_NO, false, new AssistanceInteractor.SuccessAssistanceListener() {
            @Override
            public void onSuccessAssistance() {
                mView.stopLoading();
                mView.onRegisterAssistanceSuccess(message);
            }
        }, new BaseInteractor.ErrorCallback() {
            @Override
            public void onError(Exception exception) {
                mView.stopLoading();
                mView.showErrorMessage(exception);
            }
        });
    }

    private void checkAssistance(Assistance assistance) {

        if (assistance.getType().equals(Constants.FLAG_TYPE_ASSISTANCE_EXIT)) {
            AssistanceInteractor.getInstance().checkAssistance(assistance, new AssistanceInteractor.SuccessCheckAssistanceListener() {
                @Override
                public void onSuccessCheckAssistance(ArrayList<Assistance> assistances) {
                   if (assistances != null){
                        if (assistances.size() == 0){
                            mView.showErrorMessage(new Exception(App.context.getString(R.string.message_assistance_no_registered_start)));
                            mView.stopLoading();
                        }else{
                            boolean type = true;
                            for (int i = 0 ; i< assistances.size() ; i++){
                                if (assistances.get(i).getType().equals(Constants.FLAG_TYPE_ASSISTANCE_EXIT)){
                                    type = false;
                                }
                            }

                            if (type){
                                onRegisterGeneralAssistance(assistance);
                            }else{
                                mView.stopLoading();
                                mView.showErrorMessage(new Exception(App.context.getString(R.string.message_assistance_yes_registered_exit)));
                            }
                        }
                    }else{
                        mView.stopLoading();
                        mView.showErrorMessage(new Exception(App.context.getString(R.string.message_error_general)));
                    }
                }
            }, new BaseInteractor.ErrorCallback() {
                @Override
                public void onError(Exception exception) {
                    mView.stopLoading();
                    mView.showErrorMessage(new Exception(App.context.getString(R.string.message_error_general)));
                }
            });

        }else{

            AssistanceInteractor.getInstance().checkAssistance(assistance, new AssistanceInteractor.SuccessCheckAssistanceListener() {
                @Override
                public void onSuccessCheckAssistance(ArrayList<Assistance> assistances) {
                    if (assistances != null){
                        if (assistances.size() == 0){
                            onRegisterGeneralAssistance(assistance);
                        }else{
                            mView.stopLoading();
                            mView.showErrorMessage(new Exception(App.context.getString(R.string.message_assistance_yes_registered_start)));
                        }
                    }else{
                        mView.stopLoading();
                        mView.showErrorMessage(new Exception(App.context.getString(R.string.message_error_general)));
                    }
                }
            }, new BaseInteractor.ErrorCallback() {
                @Override
                public void onError(Exception exception) {
                    mView.stopLoading();
                    mView.showErrorMessage(new Exception(App.context.getString(R.string.message_error_general)));
                }
            });
        }
    }


    private void localAssistance(Assistance assistance) {
        AssistanceInteractor.getInstance().checkAssistance(assistance, new AssistanceInteractor.SuccessCheckAssistanceListener() {
            @Override
            public void onSuccessCheckAssistance(ArrayList<Assistance> assistances) {
                assistancesResult = assistances;
            }
        }, new BaseInteractor.ErrorCallback() {
            @Override
            public void onError(Exception exception) {
                ArrayList<Assistance> assistancess = new ArrayList<>();
                assistancess = null;
                assistancesResult = assistancess;
                mView.showErrorMessage(new Exception(App.context.getString(R.string.message_error_general)));
            }
        });
    }




}


