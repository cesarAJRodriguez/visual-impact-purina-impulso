package com.visual.purina_impulso.presentation.menu;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.visual.purina_impulso.App;
import com.visual.purina_impulso.Constants;
import com.visual.purina_impulso.R;
import com.visual.purina_impulso.base.BaseFragment;
import com.visual.purina_impulso.data.source.helper.PreferencesHelper;
import com.visual.purina_impulso.domain.Assistance;
import com.visual.purina_impulso.domain.Client;
import com.visual.purina_impulso.presentation.auth.AuthActivity;
import com.visual.purina_impulso.presentation.channel.ChannelActivity;
import com.visual.purina_impulso.presentation.menu.assistance.AssistanceActivity;
import com.visual.purina_impulso.presentation.menu.clients.ClientsActivity;
import com.visual.purina_impulso.presentation.menu.pending.PendingActivity;
import com.visual.purina_impulso.presentation.menu.searchClients.SearchClientsActivity;
import com.visual.purina_impulso.util.DialogUtil;
import com.visual.purina_impulso.view.dialog.IDialogLogOut;
import com.visual.purina_impulso.view.dialog.IDialogSync;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import butterknife.BindView;
import butterknife.OnClick;


public class MenuFragment extends BaseFragment implements MenuContract.View,IDialogLogOut,IDialogSync{

    @BindView(R.id.menu_profile_names) TextView namesTextView;
    @BindView(R.id.menu_profile_channel) TextView channelTextView;
    @BindView(R.id.menu_profile_type_user) TextView typeUserTextView;
    @BindView(R.id.btn_change_channel) Button changeChannelButton;
    @BindView(R.id.ll_assistance_schedule) LinearLayout assistanceSheduleLinearLayout;
    @BindView(R.id.txt_assistance_schedule_hour) TextView assistanceSheduleHourTextView;
    @BindView(R.id.txt_pending_amount) TextView pendingAmountTextView;

    private int countPendingAssistance = 0;
    private int countPendingVisits = 0;

    private FragmentManager mFragmentManager;

    private MenuContract.Presenter mPresenter;

    private ArrayList<Assistance> assistanceStart = new ArrayList<>();

    private static final int LOCATION_REQUEST_CODE = 101;

    public static MenuFragment newInstance() {
        return new MenuFragment();
    }

    @Override
    protected int getResLayout() {
        return R.layout.fragment_menu;
    }

    @Override
    public void onResume() {
        super.onResume();

        namesTextView.setText(PreferencesHelper.getUser().getNames());
        channelTextView.setText(PreferencesHelper.getUser().getUserChannel());
        typeUserTextView.setText(PreferencesHelper.getUser().getUserType());

        checkAssistance();

    }

    @Override
    protected void setupView(Bundle savedInstanceState) {

    }

    @Override
    protected void onRestoreView(Bundle savedInstanceState) {
        checkAssistance();
     //   checkPending();
    }


    // revisar la asistencia interna
    public void checkAssistance(){

        if (mPresenter == null){
            mPresenter = new MenuPresenter(this);
        }

        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat formatDate = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        Assistance assistance = new Assistance();
        assistance.setDate(formatDate.format(calendar.getTime()));
        assistance.setEmployeeId(String.valueOf(PreferencesHelper.getUser().getEmployeeId()));
        mPresenter.onCheckAssistance(assistance);

    }


    // sincronizacion
    @OnClick(R.id.btn_sync)
    public void onSync(){
        DialogUtil.showDialogSyncMenu(mContext,getResources().getText(R.string.message_confirm_sync).toString(),this);
    }

    // cerrar la app
    @OnClick(R.id.btn_close)
    public void onLogout(){
        DialogUtil.showDialogLogOut(mContext,this);
    }

    // modulo de asistencia
    @OnClick(R.id.ll_assistance)
    public void onAssistance(){
        requestPermissionGps();
    }

    // modulo de clientes
    @OnClick(R.id.btn_clients)
    public void onClients(){
        ((MenuActivity)mContext).finish();
        mContext.startActivity(new Intent(mContext, ClientsActivity.class));
    }

    // modulo de buscar clientes
    @OnClick(R.id.btn_search_clients)
    public void onSearchClients(){
        mContext.startActivity(new Intent(mContext, SearchClientsActivity.class));
    }

    // modulo de pendientes
    @OnClick(R.id.ll_pending)
    public void onPending(){
        mContext.startActivity(new Intent(mContext, PendingActivity.class));
    }


    //solicitud de permiso localicacion segun version de android
    private void requestPermissionGps(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            if (ContextCompat.checkSelfPermission(mContext,Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        LOCATION_REQUEST_CODE);
            }else{
                mContext.startActivity(new Intent(mContext, AssistanceActivity.class));
            }
        }else{
            mContext.startActivity(new Intent(mContext, AssistanceActivity.class));
        }
    }

    //confirmar cerrar la app
    @Override
    public void onLogOut() {
        ((MenuActivity)mContext).finish();
        mContext.startActivity(new Intent( App.context, AuthActivity.class));
        PreferencesHelper.setUser(null);
        PreferencesHelper.setSystemId(null);
    }

    @Override
    public void onSyncSuccess() {}

    @Override
    public void onSyncConfirmAccept() {
        mPresenter.onSync(PreferencesHelper.getSystemId());
    }

    @Override
    public void setPresenter(@NonNull MenuContract.Presenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public void onSuccessSyncMenu(String message) {
            DialogUtil.showDialogMessage(mContext,message);
    }

    @Override
    public void onSuccessCheckAssistance(String typeAssistance,ArrayList<Assistance> assistances,ArrayList<Client> clients) {
        if (typeAssistance.equals(Constants.FLAG_TYPE_ASSISTANCE_START_EMPTY)){
            assistanceSheduleLinearLayout.setVisibility(View.VISIBLE);
            assistanceSheduleLinearLayout.setBackground(getResources().getDrawable(R.drawable.back_assistance_empty));
            assistanceSheduleHourTextView.setText(getResources().getString(R.string.internal_menu_assistance_empty));
            countPendingVisits = 0;
            if (clients!= null && clients.size()>0){
                for (int i = 0 ; i< clients.size() ; i++){
                    countPendingVisits++;
                }
            }
            pendingAmountTextView.setText(String.valueOf(countPendingAssistance  + countPendingVisits));
        }else if (typeAssistance.equals(Constants.FLAG_TYPE_ASSISTANCE_START)){

            countPendingAssistance = 0;
            for (int i = 0 ; i< assistances.size() ; i++){
                if (assistances.get(i).getPending().equals(Constants.FLAG_PENDING_ASSISTANCE_YES)){
                    countPendingAssistance++;
                }
            }

            countPendingVisits = 0;
            if (clients!= null && clients.size()>0){
                for (int i = 0 ; i< clients.size() ; i++){
                    countPendingVisits++;
                }
            }
            pendingAmountTextView.setText(String.valueOf(countPendingAssistance  + countPendingVisits));


            boolean type=false;
            int index=0;

            for (int i = 0 ; i< assistances.size() ; i++){
                if (assistances.get(i).getType().equals(Constants.FLAG_TYPE_ASSISTANCE_EXIT)){
                    type = true;
                    index = i;
                }
            }

            if (!type){
                for (int i = 0 ; i< assistances.size() ; i++){
                    if (assistances.get(i).getType().equals(Constants.FLAG_TYPE_ASSISTANCE_START)){
                        index = i;
                    }
                }
                String hour = assistances.get(index).getStartHour();
                assistanceSheduleLinearLayout.setVisibility(View.VISIBLE);
                assistanceSheduleLinearLayout.setBackground(getResources().getDrawable(R.drawable.back_assistance_en));
                assistanceSheduleHourTextView.setText("En." + " "+ hour);
            }else{
                String hour = assistances.get(index).getExitHour();
                assistanceSheduleLinearLayout.setVisibility(View.VISIBLE);
                assistanceSheduleLinearLayout.setBackground(getResources().getDrawable(R.drawable.back_assistance_ex));
                assistanceSheduleHourTextView.setText("Sal." + " "+ hour);
            }
           }
        }
}



