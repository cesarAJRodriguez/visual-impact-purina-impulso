package com.visual.purina_impulso.presentation.menu.pending;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.widget.TextView;

import com.visual.purina_impulso.Constants;
import com.visual.purina_impulso.R;
import com.visual.purina_impulso.base.BaseFragment;
import com.visual.purina_impulso.data.source.helper.PreferencesHelper;
import com.visual.purina_impulso.domain.Assistance;
import com.visual.purina_impulso.domain.Client;
import com.visual.purina_impulso.presentation.menu.MenuActivity;

import com.visual.purina_impulso.util.DeviceUtil;
import com.visual.purina_impulso.util.DialogUtil;
import com.visual.purina_impulso.view.dialog.IDialogAssistance;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import butterknife.BindView;
import butterknife.OnClick;


public class PendingFragment extends BaseFragment implements PendingContract.View , IDialogAssistance{

    @BindView(R.id.txt_pending_assistance) TextView pendingAssistanceTextView;
    @BindView(R.id.txt_pending_visit) TextView pendingVisitTextView;

    private FragmentManager mFragmentManager;
    private PendingContract.Presenter mPresenter;

    private int countPendingAssistance=0;
    private int countPendingVisit=0;
    private ArrayList<Client> clientsPending = new ArrayList<>();
    private Assistance assistancePending = new Assistance();
    private ArrayList<Assistance> assistancePendings = new ArrayList<>();



    public static PendingFragment newInstance() {
        return new PendingFragment();
    }

    @Override
    protected int getResLayout() {
        return R.layout.fragment_pending;
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.start();
        Constants.STEP_POSITION =  Constants.FLAG_PENDING_FRAGMENT;
        ((PendingActivity)mContext).setTitleToolbar(getResources().getText(R.string.toolbar_title_pending).toString());
        checkPending();
    }

    @Override
    protected void setupView(Bundle savedInstanceState) {

    }

    public  void checkPending(){
        if (mPresenter == null){
            mPresenter = new PendingPresenter(this);
        }

        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat formatDate = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        Assistance assistance = new Assistance();
        assistance.setDate(formatDate.format(calendar.getTime()));
        assistance.setEmployeeId(String.valueOf(PreferencesHelper.getUser().getEmployeeId()));
        mPresenter.onCheckCountPendingAssistance(assistance);
    }

    @Override
    protected void onRestoreView(Bundle savedInstanceState) {
        checkPending();
    }

    public void replaceFragment() {
        if (mFragmentManager == null) {
            mFragmentManager = getFragmentManager();
        }
        FragmentTransaction transaction = mFragmentManager.beginTransaction();
        transaction.replace(R.id.contentFramePending, PendingClientsFragment.newInstance());
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @OnClick(R.id.btn_pending_cancel)
    public void onCancelPending(){
        ((PendingActivity)mContext).finish();
    }

    @OnClick(R.id.ll_pending_assistance)
    public void onSendPendingAssistsnce(){
        if(countPendingAssistance == 0){
            DialogUtil.showDialogMessage(mContext,getResources().getString(R.string.message_pending_assistance_empty));
        }else{
            DialogUtil.showDialogMessageConfirmSendAssistancePending(mContext,getResources().getString(R.string.message_pending_assistance_confirm),this);
        }
    }


    @OnClick(R.id.ll_pending_visit)
    public void onSendPendingVisit(){
        if(countPendingVisit == 0){
            DialogUtil.showDialogMessage(mContext,getResources().getString(R.string.message_pending_visit_empty));
        }else{
            replaceFragment();
        }
    }





    @Override
    public void setPresenter(@NonNull PendingContract.Presenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public void onSuccessCheckCountPendingAssistance(ArrayList<Assistance> assistances, ArrayList<Client> clients) {
        if (assistances != null && assistances.size() > 0 ){

            boolean typeStart = false;
            int indexStart = 0;

            boolean typeExit = false;
            int indexExit = 0;
            countPendingAssistance = 0;
            for (int i = 0 ; i< assistances.size();i++){
                if (assistances.get(i).getPending().equals(Constants.FLAG_PENDING_ASSISTANCE_YES)){
                    countPendingAssistance++;
                    if (assistances.get(i).getType().equals(Constants.FLAG_TYPE_ASSISTANCE_START)){
                                indexStart = i;
                                typeStart = true;

                    }

                    if (assistances.get(i).getType().equals(Constants.FLAG_TYPE_ASSISTANCE_EXIT)){
                                typeExit = true;
                                indexExit = i;
                    }
                }
            }

            if (countPendingAssistance > 0){
                if (typeStart && typeExit) {
                    Assistance assistanceStart = assistances.get(indexStart);
                    assistanceStart.setExitHour(assistances.get(indexExit).getExitHour());
                    assistanceStart.setExitLatitude(assistances.get(indexExit).getExitLatitude());
                    assistanceStart.setExitLongitude(assistances.get(indexExit).getExitLongitude());
                    assistanceStart.setExitComment(assistances.get(indexExit).getExitComment());
                    assistanceStart.setExitPhoto(assistances.get(indexExit).getExitPhoto());
                    assistancePending  =  assistanceStart;
                    assistancePendings.add(assistances.get(indexStart));
                    assistancePendings.add(assistances.get(indexExit));

                }else if (typeStart){
                    assistancePending = assistances.get(indexStart);
                    assistancePendings.add(assistances.get(indexStart));
                }else{
                    assistancePending = assistances.get(indexExit);
                    assistancePendings.add(assistances.get(indexExit));
                }
            }
            pendingAssistanceTextView.setText(String.valueOf(countPendingAssistance));
        }

        if (clients != null && clients.size() > 0 ){
            clientsPending = clients;
            countPendingVisit = 0;
            for (int i = 0 ; i< clients.size();i++){
                countPendingVisit++;
            }
            pendingVisitTextView.setText(String.valueOf(countPendingVisit));
        }

    }

    @Override
    public void onSuccessCheckVisitPending(ArrayList<Client> clients) {

    }

    @Override
    public void onRegisterAssistanceSuccess(String message) {
        DialogUtil.showDialogMessageRegisterAssistance(mContext,message,this);
    }

    @Override
    public void onSendPendingVisitSuccess(String message) {

    }

    @Override
    public void onRegisterSuccess() {
        ((PendingActivity)mContext).finish();
        startActivity(new Intent(mContext, MenuActivity.class));
    }

    @Override
    public void onConfirmSendAssistancePending() {
        if (DeviceUtil.hasInternetConnection()){
            mPresenter.onRegisterAssistance(assistancePending,assistancePendings);
        }else{
            DialogUtil.showDialogMessage(mContext,getResources().getString(R.string.message_not_internet));
        }

    }
}