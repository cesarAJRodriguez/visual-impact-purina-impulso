package com.visual.purina_impulso.presentation.menu.assistance;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;

import com.visual.purina_impulso.Constants;
import com.visual.purina_impulso.R;
import com.visual.purina_impulso.base.BaseFragment;

import butterknife.OnClick;

public class AssistanceFragment extends BaseFragment implements AssistanceContract.View{

    private FragmentManager mFragmentManager;
    private AssistanceContract.Presenter mPresenter;
    private int  hour, minute;

    public static AssistanceFragment newInstance() {
        return new AssistanceFragment();
    }

    @Override
    protected int getResLayout() {
        return R.layout.fragment_assistance;
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.start();
        ((AssistanceActivity)mContext).setTitleToolbar(getResources().getText(R.string.toolbar_title_assistance).toString());
        Constants.STEP_POSITION =  Constants.FLAG_ASSISTANCE_FRAGMENT;
    }

    @Override
    protected void setupView(Bundle savedInstanceState) {

    }

    @Override
    protected void onRestoreView(Bundle savedInstanceState) {

    }


    @OnClick(R.id.btn_register_start)
    public void onRegisterAssitanceStart(){
        Intent intent = new Intent(mContext,RegisterAssistanceActivity.class);
        intent.putExtra(Constants.BUNDLE_TYPE_ASSISTANCE,Constants.FLAG_TYPE_ASSISTANCE_START);
        startActivity(intent);



     //   replaceFragment(Constants.FLAG_TYPE_ASSISTANCE_START);
    }

    @OnClick(R.id.btn_register_out)
    public void onRegisterAssitanceOut(){
        Intent intent = new Intent(mContext,RegisterAssistanceActivity.class);
        intent.putExtra(Constants.BUNDLE_TYPE_ASSISTANCE,Constants.FLAG_TYPE_ASSISTANCE_EXIT);
        startActivity(intent);
       // replaceFragment(Constants.FLAG_TYPE_ASSISTANCE_EXIT);
    }

    @OnClick(R.id.btn_cancel)
    public void onCancelAssistance(){
        ((AssistanceActivity)mContext).finish();
    }


    @Override
    public void setPresenter(@NonNull AssistanceContract.Presenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public void onRegisterAssistanceSuccess(String message) {

    }


}