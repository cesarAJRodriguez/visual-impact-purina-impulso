package com.visual.purina_impulso.presentation.menu;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;

import com.visual.purina_impulso.R;
import com.visual.purina_impulso.base.BaseActivity;
import com.visual.purina_impulso.presentation.menu.assistance.AssistanceActivity;
import com.visual.purina_impulso.util.ActivityUtils;

public class MenuActivity extends BaseActivity {

    private static final int LOCATION_REQUEST_CODE = 101;

    private MenuPresenter mPresenter;
    @Override
    protected int getResLayout() {
        return R.layout.activity_menu;
    }

    @Override
    protected void setupView(Bundle savedInstanceState) {
        MenuFragment menuFragment = (MenuFragment) getSupportFragmentManager().findFragmentById(R.id.contentFrame);

        if (menuFragment == null) {
            menuFragment = MenuFragment.newInstance();
            ActivityUtils.addFragmentToActivity(getSupportFragmentManager(), menuFragment, R.id.contentFrame);
        }

        mPresenter =  new MenuPresenter(menuFragment);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case LOCATION_REQUEST_CODE: {
                if (grantResults.length == 0 || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                } else {
                    startActivity(new Intent(this, AssistanceActivity.class));
                }
                return;
            }
        }
    }

    @Override
    public void onBackPressed() {
        // bloquea regresar al login
    }
}