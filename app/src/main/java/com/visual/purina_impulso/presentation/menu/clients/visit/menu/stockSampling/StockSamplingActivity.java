package com.visual.purina_impulso.presentation.menu.clients.visit.menu.stockSampling;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.visual.purina_impulso.App;
import com.visual.purina_impulso.Constants;
import com.visual.purina_impulso.R;
import com.visual.purina_impulso.base.BaseActivity;
import com.visual.purina_impulso.domain.ActionVisit;
import com.visual.purina_impulso.domain.Client;
import com.visual.purina_impulso.domain.DetailStockSampligVisit;
import com.visual.purina_impulso.domain.Merchandising;
import com.visual.purina_impulso.domain.ProductCategory;
import com.visual.purina_impulso.domain.StockSampligVisit;
import com.visual.purina_impulso.domain.Visit;
import com.visual.purina_impulso.presentation.menu.clients.visit.menu.MenuVisitActivity;
import com.visual.purina_impulso.presentation.menu.clients.visit.menu.MenuVisitContract;
import com.visual.purina_impulso.presentation.menu.clients.visit.menu.MenuVisitPresenter;
import com.visual.purina_impulso.util.ActivityUtils;
import com.visual.purina_impulso.util.DialogUtil;
import com.visual.purina_impulso.util.FileUtil;
import com.visual.purina_impulso.util.ImageProcessAsynTask;
import com.visual.purina_impulso.util.widget.SpinnerViewItem;
import com.visual.purina_impulso.view.adapter.MerchandisingAdapter;
import com.visual.purina_impulso.view.adapter.MerchandisingValuesAdapter;
import com.visual.purina_impulso.view.dialog.IDialogCameraView;
import com.visual.purina_impulso.view.dialog.IDialogVisit;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;
import java.util.UUID;

import butterknife.BindView;
import butterknife.OnClick;
import io.realm.RealmList;


public class StockSamplingActivity extends BaseActivity implements MenuVisitContract.View,MenuVisitContract.StockSamplingView,IDialogCameraView,MerchandisingAdapter.OnItemClickListener,IDialogVisit.VisitStockDialog {

    @BindView(R.id.toolbar_main) Toolbar mToolbar;
    @BindView(R.id.toolbar_title_text_view) TextView mToolbarTitleText;

    @BindView(R.id.iv_photo) ImageView photoImageView;
    @BindView(R.id.rv_merchandising) RecyclerView merchandisingRecyclerView;
    @BindView(R.id.btn_save_register) Button saveButton;
    @BindView(R.id.btn_take_photo) Button takePhotoButton;
    @BindView(R.id.txt_message_merchandising) TextView messageMerchandisingTextView;



    private ProductCategory categorySelect;
    private int categoryId;
    public  MenuVisitContract.Presenter mPresenter;
    public Visit visitBundle;

    private String prefixFileName = "report-image-";
    private String tempName = "";
    private static String type;
    private static final int CAMERA_REQUEST_CODE = 102;
    private static final int REQUEST_IMAGE_CAMERA = 555;


    public static String pathPhotos="";
    public static Uri uriPhotos;
    public static String nameFiles="";
    public static String base64Photo="";
    @Override
    protected int getResLayout() {
        return R.layout.activity_stock_sampling;
    }

    @Override
    protected void setupView(Bundle savedInstanceState) {

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        mPresenter =  new MenuVisitPresenter(this);

        setTitleToolbar(getResources().getString(R.string.menu_visit_stock_sampling));

        if (mPresenter == null){
            mPresenter = new MenuVisitPresenter(this);
        }

        if (getVisit().getStockSampligVisit() != null && getVisit().getStockSampligVisit().size()>0){
            saveButton.setVisibility(View.GONE);
            takePhotoButton.setVisibility(View.GONE);
           ArrayList<DetailStockSampligVisit> detailStockSampligVisits = new ArrayList<>();

            if ( getVisit().getDetailStockSampligVisit() != null && getVisit().getDetailStockSampligVisit().size()>0){
                for (int i = 0 ; i<getVisit().getDetailStockSampligVisit().size() ; i++){
                    DetailStockSampligVisit detailStockSampligVisit = new DetailStockSampligVisit();
                    detailStockSampligVisit.setMerchandisingName(getVisit().getDetailStockSampligVisit().get(i).getMerchandisingName());
                    detailStockSampligVisit.setStock(getVisit().getDetailStockSampligVisit().get(i).getStock());
                     detailStockSampligVisits.add(detailStockSampligVisit);
                }
            }

            MerchandisingValuesAdapter merchandisingValuesAdapter = new MerchandisingValuesAdapter(this,this,detailStockSampligVisits);
            merchandisingRecyclerView.setLayoutManager(new LinearLayoutManager(this));
            merchandisingRecyclerView.setAdapter(merchandisingValuesAdapter);
        }else{
            mPresenter.getMerchandising();
          merchandisingRecyclerView.setVisibility(View.GONE);
          saveButton.setVisibility(View.GONE);
           takePhotoButton.setVisibility(View.GONE);
           messageMerchandisingTextView.setVisibility(View.GONE);
        }




        ActivityUtils.setToolBarBack(this,mToolbar,true);
    }

    public void setTitleToolbar(String mTitleToolbar) {
        mToolbarTitleText.setText(mTitleToolbar);
    }

    @Override
    public void setPresenter(@NonNull MenuVisitContract.Presenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public Client getClient() {
        return getIntent().getParcelableExtra(Constants.BUNDLE_CLIENT_VISIT);
    }

    @Override
    public Visit getVisit() {
        return getIntent().getParcelableExtra(Constants.BUNDLE_CLIENT_VISIT_DETAIL);
    }

    @OnClick(R.id.btn_take_photo)
    public void onTakePhoto(){
        requestPermissionCamera();
    }

    private void requestPermissionCamera(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.CAMERA},
                        CAMERA_REQUEST_CODE);
            }else{
                callCamera();
            }
        }else{
            callCamera();
        }

    }

    public void callCamera(){
        tempName = prefixFileName + System.currentTimeMillis();
        Intent takeCameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        takeCameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, FileUtil.getPhotoFileUri(this, tempName));

        if (takeCameraIntent.resolveActivity(this.getPackageManager()) != null) {
            startActivityForResult(takeCameraIntent, REQUEST_IMAGE_CAMERA);
        } else {
            DialogUtil.showDialogMessage(this, getString(R.string.alert_error_to_camera));
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            System.gc();
            if (requestCode == REQUEST_IMAGE_CAMERA) {
                startLoading();
                ImageProcessAsynTask imageProcessAsynTask = new ImageProcessAsynTask(this, tempName,this, true);
                imageProcessAsynTask.execute();
                nameFiles = tempName;
            }
        }

    }

    @Override
    public void  onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case CAMERA_REQUEST_CODE: {
                if (grantResults.length == 0 || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                } else {

                    callCamera();
                }
                return;
            }
        }
    }




    @Override
    public void callCamera(Context context) {

    }

    @Override
    public void callGallery() {

    }

    @Override
    public void removeItem(int position) {

    }

    @Override
    public void showMessageError() {
        stopLoading();
        DialogUtil.showDialogMessage(this, this.getResources().getString(R.string.message_error_image_memory));

    }

    @Override
    public void SuccessUpload(String size, String path) {
        stopLoading();
        if (path!= null || path != ""){
            File file = new File(path);
            Picasso.with(this).load(file).placeholder(android.R.drawable.progress_horizontal).into(photoImageView);
            pathPhotos = path;
            Bitmap myBitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            myBitmap.compress(Bitmap.CompressFormat.JPEG, 70, stream);
            byte[] byteFormat = stream.toByteArray();
            String imgString = Base64.encodeToString(byteFormat, Base64.NO_WRAP);
            base64Photo = "data:image/jpeg;base64," + imgString;
        }
    }

    @OnClick(R.id.btn_save_register)
    public void onRegisterPhotoVisit(){
        if (formValidate()){
            String photo =  "";
            if (pathPhotos == ""){
                photo="";
            }else{
                photo=base64Photo;
            }

            Calendar calendar = Calendar.getInstance();
            SimpleDateFormat formatDate = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
            String date =  formatDate.format(calendar.getTime());

            SimpleDateFormat formatHour = new SimpleDateFormat("HH:mm:ss");
            String hour =   formatHour.format(calendar.getTime());

            Visit visit = getVisit();

            RealmList<StockSampligVisit> stockSampligVisits = new RealmList<>();
            RealmList<DetailStockSampligVisit> detailStockSampligVisits = new RealmList<>();

            String stockSamplingId = UUID.randomUUID().toString();

            StockSampligVisit stockSampligVisit = new StockSampligVisit(
                    stockSamplingId,
                    Integer.parseInt(getClient().getEmployeeId()),
                    date,
                    getClient().getClientId(),
                    photo,
                    hour,
                    visit.getId(),
                    Constants.FLAG_DATA_PENDING_YES
            );
            stockSampligVisits.add(stockSampligVisit);

            visit.setStockSampligVisit(stockSampligVisits);

            for (int i=0 ; i<Constants.MERCHANDISING_LIST.size();i++){
                String detailStockSamplingId = UUID.randomUUID().toString();
                DetailStockSampligVisit detailStockSampligVisit = new DetailStockSampligVisit(
                        detailStockSamplingId,
                        stockSamplingId,
                        Integer.parseInt(getClient().getEmployeeId()),
                        date,
                        getClient().getClientId(),
                        Constants.MERCHANDISING_LIST.get(i).getMerchandisingId(),
                        Constants.MERCHANDISING_LIST.get(i).getName(),
                        Constants.MERCHANDISING_LIST.get(i).getStock(),
                        Constants.MERCHANDISING_LIST.get(i).getStock(),
                        visit.getId(),
                        Constants.FLAG_DATA_PENDING_YES
                );
                detailStockSampligVisits.add(detailStockSampligVisit);
            }

            visit.setDetailStockSampligVisit(detailStockSampligVisits);
            mPresenter.onRegisterVisit(visit,Constants.FLAG_VISIT_STOCK,getClient());
        }
    }




    @OnClick(R.id.btn_cancel)
    public void onCancelRegister(){
        Intent intent = new Intent(this, MenuVisitActivity.class);
        intent.putExtra(Constants.BUNDLE_CLIENT_VISIT,getClient());
        intent.putExtra(Constants.BUNDLE_CLIENT_VISIT_DETAIL,getVisit());
        startActivity(intent);
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home) {

            Intent intent = new Intent(this, MenuVisitActivity.class);
            intent.putExtra(Constants.BUNDLE_CLIENT_VISIT,getClient());
            intent.putExtra(Constants.BUNDLE_CLIENT_VISIT_DETAIL,getVisit());
            startActivity(intent);
            finish();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(this, MenuVisitActivity.class);
        intent.putExtra(Constants.BUNDLE_CLIENT_VISIT,getClient());
        intent.putExtra(Constants.BUNDLE_CLIENT_VISIT_DETAIL,getVisit());
        startActivity(intent);
        finish();
    }




    @Override
    public void getMerchandisingSuccess(ArrayList<Merchandising> merchandisings) {
        if (merchandisings !=null && merchandisings.size()>0){
            setupRecycler(merchandisings);
            merchandisingRecyclerView.setVisibility(View.VISIBLE);
            saveButton.setVisibility(View.VISIBLE);
            takePhotoButton.setVisibility(View.VISIBLE);
            messageMerchandisingTextView.setVisibility(View.GONE);
        }else{
            merchandisingRecyclerView.setVisibility(View.GONE);
            saveButton.setVisibility(View.GONE);
            takePhotoButton.setVisibility(View.GONE);
            messageMerchandisingTextView.setVisibility(View.VISIBLE);
        }
    }

    private void setupRecycler(ArrayList<Merchandising> merchandisings) {
        Constants.MERCHANDISING_LIST.clear();
        MerchandisingAdapter merchandisingAdapter = new MerchandisingAdapter(this,this,merchandisings);
        merchandisingRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        merchandisingRecyclerView.setAdapter(merchandisingAdapter);
    }

    @Override
    public void onRegisterMerchandisingSuccess(String message, Visit visit) {
        visitBundle = visit;
        DialogUtil.showDialogMessageRegisterStockVisit(this,message,this);

    }

    @Override
    public void onItemClick(Object object, int position) {

    }

    private boolean formValidate(){
        if (Constants.MERCHANDISING_LIST == null || Constants.MERCHANDISING_LIST.size() == 0){
            DialogUtil.showDialogMessage(this,getResources().getString(R.string.message_merchandising_empty));
            return false;
        }
        return true;
    }

    @Override
    public void onConfirmStockVisit() {
        Intent intent = new Intent(this, MenuVisitActivity.class);
        intent.putExtra(Constants.BUNDLE_CLIENT_VISIT,getClient());
        intent.putExtra(Constants.BUNDLE_CLIENT_VISIT_DETAIL,visitBundle);
        startActivity(intent);
        finish();
    }
}