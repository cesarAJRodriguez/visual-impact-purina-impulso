package com.visual.purina_impulso.presentation.menu.clients.visit;

import android.support.annotation.NonNull;

import com.visual.purina_impulso.App;
import com.visual.purina_impulso.Constants;
import com.visual.purina_impulso.R;
import com.visual.purina_impulso.base.BaseInteractor;
import com.visual.purina_impulso.domain.Client;
import com.visual.purina_impulso.domain.Incidence;
import com.visual.purina_impulso.domain.IncidenceVisit;
import com.visual.purina_impulso.domain.StartVisit;
import com.visual.purina_impulso.domain.Visit;
import com.visual.purina_impulso.domain.interactor.ClientInteractor;
import com.visual.purina_impulso.domain.interactor.VisitInteractor;
import com.visual.purina_impulso.util.DeviceUtil;

import java.util.ArrayList;

class VisitPresenter implements VisitContract.Presenter {

    private final VisitContract.View mView;


    VisitPresenter(@NonNull VisitContract.View view) {
        mView = view;
        mView.setPresenter(this);
    }

    @Override
    public void start() {

    }

    @Override
    public void getTypeIncidences() {
        mView.startLoading();
        int channelId = Integer.parseInt("2");
        VisitInteractor.getInstance().getTypeIncidences(channelId, new VisitInteractor.SuccessGetTypeIncidencesListener() {
            @Override
            public void onSuccessGetTypeIncidences(ArrayList<Incidence> incidences) {
                mView.stopLoading();
                mView.getTypeIncidencesSuccess(incidences);
            }

        }, new BaseInteractor.ErrorCallback() {
            @Override
            public void onError(Exception exception) {
                mView.stopLoading();
                mView.showErrorMessage(exception);
            }
        });
    }

    @Override
    public void registerStartVisit(StartVisit startVisit, Visit visit, Client client) {
        mView.startLoading();
        if (DeviceUtil.hasInternetConnection()){
            VisitInteractor.getInstance().registerStartVisit(startVisit,visit,true, new VisitInteractor.SuccessRegisterStartVisitListener() {
                @Override
                public void onSuccessRegisterStartVisit(Visit visit1) {
                    mView.stopLoading();
                    onSaveRegisterClient(startVisit,true,visit,client);
                    //onSaveRegisterStartVisit(startVisit,true,visit);
                }

            }, new BaseInteractor.ErrorCallback() {
                @Override
                public void onError(Exception exception) {
                    mView.stopLoading();
                    client.setPending(Constants.FLAG_DATA_PENDING_YES);
                    visit.getStartVisit().get(0).setPending(Constants.FLAG_DATA_PENDING_YES);
                    onSaveRegisterClient(startVisit,false,visit,client);
               //onSaveRegisterStartVisit(startVisit,false,visit);
                }
            });
        }else{
            client.setPending(Constants.FLAG_DATA_PENDING_YES);
            visit.getActionVisit().get(0).setPending(Constants.FLAG_DATA_PENDING_YES);
            onSaveRegisterClient(startVisit,false,visit,client);
          //onSaveRegisterStartVisit(startVisit,false,visit);
        }
    }

    @Override
    public void registerIncidence(StartVisit visit, IncidenceVisit incidenceVisit) {
       if (DeviceUtil.hasInternetConnection()){
           mView.startLoading();
           VisitInteractor.getInstance().registerIncidenceVist(visit,incidenceVisit, new VisitInteractor.SuccessRegisterIncidenceVisitListener() {
               @Override
               public void onSuccessRegisterIncidenceVisit() {
                   mView.stopLoading();
                   mView.onRegisterIncidenceVisitSuccess(App.context.getResources().getString(R.string.message_send_register_incidence));
               }
           }, new BaseInteractor.ErrorCallback() {
               @Override
               public void onError(Exception exception) {
                   mView.stopLoading();
                   mView.onRegisterIncidenceVisitSuccess(App.context.getResources().getString(R.string.message_save_data_internal));
               }
           });
       }else{
           mView.stopLoading();
           mView.onRegisterIncidenceVisitSuccess(App.context.getResources().getString(R.string.message_save_data_internal));
       }

    }


    private void onSaveRegisterStartVisit(StartVisit startVisit, boolean isOnline,Visit visit){
        String message;
        if (isOnline){
            message = App.context.getResources().getString(R.string.message_register_start_success_remote);
        }else{
            message = App.context.getResources().getString(R.string.message_register_start_success_local);
        }

        VisitInteractor.getInstance().registerStartVisit(startVisit,visit,false, new VisitInteractor.SuccessRegisterStartVisitListener() {
            @Override
            public void onSuccessRegisterStartVisit(Visit visit1) {
                mView.stopLoading();
                mView.onRegisterStartVisitSuccess(message,visit);
            }

        }, new BaseInteractor.ErrorCallback() {
            @Override
            public void onError(Exception exception) {
                mView.stopLoading();
                mView.showErrorMessage(exception);
            }
        });
    }

    private void onSaveRegisterClient(StartVisit startVisit, boolean isOnline,Visit visit,Client client){

        ClientInteractor.getInstance().registerClient(client, new ClientInteractor.SuccessRegisterClientListener() {
            @Override
            public void onSuccessRegisterClient() {
                onSaveRegisterStartVisit(startVisit,isOnline,visit);
            }
        }, new BaseInteractor.ErrorCallback() {
            @Override
            public void onError(Exception exception) {
                mView.stopLoading();
                mView.showErrorMessage(exception);
            }
        });
    }
}


