package com.visual.purina_impulso.presentation.menu.clients.visit.menu.quiz;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.visual.purina_impulso.Constants;
import com.visual.purina_impulso.R;
import com.visual.purina_impulso.base.BaseActivity;
import com.visual.purina_impulso.domain.Alternative;
import com.visual.purina_impulso.domain.Client;
import com.visual.purina_impulso.domain.DetailQuizVisit;
import com.visual.purina_impulso.domain.Question;
import com.visual.purina_impulso.domain.Quiz;
import com.visual.purina_impulso.domain.QuizVisit;
import com.visual.purina_impulso.domain.Visit;
import com.visual.purina_impulso.presentation.menu.clients.visit.menu.MenuVisitActivity;
import com.visual.purina_impulso.presentation.menu.clients.visit.menu.MenuVisitContract;
import com.visual.purina_impulso.presentation.menu.clients.visit.menu.MenuVisitPresenter;
import com.visual.purina_impulso.util.ActivityUtils;
import com.visual.purina_impulso.util.DialogUtil;
import com.visual.purina_impulso.util.FileUtil;
import com.visual.purina_impulso.util.ImageProcessAsynTask;
import com.visual.purina_impulso.view.adapter.QuestionAdapter;
import com.visual.purina_impulso.view.dialog.IDialogCameraView;
import com.visual.purina_impulso.view.dialog.IDialogQuizQuestion;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;
import java.util.UUID;

import butterknife.BindView;
import butterknife.OnClick;
import io.realm.RealmList;


public class QuizQuestionActivity extends BaseActivity implements MenuVisitContract.QuizQuestionView,QuestionAdapter.OnItemClickListener,IDialogCameraView,IDialogQuizQuestion {

    @BindView(R.id.toolbar_main) Toolbar mToolbar;
    @BindView(R.id.toolbar_title_text_view) TextView mToolbarTitleText;
    @BindView(R.id.txt_title_quiz) TextView titleQuizTextView;
    @BindView(R.id.rv_quiz_question) RecyclerView questionRecyclerView;
    @BindView(R.id.iv_photo) ImageView photoImageView;
    @BindView(R.id.btn_take_photo_quiz) Button takePhotoButton;
    @BindView(R.id.txt_message_quiz_question) TextView messageQuizQuestionTextView;
    @BindView(R.id.btn_save_register) Button saveButton;
    private MenuVisitContract.Presenter mPresenter;

    public Visit visitBundle;

    private String prefixFileName = "report-image-quiz-";
    private String tempName = "";
    private static final int CAMERA_REQUEST_CODE = 102;
    private static final int REQUEST_IMAGE_CAMERA = 555;


    public static String pathPhotos="";
    public static Uri uriPhotos;
    public static String nameFiles="";
    public static String base64Photo="";

    @Override
    protected int getResLayout() {
        return R.layout.activity_visit_quiz_question;
    }

    @Override
    protected void setupView(Bundle savedInstanceState) {

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        setTitleToolbar(getResources().getString(R.string.menu_visit_quiz));
        if (getQuiz() != null){
            titleQuizTextView.setText(getQuiz().getQuizName());
        }else{
            titleQuizTextView.setText("");
        }

        if (mPresenter == null){
            mPresenter = new MenuVisitPresenter(this);
        }

        mPresenter.getQuizQuestions(getQuiz().getQuizId());
      /*  questionRecyclerView.setVisibility(View.GONE);
        messageQuizQuestionTextView.setVisibility(View.GONE);
        saveButton.setVisibility(View.GONE);
        takePhotoButton.setVisibility(View.GONE);*/
        ActivityUtils.setToolBarBack(this,mToolbar,true);
    }

    public void setTitleToolbar(String mTitleToolbar) {
        mToolbarTitleText.setText(mTitleToolbar);
    }

    @Override
    public void setPresenter(@NonNull MenuVisitContract.Presenter presenter) {
        mPresenter = presenter;
    }

    private void setupRecycler(ArrayList<Question> questions,ArrayList<Alternative> alternatives) {

        Constants.ALTERNATIVES_QUESTION_CHECKD.clear();

        for (int i=0;i<questions.size();i++){
            DetailQuizVisit detailQuizVisit =  new DetailQuizVisit();
            detailQuizVisit.setQuestionId(questions.get(i).getQuestionId());
            detailQuizVisit.setAlternativeId(-1);
            Constants.ALTERNATIVES_QUESTION_CHECKD.add(detailQuizVisit);
        }

        QuestionAdapter quizAdapter = new QuestionAdapter(this,this,questions,alternatives);
        questionRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        questionRecyclerView.setAdapter(quizAdapter);
       
    }

    @Override
    public Client getClient() {
        return getIntent().getParcelableExtra(Constants.BUNDLE_CLIENT_VISIT);
    }

    @Override
    public Visit getVisit() {
        return getIntent().getParcelableExtra(Constants.BUNDLE_CLIENT_VISIT_DETAIL);
    }

    public void registerQuiz(){

    }

    @Override
    public Quiz getQuiz() {
        return getIntent().getParcelableExtra(Constants.BUNDLE_CLIENT_VISIT_QUIZ);
    }

    @Override
    public void getQuizQuestionsAlternativesSuccess(ArrayList<Question> questions, ArrayList<Alternative> alternatives) {
        if (questions != null && questions.size()>0 ){
            setupRecycler(questions,alternatives);
         /*   questionRecyclerView.setVisibility(View.VISIBLE);
            messageQuizQuestionTextView.setVisibility(View.GONE);
            saveButton.setVisibility(View.VISIBLE);
            takePhotoButton.setVisibility(View.VISIBLE);
        }else{
            questionRecyclerView.setVisibility(View.GONE);
            messageQuizQuestionTextView.setVisibility(View.VISIBLE);
            saveButton.setVisibility(View.GONE);
            takePhotoButton.setVisibility(View.GONE);*/
        }


    }

    @Override
    public void onRegisterQuizSuccess(String message, Visit visit) {
        visitBundle = visit;
        DialogUtil.showDialogMessageRegisterQuizVisit(this,message,this);

    }

    @Override
    public void onItemClick(Object object, int position) {

    }

    private void requestPermissionCamera(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.CAMERA},
                        CAMERA_REQUEST_CODE);
            }else{
                callCamera();
            }
        }else{
            callCamera();
        }

    }

    public void callCamera(){
        tempName = prefixFileName + System.currentTimeMillis();
        Intent takeCameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        takeCameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, FileUtil.getPhotoFileUri(this, tempName));

        if (takeCameraIntent.resolveActivity(this.getPackageManager()) != null) {
            startActivityForResult(takeCameraIntent, REQUEST_IMAGE_CAMERA);
        } else {
            DialogUtil.showDialogMessage(this, getString(R.string.alert_error_to_camera));
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            System.gc();
            if (requestCode == REQUEST_IMAGE_CAMERA) {
                startLoading();
                ImageProcessAsynTask imageProcessAsynTask = new ImageProcessAsynTask(this, tempName,this, true);
                imageProcessAsynTask.execute();
                nameFiles = tempName;
            }
        }
    }

    @Override
    public void  onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case CAMERA_REQUEST_CODE: {
                if (grantResults.length == 0 || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                } else {

                    callCamera();
                }
                return;
            }
        }
    }

    @Override
    public void callCamera(Context context) {

    }

    @Override
    public void callGallery() {

    }

    @Override
    public void removeItem(int position) {

    }

    @OnClick(R.id.btn_take_photo_quiz)
    public void takePhoto() {
        requestPermissionCamera();
    }

    @OnClick(R.id.btn_save_register)
    public void saveRegister() {
      if (formValidate()){
          Visit visit = getVisit();

          String quizId = UUID.randomUUID().toString();
          RealmList<QuizVisit> quizVisits = new RealmList<>();
          RealmList<DetailQuizVisit> detailQuizVisits = new RealmList<>();

          if (visit.getQuizVisit() != null ){
              if (visit.getQuizVisit().size() > 0){
                  quizVisits.addAll(visit.getQuizVisit());
              }
          }

          Calendar calendar = Calendar.getInstance();
          SimpleDateFormat formatDate = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
          String date =  formatDate.format(calendar.getTime());

          SimpleDateFormat formatHour = new SimpleDateFormat("HH:mm:ss");
          String hour =   formatHour.format(calendar.getTime());


          QuizVisit quizVisit = new QuizVisit(quizId,
                  getQuiz().getQuizId(),
                  getVisit().getId(),
                  Integer.parseInt(getClient().getEmployeeId()),
                  date,
                  getClient().getClientId(),
                  base64Photo,
                  hour,
                  Constants.FLAG_DATA_PENDING_YES);

          quizVisits.add(quizVisit);

          if (visit.getDetailQuizVisit() != null ){
              if (visit.getDetailQuizVisit().size() > 0){
                  detailQuizVisits.addAll(visit.getDetailQuizVisit());
              }
          }

          for (int i = 0 ; i< Constants.ALTERNATIVES_QUESTION_CHECKD.size();i++){
              String detailQuizId = UUID.randomUUID().toString();
              DetailQuizVisit detailQuizVisit = new DetailQuizVisit(
                      detailQuizId,
                      quizId,
                      Constants.ALTERNATIVES_QUESTION_CHECKD.get(i).getQuestionId(),
                      getVisit().getId(),
                      Integer.parseInt(getClient().getEmployeeId()),
                      date,
                      getClient().getClientId(),
                      getQuiz().getQuizId(),
                      Constants.ALTERNATIVES_QUESTION_CHECKD.get(i).getAlternativeId(),
                      Constants.ALTERNATIVES_QUESTION_CHECKD.get(i).getAnswer(),
                      Constants.FLAG_DATA_PENDING_YES);
              detailQuizVisits.add(detailQuizVisit);
          }

          visit.setQuizVisit(quizVisits);
          visit.setDetailQuizVisit(detailQuizVisits);

          mPresenter.onRegisterVisit(visit,Constants.FLAG_VISIT_QUIZ,getClient());
      }
    }


    private boolean formValidate(){

      int totalQuestion =   Constants.ALTERNATIVES_QUESTION_CHECKD.size() ;
      int questionAlterAns =   0 ;

        for (int i = 0 ; i<Constants.ALTERNATIVES_QUESTION_CHECKD.size();i++){
            if(Constants.ALTERNATIVES_QUESTION_CHECKD.get(i).getAlternativeId() != -1){
                questionAlterAns++;
            }

            if(!Constants.ALTERNATIVES_QUESTION_CHECKD.get(i).getAnswer().equals("")){
                questionAlterAns++;
            }
        }

        if (totalQuestion != questionAlterAns){
            DialogUtil.showDialogMessage(this,getResources().getString(R.string.message_quiz_empty));
            return false;
        }

        if (base64Photo.equals("")){
            DialogUtil.showDialogMessage(this,getResources().getString(R.string.message_photo_empty));
            return false;
        }

        return true;
    }

    @Override
    public void showMessageError() {
        stopLoading();
        DialogUtil.showDialogMessage(this, this.getResources().getString(R.string.message_error_image_memory));

    }

    @Override
    public void SuccessUpload(String size, String path) {
        stopLoading();
        if (path != null || path != ""){
            File file = new File(path);
            Picasso.with(this).load(file).placeholder(android.R.drawable.progress_horizontal).into(photoImageView);
            pathPhotos = path;
            Bitmap myBitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            myBitmap.compress(Bitmap.CompressFormat.JPEG, 70, stream);
            byte[] byteFormat = stream.toByteArray();
            String imgString = Base64.encodeToString(byteFormat, Base64.NO_WRAP);
            base64Photo = "data:image/jpeg;base64," + imgString;
        }
    }

    @Override
    public void onConfirmRegisterQuiz() {
        Intent intent = new Intent(this, MenuVisitActivity.class);
        intent.putExtra(Constants.BUNDLE_CLIENT_VISIT,getClient());
        intent.putExtra(Constants.BUNDLE_CLIENT_VISIT_DETAIL,visitBundle);
        startActivity(intent);
        finish();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home) {
            finish();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @OnClick(R.id.btn_close)
    public void onClose() {
        finish();
    }


}