package com.visual.purina_impulso.presentation.menu.pending;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.visual.purina_impulso.Constants;
import com.visual.purina_impulso.R;
import com.visual.purina_impulso.base.BaseFragment;
import com.visual.purina_impulso.domain.Assistance;
import com.visual.purina_impulso.domain.Client;
import com.visual.purina_impulso.domain.Visit;
import com.visual.purina_impulso.presentation.menu.MenuActivity;
import com.visual.purina_impulso.presentation.menu.clients.visit.VisitActivity;
import com.visual.purina_impulso.util.DialogUtil;
import com.visual.purina_impulso.view.adapter.ClientAdapter;
import com.visual.purina_impulso.view.adapter.ClientPendingAdapter;
import com.visual.purina_impulso.view.dialog.IDialogClient;
import com.visual.purina_impulso.view.dialog.IDialogVisit;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;


public class PendingClientsFragment extends BaseFragment implements PendingContract.View,ClientAdapter.OnItemClickListener,IDialogClient,IDialogVisit.VisitSendPendingDialog,IDialogVisit.VisitSendPendingSuccessDialog {

    @BindView(R.id.btn_channel) Button channelButton;
    @BindView(R.id.ll_clients) LinearLayout clientsLinearLayout;
    @BindView(R.id.rv_clients) RecyclerView clientsRecyclerView;
    @BindView(R.id.txt_message_clients) TextView  messageClientsTextView;

    private PendingContract.Presenter mPresenter;

    private Client clientSelect;
    private Visit visitInternal;
    private static final int LOCATION_REQUEST_CODE = 101;
    private int amountVisitStart = 0;

    public static PendingClientsFragment newInstance() {
        return new PendingClientsFragment();
    }

    @Override
    protected int getResLayout() {
        return R.layout.fragment_clients;
    }

    @Override
    public void onResume() {
        super.onResume();
        Constants.STEP_POSITION =  Constants.FLAG_PENDING__VISIT_FRAGMENT;
        ((PendingActivity)mContext).setTitleToolbar(getResources().getText(R.string.toolbar_title_clients).toString());
        if (mPresenter == null){
            mPresenter =  new PendingPresenter(this);
        }
        mPresenter.onCheckVisitPending();

    }

    @Override
    protected void setupView(Bundle savedInstanceState) {

    }

    @Override
    protected void onRestoreView(Bundle savedInstanceState) {

    }




   /* @Override
    public void getClientInternalSuccess(Client client, Visit visit) {
           onVisitClient(visit);
    }*/

    @OnClick(R.id.btn_channel)
    public void onClickChannel(){
        if (clientsRecyclerView.getVisibility() == View.VISIBLE) {
            channelButton.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_add_circle_outline_white, 0, 0, 0);
            clientsRecyclerView.setVisibility(View.GONE);
            clientsLinearLayout.setVisibility(View.GONE);
        } else {
            channelButton.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_remove_circle_outline_white, 0, 0, 0);
            clientsRecyclerView.setVisibility(View.VISIBLE);
            clientsLinearLayout.setVisibility(View.VISIBLE);
        }
    }

    private void setupRecycler(ArrayList<Client> clients) {
        ClientPendingAdapter clientAdapter = new ClientPendingAdapter(this,this,mContext,clients);
        clientsRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        clientsRecyclerView.setAdapter(clientAdapter);
    }

    @Override
    public void onItemClick(Object object, int position) {
    }


    @OnClick(R.id.btn_cancel_clients)
    public void onCancelClients(){
        ((PendingActivity)mContext).backStack();
    }


    public void onVisitClient(Visit visit){
        Intent intent  = new Intent(mContext, VisitActivity.class);
        intent.putExtra(Constants.BUNDLE_CLIENT_VISIT,clientSelect);

        if (visit !=  null){
            intent.putExtra(Constants.BUNDLE_CLIENT_VISIT_DETAIL,visit);
        }else{
            intent.putExtra(Constants.BUNDLE_CLIENT_VISIT_DETAIL,visitInternal);
        }
        mContext.startActivity(intent);
    }

    @Override
    public void setPresenter(@NonNull PendingContract.Presenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public void onSuccessCheckCountPendingAssistance(ArrayList<Assistance> assistances, ArrayList<Client> clients) {

    }

    @Override
    public void onSuccessCheckVisitPending(ArrayList<Client> clients) {
        if (clients!= null){
            channelButton.setText(getResources().getText(R.string.internal_change_channel_modern).toString() + " (" +clients.size()+")" );
            channelButton.setVisibility(View.VISIBLE);
            clientsLinearLayout.setVisibility(View.GONE);
            clientsRecyclerView.setVisibility(View.GONE);
            messageClientsTextView.setVisibility(View.GONE);
                setupRecycler(clients);
        }
    }

    @Override
    public void onRegisterAssistanceSuccess(String message) {

    }

    @Override
    public void onSendPendingVisitSuccess(String message) {
        DialogUtil.showDialogMessageSendPendingSuccess(mContext,message,this);
    }

    @Override
    public void onVisit(Client client) {
        clientSelect = client;
        DialogUtil.showDialogMessageConfirmSendPending(mContext,getResources().getString(R.string.message_confirm_send_pending),this);
    }

    @Override
    public void onConfirmSendPendingVisit() {
        mPresenter.onSendPendingVisit(clientSelect);
    }

    @Override
    public void onConfirmSendPendingSuccessVisit() {
        startActivity(new Intent(mContext, MenuActivity.class));
    }
}