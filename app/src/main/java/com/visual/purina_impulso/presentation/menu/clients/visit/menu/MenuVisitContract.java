package com.visual.purina_impulso.presentation.menu.clients.visit.menu;

import com.visual.purina_impulso.base.BasePresenter;
import com.visual.purina_impulso.base.BaseViewPresenter;
import com.visual.purina_impulso.domain.Alternative;
import com.visual.purina_impulso.domain.BaseProduct;
import com.visual.purina_impulso.domain.BrandProduct;
import com.visual.purina_impulso.domain.Client;
import com.visual.purina_impulso.domain.ListAdvanceDay;
import com.visual.purina_impulso.domain.ListBusinessDay;
import com.visual.purina_impulso.domain.ListFee;
import com.visual.purina_impulso.domain.ListFeeImpulso;
import com.visual.purina_impulso.domain.ListSale;
import com.visual.purina_impulso.domain.ListVisit;
import com.visual.purina_impulso.domain.Merchandising;
import com.visual.purina_impulso.domain.Photo;
import com.visual.purina_impulso.domain.Price;
import com.visual.purina_impulso.domain.Product;
import com.visual.purina_impulso.domain.ProductCategory;
import com.visual.purina_impulso.domain.Question;
import com.visual.purina_impulso.domain.Quiz;
import com.visual.purina_impulso.domain.SubCategoryProduct;
import com.visual.purina_impulso.domain.Visit;

import java.util.ArrayList;



public interface MenuVisitContract {

    interface Presenter extends BasePresenter {
        void getQuizzes();
        void getQuizQuestions(int quizId);
        void getPrices(int clientId,String typePrice,String search);
        void getTypePhotos();
        void getCategoryProducts();
        void getMerchandising();

        void onRegisterVisit(Visit visit,String moduleVisit,Client client);

        void getSubCategoryProduct();
        void getBrandProduct(int subCategoryProduct);
        void getBaseProduct(int subCategoryProduct,int brandProduct);
        void getProducts(String baseProduct);

        void getValuesDaySale(Client client);
    }

    interface QuizView extends View {
        void getQuizzesSuccess(ArrayList<Quiz> quizzes);
    }

    interface QuizQuestionView extends View {
       Quiz getQuiz();
        void getQuizQuestionsAlternativesSuccess(ArrayList<Question> questions, ArrayList<Alternative> alternatives );
        void onRegisterQuizSuccess(String message,Visit visit);
    }

    interface CommentShooperView extends View {
        void onRegisterCommentShooperSuccess(String message,Visit visit);
    }

    interface PriceView extends View {
        String getTypePrice();
        void getPricesSuccess(ArrayList<Price> prices);
        void onRegisterPriceSuccess(String message,Visit visit);
    }

    interface PhotoView extends View {
        void getTypePhotosSuccess(ArrayList<Photo> photos);
        void onRegisterPhotoSuccess(String message,Visit visit);
    }

    interface ActionView extends View {
        void getCategoryProductsSuccess(ArrayList<ProductCategory> productCategories);
        void onRegisterActionSuccess(String message, Visit visit);
    }

    interface StockSamplingView extends View {
        void getMerchandisingSuccess(ArrayList<Merchandising> merchandisings);
        void onRegisterMerchandisingSuccess(String message, Visit visit);
    }

    interface DeliveryProductView extends View {
        void onRegisterDeliveryProductSuccess(String message, Visit visit);

        void getSubCategoryProductSuccess(ArrayList<SubCategoryProduct> subCategoryProducts);
        void getBrandProductSuccess(ArrayList<BrandProduct> brandProducts);
        void getBaseProductSuccess(ArrayList<BaseProduct> baseProducts);
        void getProductsSuccess(ArrayList<Product> products);

    }

    interface SaleDayView extends View {
        void getValuesDaySaleSuccess(ArrayList<ListAdvanceDay> listAdvanceDays, ArrayList<ListBusinessDay> listBusinessDays,
                                     ArrayList<ListFee> listFees, ArrayList<ListSale> listSales, ArrayList<ListVisit> listVisits,
                                     ArrayList<ListFeeImpulso> listFeeImpulsos);
    }

    interface StockSamplingFinalView extends View {
        void onRegisterMerchandisingStockFinalSuccess(String message, Visit visit);
    }


    interface View extends BaseViewPresenter<Presenter> {
        Client getClient();
        Visit getVisit();
    }

    interface FinishView extends View{
        void onRegisterSuccess(String message);
    }


}
