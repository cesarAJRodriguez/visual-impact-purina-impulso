package com.visual.purina_impulso.presentation.auth;

import android.os.Bundle;

import com.visual.purina_impulso.R;
import com.visual.purina_impulso.base.BaseActivity;
import com.visual.purina_impulso.util.ActivityUtils;

public class AuthActivity extends BaseActivity {

    private LoginPresenter mLoginPresenter;

    @Override
    protected int getResLayout() {
        return R.layout.activity_auth;
    }

    @Override
    protected void setupView(Bundle savedInstanceState) {

        // declaramos el fragment del activity
        LoginFragment loginFragment = (LoginFragment) getSupportFragmentManager().findFragmentById(R.id.contentFrame);

        if (loginFragment == null) {
            loginFragment = LoginFragment.newInstance();


            ActivityUtils.addFragmentToActivity(getSupportFragmentManager(), loginFragment, R.id.contentFrame);
        }

        mLoginPresenter = new LoginPresenter(loginFragment);

    }
}