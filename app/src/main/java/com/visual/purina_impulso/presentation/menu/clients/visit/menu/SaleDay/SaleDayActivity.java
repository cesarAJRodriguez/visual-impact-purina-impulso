package com.visual.purina_impulso.presentation.menu.clients.visit.menu.SaleDay;

import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.visual.purina_impulso.Constants;
import com.visual.purina_impulso.R;
import com.visual.purina_impulso.base.BaseActivity;
import com.visual.purina_impulso.domain.Client;
import com.visual.purina_impulso.domain.ListAdvanceDay;
import com.visual.purina_impulso.domain.ListBusinessDay;
import com.visual.purina_impulso.domain.ListFee;
import com.visual.purina_impulso.domain.ListFeeImpulso;
import com.visual.purina_impulso.domain.ListSale;
import com.visual.purina_impulso.domain.ListVisit;
import com.visual.purina_impulso.domain.Visit;
import com.visual.purina_impulso.presentation.menu.clients.visit.menu.MenuVisitActivity;
import com.visual.purina_impulso.presentation.menu.clients.visit.menu.MenuVisitContract;
import com.visual.purina_impulso.presentation.menu.clients.visit.menu.MenuVisitPresenter;
import com.visual.purina_impulso.util.ActivityUtils;

import java.text.DecimalFormat;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;

public class SaleDayActivity extends BaseActivity implements MenuVisitContract.View , MenuVisitContract.SaleDayView{

    @BindView(R.id.toolbar_main) Toolbar mToolbar;
    @BindView(R.id.toolbar_title_text_view) TextView mToolbarTitleText;
    @BindView(R.id.et_amount_sku) EditText amountSkuEditText;
    @BindView(R.id.txt_day_business) TextView dayBusinessTextView;
    @BindView(R.id.txt_day_advance) TextView dayAdvanceTextView;
    @BindView(R.id.txt_sale_day_fee) TextView dayFeeTextView;
    @BindView(R.id.txt_sale_day_sale) TextView daySaleTextView;
    @BindView(R.id.txt_sale_day_proy) TextView dayProyTextView;
    @BindView(R.id.txt_day_proy_fee) TextView proyFeeTextView;
    @BindView(R.id.ll_status_proy_fee) LinearLayout statusProyFeeLinearLayout;
    @BindView(R.id.txt_day_sale_fee) TextView saleFeeTextView;
    @BindView(R.id.ll_status_sale_fee) LinearLayout statusSaleFeeLinearLayout;
    @BindView(R.id.txt_coverage_visit_prog) TextView visitProgTextView;
    @BindView(R.id.txt_coverage_visit_exec) TextView visitExecTextView;
    @BindView(R.id.txt_coverage_visit_exec_percentage) TextView visitExecPercentageTextView;
    @BindView(R.id.txt_coverage_visit_hab) TextView visitHabTextView;
    @BindView(R.id.txt_coverage_visit_hab_percentage) TextView visitHabPercentageTextView;
    @BindView(R.id.txt_coverage_visit_efec) TextView visitEfecTextView;
    @BindView(R.id.txt_coverage_visit_efec_percentage) TextView visitEfecPercentageTextView;
    @BindView(R.id.txt_coverage_visit_inc) TextView visitIncTextView;
    @BindView(R.id.txt_coverage_visit_inc_percentage) TextView visitIncPercentageTextView;
    @BindView(R.id.txt_coverage_visit_no_efec) TextView visitNoEfecTextView;
    @BindView(R.id.txt_coverage_visit_no_efec_percentage) TextView visitNoEfecPercentageTextView;
    @BindView(R.id.txt_fee_fee) TextView feeFeeTextView;
    @BindView(R.id.txt_fee_range) TextView feeRangeTextView;
    @BindView(R.id.txt_fee_sale_day) TextView feeSaleDayTextView;

    public  MenuVisitContract.Presenter mPresenter;

    @Override
    protected int getResLayout() {
        return R.layout.activity_visit_sale_day;
    }

    @Override
    protected void setupView(Bundle savedInstanceState) {

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        setTitleToolbar(getResources().getString(R.string.menu_visit_sale_day));

        if (mPresenter == null){
            mPresenter = new MenuVisitPresenter(this);
        }
            mPresenter.getValuesDaySale(getClient());
        setupData();

        ActivityUtils.setToolBarBack(this,mToolbar,true);
    }

    public void setTitleToolbar(String mTitleToolbar) {
        mToolbarTitleText.setText(mTitleToolbar);
    }

    @Override
    public void setPresenter(@NonNull MenuVisitContract.Presenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public Client getClient() {
        return getIntent().getParcelableExtra(Constants.BUNDLE_CLIENT_VISIT);
    }

    @Override
    public Visit getVisit() {
        return getIntent().getParcelableExtra(Constants.BUNDLE_CLIENT_VISIT_DETAIL);
    }

    private void setupData(){
        dayBusinessTextView.setText("0");
        dayAdvanceTextView.setText("0");
        dayFeeTextView.setText(getResources().getString(R.string.text_unit_money) + " "+  "0");
        daySaleTextView.setText(getResources().getString(R.string.text_unit_money) + " "+  "0");
        dayProyTextView.setText(getResources().getString(R.string.text_unit_money) + " "+  "0");
        proyFeeTextView.setText("0" +getResources().getString(R.string.text_unit_percentage));
        saleFeeTextView.setText("0" +getResources().getString(R.string.text_unit_percentage));
        visitProgTextView.setText("0");
        visitExecTextView.setText("0");
        visitExecPercentageTextView.setText("0.0" + getResources().getString(R.string.text_unit_percentage));
        visitHabTextView.setText("0");
        visitHabPercentageTextView.setText("0.0" + getResources().getString(R.string.text_unit_percentage));
        visitEfecTextView.setText("0");
        visitEfecPercentageTextView.setText("0.0" + getResources().getString(R.string.text_unit_percentage));
        visitIncTextView.setText("0");
        visitIncPercentageTextView.setText("0.0" + getResources().getString(R.string.text_unit_percentage));
        visitNoEfecTextView.setText("0");
        visitNoEfecPercentageTextView.setText("0.0" + getResources().getString(R.string.text_unit_percentage));
        feeFeeTextView.setText("0");
        feeRangeTextView.setText("0.0" + getResources().getString(R.string.text_unit_percentage));
        feeSaleDayTextView.setText("0");


        if (getVisit().getDeliveryDayVisit() != null){
            int amountSku =  0;
            for (int i = 0 ; i< getVisit().getDeliveryDayVisit().size();i++){
                amountSku = amountSku + getVisit().getDeliveryDayVisit().get(i).getAmountProduct();
            }
            amountSkuEditText.setText(String.valueOf(amountSku));
        }else{
            amountSkuEditText.setText("0");
        }
    }

    @OnClick(R.id.btn_cancel)
    public void onCancelRegister(){
        Intent intent = new Intent(this, MenuVisitActivity.class);
        intent.putExtra(Constants.BUNDLE_CLIENT_VISIT,getClient());
        intent.putExtra(Constants.BUNDLE_CLIENT_VISIT_DETAIL,getVisit());
        startActivity(intent);
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home) {

            Intent intent = new Intent(this, MenuVisitActivity.class);
            intent.putExtra(Constants.BUNDLE_CLIENT_VISIT,getClient());
            intent.putExtra(Constants.BUNDLE_CLIENT_VISIT_DETAIL,getVisit());
            startActivity(intent);
            finish();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(this, MenuVisitActivity.class);
        intent.putExtra(Constants.BUNDLE_CLIENT_VISIT,getClient());
        intent.putExtra(Constants.BUNDLE_CLIENT_VISIT_DETAIL,getVisit());
        startActivity(intent);
        finish();
    }

    @Override
    public void getValuesDaySaleSuccess(ArrayList<ListAdvanceDay> listAdvanceDays, ArrayList<ListBusinessDay> listBusinessDays, ArrayList<ListFee> listFees, ArrayList<ListSale> listSales, ArrayList<ListVisit> listVisits, ArrayList<ListFeeImpulso> listFeeImpulsos) {
        int listAdvanceDaysTotal = 0;
        int listBusinessDaysTotal = 0;
        double fee = 0;
        double sale = 0;
        double proy = 0;
        double proyFee = 0;
        double saleFee = 0;

        int visitProg = 0 ;
        int visitExec = 0 ;
        int visitHab = 0 ;
        int visitEfec = 0 ;
        int visitInc = 0 ;
        int visitNoEfec = 0 ;

        double visitExecPercentage = 0.00;
        double visitHabPercentage = 0.00;
        double visitEfecPercentage = 0.00;
        double visitIncPercentage = 0.00;
        double visitNoEfecPercentage = 0.00;

        int feeImpulso = 0;
        int rangeImpulso = 0;
        int saleDayImpulso = 0;

        if (listAdvanceDays != null && listAdvanceDays.size()>0){
            listAdvanceDaysTotal = listAdvanceDays.size();
            dayAdvanceTextView.setText(String.valueOf(listAdvanceDays.size()));
        }else{
            dayAdvanceTextView.setText("0");
        }

        if (listBusinessDays != null && listBusinessDays.size()>0){
            listBusinessDaysTotal = listBusinessDays.size();
            dayBusinessTextView.setText(String.valueOf(listBusinessDays.size()));
        }else{
            dayBusinessTextView.setText("0");
        }

        if (listFees != null && listFees.size()>0){
            fee = Double.parseDouble(listFees.get(0).getFee().replace(",","."));
            DecimalFormat df = new DecimalFormat("#.##");
            String feeFormat = df.format(fee);
            dayFeeTextView.setText(getResources().getString(R.string.text_unit_money) + " "+feeFormat);
        }else{
            dayFeeTextView.setText(getResources().getString(R.string.text_unit_money) + " "+"0");
        }

        if (listSales != null && listSales.size()>0){

            sale = Double.parseDouble(listSales.get(0).getSale().replace(",","."));
            DecimalFormat df = new DecimalFormat("#.##");
            String saleFormat = df.format(sale);
            daySaleTextView.setText(getResources().getString(R.string.text_unit_money) + " "+saleFormat);
        }else{
            daySaleTextView.setText(getResources().getString(R.string.text_unit_money) + " "+"0");
        }

        if (sale > 0 && listAdvanceDaysTotal > 0){
            proy = (sale/listAdvanceDaysTotal)*listBusinessDaysTotal;
            DecimalFormat df = new DecimalFormat("#.##");
            String proyFormat = df.format(proy);
            dayProyTextView.setText(getResources().getString(R.string.text_unit_money) + " "+ proyFormat);
        }else{
            dayProyTextView.setText(getResources().getString(R.string.text_unit_money) + " "+"0");
        }


        if (fee > 0){
            proyFee = (proy/fee)*100;
            DecimalFormat df = new DecimalFormat("#.##");
            String proyFeeFormat = df.format(proyFee);
            proyFeeTextView.setText(proyFeeFormat +getResources().getString(R.string.text_unit_percentage));
        }else{
            proyFeeTextView.setText("0" +getResources().getString(R.string.text_unit_percentage));
        }

        if( proyFee < 80 ){
            statusProyFeeLinearLayout.setBackgroundColor(getResources().getColor(R.color.colorRedLightStatus));
        } else if( proyFee >= 80 && proyFee < 100 ){
            statusProyFeeLinearLayout.setBackgroundColor(getResources().getColor(R.color.colorYellowStatus));
        } else if( proyFee >= 100 ){
            statusProyFeeLinearLayout.setBackgroundColor(getResources().getColor(R.color.colorGreenStatus));
        }

        if (fee > 0){
            saleFee = (sale/fee)*100;
            DecimalFormat df = new DecimalFormat("#.##");
            String saleFeeFormat = df.format(saleFee);
            saleFeeTextView.setText(saleFeeFormat +getResources().getString(R.string.text_unit_percentage));
        }else{
            saleFeeTextView.setText("0" +getResources().getString(R.string.text_unit_percentage));
        }

        if( saleFee < 80 ){
            statusSaleFeeLinearLayout.setBackgroundColor(getResources().getColor(R.color.colorRedLightStatus));
        } else if( saleFee >= 80 && saleFee < 100 ){
            statusSaleFeeLinearLayout.setBackgroundColor(getResources().getColor(R.color.colorYellowStatus));
        } else if( saleFee >= 100 ){
            statusSaleFeeLinearLayout.setBackgroundColor(getResources().getColor(R.color.colorGreenStatus));
        }


        if (listVisits != null && listVisits.size()>0){
             visitProg = listVisits.get(0).getViProg() ;
             visitExec = listVisits.get(0).getViExc() ;
             visitHab = listVisits.get(0).getViHab() ;
             visitEfec = listVisits.get(0).getViEfec() ;
             visitInc = listVisits.get(0).getViIn() ;
             visitNoEfec   = ( visitHab - ( visitEfec - visitInc ) );

            if (visitProg > 0){
                visitExecPercentage = (visitExec/visitProg)*100;
                DecimalFormat df = new DecimalFormat("#.##");
                visitExecPercentage = Double.valueOf(df.format(visitExecPercentage));
                String visitExecPercentageFormat = df.format(visitExecPercentage);
                visitExecPercentageTextView.setText(visitExecPercentageFormat +getResources().getString(R.string.text_unit_percentage));
            }else{
                visitExecPercentageTextView.setText(String.valueOf(visitExecPercentage) +getResources().getString(R.string.text_unit_percentage));
            }

            if (visitProg > 0){
                visitHabPercentage = (visitHab/visitProg)*100;
                DecimalFormat df = new DecimalFormat("#.##");
                String visitHabPercentageFormat = df.format(visitHabPercentage);
                visitHabPercentageTextView.setText(visitHabPercentageFormat +getResources().getString(R.string.text_unit_percentage));
            }else{
                visitHabPercentageTextView.setText(String.valueOf(visitHabPercentage) +getResources().getString(R.string.text_unit_percentage));
            }

            if (visitHab > 0){
                visitEfecPercentage = (visitEfec/visitHab)*100;
                DecimalFormat df = new DecimalFormat("#.##");
                String visitEfecPercentageFormat = df.format(visitEfecPercentage);
                visitEfecPercentageTextView.setText(visitEfecPercentageFormat+getResources().getString(R.string.text_unit_percentage));
            }else{
                visitEfecPercentageTextView.setText(String.valueOf(visitEfecPercentage) +getResources().getString(R.string.text_unit_percentage));
            }

            if (visitHab > 0){
                visitIncPercentage = (visitInc/visitHab)*100;
                DecimalFormat df = new DecimalFormat("#.##");
                String visitIncPercentageFormat = df.format(visitIncPercentage);
                visitIncPercentageTextView.setText(visitIncPercentageFormat +getResources().getString(R.string.text_unit_percentage));
            }else{
                visitIncPercentageTextView.setText(String.valueOf(visitIncPercentage) +getResources().getString(R.string.text_unit_percentage));
            }

            if (visitHab > 0){
                visitNoEfecPercentage = (visitNoEfec/visitHab)*100;
                DecimalFormat df = new DecimalFormat("#.##");
                String visitNoEfecPercentageFormat = df.format(visitNoEfecPercentage);
                visitNoEfecPercentageTextView.setText(visitNoEfecPercentageFormat +getResources().getString(R.string.text_unit_percentage));
            }else{
                visitNoEfecPercentageTextView.setText(String.valueOf(visitNoEfecPercentage) +getResources().getString(R.string.text_unit_percentage));
            }

            visitProgTextView.setText(String.valueOf(visitProg));
            visitExecTextView.setText(String.valueOf(visitExec));
            visitHabTextView.setText(String.valueOf(visitHab));
            visitEfecTextView.setText(String.valueOf(visitEfec));
            visitIncTextView.setText(String.valueOf(visitInc));
            visitNoEfecTextView.setText(String.valueOf(visitNoEfec));
        }


        if (listFeeImpulsos != null && listFeeImpulsos.size()>0){
             feeImpulso = listFeeImpulsos.get(0).getFee();
             rangeImpulso = listFeeImpulsos.get(0).getRange();
             saleDayImpulso = listFeeImpulsos.get(0).getSaleDay();
            DecimalFormat df = new DecimalFormat("#.##");
            String rangeImpulsoNewFormat = df.format(rangeImpulso);
            feeFeeTextView.setText(String.valueOf(feeImpulso));
            feeRangeTextView.setText(rangeImpulsoNewFormat +getResources().getString(R.string.text_unit_percentage));
            feeSaleDayTextView.setText(String.valueOf(saleDayImpulso));

        }


    }
}