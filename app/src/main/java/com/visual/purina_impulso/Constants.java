package com.visual.purina_impulso;

import com.visual.purina_impulso.domain.DetailQuizVisit;
import com.visual.purina_impulso.domain.Merchandising;
import com.visual.purina_impulso.domain.Price;


import java.util.ArrayList;

//DECLARACION DE CONSTANTES EN LA APP

public class Constants {


    public static final String MESSAGE_GENERIC_ERROR = "Ha ocurrido un error, intentelo nuevamente";

    public static final int FLAG_ASSISTANCE_FRAGMENT = 1;
    public static final int FLAG_REGISTER_ASSISTANCE_FRAGMENT = 2;

    public static final int FLAG_CLIENTS_FRAGMENT = 3;

    public static final int FLAG_SEARCH_CLIENTS_FRAGMENT = 4;

    public static final int FLAG_PENDING_FRAGMENT = 5;
    public static final int FLAG_PENDING__VISIT_FRAGMENT = 8;

    public static final int FLAG_VISIT_FRAGMENT = 6;
    public static final int FLAG_VISIT_INCIDENCE_FRAGMENT = 7;


    public static int STEP_POSITION;

    public static final String BUNDLE_CLIENT_VISIT = "bundle_client_visit";
    public static final String BUNDLE_CLIENT_VISIT_DETAIL = "bundle_client_visit_detail";
    public static final String BUNDLE_CLIENT_QUIZ_VISIT = "bundle_client_quiz_visit";
    public static final String BUNDLE_TYPE_PRICE = "bundle_type_price";
    public static final String BUNDLE_TYPE_ASSISTANCE = "bundle_type_assistance";
    public static final String BUNDLE_CLIENT_VISIT_QUIZ = "bundle_client_visit_quiz";

    public static final String FLAG_TYPE_ASSISTANCE_START_EMPTY =  "0";
    public static final String FLAG_TYPE_ASSISTANCE_START =  "1";
    public static final String FLAG_TYPE_ASSISTANCE_EXIT =  "2";
    public static final String FLAG_PENDING_ASSISTANCE_YES =  "1";
    public static final String FLAG_PENDING_ASSISTANCE_NO =  "0";

    public static final String FLAG_CLIENT_ASSIGNED =  "1";
    public static final String FLAG_CLIENT =  "0";

    public static final String EXTENTION_IMAGE = ".png";


    public static final String FLAG_CHANNEL_MODERN=  "2";

    public static final String FLAG_CHANNEL_GENERAL =  "4";

    public static final int FLAG_TYPE_QUESTION_OPTION =  2;
    public static final int FLAG_TYPE_QUESTION_ANSWER =  1;

    public static final String FLAG_PRICE_TYPE_BREAK =  "1";
    public static final String FLAG_PRICE_TYPE_COMPETITION =  "2";
    public static  String FLAG_PRICE_TYPE = "flag_price_type";

    public static final String FLAG_CLIENT_VISIT_NO_START=  "1";
    public static final String FLAG_CLIENT_VISIT_START=  "2";
    public static final String FLAG_CLIENT_VISIT_EXIT=  "3";

    public static final String FLAG_CLIENT_VISIT_STATUS_START=  "INICIADA";
    public static final String FLAG_CLIENT_VISIT_STATUS_INCIDENCE=  "INCIDENCIA";

    public static final String FLAG_DATA_PENDING_YES=  "YES";
    public static final String FLAG_DATA_PENDING_NO=  "NO";

    public static final String FLAG_VISIT_QUIZ =  "E";
    public static final String FLAG_VISIT_PHOTO =  "P";
    public static final String FLAG_VISIT_ACTION =  "A";
    public static final String FLAG_VISIT_PRICE_BREAK =  "PQ";
    public static final String FLAG_VISIT_PRICE_COMPETITION =  "PC";
    public static final String FLAG_VISIT_SHOOPER =  "SH";
    public static final String FLAG_VISIT_T =  "T";
    public static final String FLAG_VISIT_STOCK =  "ST";
    public static final String FLAG_VISIT_STOCK_FINAL =  "STF";
    public static final String FLAG_VISIT_DELIVERY =  "DXD";

    public static ArrayList<String> SEARCH_CLIENTS_CHECK = new ArrayList<>();
    public static ArrayList<DetailQuizVisit> ALTERNATIVES_QUESTION_CHECKD = new ArrayList<>();



    public static ArrayList<Price> SEARCH_PRICE_COMPETITION = new ArrayList<>();
    public static ArrayList<Price> SEARCH_PRICE_BREAK = new ArrayList<>();
    public static ArrayList<Merchandising> MERCHANDISING_LIST = new ArrayList<>();
    public static final int PRICE_COMPETITION_DISTRIBUTION = 1;



    public static final String FLAG_PENDING_CLIENTS_VISITS =  "$_PENDING_CLIENTS_VISITS123_IMPULSO_$";
}
