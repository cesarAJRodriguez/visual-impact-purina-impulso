package com.visual.purina_impulso;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;
import android.util.Log;

import com.facebook.stetho.Stetho;

import java.util.regex.Pattern;

import io.realm.Realm;
import io.realm.RealmConfiguration;

import com.visual.purina_impulso.data.source.local.realm.RealmInspectorModulesProvider;

import static android.R.attr.key;


//CLASE QUE SE DECLARO EN EL MANIFEST
public class App extends Application{

    public static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        MultiDex.install(this);
        context = getApplicationContext();
        //REALm : libreria el cual es una alternativa para crear una base de datos interna
        Realm.init(this);
        initRealmConfiguration();

        //Stetho : libreria de faceboook el cual ayuda a visualizar la base de datos interna

        Stetho.initialize(
                Stetho.newInitializerBuilder(this)
                        .enableDumpapp(Stetho.defaultDumperPluginsProvider(this))
                        .enableWebKitInspector(RealmInspectorModulesProvider.builder(this).build())
                        .build());

        RealmInspectorModulesProvider.builder(this)
                .withFolder(getCacheDir())
                .withEncryptionKey("encrypted.realm", key)
                .withMetaTables()
                .withDescendingOrder()
                .withLimit(1000)
                .databaseNamePattern(Pattern.compile(".+\\.realm"))
                .build();


    }

    public void initRealmConfiguration(){
        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder()
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(realmConfiguration);
    }

    //Log : metodo habilitar los log en la app
    public static void log(String tag, Object body) {
        if (BuildConfig.LOG_AVAILABLE) {
            Log.d(tag, body.toString());
        }
    }

    public static void log(Object body) {
        if (BuildConfig.LOG_AVAILABLE) {
            Log.d("Log App: ", body.toString());
        }
    }

}