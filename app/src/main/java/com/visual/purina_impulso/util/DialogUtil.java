package com.visual.purina_impulso.util;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.visual.purina_impulso.R;
import com.visual.purina_impulso.view.dialog.IDialogAssistance;
import com.visual.purina_impulso.view.dialog.IDialogLogOut;
import com.visual.purina_impulso.view.dialog.IDialogQuizQuestion;
import com.visual.purina_impulso.view.dialog.IDialogSync;
import com.visual.purina_impulso.view.dialog.IDialogVisit;


public class DialogUtil {


    public static Dialog setupDialog(Context ctx, int res) {
        Dialog dialog = new Dialog(ctx);
        dialog.setCancelable(false);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(res);
        return dialog;
    }

    public static Dialog loading(Context context, int res) {
        final Dialog dialog = new Dialog(context);
        dialog.setCancelable(false);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(res);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return dialog;
    }

    public static void showDialogMessage(Context context,String message) {
        final Dialog dialog = setupDialog(context, R.layout.dialog_message);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        TextView messageDialog = (TextView) dialog.findViewById(R.id.message);
        messageDialog.setText(message);

        Button btnAccept = (Button) dialog.findViewById(R.id.btn_accept);

        btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.setCancelable(false);
        dialog.show();
    }

    public static void showDialogWelcome(Context context,String nameUser) {
        final Dialog dialog = setupDialog(context, R.layout.dialog_welcome_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        TextView txtNameUser= (TextView) dialog.findViewById(R.id.name_user);
        txtNameUser.setText(nameUser);

        dialog.setCancelable(false);
        dialog.show();
    }

    public static void showDialogLogOut(Context context, IDialogLogOut iDialogLogOut) {
        final Dialog dialog = setupDialog(context, R.layout.dialog_logout);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        Button btnYes = (Button) dialog.findViewById(R.id.btn_yes);
        Button btnNo = (Button) dialog.findViewById(R.id.btn_no);

        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iDialogLogOut.onLogOut();
                dialog.dismiss();
            }
        });

        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.setCancelable(false);
        dialog.show();
    }

    public static void showDialogMessageConfirmSendAssistancePending(Context context,String message,IDialogAssistance iDialogAssistance) {

        final Dialog dialog = setupDialog(context, R.layout.dialog_confirm);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        TextView messageDialog = (TextView) dialog.findViewById(R.id.txt_message_confirm);
        messageDialog.setText(message);

        Button btnAccept = (Button) dialog.findViewById(R.id.btn_accept);
        Button btnCancel = (Button) dialog.findViewById(R.id.btn_cancel);

        btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iDialogAssistance.onConfirmSendAssistancePending();
                dialog.dismiss();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.setCancelable(false);
        dialog.show();

    }




    public static void showDialogMessageRegisterAssistance(Context context,String message,IDialogAssistance iDialogAssistance) {
        final Dialog dialog = setupDialog(context, R.layout.dialog_message);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        TextView messageDialog = (TextView) dialog.findViewById(R.id.message);
        messageDialog.setText(message);

        Button btnAccept = (Button) dialog.findViewById(R.id.btn_accept);

        btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iDialogAssistance.onRegisterSuccess();
                dialog.dismiss();
            }
        });

        dialog.setCancelable(false);
        dialog.show();
    }

    public static void showDialogConfirmAssignedVisit(Context context,String message,IDialogVisit.Visit iDialogVisit) {
        final Dialog dialog = setupDialog(context, R.layout.dialog_confirm);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        TextView messageDialog = (TextView) dialog.findViewById(R.id.txt_message_confirm);
        messageDialog.setText(message);

        Button btnAccept = (Button) dialog.findViewById(R.id.btn_accept);
        Button btnCancel = (Button) dialog.findViewById(R.id.btn_cancel);

        btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iDialogVisit.onConfirmAssignVisit();
                dialog.dismiss();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.setCancelable(false);
        dialog.show();
    }

    public static void showDialogSync(Context context,String message, IDialogSync iDialogSync) {
        final Dialog dialog = setupDialog(context, R.layout.dialog_message);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        Button acceptButton = (Button) dialog.findViewById(R.id.btn_accept);
        TextView messageTextView = (TextView) dialog.findViewById(R.id.message);
        messageTextView.setText(message);

        acceptButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iDialogSync.onSyncSuccess();
                dialog.dismiss();
            }
        });

        dialog.setCancelable(false);
        dialog.show();
    }

    public static void showDialogSyncVisit(Context context,String message, IDialogVisit.Visit iDialogVisit) {
        final Dialog dialog = setupDialog(context, R.layout.dialog_confirm);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        Button acceptButton = (Button) dialog.findViewById(R.id.btn_accept);
        Button cancelButton = (Button) dialog.findViewById(R.id.btn_cancel);
        TextView messageTextView = (TextView) dialog.findViewById(R.id.txt_message_confirm);
        messageTextView.setText(message);

        acceptButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              iDialogVisit.onSyncVisitAccept();
                dialog.dismiss();
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iDialogVisit.onSyncVisitCancel();
                dialog.dismiss();
            }
        });

        dialog.setCancelable(false);
        dialog.show();
    }

    public static void showDialogSyncMenu(Context context,String message, IDialogSync iDialogSync) {
        final Dialog dialog = setupDialog(context, R.layout.dialog_confirm);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        Button acceptButton = (Button) dialog.findViewById(R.id.btn_accept);
        Button cancelButton = (Button) dialog.findViewById(R.id.btn_cancel);
        TextView messageTextView = (TextView) dialog.findViewById(R.id.txt_message_confirm);
        messageTextView.setText(message);

        acceptButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iDialogSync.onSyncConfirmAccept();
                dialog.dismiss();
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.setCancelable(false);
        dialog.show();
    }

    public static void showDialogMessageRegisterStartVisit(Context context,String message,IDialogVisit.StartVisit iDialogVisit) {
        final Dialog dialog = setupDialog(context, R.layout.dialog_message);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        TextView messageDialog = (TextView) dialog.findViewById(R.id.message);
        messageDialog.setText(message);

        Button btnAccept = (Button) dialog.findViewById(R.id.btn_accept);

        btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                iDialogVisit.onConfirmStartVisit();
            }
        });

        dialog.setCancelable(false);
        dialog.show();
    }

    public static void showDialogMessageRegisterVisit(Context context,String message,IDialogVisit.VisitMenu iDialogVisit) {
        final Dialog dialog = setupDialog(context, R.layout.dialog_message);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        TextView messageDialog = (TextView) dialog.findViewById(R.id.message);
        messageDialog.setText(message);

        Button btnAccept = (Button) dialog.findViewById(R.id.btn_accept);

        btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                iDialogVisit.onRegisterVisitSuccess();
            }
        });

        dialog.setCancelable(false);
        dialog.show();
    }


    public static void showDialogConfirmFinishVisit(Context context,String message,IDialogVisit.VisitFinish iDialogVisit) {
        final Dialog dialog = setupDialog(context, R.layout.dialog_confirm);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        TextView messageDialog = (TextView) dialog.findViewById(R.id.txt_message_confirm);
        messageDialog.setText(message);

        Button btnAccept = (Button) dialog.findViewById(R.id.btn_accept);
        Button btnCancel = (Button) dialog.findViewById(R.id.btn_cancel);

        btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iDialogVisit.onConfirmFinishVisit();
                dialog.dismiss();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.setCancelable(false);
        dialog.show();
    }


    public static void showDialogMessageFinishVisit(Context context,String message,IDialogVisit.VisitFinishDialog visitFinishDialog) {
        final Dialog dialog = setupDialog(context, R.layout.dialog_message);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        TextView messageDialog = (TextView) dialog.findViewById(R.id.message);
        messageDialog.setText(message);

        Button btnAccept = (Button) dialog.findViewById(R.id.btn_accept);

        btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                visitFinishDialog.onFinishVisitDialog();
                dialog.dismiss();
            }
        });

        dialog.setCancelable(false);
        dialog.show();
    }


    public static void showDialogMessageRegisterPriceVisit(Context context,String message,IDialogVisit.VisitPriceDialog iDialogVisit) {
        final Dialog dialog = setupDialog(context, R.layout.dialog_message);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        TextView messageDialog = (TextView) dialog.findViewById(R.id.message);
        messageDialog.setText(message);

        Button btnAccept = (Button) dialog.findViewById(R.id.btn_accept);

        btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                iDialogVisit.onConfirmPriceVisit();
            }
        });

        dialog.setCancelable(false);
        dialog.show();
    }


    public static void showDialogMessageRegisterStockVisit(Context context,String message,IDialogVisit.VisitStockDialog iDialogVisit) {
        final Dialog dialog = setupDialog(context, R.layout.dialog_message);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        TextView messageDialog = (TextView) dialog.findViewById(R.id.message);
        messageDialog.setText(message);

        Button btnAccept = (Button) dialog.findViewById(R.id.btn_accept);

        btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                iDialogVisit.onConfirmStockVisit();
            }
        });

        dialog.setCancelable(false);
        dialog.show();
    }

    public static void showDialogMessageRegisterStockFinalVisit(Context context,String message,IDialogVisit.VisitStockFinalDialog iDialogVisit) {
        final Dialog dialog = setupDialog(context, R.layout.dialog_message);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        TextView messageDialog = (TextView) dialog.findViewById(R.id.message);
        messageDialog.setText(message);

        Button btnAccept = (Button) dialog.findViewById(R.id.btn_accept);

        btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                iDialogVisit.onConfirmStockFinalVisit();
            }
        });

        dialog.setCancelable(false);
        dialog.show();
    }

    public static void showDialogMessageRegisterDeliveryVisit(Context context,String message,IDialogVisit.VisitDeliveryDialog iDialogVisit) {
        final Dialog dialog = setupDialog(context, R.layout.dialog_message);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        TextView messageDialog = (TextView) dialog.findViewById(R.id.message);
        messageDialog.setText(message);

        Button btnAccept = (Button) dialog.findViewById(R.id.btn_accept);

        btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                iDialogVisit.onConfirmDeliveryVisit();
            }
        });

        dialog.setCancelable(false);
        dialog.show();
    }


    public static void showDialogMessageRegisterQuizVisit(Context context, String message, IDialogQuizQuestion iDialogQuizQuestion) {
        final Dialog dialog = setupDialog(context, R.layout.dialog_message);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        TextView messageDialog = (TextView) dialog.findViewById(R.id.message);
        messageDialog.setText(message);

        Button btnAccept = (Button) dialog.findViewById(R.id.btn_accept);

        btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                iDialogQuizQuestion.onConfirmRegisterQuiz();
            }
        });

        dialog.setCancelable(false);
        dialog.show();
    }


    public static void showDialogMessageConfirmClearDB(Context context,String message,IDialogSync.clearDialog iDialogSync) {

        final Dialog dialog = setupDialog(context, R.layout.dialog_confirm);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        TextView messageDialog = (TextView) dialog.findViewById(R.id.txt_message_confirm);
        messageDialog.setText(message);

        Button btnAccept = (Button) dialog.findViewById(R.id.btn_accept);
        Button btnCancel = (Button) dialog.findViewById(R.id.btn_cancel);

        btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iDialogSync.onClearDB();
                dialog.dismiss();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.setCancelable(false);
        dialog.show();

    }


    public static void showDialogMessageConfirmSendPending(Context context,String message,IDialogVisit.VisitSendPendingDialog visitSendPendingDialog) {

        final Dialog dialog = setupDialog(context, R.layout.dialog_confirm);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        TextView messageDialog = (TextView) dialog.findViewById(R.id.txt_message_confirm);
        messageDialog.setText(message);

        Button btnAccept = (Button) dialog.findViewById(R.id.btn_accept);
        Button btnCancel = (Button) dialog.findViewById(R.id.btn_cancel);

        btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                visitSendPendingDialog.onConfirmSendPendingVisit();
                dialog.dismiss();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.setCancelable(false);
        dialog.show();

    }


    public static void showDialogMessageSendPendingSuccess(Context context, String message, IDialogVisit.VisitSendPendingSuccessDialog visitSendPendingSuccessDialog) {
        final Dialog dialog = setupDialog(context, R.layout.dialog_message);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        TextView messageDialog = (TextView) dialog.findViewById(R.id.message);
        messageDialog.setText(message);

        Button btnAccept = (Button) dialog.findViewById(R.id.btn_accept);

        btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                visitSendPendingSuccessDialog.onConfirmSendPendingSuccessVisit();
            }
        });

        dialog.setCancelable(false);
        dialog.show();
    }

    public static void showDialogMessageSendIncidenceSuccess(Context context, String message, IDialogVisit.VisitSendIncidenceSuccessDialog visitSendIncidenceSuccessDialog) {
        final Dialog dialog = setupDialog(context, R.layout.dialog_message);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        TextView messageDialog = (TextView) dialog.findViewById(R.id.message);
        messageDialog.setText(message);

        Button btnAccept = (Button) dialog.findViewById(R.id.btn_accept);

        btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                visitSendIncidenceSuccessDialog.onConfirmSendIncidenceSuccessVisit();
            }
        });

        dialog.setCancelable(false);
        dialog.show();
    }
}
