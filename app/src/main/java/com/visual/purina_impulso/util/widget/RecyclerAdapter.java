package com.visual.purina_impulso.util.widget;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;

public abstract class RecyclerAdapter<T> extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    protected ArrayList<T> mList;

    public interface OnItemClickListener {
        void onItemClick(Object object, int position);
    }

    protected OnItemClickListener mItemClickListener;

    public RecyclerAdapter() {
        super();
        mList = new ArrayList<>();
    }

    public void addItems(@NonNull ArrayList<T> list){

        mList.addAll(list);

        notifyDataSetChanged();
    }

    public void removeItems(){

        mList.clear();

        notifyDataSetChanged();
    }



    public T getItem(int position) {
        if (mList != null && position >= 0 && position < mList.size()) {
            return mList.get(position);
        }
        return null;
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

}
