package com.visual.purina_impulso.util.widget;

import android.content.Context;
import android.content.DialogInterface;
import android.content.res.TypedArray;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.util.AttributeSet;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;

import com.visual.purina_impulso.R;


public class SpinnerViewItem<T> extends EditText implements View.OnClickListener {

    private T[] mEntries;
    private T mCurrentItem;
    private int mIndex;
    private String mTitle;
    private OnSpinnerClick listerner = null;

    public interface OnSpinnerClick{
        void onItemClick(int position);
        void onRestoreItems();
    }

    public SpinnerViewItem(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
        setFocusable(false);
        setInputType(InputType.TYPE_TEXT_VARIATION_NORMAL);
        setOnClickListener(this);

    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public void setOnItemSpinnerClick(OnSpinnerClick listerner){
        this.listerner = listerner;
    }

    private void init(AttributeSet attrs) {
        TypedArray a = getContext().obtainStyledAttributes(attrs,
                R.styleable.SpinnerView);

        mTitle = a.getString(R.styleable.SpinnerView_alertTitle);

        a.recycle();
    }

    public void setEntries(T[] entries) {
        mEntries = entries;
    }

    public T[] getEntries() {
        return mEntries;
    }

    public T getSelectedItem() {
        return mCurrentItem;
    }

    @Override
    public void setSelection(int index) {
        if( mEntries != null && index!=-1){
            mIndex = index;
            mCurrentItem = mEntries[index];
            setText(mCurrentItem.toString());
        }else{
            mCurrentItem = null;
        }
    }

    public int getSelectedItemPosition() {
        return mIndex;
    }

    @Override
    public void onClick(View v) {
        if (mEntries != null ) {
            CharSequence[] items = new CharSequence[mEntries.length];
            for (int i = 0; i < mEntries.length; i++) {
                items[i] = mEntries[i].toString();
            }

            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            builder.setItems(items, new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int position) {

                    setSelection(position);
                    if (listerner != null) {
                        listerner.onItemClick(position);
                    }

                }
            });

            builder.setTitle(mTitle);

            AlertDialog alert = builder.create();
            ListView listView=alert.getListView();
            listView.setDivider(new ColorDrawable(getResources().getColor(R.color.colorGreyDark))); // set color
            listView.setDividerHeight(1);

            /*
            AlertDialog alertDialogObject = dialogBuilder.create();
ListView listView=alertDialogObject.getListView();
listView.setDivider(new ColorDrawable(Color.BLUE)); // set color
listView.setDividerHeight(2); // set height
alertDialogObject.show();
            * */


            alert.show();
        }else{

            listerner.onRestoreItems();
        }
    }
}
