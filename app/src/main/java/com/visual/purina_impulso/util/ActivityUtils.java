package com.visual.purina_impulso.util;

import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.visual.purina_impulso.R;

/**
 * Created by juan on 24/04/2017.
 */

public class ActivityUtils {

    public static void addFragmentToActivity (@NonNull FragmentManager fragmentManager,
                                              @NonNull Fragment fragment, int frameId) {
        ActivityUtils.addFragmentToActivity(fragmentManager, fragment, frameId, false);
    }

    public static void addFragmentToActivity(@NonNull FragmentManager fragmentManager,
                                             @NonNull Fragment fragment, int frameId, boolean backStack) {
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.add(frameId, fragment);
        if (backStack) {
            transaction.addToBackStack(null);
        }
        transaction.commit();
    }

    public static void addFragmentToActivity(@NonNull FragmentManager fragmentManager,
                                             @NonNull Fragment fragment, int frameId, boolean backStack, String tag) {
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.add(frameId, fragment, tag);
        if (backStack) {
            transaction.addToBackStack(null);
        }
        transaction.commit();
    }

    public static void replaceFragmentInActivity(@NonNull FragmentManager fragmentManager,
                                                 @NonNull Fragment fragment, int frameId) {
        ActivityUtils.replaceFragmentInActivity(fragmentManager, fragment, frameId, false);
    }

    public static void replaceFragmentInActivity(@NonNull FragmentManager fragmentManager,
                                                 @NonNull Fragment fragment, int frameId, boolean backStack) {
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(frameId, fragment);
        if (backStack) {
            transaction.addToBackStack(null);
        }
        transaction.commit();
    }

    public static void replaceFragmentInActivity(@NonNull FragmentManager fragmentManager,
                                                 @NonNull Fragment fragment, int frameId, boolean backStack, String tag) {
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(frameId, fragment, tag);
        if (backStack) {
            transaction.addToBackStack(null);
        }
        transaction.commit();
    }

    public static void setToolBar(AppCompatActivity activity, Toolbar toolbar, boolean homeEnable){
        toolbar.setTitle("");
        activity.setSupportActionBar(toolbar);
        if (homeEnable) {
            final ActionBar bar = activity.getSupportActionBar();
            if (bar != null) {
                bar.setDisplayHomeAsUpEnabled(true);
                bar.setHomeButtonEnabled(true);
               // bar.setHomeAsUpIndicator(R.drawable.ic_menu);
            }
        }
    }

    public static void setToolBarBack(AppCompatActivity activity, Toolbar toolbar, boolean homeEnable){
        toolbar.setTitle("");
        activity.setSupportActionBar(toolbar);
       // if (homeEnable){
        final ActionBar bar = activity.getSupportActionBar();
        if (bar != null) {
            bar.setDisplayHomeAsUpEnabled(homeEnable);
            bar.setHomeAsUpIndicator(R.drawable.ic_keyboard_backspace_white);
         }
        //}
    }
}
