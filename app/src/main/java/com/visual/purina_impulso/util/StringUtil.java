package com.visual.purina_impulso.util;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import okhttp3.RequestBody;
import okio.Buffer;

public class StringUtil {

    private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    public static boolean isEmpty(String s) {
        return s != null && !s.isEmpty();
    }

    public static boolean validateEmail(String email) {
        Pattern pattern;
        Matcher matcher;
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public static  String timeOrderToSecondsOrder(String timeOrder) {
        String[] arr= timeOrder.split(":");
        int seconds=0;
        if (arr.length == 2) {
            seconds=Integer.parseInt(arr[0])*60+Integer.parseInt(arr[1]);
        }
        return  String.valueOf(seconds);
    }

    public static String bodyToString(final RequestBody request){
        try {
            final RequestBody copy = request;
            final Buffer buffer = new Buffer();

            copy.writeTo(buffer);

            return buffer.readUtf8();
        }
        catch (final IOException e) {
            return "did not work";
        }
    }

    public static String parseRequestBody(String str){
        String[] names = str.split("&");
        String output = "";
        for(int i =0 ; i< names.length ; i++){
            String[] values = names[i].split("=");
            if(values.length == 2){
                output += (","+"\"" + values[0] + "\"" + ":" + "\"" + values[1] + "\"");
            }
        }

        output += "}";
        return output;
    }

    public static String fixDecimalString(String s, int maxDecimalLength) {
        String fixed = "";
        boolean flag = false;
        int decimalCount = 0;
        for (int i = 0; i < s.length(); ++i) {
            char x = s.charAt(i);
            fixed = fixed.concat(String.valueOf(x));
            if (flag) {
                ++decimalCount;
                if (decimalCount == maxDecimalLength) {
                    return fixed;
                }
            }
            if (x == '.') flag = true;
        }
        return fixed;
    }

}