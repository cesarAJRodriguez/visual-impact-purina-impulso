package com.visual.purina_impulso.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;

import com.visual.purina_impulso.App;
import com.visual.purina_impulso.Constants;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;


public class FileUtil {

    public static final String FOLDER_IMAGES = "impulso";
    public static final String SUFIX_RESIZED_IMAGE = "_low";

    private static boolean isExternalStorageAvailable() {
        String state = Environment.getExternalStorageState();
        return state.equals(Environment.MEDIA_MOUNTED);
    }

    public static Uri getPhotoFileUri(Context context, String fileName) {
        if (isExternalStorageAvailable()) {

            File mediaStorageDir = new File(
                    context.getExternalFilesDir(Environment.DIRECTORY_PICTURES), FOLDER_IMAGES);

            if (!mediaStorageDir.exists() && !mediaStorageDir.mkdirs()){
                Log.d(FOLDER_IMAGES, "failed to create directory");
            }

            return Uri.fromFile(new File(mediaStorageDir.getPath() + File.separator + fileName + Constants.EXTENTION_IMAGE));
        }
        return null;
    }

    public static void copy(Context context,Uri uri,String fileName) throws IOException {

        String path =  DeviceUtil.getRealPathFromUri(context,uri);
        File mediaStorageDir = new File(
                context.getExternalFilesDir(Environment.DIRECTORY_PICTURES), FOLDER_IMAGES);

        mediaStorageDir.mkdirs();
        File src =  new File(path);
        File dst = new File(mediaStorageDir.getPath() + File.separator + fileName + Constants.EXTENTION_IMAGE);

        InputStream in = new FileInputStream(src);
        OutputStream out = new FileOutputStream(dst);

        // Transfer bytes from in to out
        byte[] buf = new byte[1024];
        int len;
        while ((len = in.read(buf)) > 0) {
            out.write(buf, 0, len);
        }
        in.close();
        out.close();
    }

    public static String getPathFile(String fileName) {
        File mediaStorageDir = new File(
                App.context.getExternalFilesDir(Environment.DIRECTORY_PICTURES), FOLDER_IMAGES);

        File file = new File(mediaStorageDir.getPath() + File.separator + fileName + SUFIX_RESIZED_IMAGE + Constants.EXTENTION_IMAGE);
        return file.getAbsolutePath();
    }

    public static String getSizeFile(String fileName) {
        File mediaStorageDir = new File(
                App.context.getExternalFilesDir(Environment.DIRECTORY_PICTURES), FOLDER_IMAGES);

        File file = new File(mediaStorageDir.getPath() + File.separator + fileName + SUFIX_RESIZED_IMAGE + Constants.EXTENTION_IMAGE);

        return String.valueOf(file.length()/1024) + " kb";
    }

    public static boolean getFileFromBitmap(Bitmap imageData, String fileName, Context context) {

        File mediaStorageDir = new File(
                context.getExternalFilesDir(Environment.DIRECTORY_PICTURES), FOLDER_IMAGES);

        File sdIconStorageDir = new File(mediaStorageDir.getPath() + File.separator + fileName + SUFIX_RESIZED_IMAGE + Constants.EXTENTION_IMAGE);

        try {
            String filePath = sdIconStorageDir.toString();
            FileOutputStream fileOutputStream = new FileOutputStream(filePath);

            BufferedOutputStream bos = new BufferedOutputStream(fileOutputStream);

            //choose another format if PNG doesn't suit you
            imageData.compress(Bitmap.CompressFormat.PNG, 0, bos);

            bos.flush();
            bos.close();

        } catch (FileNotFoundException e) {
            Log.w("TAG", "Error saving image file: " + e.getMessage());
            return false;
        } catch (IOException e) {
            Log.w("TAG", "Error saving image file: " + e.getMessage());
            return false;
        }

        return true;

    }
}
