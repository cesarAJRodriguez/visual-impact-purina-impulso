package com.visual.purina_impulso.util;

import java.util.Set;

import com.visual.purina_impulso.App;



public class ThreadUtil {

    public static String idCurrentThread = "";
    public static final long MAIN_THREAD_ID = 1;
    public static final String MAIN_THREAD_NAME = "main";
    public static void showAllThreads(){
        Set<Thread> threadSet = Thread.getAllStackTraces().keySet();
        Thread[] threadArray = threadSet.toArray(new Thread[threadSet.size()]);
        for(int i =0 ; i<threadArray.length ; i++){
            App.log(" thread ["+i+"] ->" + threadArray[i].getId() + " --- "+ threadArray[i].getName() + "----" + threadArray[i].isAlive() + "--" + threadArray[i].isInterrupted());
        }
    }
    public static boolean isMainThread(){
        App.log("is Main Thread ->" + Thread.currentThread().getId() + "--" + Thread.currentThread().getName());
        return (Thread.currentThread().getId()==MAIN_THREAD_ID && Thread.currentThread().getName().equals(MAIN_THREAD_NAME));
    }
}
