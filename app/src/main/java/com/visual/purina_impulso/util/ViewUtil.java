package com.visual.purina_impulso.util;

import android.graphics.Typeface;

import com.visual.purina_impulso.App;

public class ViewUtil {

    private static final String pathOpenSansBold = "OpenSans-Bold.ttf";
    private static final String pathOpenSansLight = "OpenSans-Light.ttf";
    private static final String pathOpenSansRegular = "OpenSans-Regular.ttf";
    private static final String pathOpenSansSemiBold = "OpenSans-Semibold.ttf";

    public static Typeface openSansBold() {
        return Typeface.createFromAsset(App.context.getAssets(), pathOpenSansBold);
    }

    public static Typeface openSansLight() {
        return Typeface.createFromAsset(App.context.getAssets(), pathOpenSansLight);
    }

    public static Typeface openSansRegular() {
        return Typeface.createFromAsset(App.context.getAssets(), pathOpenSansRegular);
    }

    public static Typeface openSansSemiBold() {
        return Typeface.createFromAsset(App.context.getAssets(), pathOpenSansSemiBold);
    }
}