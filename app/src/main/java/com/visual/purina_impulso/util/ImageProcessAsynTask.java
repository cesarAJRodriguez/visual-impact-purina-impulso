package com.visual.purina_impulso.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;


import com.visual.purina_impulso.App;
import com.visual.purina_impulso.view.dialog.IDialogCameraView;

import java.io.IOException;


public class ImageProcessAsynTask extends AsyncTask<Uri,Void,Boolean> {

    public OutOfMemoryError error;
    public Exception exception;

    private Context mContext;
    private String mFileName;
    private IDialogCameraView mDialogCameraView;
    private boolean mIsFromCamera;
    private String mSize;
    private String mPath;

    public ImageProcessAsynTask (Context context, String fileName, IDialogCameraView iDialogCameraView, boolean isFromCamera){
        mContext = context;
        mFileName =  fileName;
        mDialogCameraView =  iDialogCameraView;
        mIsFromCamera = isFromCamera;
        mSize = "";
        mPath = "";
    }

    @Override
    protected Boolean doInBackground(Uri... uris) {

        try{
            if (mIsFromCamera){
                Uri newUri = FileUtil.getPhotoFileUri(mContext,mFileName);
                if (newUri != null) {
                    Bitmap bitmap = BitmapUtil.getBitmapResized(newUri.getPath());
                    if(FileUtil.getFileFromBitmap(bitmap,mFileName, App.context)){
                        mSize =  FileUtil.getSizeFile(mFileName);
                        mPath = FileUtil.getPathFile(mFileName);
                    }else{
                        return false;
                    }
                }
            }else {
                try {
                    FileUtil.copy(mContext,uris[0],mFileName);
                    Uri newUri = FileUtil.getPhotoFileUri(mContext,mFileName);
                    if (newUri != null) {
                        Bitmap bitmap = BitmapUtil.getBitmapResized(newUri.getPath());
                        if(FileUtil.getFileFromBitmap(bitmap,mFileName, App.context)){
                            mSize =  FileUtil.getSizeFile(mFileName);
                            mPath = FileUtil.getPathFile(mFileName);
                        }else{
                            App.log(" error 1");
                            return false;
                        }
                    }else{
                        App.log(" error 2");
                        return false;
                    }
                } catch (IOException e) {
                    App.log(" error 3");
                    App.log(e.getMessage());
                    return false;
                }
            }
        }catch (OutOfMemoryError | Exception error) {
            App.log(" error 4");
            App.log(error.getMessage());
            mDialogCameraView.showMessageError();
        }

        return true;
    }

    @Override
    protected void onPostExecute(Boolean isSuccess) {
        if (isSuccess) {
            mDialogCameraView.SuccessUpload(mSize,mPath);
        }else {
            mDialogCameraView.showMessageError();
        }
    }
}
